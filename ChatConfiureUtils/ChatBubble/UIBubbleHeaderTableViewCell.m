    //
//  UIBubbleHeaderTableViewCell.m
//  UIBubbleTableViewExample
//
//  Created by Александр Баринов on 10/7/12.
//  Copyright (c) 2012 Stex Group. All rights reserved.
//

#import "UIBubbleHeaderTableViewCell.h"
#import "NSBubbleData.h"

@interface UIBubbleHeaderTableViewCell ()

@property (nonatomic, retain) UILabel *label;

@end

@implementation UIBubbleHeaderTableViewCell

@synthesize label = _label;
@synthesize date = _date;
@synthesize data=_data;


+ (CGFloat)height
{
    return 28.0;
}

- (void)setData:(NSBubbleData *)value
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setDateFormat:@"MMM dd, h:mm a"];
    
     NSString *text=[dateFormatter stringFromDate:value.date];
    
      // [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
     // [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    //  NSString *text = [dateFormatter stringFromDate:value.date];
#if !__has_feature(objc_arc)
    [dateFormatter release];
#endif
    
    if (self.label)
    {
        self.label.text = text;
        return;
    }
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(60, 0,screenWidth-130, [UIBubbleHeaderTableViewCell height])];
    self.label.text = nil;
    self.label.font = [UIFont fontWithName:@"Roboto-Regular" size:10];
    
    if (value.type==BubbleTypeMine ) {
        self.label.textAlignment = NSTextAlignmentRight;
    }
    else
        self.label.textAlignment = NSTextAlignmentLeft;

    self.label.shadowOffset = CGSizeMake(0, 1);
    self.label.shadowColor = [UIColor whiteColor];
    self.label.textColor = [UIColor darkGrayColor];
    self.label.backgroundColor = [UIColor clearColor];
    [self addSubview:self.label];
}



@end
