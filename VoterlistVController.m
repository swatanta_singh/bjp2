//
//  BoothListVController.m
//  BJP
//
//  Created by PDSingh on 12/14/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "VoterlistVController.h"
#import "UIImageView+AFNetworking.h"
#import "PresentionHandler.h"
#import "DataPickerViewController.h"

#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "VoterFormViewController.h"

@interface VoterlistVController (){
    
    NSUInteger selectIndex;
    UIRefreshControl              *refreshControl;
    NSMutableArray *arrEventList;
    
    NSInteger selected;
    NSString* keyselected;
    
    UIToolbar*   numberToolbar;

}
@property (weak, nonatomic) IBOutlet UIButton *pendingButton;
@property (weak, nonatomic) IBOutlet UIButton *completeButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSArray *boothArrData;
- (IBAction)receiveTabButtonAction:(id)sender;
- (IBAction)sentTabButtonAction:(id)sender;

@property(nonatomic,strong)NSMutableArray *arrData;
@property (weak, nonatomic) IBOutlet UIButton *receiveTabButton;
@property (weak, nonatomic) IBOutlet UIButton *sentTabButton;
@property (strong, nonatomic) PresentionHandler *presentHandler;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTopContarints;
@property (weak, nonatomic) IBOutlet UICollectionView *headerMenuCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *containerCollectionView;
@end

@implementation VoterlistVController

- (void)viewDidLoad {
   
    [super viewDidLoad];
   
    selectIndex=0;
    
    keyselected=@"received";
    self.tableView.tableFooterView = [[UIView alloc ]initWithFrame:CGRectZero];
    UITextField *searchField = [self.searchBar valueForKey:@"_searchField"];
    searchField.textColor = [UIColor blackColor];
    
    [self initilaize];
    [self selectReciverTab];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)initilaize
{
    self.tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
}

-(void)selectReciverTab
{
    [self.receiveTabButton setTitleColor:[UIColor colorWithRed:18.0f/255.0f green:106.0f/255.0f blue:160.0f/255.0f alpha:1] forState:UIControlStateNormal ];
    [self.sentTabButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal ];
    [self.receiveTabButton setSelected:YES];
    [self.sentTabButton setSelected:NO];
}


-(void)viewWillDisappear:(BOOL)animated
{
     [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [self setTitleEngHindi];
    [self menuModel];
    
    [super viewWillAppear:animated];
     VoterFormViewController* objVerifyView=(VoterFormViewController*) [AppHelper intialiseViewControllerFromMainStoryboard:@"Voter" WithName:@"VoterFormViewController"];
    
     [self.headerView.btnRight setImage:[UIImage imageNamed:@"Ok"] forState:UIControlStateSelected];
    __weak VoterlistVController *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"Voter", nil);
    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:@"AddUser" WithComilation:^(int navigateValue) {
        if (navigateValue==1) {
            
            [weekSelf.navigationController popViewControllerAnimated:YES];
            objVerifyView.requestType=@"0";
        }
        else{
            objVerifyView.boothId=weekSelf.boothId;
             objVerifyView.requestType=@"0";
            [weekSelf.navigationController  pushViewController:objVerifyView animated:YES];
        }

    }];
}

-(void)setTitleEngHindi
{
    NSString *search = NSLocalizedString(@"search", nil);
    self.searchBar.placeholder =search;
    NSString *pendingButton = NSLocalizedString(@"pending", nil);
    NSString *compelted = NSLocalizedString(@"complete", nil);
    [self.pendingButton setTitle:pendingButton forState:UIControlStateNormal];
    [self.completeButton setTitle:compelted forState:UIControlStateNormal];
}
-(void)initialiseTheListViewWithDataSource:(NSArray *)dataSource withField:(UIButton*)txtField selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    [self.view endEditing:YES];
   
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];
    
    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            if ([[selectedvalues lastObject] isEqualToString:@"Add New"]) {
                
            }
            else
            {
                self.searchBar.text=@"";
                [self.view endEditing:YES];
                if( self.searchTopContarints.constant<25){
                    self.searchTopContarints.constant=42;
                }else{
                    self.searchTopContarints.constant=-5;
                }
                [UIView animateWithDuration:0.3
                                 animations:^{
                                     [self.view layoutIfNeeded];
                                     // Called on parent view
                                 }];

            }
          
        }
    }];
    
    self.presentHandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:CGSizeMake(100, 100)];
    
}

-(void)selectSentTab
{
    [self.sentTabButton setTitleColor:[UIColor colorWithRed:18.0f/255.0f green:106.0f/255.0f blue:160.0f/255.0f alpha:1] forState:UIControlStateNormal ];
    [self.receiveTabButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal ];
    [self.sentTabButton setSelected:YES];
    [self.receiveTabButton setSelected:NO];
}


-(void)menuModel
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        [[AppHelper sharedInstance]showIndicator];

        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERshowBoothUsers];
        NSMutableDictionary *parameters =[NSMutableDictionary new];
        parameters[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameters[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameters[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        parameters[@"boothid"]=self.boothId;
        
        if ([self.receiveTabButton isSelected]) {
            
            if (arrEventList.count==0)[[AppHelper sharedInstance]showIndicator];;
            parameters[@"verificationstatus"]=@"0";
            [self httpRequestGetEventStatus:@"0" url:baseURL param:parameters];
        }
        else
        {
            parameters[@"verificationstatus"]=@"1";
            [self httpRequestGetEventStatus:@"0" url:baseURL param:parameters];
        }
    }
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)httpRequestGetEventStatus:(NSString *)status url:(NSString *)baseURL param:(NSDictionary *)parameters
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager POST:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]])
                self.boothArrData = [responseObject valueForKey:@"data"];
                else
                    self.boothArrData=nil;
            }
            else
                self.boothArrData=nil;
             [self.tableView reloadData];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
    }
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
}
#pragma mark -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.boothArrData count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCell" forIndexPath:indexPath];
    
 /*   UIView *view = (UIView*)[cell.contentView viewWithTag:6001];
    
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(1, 1);
    view.layer.shadowRadius = 2;
    view.layer.shadowOpacity = 0.5;
    */
    
    NSDictionary *boothItem=[self.boothArrData objectAtIndex:indexPath.row];
    
    NSString *subName=[NSString stringWithFormat:@"Booth Name : %@",[AppHelper nullCheckWithNA:boothItem[@"booth"]]];
    
   NSString *subTitle = [NSString stringWithFormat:@"Voter no : %@\nVoter id : %@\nName : %@\nF'Name : %@\nHouse No : %@\nAge : %@",[AppHelper nullCheckWithNA:boothItem[@"voter_no"]],[AppHelper nullCheckWithNA:boothItem[@"voter_id"]],[AppHelper nullCheck:boothItem[@"name"]],[AppHelper nullCheckWithNA:boothItem[@"fathers_name"]],[AppHelper nullCheckWithNA:boothItem[@"house_no"]],[AppHelper nullCheckWithNA:boothItem[@"age"]]];
    
  /*  NSString *subgenRal= [NSString stringWithFormat:@"F'Name: %@\nAge: %@\nGender: %@,\nCasteCategory: %@\nFamily members: %@\nEducation: %@\nOccupation: %@\nReligion: %@\nHead of family: %@\nIncome: %@\nResidentialStatus: %@",[AppHelper nullCheckWithNA:boothItem[@"fathers_name"]],[AppHelper nullCheck:boothItem[@"age"]],[AppHelper nullCheckWithNA:boothItem[@"gender"]],[AppHelper nullCheckWithNA:boothItem[@"casteCategory"]],[AppHelper nullCheckWithNA:boothItem[@"family_members"]],[AppHelper nullCheckWithNA:boothItem[@"education"]],[AppHelper nullCheckWithNA:boothItem[@"occupation"]],[AppHelper nullCheckWithNA:boothItem[@"religion"]],[AppHelper nullCheckWithNA:boothItem[@"head_of_family"]],[AppHelper nullCheckWithNA:boothItem[@"income"]],[AppHelper nullCheckWithNA:boothItem[@"residentialStatus"]]];*/
    
    UILabel *lblname = (UILabel *)[cell.contentView viewWithTag:1001];
    lblname.text = subName;
    
    UILabel *lblSublable = (UILabel*)[cell.contentView viewWithTag:1002];
    lblSublable.text = subTitle;
    
    UILabel *lastlable = (UILabel*)[cell.contentView viewWithTag:1003];
    lastlable.text=@"";

    UILabel *datelable = (UILabel*)[cell.contentView viewWithTag:1004];
    datelable.text=@"";
    
    return cell;
}

-(NSDate *)returnDateFromUnixTimeStampString:(double )unixTimeStampString
{
    NSDate*date= [NSDate dateWithTimeIntervalSince1970:unixTimeStampString];
    return [self getLocalDate:date];
    
}
-(NSDate *)getLocalDate:(NSDate *)localDate
{
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:localDate];
    NSDate *gmtDate = [localDate dateByAddingTimeInterval:timeZoneOffset];
    return gmtDate;
}
-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    

    NSInteger numOfSections = 0;
    if ([self.boothArrData count]>0 )
    {
        numOfSections  = 1;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        
        NSString *dataNot = NSLocalizedString(@"nodata", nil);
        noDataLabel.text             = dataNot;
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   NSDictionary *groupDict=[self.boothArrData objectAtIndex:indexPath.row];
   
    VoterFormViewController* objVerifyView=(VoterFormViewController*) [AppHelper intialiseViewControllerFromMainStoryboard:@"Voter" WithName:@"VoterFormViewController"];
    objVerifyView.serveyId=groupDict[@"id"];
      objVerifyView.requestType=@"1";
    
    if ([self.receiveTabButton isSelected]) {
        
        objVerifyView.status=@"0";
    }
    else
    {
        objVerifyView.status=@"1";

    }
    [self.navigationController  pushViewController:objVerifyView animated:YES];
    
}

#pragma mark - Action
- (IBAction)receiveTabButtonAction:(id)sender {
    
    [self selectReciverTab];
    keyselected=@"received";
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERshowBoothUsers];
        NSMutableDictionary *parameters =[NSMutableDictionary new];
        parameters[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameters[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameters[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        parameters[@"boothid"]=self.boothId;
        
        if (arrEventList.count==0)[[AppHelper sharedInstance]showIndicator];;
        parameters[@"verificationstatus"]=@"0";
        selected=0;
        [self httpRequestGetEventStatus:@"0" url:baseURL param:parameters];
      
    }
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}

- (IBAction)sentTabButtonAction:(id)sender {
    
    [self selectSentTab];
    keyselected=@"send";
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERshowBoothUsers];
        NSMutableDictionary *parameters =[NSMutableDictionary new];
        parameters[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameters[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameters[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        parameters[@"boothid"]=self.boothId;
        parameters[@"verificationstatus"]=@"1";
        selected=0;
        [self httpRequestGetEventStatus:@"0" url:baseURL param:parameters];
    }
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
}

#pragma mark - Searc Bar delegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    // called only once
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    // called twice every time
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    // called only once
    
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
    // called only once
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
     self.searchBar.text=@"";
     [self.view endEditing:YES];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
   
    if ([searchBar.text length]>=3) {
        
         [self.view endEditing:YES];
         [self searchText:searchBar.text];
    }
    else
    {
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Search text must be minimum three character" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
   
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
     if ([searchBar.text length]==0)
     {
         [self.view endEditing:YES];
         if([AppHelper appDelegate].checkNetworkReachability)
         {
             NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERshowBoothUsers];
             NSMutableDictionary *parameters =[NSMutableDictionary new];
             parameters[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
             parameters[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
             parameters[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
             parameters[@"boothid"]=self.boothId;
             
             if ([self.receiveTabButton isSelected]) {
                 
                 parameters[@"verificationstatus"]=@"0";
                 [self httpRequestGetEventStatus:@"0" url:baseURL param:parameters];
             }
             else
             {
                 parameters[@"verificationstatus"]=@"1";
                 [self httpRequestGetEventStatus:@"0" url:baseURL param:parameters];
             }
         }
         else{
             [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
         }
     }
}


-(void)searchText:(NSString *)string
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kSearchSurveyVoter];
        NSMutableDictionary *parameters =[NSMutableDictionary new];
        parameters[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameters[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameters[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        parameters[@"surveyName"]=string;
        parameters[@"boothid"]=self.boothId;
        
        if ([self.receiveTabButton isSelected]) {
            
            if (arrEventList.count==0)[[AppHelper sharedInstance]showIndicator];;
            parameters[@"verificationstatus"]=@"0";
            [self httpRequestGetEventStatus:@"0" url:baseURL param:parameters];
        }
        else
        {
            parameters[@"verificationstatus"]=@"1";
            [self httpRequestGetEventStatus:@"0" url:baseURL param:parameters];
        }
    }
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }

}
@end
