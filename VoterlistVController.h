//
//  BoothListVController.h
//  BJP
//
//  Created by PDSingh on 12/14/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface VoterlistVController : BaseViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeightConstant;
@property (strong, nonatomic) NSString  *boothId;

@end
