//
//  AppDelegate.m
//  BJP
//
//  Created by swatantra on 8/19/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachability.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "EventsViewController.h"
#import "UIImageView+AFNetworking.h"
#import "PVoiceVController.h"
#import "NotificationViewController.h"
#import "SettingsAppVController.h"
#import "WriteToUsViewController.h"
#import "InfoGraphicesVC.h"
#import "MagzineViewC.h"
#import "HomeeViewController.h"
#import "LanguageChangeViewController.h"


@interface AppDelegate ()<CLLocationManagerDelegate>

@end

@implementation AppDelegate

- (void)registerForRemoteNotifications
{
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else{
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
    }
#else
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
#endif
}
#pragma mark Location

- (void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    [AppHelper saveToUserDefaults:[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude] withKey:K_LATITUDE];
    [AppHelper saveToUserDefaults:[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude] withKey:K_LONGITUDE];
    
       
    
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {

    [AppHelper saveToUserDefaults:@"28.539934" withKey:K_LATITUDE];
    [AppHelper saveToUserDefaults:@"77.401408" withKey:K_LONGITUDE];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    self.locationManager   = [[CLLocationManager alloc] init];
    self.locationManager.delegate  = self;
    self.locationManager.distanceFilter     = 100;
    self.locationManager.desiredAccuracy    = kCLLocationAccuracyBestForNavigation;
    
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
          [self.locationManager requestWhenInUseAuthorization];
       // [self.locationManager requestAlwaysAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
    
    [LanguageChangeViewController setLangaugeUtils];
    [self registerForRemoteNotifications];
    [self initGooglePlus];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    SplashViewController *expandAndCollaps=(SplashViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"SplashViewController"];
    expandAndCollaps.view.tag=111;
    [[[self.window rootViewController] view]addSubview:expandAndCollaps.view];
    
   
    if (launchOptions) {
         NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
         if (userInfo)
             [self screenOpenBasedONPushPayload:userInfo];
     }
    
     return YES;
}


-(void)initGooglePlus
{
    //[GIDSignIn sharedInstance].clientID = KGOOGLECLIENT_KEY;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {

//    if ([[url scheme] isEqualToString:GOOGLE_SCHEME])
//        return [[GIDSignIn sharedInstance] handleURL:url sourceApplication:sourceApplication
//                                          annotation:annotation];
    
    if ([[url scheme] isEqualToString:FACEBOOK_SCHEME])
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                    openURL:url
                                                          sourceApplication:sourceApplication
                                                               annotation:annotation];
    
    return NO;


}

//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation {
//    
//    
//    return [[FBSDKApplicationDelegate sharedInstance] application:application
//                                                          openURL:url
//                                                sourceApplication:sourceApplication
//                                                       annotation:annotation];
//}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    if([AppHelper  userDefaultsForKey:USER_ID]){
        [AppHelper appDelegate].cureentUser=[[Service sharedEventController] getLoginDetailsWith:[AppHelper userDefaultsForKey:USER_ID]];
    }

    application.applicationIconBadgeNumber = 0;
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "cc.BJP" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"BJP" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"BJP.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
       // NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
#pragma mark- NetworkReachability

-(BOOL)checkNetworkReachability
{
    Reachability *rm = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    NetworkStatus internetStatus = [rm currentReachabilityStatus];
    
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))
    {
        UIAlertView*    aAlert = [[UIAlertView alloc] initWithTitle:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        [aAlert show];
        return NO;
    }
    else{
        return YES;
    }
    
}
#pragma mark SetDeviceToken
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString* deviceTkn = [[NSString stringWithFormat:@"%@",deviceToken] stringByReplacingOccurrencesOfString:@"<" withString:@""];
    deviceTkn = [deviceTkn stringByReplacingOccurrencesOfString:@">" withString:@""];
    deviceTkn = [deviceTkn stringByReplacingOccurrencesOfString:@" " withString:@""];
   // NSLog(@"Device Token ==>%@",deviceTkn);
    
    if(deviceTkn)
        [AppHelper saveToUserDefaults:deviceTkn withKey:Device_Token];
    //NSLog(@"device token %@",[AppHelper saveToUserDefaults:deviceTkn withKey:Device_Token]);
    else
        [AppHelper saveToUserDefaults:@"7063b6006efd7df5ebeddd5460ea0aef43244a3e89d123f143dc737de8d3f81f" withKey:Device_Token];
    
   [self httpUpdateDeviceToken];
    
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    
    [AppHelper saveToUserDefaults:@"7063b6006efd7df5ebeddd5460ea0aef43244a3e89d123f143dc737de8d3f81f" withKey:Device_Token];
     [self httpUpdateDeviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [self screenOpenBasedONPushPayload:userInfo];
}


-(void)screenOpenBasedONPushPayload:(NSDictionary *)userInfo
{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
    {
        
        if (userInfo) {
            
            if (userInfo[@"aps"]) {
                
                NSDictionary *infoDict=userInfo[@"aps"];
                NSString *pageType=[AppHelper nullCheck:infoDict[@"section"]];
                UIViewController *viControlroller=nil;
                NSArray *allViewControllers = [[AppHelper navigationController] viewControllers];
                
                if([AppHelper  userDefaultsForKey:USER_ID]){
                    
                    
                    if ([pageType isEqualToString:@"event"]) {
                        
                        for (id aViewController in allViewControllers) {
                            if ([aViewController isKindOfClass:[EventsViewController class]]) {
                                viControlroller=aViewController;
                            }
                        }
                        
                        if (!viControlroller) {
                            UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"EventsViewController"];
                            [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
                        }
                        else
                        {
                            [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
                        }
                        
                    }
                    else if ([pageType isEqualToString:@"survey"]) {
                        
                        for (id aViewController in allViewControllers) {
                            if ([aViewController isKindOfClass:[PVoiceVController class]]) {
                                viControlroller=aViewController;
                            }
                        }
                        
                        if (!viControlroller) {
                            
                            UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"PVoiceVController"];
                            [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
                        }
                        else
                            [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
                    }
                    else if ([pageType isEqualToString:@"notice"]) {
                        
                        
                        for (id aViewController in allViewControllers) {
                            if ([aViewController isKindOfClass:[NotificationViewController class]]) {
                                viControlroller=aViewController;
                            }
                        }
                        
                        if (!viControlroller) {
                            
                            UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"NotificationViewController"];
                            [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
                        }
                        else
                            [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
                    }
                    else if ([pageType isEqualToString:@"item"]) {
                        
                        for (id aViewController in allViewControllers) {
                            if ([aViewController isKindOfClass:[HomeeViewController class]]) {
                                viControlroller=aViewController;
                            }
                        }
                        
                        if (!viControlroller) {
                            UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"HomeeViewController"];
                            [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
                        }
                        else
                        {
                            [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
                        }
                    }
                    else if ([pageType isEqualToString:@"magazines"]) {
                        
                        for (id aViewController in allViewControllers) {
                            if ([aViewController isKindOfClass:[MagzineViewC class]]) {
                                viControlroller=aViewController;
                            }
                        }
                        
                        if (!viControlroller) {
                            
                            UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"MagzineViewC"];
                            [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
                        }
                        else
                            [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
                    }
                    else if ([pageType isEqualToString:@"infographics"]) {
                        
                        for (id aViewController in allViewControllers) {
                            if ([aViewController isKindOfClass:[InfoGraphicesVC class]]) {
                                viControlroller=aViewController;
                            }
                        }
                        
                        if (!viControlroller) {
                            
                            UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"InfoGraphicesVC"];
                            [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
                        }
                        else
                            [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
                    }
                    else if ([pageType isEqualToString:@"chat"]) {
                        
                        for (id aViewController in allViewControllers) {
                            if ([aViewController isKindOfClass:[InfoGraphicesVC class]]) {
                                viControlroller=aViewController;
                            }
                        }
                        
                        if (!viControlroller) {
                            
                            UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ChatViewController"];
                            [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
                        }
                        else
                            [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
                    }
                    else {
                        for (id aViewController in allViewControllers) {
                            if ([aViewController isKindOfClass:[HomeeViewController class]]) {
                                viControlroller=aViewController;
                            }
                        }
                        
                        if (!viControlroller) {
                            UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"HomeeViewController"];
                            [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
                        }
                        else
                        {
                            [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
                        }
                    }

                }
                else {
                    for (id aViewController in allViewControllers) {
                        if ([aViewController isKindOfClass:[HomeeViewController class]]) {
                            viControlroller=aViewController;
                        }
                    }
                    
                    if (!viControlroller) {
                        UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"HomeeViewController"];
                        [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
                    }
                    else
                    {
                        [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
                    }
                }
            }
        }
    }
}
-(void)httpUpdateDeviceToken
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"device_type"]=@"iPhone";
        parameter[@"gcm_regId"]=[AppHelper userDefaultsForKey:Device_Token];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kNewsetGcmRegId];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                
            };
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}


@end
