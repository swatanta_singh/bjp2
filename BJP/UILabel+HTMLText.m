//
//  UILabel+HTMLText.m
//  BJP
//
//  Created by PDSingh on 9/28/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "UILabel+HTMLText.h"

@implementation UILabel (HTMLText)

- (void)setHTMLString:(NSString *)string {
    
    
    NSMutableAttributedString *strTest = [[NSMutableAttributedString alloc]
                                                     initWithData: [string dataUsingEncoding:NSUnicodeStringEncoding
                                                                                              allowLossyConversion:NO]
                                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                                     documentAttributes:nil error:nil];

    [ strTest  addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [ strTest  length])];
    [strTest addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Roboto-Regular" size:13] range:NSMakeRange(0, [strTest length])];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setAlignment:NSTextAlignmentLeft];
    
    [ strTest  addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,  strTest .length)];
    self.attributedText=strTest;
    strTest=nil;

}
@end
