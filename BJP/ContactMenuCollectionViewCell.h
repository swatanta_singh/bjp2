//
//  ContactMenuCollectionViewCell.h
//  KINCT
//
//  Created by Toyaj Nigam on 3/10/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface ContactMenuCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *menuLable;
@property (weak, nonatomic) IBOutlet UILabel *selectedLable;
@end
