//
//  UILabel+HTMLText.h
//  BJP
//
//  Created by PDSingh on 9/28/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (HTMLText)
- (void)setHTMLString:(NSString *)string ;

@end
