//
//  AppDelegate.h
//  BJP
//
//  Created by swatantra on 8/19/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import <CoreData/CoreData.h>
#import "SplashViewController.h"
#import <objc/runtime.h>
#import "LoginViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) User *cureentUser;
@property(strong, nonatomic) LoginViewController *loginObjct;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic,strong)NSArray *categoryArray;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(BOOL)checkNetworkReachability;

@end

