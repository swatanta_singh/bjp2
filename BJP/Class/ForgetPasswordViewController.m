//
//  ForgetPasswordViewController.m
//  3O Seconds
//
//  Created by Swatantra Singh on 18/01/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "AFNetworking.h"
#import "Service.h"
#import "Defines.h"
#import "AppHelper.h"
#import "MenuBarActionItem.h"
@interface ForgetPasswordViewController ()
{
    NSDictionary *dictCodes;
    UIToolbar*   numberToolbar;
}


@property (weak, nonatomic) IBOutlet UILabel *messageTittle;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileId;
@property (weak, nonatomic) IBOutlet UIImageView *txtBg;
@property (nonatomic,strong) NSString *contryCode;
@property (weak, nonatomic) IBOutlet UITextField *txtOTP;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirm;

@end

@implementation ForgetPasswordViewController
@synthesize mobilenumber;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    numberToolbar.items = @[flexibleItem,[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor darkGrayColor];
    [numberToolbar setBackgroundColor:[UIColor lightGrayColor]];
    self.txtOTP.inputAccessoryView = numberToolbar;
    self.txtNewPassword.inputAccessoryView = numberToolbar;
    self.txtConfirm.inputAccessoryView = numberToolbar;

}

 -(void)viewDidAppear:(BOOL)animated{
     
 }

-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated {
   
    [super viewWillAppear:YES];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    __weak ForgetPasswordViewController *weekSelf=self;
    [self setUpHeaderWithTitle:@"Reset Password" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
         [weekSelf dismissViewControllerAnimated:NO completion:nil];
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark button action

- (IBAction)doneButtonClick:(id)sender {
    
    if(self.txtOTP.text.length>0)
        {
                if  (self.txtNewPassword.text.length > 0)
                {
                    if ([self.txtNewPassword.text length]< 8)
                    {
                        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"New password must be minimum 8 characters" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        return;
                    }
                    
                    if  (self.txtConfirm.text.length  > 0)
                    {
                        
                        if  ([self.txtConfirm.text isEqualToString:self.txtNewPassword.text])
                        {
                            [self.view endEditing:YES];
                            [self httpRequestForgotApi];

                        }
                        else
                        {
                            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"New password and confirm password must be same" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        }
                        
                    }
                    else
                    {
                        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please enter confirm password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    }
                }
                else
                {
                    [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please enter new password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                }
         }
        else
        {
             [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please type OTP" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        }
    
}

-(BOOL)checkStringPhone:(NSString *)text
{
    BOOL isYes=YES;
    NSString *str = text;
    NSRange range = [str rangeOfString:@"@"];
    if (range.location != NSNotFound)
    {
        isYes= YES;
    }
    else if( [str rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]].location != NSNotFound)
    {
        isYes= NO;
    }
    
    return isYes;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
      [self dismissViewControllerAnimated:YES completion:nil];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}



-(void)httpRequestForgotApi{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameters =[NSMutableDictionary new];
        parameters[@"otppin"] =self.txtOTP.text;
        parameters[@"mobile"] =self.mobilenumber;
        parameters[@"newPassword"] =self.txtNewPassword.text;

        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kNewOtpverifyForgotPassword];

        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
      //  manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        [manager POST:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            NSString *errorMsg=@"";
            
            if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Reset password successfully"
                                                                               message:nil
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction *action) {
                                                                    [self dismissViewControllerAnimated:NO completion:nil];
                                                                 }];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];

            }
            else
            {
                
                    
                    errorMsg=[AppHelper getErrorMessage:[responseObject objectForKey:@"error_description"]];
                    [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];

            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}
@end
