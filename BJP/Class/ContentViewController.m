//
//  ContentViewController.m
//  FragmentsTabsPager
//
//  Created by nakajijapan on 11/28/14.
//  Copyright (c) 2015 net.nakajijapan. All rights reserved.
//

#import "ContentViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "ContentViewController.h"
#import "UIImageView+AFNetworking.h"

@interface ContentViewController ()
@property(nonatomic,strong)NSArray *newsArray;
@property (weak, nonatomic) IBOutlet UITableView *tableViewNews;

@end

@implementation ContentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self httpNewsRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.newsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"NewsCell" forIndexPath:indexPath];
    NSDictionary *newsDictionary=[self.newsArray objectAtIndex:indexPath.row];
    UIImageView *imageV=[cellYouAvailable.contentView viewWithTag:1001];
    UILabel *newsType=[cellYouAvailable.contentView viewWithTag:1002];
    // UILabel *newsDate=[cellYouAvailable.contentView viewWithTag:1003];
    UILabel *newsTitle=[cellYouAvailable.contentView viewWithTag:1004];
    
    NSString *strImg=[AppHelper nullCheck:newsDictionary[@"imgUrl"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    
    [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user"]];
    
    newsType.text=[AppHelper nullCheck:newsDictionary[@"categoryTitle"]];
    newsTitle.text=[[AppHelper nullCheck:newsDictionary[@"itemTitle"]]capitalizedString];
    newsType.text=[[AppHelper nullCheck:newsDictionary[@"categoryTitle"]]capitalizedString];
    
    
    return cellYouAvailable;
}

-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /* Return an estimated height or calculate
     * estimated height dynamically on information
     * that makes sense in your case.
     */
    return 300.0f;
}

-(void)httpNewsRequest
{
    
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=@"1";
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        parameter[@"accessToken"]=@"";
        parameter[@"itemId"]=@"";
        parameter[@"accountId"]=@"";
        
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kRecent_News];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:nil];
        NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSDictionary *dict = @{@"body":json};
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        [manager POST:baseURL parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                 if ([responseObject objectForKey:@"items"] && [[responseObject objectForKey:@"items"] isKindOfClass:[NSArray class]]) {
                    self.newsArray = [responseObject objectForKey:@"items"];
                }
                
                [self.tableViewNews reloadData];
            };
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}


@end
