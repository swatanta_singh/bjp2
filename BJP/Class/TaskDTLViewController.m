//
//  TaskDTLViewController.m
//  BJP
//
//  Created by swatantra on 11/16/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "TaskDTLViewController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "UIImageView+AFNetworking.h"
#import "DateFormatters.h"
#import "CreateTaskViewController.h"
#import "ImageController.h"
#import "DetailsViewController.h"
@interface TaskDTLViewController (){
    UIToolbar*   numberToolbar;
    NSMutableArray *arrDataImg;
    
}
@property (strong, nonatomic) PresentionHandler *presentHandler;

@property (strong, nonatomic)NSDictionary *dictReport;
@property (weak, nonatomic) IBOutlet UIImageView *imgVBase;
@property (weak, nonatomic) IBOutlet UIView *viewFoter;

@property (weak, nonatomic) IBOutlet UIButton *btnComplete;

@property(nonatomic,strong)NSArray *arrData;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblSubject;
@property (weak, nonatomic) IBOutlet UILabel *lblmsz;
- (IBAction)viewMoreButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnViewMore;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnAttacmnt;

@property (weak, nonatomic) IBOutlet UITextView *txtViewReport;



@end

@implementation TaskDTLViewController
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
    
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.tblView.contentInset = UIEdgeInsetsMake(self.tblView.contentInset.top, 0, keyboardSize.height, 0);
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.tblView.contentInset = UIEdgeInsetsMake(self.tblView.contentInset.top, 0, 0, 0);
}
-(void)setTexrOnDelt{
    self.lblmsz.text=nil;

    NSString * htmlString = [AppHelper nullCheck:self.dictData[@"message"]];
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [attrStr length])];
    [attrStr addAttribute:NSFontAttributeName value:self.lblmsz.font range:NSMakeRange(0, [attrStr length])];
    //add alignment
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentLeft];
    [attrStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attrStr.length)];
    self.lblmsz.attributedText = attrStr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    arrDataImg=[NSMutableArray new];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    // Do any additional setup after loading the view.
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor blackColor];
    [numberToolbar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Header"]]];
    self.txtViewReport.inputAccessoryView = numberToolbar;
    
    self.lblEndDate.text=@"";
    self.lblStartDate.text=@"";
    if([[AppHelper nullCheck:self.dictData[@"message"]] length]<140){
        self.btnViewMore.hidden=YES;
    }
  
    self.lblSubject.text=[NSString stringWithFormat:@"Subject: %@",[AppHelper nullCheck:[self.dictData valueForKey:@"subject"]]];
    if([[AppHelper nullCheck:[self.dictData valueForKey:@"img_url"]]length]>2){
        NSURL *url2 = [NSURL URLWithString:[self normalizePath:self.dictData[@"img_url"]]];
        [self.imgView setImageWithURL:url2 placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    }
    NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];
    
    if([[AppHelper nullCheck:[self.dictData valueForKey:@"end_date"]]length]>2){
        NSDate *strDate=[AppHelper returnDateFromUnixTimeStampString:[self.dictData valueForKey:@"end_date"]];
        self.lblEndDate.text=[NSString stringWithFormat:@"%@ %@",@"End Date:",[formatter stringFromDate:strDate]];
    }
    if([[AppHelper nullCheck:[self.dictData valueForKey:@"start_date"]]length]>2){
        NSDate *strDate=[AppHelper returnDateFromUnixTimeStampString:[self.dictData valueForKey:@"start_date"]];
        self.lblStartDate.text=[NSString stringWithFormat:@"%@ %@",@"Start Date:",[formatter stringFromDate:strDate]];
    }
    self.imgVBase.backgroundColor=[UIColor colorWithRed:200.0/255.0 green:226.0/255.0 blue:204.0/255.0 alpha:1];
    if([self.dictData[@"status"] isEqualToString:@"Pending"]){
        self.imgVBase.backgroundColor=[UIColor colorWithRed:250.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1];
    }
    else   if([self.dictData[@"status"] isEqualToString:@"Compleated"]){
        self.btnComplete.selected=YES;
        self.imgVBase.backgroundColor=[UIColor colorWithRed:255.0/255.0 green:220.0/255.0 blue:200.0/255.0 alpha:1];
    }
    else {
        self.imgVBase.backgroundColor=[UIColor colorWithRed:200.0/255.0 green:226.0/255.0 blue:204.0/255.0 alpha:1];
    }
    
    if([self.serviceType isEqualToString:@"2"]){
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"taskId"]=self.dictData[@"taskId"];;
        parameter[@"org_member_id"]=self.dictData[@"org_member_id"];
        parameter[@"assignId"]=self.dictData[@"assignId"];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_REPORT];
        [self getReportData:parameter url:baseURL];
        self.viewFoter.hidden=NO;
        [self.btnSubmit setTitle:@"Submit Report" forState:UIControlStateNormal];
        if([self.dictData[@"status_id"] intValue]==0){
            self.btnSubmit.hidden=YES;
            self.txtViewReport.editable=NO;
            self.btnAttacmnt.enabled=NO;
        }
        else{
            self.txtViewReport.editable=YES;
            self.btnComplete.enabled=YES;
            self.btnSubmit.hidden=NO;
            self.btnAttacmnt.enabled=YES;
        }
        
    }
    else  if([self.serviceType isEqualToString:@"3"]){
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"taskId"]=self.dictData[@"taskId"];;
        parameter[@"org_member_id"]=self.dictData[@"to_member"];
        parameter[@"assignId"]=self.dictData[@"assignId"];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_REPORT];
        [self getReportData:parameter url:baseURL];
        self.btnSubmit.hidden=YES;
        self.txtViewReport.editable=NO;
        //self.btnComplete.enabled=NO;
        self.btnAttacmnt.enabled=NO;
    }
    else{
        [self getMembertData];
        self.btnSubmit.hidden=YES;
        self.viewFoter.hidden=YES;
        
    }
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
      [self setTexrOnDelt];
    __weak TaskDTLViewController *weekSelf=self;
    [self setUpHeaderWithTitle:@"Task Details" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [  weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - uicollectionview View
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [arrDataImg count];
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    UIImageView *imgeView=(UIImageView *)[cell.contentView viewWithTag:100];
    UIButton *btnD=(UIButton *)[cell.contentView viewWithTag:102];
    btnD.hidden=YES;
    // [btnD addTarget:self action:@selector(delteContact:) forControlEvents:UIControlEventTouchUpInside];
    cell.tag=indexPath.row;
    
    if([[arrDataImg objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]]){
        NSString *strImg=[AppHelper nullCheck:[[arrDataImg objectAtIndex:indexPath.row] valueForKey:@"media_url"]];
        NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
        [imgeView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    }
    else{
        imgeView.image=arrDataImg[indexPath.row];
        
    }
    return cell;
}
-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell=[collectionView cellForItemAtIndexPath:indexPath];
    UIImageView *imgeView=(UIImageView *)[cell.contentView viewWithTag:100];
    
    ImageController *expandAndCollaps=(ImageController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ImageController"];
    expandAndCollaps.image =imgeView.image;
    [self presentViewController:expandAndCollaps animated:YES completion:nil];
}

#pragma mark - Table View


- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if([self.serviceType isEqualToString:@"1"]){
        return @"Members";
    }
    else{
        return nil;
        
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=nil;
    
    
    cell=[tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCell" forIndexPath:indexPath];
    cell.accessoryType=UITableViewCellAccessoryNone;
    NSDictionary *groupList=[self.arrData objectAtIndex:indexPath.row];
    UIImageView *imageV = (UIImageView*)[cell.contentView viewWithTag:1001];
    UIImageView *imgVBase = (UIImageView*)[cell.contentView viewWithTag:1002];
    
    UILabel *lblname = (UILabel*)[cell.contentView viewWithTag:1003];
    UILabel *lblSublable = (UILabel*)[cell.contentView viewWithTag:1004];
    UILabel *lblDistrict = (UILabel*)[cell.contentView viewWithTag:1005];
    UILabel *lblStatius = (UILabel*)[cell.contentView viewWithTag:1006];
    lblStatius.text=[AppHelper nullCheck:[groupList valueForKey:@"status"]];
    lblname.text=[AppHelper nullCheck:[groupList[@"assingTo"] valueForKey:@"name"]];
    lblSublable.text=[AppHelper nullCheck:[groupList[@"assingTo"] valueForKey:@"desg_hi"]];
    lblDistrict.text=[AppHelper nullCheck:[groupList[@"assingTo"] valueForKey:@"location"]];
    NSString *strImg=[AppHelper nullCheck:[groupList[@"member_img"] valueForKey:@"user_image"]];
    if(strImg.length>3){
        NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
        [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    }
    
    if([groupList[@"status"] isEqualToString:@"Ongoing"]){
        imgVBase.backgroundColor=[UIColor colorWithRed:255/255.0 green:121/255.0 blue:51/255.0 alpha:1];
        
        
    }
    else   if([groupList[@"status"] isEqualToString:@"Compleated"]){
        imgVBase.backgroundColor=[UIColor colorWithRed:30/255.0 green:137/255.0 blue:56/255.0 alpha:1];
    }
    else {
        imgVBase.backgroundColor=[UIColor colorWithRed:250.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1];
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TaskDTLViewController *expandAndCollaps=(TaskDTLViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"TaskDTLViewController"];
    expandAndCollaps.dictData=self.arrData[indexPath.row];
    expandAndCollaps.serviceType=@"3";
    [self.navigationController pushViewController:expandAndCollaps animated:YES];
}
#pragma mark buttonAction
- (IBAction)imgHederButtonClick:(id)sender {
    if([[AppHelper nullCheck:[self.dictData valueForKey:@"img_url"]]length]>2){
        
        ImageController *expandAndCollaps=(ImageController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ImageController"];
        expandAndCollaps.image =self.imgView.image;
        [self presentViewController:expandAndCollaps animated:YES completion:nil];
    }
    
}

- (IBAction)btnCompleteAction:(id)sender{
    if([self.serviceType isEqualToString:@"3"]){
    }
    else  if([self.dictData[@"status_id"] intValue]==0){
    }
    else{
        if(self.btnComplete.selected){
            self.btnComplete.selected=NO;
        }
        else{
            self.btnComplete.selected=YES;
        }
    }
}
- (IBAction)viewMoreButtonAction:(id)sender {
    DetailsViewController* objVerifyView= (DetailsViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"DetailsViewController"];
    objVerifyView.strDtl=self.dictData[@"message"];
    self.presentHandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objVerifyView fromPresentingViewController:self withStyle:UIModalPresentationCustom fromView:sender andPreferedSize:CGSizeMake(SCREEN_WIDTH-50, SCREEN_HEIGHT-150)];
    
}
- (IBAction)editButtonAction:(id)sender {
    if([self.serviceType isEqualToString:@"2"]){
        if(self.txtViewReport.text.length<5){
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please enter the report." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }
        //           else if(arrDataImg.count==0){
        //                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please select a attachment." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        //           }
        else{
            if(self.dictReport){
                [self upDateReportAssignment];
                
            }
            else{
                [self addReportAssignment];
                
            }
            
            
        }
    }
    else{
        
    }
}
- (IBAction)attachmentButtonClick:(id)sender {
    if(arrDataImg.count==5){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"you can upload maximum 5 photos." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        return;
    }
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
            // There is not a camera on this device, so don't show the camera button.
            
        }
        else{
            [AppHelper showAlertViewWithTag:10 title:APP_NAME message:@"Camera not available." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        }
        // Camera button tapped.
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Library button tapped.
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        // [self launchSpecialController];
        
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}
- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    // imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}
#pragma mark UIImagePickerControllerDelegate ------------


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    [arrDataImg addObject:image];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.collectionView reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Text Field
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView{
    //[self.tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}
- (void)textViewDidEndEditing:(UITextView *)textView{
}
#pragma  mark service
-(void)upDateReportAssignment{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        NSMutableDictionary *parameters=[NSMutableDictionary new];
        parameters[@"clientId"]=@"1";
        parameters[@"report_desc"]=self.txtViewReport.text;
        parameters[@"status"]=@"1";
        
        if(self.btnComplete.selected){
            parameters[@"status"]=@"2";
            
        }
        parameters[@"file_type"]=@"1";
        parameters[@"assignId"]=self.dictData[@"assignId"];
        parameters[@"activity_id"]=self.dictReport[@"activity_id"];
        
        parameters[@"org_member_id"]=self.dictData[@"org_member_id"];//[AppHelper appDelegate].cureentUser.accountId;
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUPDATE_REPORT];
        [[AppHelper sharedInstance]showIndicator];
        
        
        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
        
        AFHTTPRequestOperation *op = [manager POST:baseURL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //do not put image inside parameters dictionary as I did, but append it!
            int i=1;
            
            for(id imgData in arrDataImg){
                if([imgData isKindOfClass:[NSDictionary class]]){
                    
                }
                else{
                    NSData *imageData = UIImageJPEGRepresentation(imgData, 0.5);
                    [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"uploaded_file%d",i] fileName:@"photo.jpg" mimeType:@"image/jpeg"];
                    i++;
                }
                
            }
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Submitted Successfully.." delegate:self cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
        [op start];
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
}

-(void)addReportAssignment{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        NSMutableDictionary *parameters=[NSMutableDictionary new];
        parameters[@"clientId"]=@"1";
        parameters[@"report_desc"]=self.txtViewReport.text;
        parameters[@"status"]=@"1";
        
        if(self.btnComplete.selected){
            parameters[@"status"]=@"2";
            
        }
        parameters[@"file_type"]=@"1";
        parameters[@"assignId"]=self.dictData[@"assignId"];
        parameters[@"taskId"]=self.dictData[@"taskId"];
        
        parameters[@"org_member_id"]=self.dictData[@"org_member_id"];//[AppHelper appDelegate].cureentUser.accountId;
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kADD_REPORT];
        [[AppHelper sharedInstance]showIndicator];
        
        
        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
        
        AFHTTPRequestOperation *op = [manager POST:baseURL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //do not put image inside parameters dictionary as I did, but append it!
            int i=1;
            for(UIImage *img in arrDataImg){
                NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
                [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"uploaded_file%d",i] fileName:@"photo.jpg" mimeType:@"image/jpeg"];
                i++;
            }
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Submitted Successfully.." delegate:self cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
        [op start];
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==100){
        [[AppHelper navigationController] popViewControllerAnimated:YES];
        
    }
}

-(void)getReportData:(NSDictionary *)parameter url:(NSString*)baseURL{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        //  manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                
                self.dictReport=responseObject;
                self.txtViewReport.text=[responseObject valueForKey:@"report_desc"];
                [arrDataImg addObjectsFromArray:[responseObject valueForKey:@"attachment"]];
                //ata
                [self.collectionView reloadData];
                [self.tblView reloadData];
            }
            
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}
-(void)getMembertData{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        //clientId
        parameter[@"taskId"]=self.dictData[@"taskId"];;
        parameter[@"org_from_member"]=self.dictData[@"org_from_member"];;//[AppHelper appDelegate].cureentUser.accountId;
        //parameter[@"assignId"]=self.dictData[@"assignId"];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_MEMBER_TASK];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        //  manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                self.arrData=[responseObject valueForKey:@"list"];
                [self.tblView reloadData];
            }
            
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}

@end
