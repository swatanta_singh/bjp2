//
//  HomeViewController.m
//  BJP
//
//  Created by swatantra on 8/19/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "HomeeViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "ContentViewController.h"
#import "UIImageView+AFNetworking.h"
#import "ContactMenuCollectionViewCell.h"
#import "ContactContainCollectionViewCell.h"
#import "DetlsViewController.h"
#import "Service.h"
#import "LanguageChangeViewController.h"
#import "PresentionHandler.h"
 #import <MediaPlayer/MediaPlayer.h>
#import "FavouriteViewC.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface HomeeViewController ()
{
    NSUInteger selectIndex;
    NSUInteger maxCount;
    NSString *lastId;
    BOOL isLoadmore;
    
    UILabel *noDataLabel;
    UIRefreshControl   *refreshControl;
}

@property (strong, nonatomic)NSArray *menuArray;
@property(nonatomic,strong)NSArray *newsArray;
@property (strong, nonatomic) PresentionHandler *presentHandler;
@property (weak, nonatomic) IBOutlet UICollectionView *headerMenuCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *containerCollectionView;

@end

@implementation HomeeViewController


-(void)finalMethoForLanguge{
    
    if([AppHelper nullCheck:  [AppHelper userDefaultsForKey:APP_LANGAUGE]].length==0){
     
        [AppHelper saveToUserDefaults:@"en" withKey:APP_LANGAUGE];
        LanguageChangeViewController* objLanguageView= (LanguageChangeViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LanguageChangeViewController"];
        
        self.presentHandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objLanguageView fromPresentingViewController:self withStyle:UIModalPresentationCustom fromView:nil andPreferedSize:CGSizeMake(SCREEN_WIDTH-50, 280)];
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];

    maxCount=1;
    [self performSelector:@selector(finalMethoForLanguge) withObject:nil afterDelay:8];
    if([AppHelper  userDefaultsForKey:USER_ID]){
        [AppHelper appDelegate].cureentUser=[[Service sharedEventController] getLoginDetailsWith:[AppHelper userDefaultsForKey:USER_ID]];
    }
    
  //[self playBtnPressed:nil];
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
}


-(void)refreshTable {
    
    maxCount=1;
    lastId=@"0";
    [refreshControl endRefreshing];
    [self menuModel];
}

-(void)addLeftView{
    [[[AppHelper sharedInstance]menuViewController] setUpMove];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
   
    [[AVAudioSession sharedInstance]
     setCategory: AVAudioSessionCategoryPlayback
     error: nil];
    [[AVAudioSession sharedInstance] setActive:true error:nil];
    
    
     isLoadmore=YES;
     maxCount=1;
     lastId=@"0";
    
    if (selectIndex>0) {
        [self menuModel];
    }
    else
    {
        [self httpRequestForCategoryCompletionHandler:^(id result) {
            if([AppHelper appDelegate].checkNetworkReachability)
            {
                [[AppHelper sharedInstance]showIndicator];
                NSDictionary *tempDict=[self.menuArray firstObject];
                [self httpNewsRequest:tempDict[@"id"]];
            }
            
        }];
    }
    
    [super viewWillAppear:animated];
    __weak HomeeViewController *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"News", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"menu" withRigthbtn:@"Favorite" WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [weekSelf addLeftView];
        }
        else if(navigateValue==2){
            
            if ([AppHelper appDelegate].cureentUser.accountId) {
              
                FavouriteViewC *expandAndCollaps=(FavouriteViewC*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"FavouriteViewC"];
                [weekSelf.navigationController pushViewController:expandAndCollaps animated:YES];
            }
            else
            {
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LoginViewController"];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
            }
        }
        else [weekSelf.view endEditing:YES];

    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - UICollection view
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
    return [self.menuArray count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView.tag==1001)
    {
        ContactMenuCollectionViewCell *cell = (ContactMenuCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ContactMenuCollectionViewCell" forIndexPath:indexPath];
        cell.menuLable.tag = indexPath.row;
        
        NSDictionary *catagryDict=[self.menuArray objectAtIndex:indexPath.row];
        [cell.menuLable setText:[AppHelper nullCheck:catagryDict[@"title"]]];
        
        if (indexPath.row == selectIndex) {
            
            [cell.selectedLable setHidden:NO];
            cell.menuLable.textColor=[UIColor colorWithRed:246.0f/255.0f green:129.0f/255.0f blue:33.0f/255.0f alpha:1];
            cell.selectedLable.backgroundColor =[UIColor colorWithRed:246.0f/255.0f green:129.0f/255.0f blue:33.0f/255.0f alpha:1];
        }
        else
        {
            cell.menuLable.textColor=[UIColor grayColor];
            [cell.selectedLable setHidden:YES];
        }
        return cell;
    }
    else
    {
        ContactContainCollectionViewCell *cell = (ContactContainCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ContactContainCollectionViewCell" forIndexPath:indexPath];
        UITableView *tempTable = (UITableView*)[cell.contentView viewWithTag:100];
        [tempTable addSubview:refreshControl];
        tempTable.tableFooterView =[[UIView alloc]initWithFrame:CGRectZero];
        [tempTable reloadData];
        return cell;
    }
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView.tag==1001)
    {
        NSDictionary *catagryDict=[self.menuArray objectAtIndex:indexPath.row];
        NSString *menuTitle=[AppHelper nullCheck:catagryDict[@"title"]];
        CGSize calCulateSizze=[self widthOfString:menuTitle withFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
        calCulateSizze.width= calCulateSizze.width+20;
        calCulateSizze.height = calCulateSizze.height + 20;
        return calCulateSizze;

    }
    else
        return CGSizeMake(_containerCollectionView.frame.size.width, _containerCollectionView.frame.size.height);
    
}

- (CGSize)widthOfString:(NSString *)string withFont:(UIFont *)font {
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size];
}


- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (_headerMenuCollection == collectionView)
    {
        if (selectIndex != indexPath.row)
        {
            selectIndex = indexPath.row;
          
             [_containerCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
             [_headerMenuCollection scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            
            self.newsArray=nil;
            lastId=@"0";
             maxCount=1;
            
            if([AppHelper appDelegate].checkNetworkReachability)
            {
                [[AppHelper sharedInstance]showIndicator];
                noDataLabel.hidden=YES;
                [self menuModel];
            }
            
        }
    }
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (selectIndex<self.menuArray.count) {
        
        if (_containerCollectionView == scrollView)
        {
            NSIndexPath *path =
            [self.containerCollectionView indexPathForItemAtPoint:
             [self.view convertPoint:[self.view center] toView:self.containerCollectionView]];
         
                if (selectIndex != path.row)
                {
                    selectIndex = path.row;
                    self.newsArray=nil;
                    
                    lastId=@"0";
                     maxCount=1;
                    
                    [_headerMenuCollection scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
                    if([AppHelper appDelegate].checkNetworkReachability)
                    {
                        [[AppHelper sharedInstance]showIndicator];
                        noDataLabel.hidden=YES;
                        [self menuModel];
                    }
                }
           // }
        }

    }
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([self.newsArray count]>0 )
    {
        numOfSections  = 1;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView = nil;
    }
    else
    {
        if (!noDataLabel) {
            noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
            noDataLabel.text             =  NSLocalizedString(@"NOUpdate", nil);
            noDataLabel.textColor        = [UIColor blackColor];
            noDataLabel.textAlignment    = NSTextAlignmentCenter;
        }
        
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row>=self.newsArray.count)
        return 40.0;
    else
        return 320.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.newsArray.count == maxCount)
        return self.newsArray.count;
    else
        return self.newsArray.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if((indexPath.row > self.newsArray.count-1)){
       
        UITableViewCell *myCell=[tableView dequeueReusableCellWithIdentifier:@"ProviderDetail" forIndexPath:indexPath];
        return myCell;
    }
    else
    {
        UITableViewCell *cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"NewsCell" forIndexPath:indexPath];
        NSDictionary *newsDictionary=[self.newsArray objectAtIndex:indexPath.row];
        UIImageView *imageV=[cellYouAvailable.contentView viewWithTag:1001];
        UILabel *newsType=[cellYouAvailable.contentView viewWithTag:1002];
        UILabel *newsDate=[cellYouAvailable.contentView viewWithTag:1003];
        UILabel *newsTitle=[cellYouAvailable.contentView viewWithTag:1004];
        UILabel *newsLike=[cellYouAvailable.contentView viewWithTag:1005];
        UIWebView *webView=[cellYouAvailable.contentView viewWithTag:111];
        UIButton *playButton=[cellYouAvailable.contentView viewWithTag:5001];
        cellYouAvailable.tag=indexPath.row;
        
        imageV.hidden=NO;
       
        if([newsDictionary[@"videoUrl"] hasPrefix:@"https://www.youtube.com"] || [newsDictionary[@"videoUrl"] hasPrefix:@"http://www.youtube.com"]) {
               
            [playButton setHidden:YES];
            [webView setHidden:NO];
            
            imageV.hidden=YES;
            webView.hidden=NO;

            NSString *vedioId = [self extractYoutubeIdFromLink:newsDictionary[@"videoUrl"]];
            
            NSString *embedHTML =[NSString stringWithFormat:@"<iframe width=\"%f\" height=\"%f\" src=\"http://www.youtube.com/embed/%@\" autoplay=1 frameborder=\"0\" allowfullscreen></iframe>",webView.frame.size.width-15,webView.frame.size.height,vedioId];
            
            webView.scrollView.bounces = NO;
            webView.scrollView.scrollEnabled=NO;
            [webView loadHTMLString:embedHTML baseURL:nil];

            }
            else  if ([[AppHelper nullCheck:newsDictionary[@"videoUrl"]] length]>10)
            {
                [playButton setHidden:NO];
                [webView setHidden:YES];
                [playButton addTarget:self action:@selector(playBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
                NSString *strImg=[AppHelper nullCheck:newsDictionary[@"imgUrl"]];
                NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
                [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
            }
           else
           {
          
           [playButton setHidden:YES];
           [webView setHidden:YES];
            
            NSString *strImg=[AppHelper nullCheck:newsDictionary[@"imgUrl"]];
            NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
            [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
           }
        
        if ([newsDictionary[@"likesCount"]intValue]==0)
            newsLike.text=@"";
        else
            newsLike.text=[NSString stringWithFormat:@"👍 Likes %@",newsDictionary[@"likesCount"]];
        
        newsType.text=[AppHelper nullCheck:newsDictionary[@"categoryTitle"]];
        newsDate.text=[AppHelper nullCheck:newsDictionary[@"date"]];
        newsTitle.text=[AppHelper nullCheck:newsDictionary[@"itemTitle"]];
        newsType.text=[[AppHelper nullCheck:newsDictionary[@"categoryTitle"]]capitalizedString];
        return cellYouAvailable;
    }
    
}

- (NSString *)extractYoutubeIdFromLink:(NSString *)link {
    
    NSString *regexString = @"((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)";
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    
    NSArray *array = [regExp matchesInString:link options:0 range:NSMakeRange(0,link.length)];
    if (array.count > 0) {
        NSTextCheckingResult *result = array.firstObject;
        return [link substringWithRange:result.range];
    }
    return nil;
}


-(void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        if (isLoadmore)
        [self menuModel];
    }
}

-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[ self playBtnPressed:@"https://www.youtube.com/watch?v=wtQrl35l26Y"];
   DetlsViewController *expandAndCollaps=(DetlsViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"DetlsViewController"];
   expandAndCollaps.servicDict = [[self.newsArray objectAtIndex:indexPath.row]mutableCopy];
   [self.navigationController pushViewController:expandAndCollaps animated:YES];

  }

#pragma mark video play
-(void)playBtnPressed:(UIButton *)sender{
    
    UITableViewCell *cell = (UITableViewCell *)[sender.superview.superview superview];
    NSDictionary *newsDictionary =[self.newsArray objectAtIndex:cell.tag];
    if ([self validUrl:newsDictionary[@"videoUrl"]]) {
        
        
        NSURL *url=[[NSURL alloc] initWithString:newsDictionary[@"videoUrl"]];
        AVPlayer *player = [AVPlayer playerWithURL:url];
        player.closedCaptionDisplayEnabled = NO;
        player.muted=NO;
        AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
        [self presentViewController:controller animated:YES completion:nil];
        controller.player = player;
  }

}

-(BOOL)validUrl:(NSString *)stringUrl
{
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:stringUrl]];
    bool valid = [NSURLConnection canHandleRequest:req];
    return valid;
}
#pragma mark - Reload News

-(void)menuModel
{
    
    if(self.newsArray.count < maxCount)
    {
      [self.headerMenuCollection reloadData];
      [self.containerCollectionView reloadData];
      if (self.menuArray && [self.menuArray count]>0) {
        
        if([AppHelper appDelegate].checkNetworkReachability){
           NSDictionary *tempDict=[self.menuArray objectAtIndex:selectIndex];
                [self httpNewsRequest:tempDict[@"id"]];
        }
     }
   }
  
}

#pragma mark - NKJPagerViewDelegate

-(void)httpRequestForCategoryCompletionHandler:(void (^)(id result))completionHandler
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGet_Categrories];

        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"clientId"]=@"1";
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                
                if (responseObject[@"user_type"])
                    [AppHelper saveToUserDefaults:responseObject[@"user_type"] withKey:USER_TYPE];
                
                if ([responseObject objectForKey:@"items"] && [[responseObject objectForKey:@"items"] isKindOfClass:[NSArray class]]) {
                   self.menuArray = [responseObject objectForKey:@"items"];
                }
                
                [self.headerMenuCollection reloadData];
                completionHandler(self.menuArray);
            }
               [self.containerCollectionView reloadData];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          
            [[AppHelper sharedInstance]hideIndicator];
               [self.containerCollectionView reloadData];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
}

-(void)httpNewsRequest:(NSString *)categoryId
{
        isLoadmore = NO;
    
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=@"1";
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        parameter[@"lastId"]=lastId;
        parameter[@"categoryId"] = categoryId;
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGet_Categroy];

        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
         manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
           
            isLoadmore = YES;
            [noDataLabel setHidden:NO];
            
            [refreshControl endRefreshing];
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([responseObject objectForKey:@"items"] && [[responseObject objectForKey:@"items"] isKindOfClass:[NSArray class]]) {
                    
                    
                    maxCount=[[responseObject objectForKey:@"MaxCount"]intValue];
                    if ([lastId intValue]>0)
                        self.newsArray =[self.newsArray arrayByAddingObjectsFromArray:[responseObject objectForKey:@"items"]];
                    else
                        self.newsArray = [responseObject objectForKey:@"items"];
                }
            }
            
            if ([self.newsArray count]>0) {
                lastId=[[self.newsArray lastObject] objectForKey:@"id"];
            }
            [self.containerCollectionView reloadData];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            self.newsArray=nil;
            [noDataLabel setHidden:NO];
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
               [self.containerCollectionView reloadData];
        }];
        
    }
    


@end
