//
//  ChatViewController.m
//  KINCT
//
//  Created by Toyaj Nigam on 3/10/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import "ChatViewController.h"
#import "ContainerCollectionViewCell.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "ChatDtlViewController.h"
#import "GroupDetailViewC.h"
#import "UIImageView+AFNetworking.h"

@interface ChatViewController (){
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSArray *groupListArray;


@end

@implementation ChatViewController

#pragma mark viewLife
- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}




-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self httpGrouplistRequest];
    
    __weak ChatViewController *weekSelf=self;
    [self setUpHeaderWithTitle:@"Recent Chat" withLeftbtn:@"menu" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [[[AppHelper sharedInstance]menuViewController] setUpMove];
        [weekSelf.view endEditing:YES];
    }];
}


#pragma mark - Update Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.groupListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCell" forIndexPath:indexPath];
    
    UIView *view = (UIView*)[cell.contentView viewWithTag:6001];

    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(1, 1);
    view.layer.shadowRadius = 2;
    view.layer.shadowOpacity = 0.5;
    
    NSDictionary *groupList=[self.groupListArray objectAtIndex:indexPath.row];
    UILabel *lblname = (UILabel*)[cell.contentView viewWithTag:1001];
    lblname.text=[[AppHelper nullCheck:groupList[@"ac_title"]]capitalizedString];
   
    UILabel *lblSublable = (UILabel*)[cell.contentView viewWithTag:1002];
    lblSublable.text=[AppHelper nullCheck:[groupList[@"Last_message"] objectForKey:@"last_meesage"]];

    UILabel *lastlable = (UILabel*)[cell.contentView viewWithTag:1003];
    lastlable.text=[NSString stringWithFormat:@"%@ Members",groupList[@"MemberCount"]];

    UILabel *datelable = (UILabel*)[cell.contentView viewWithTag:1004];
    double stimeStamp =[[AppHelper nullCheck:[groupList[@"Last_message"] objectForKey:@"sent_time"]]doubleValue];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setDateFormat:@"MMM dd, h:mm a"];
    NSDate *date = [self returnDateFromUnixTimeStampString:stimeStamp];
    datelable.text=[dateFormatter stringFromDate:date];
   
    UIImageView *imageV = (UIImageView*)[cell.contentView viewWithTag:5002];
    NSString *strImg=[AppHelper nullCheck:groupList[@"ac_image"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];

    UIButton *infoButton = (UIButton*)[cell.contentView viewWithTag:5001];
    infoButton.tag=indexPath.row;
    [infoButton addTarget:self action:@selector(infoButton:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(NSDate *)returnDateFromUnixTimeStampString:(double )unixTimeStampString
{
    NSDate*date= [NSDate dateWithTimeIntervalSince1970:unixTimeStampString];
    return [self getLocalDate:date];
    
}
-(NSDate *)getLocalDate:(NSDate *)localDate
{
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:localDate];
    NSDate *gmtDate = [localDate dateByAddingTimeInterval:timeZoneOffset];
    return gmtDate;
}
-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    NSInteger numOfSections = 0;
    if ([self.groupListArray count]>0 )
    {
        numOfSections  = 1;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"No Groups";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *groupDict=[self.groupListArray objectAtIndex:indexPath.row];

    ChatDtlViewController* objVerifyView=(ChatDtlViewController*) [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ChatDtlViewController"];
    objVerifyView.groupDict=groupDict;
    [self.navigationController  pushViewController:objVerifyView animated:YES];
    
}

-(void)infoButton:(UIButton *)sender
{
    NSDictionary *groupDict=[self.groupListArray objectAtIndex:sender.tag];
    GroupDetailViewC* objVerifyView=(GroupDetailViewC*) [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"GroupDetailViewC"];
    objVerifyView.groupDict=groupDict;
    [self.navigationController  pushViewController:objVerifyView animated:YES];

}

#pragma mark -

-(void)httpGrouplistRequest
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=[NSNumber numberWithInt:1];
        parameter[@"memberId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        
//         [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[NSString  stringWithFormat: @"param %@",parameter] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kNewChatGroupList];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
//                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[NSString  stringWithFormat: @"respionse %@",responseObject] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                
                
                if([[responseObject objectForKey:@"group_list"] count]>0){
                    self.groupListArray = [responseObject objectForKey:@"group_list"];
                }
                else{
                   [AppHelper showAlertViewWithTag:110 title:APP_NAME message:NSLocalizedString(@"CHAT_ALERT", nil) delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES"];
                }
                
                [self.tableView reloadData];
            }
            else{
                  [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Some error found." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==110){
        if(buttonIndex==0){
            [[AppHelper navigationController]popViewControllerAnimated:YES];
        }
        else{
            UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ProfileViewController"];
            [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
        }
    }
}
@end
