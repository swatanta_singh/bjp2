//
//  ChatDtlViewController.m
//  KINCT
//
//  Created by swatantra on 3/16/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import "ChatDtlViewController.h"

#import "Defines.h"
#import "AppHelper.h"
#import "UIBubbleTableView.h"
#import "UIBubbleTableViewDataSource.h"
#import "NSBubbleData.h"
#import "DAKeyboardControl.h"
#import "HPGrowingTextView.h"
#import "MessageComposerView.h"

#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "GroupDetailViewC.h"


@interface ChatDtlViewController()<UIBubbleTableViewDataSource,HPGrowingTextViewDelegate,MessageComposerViewDelegate>
{
    BOOL isConnectionConnect;
    NSMutableArray *bubbleData;
    
    BOOL isSentMessage;
    
    NSString *selectedChannellName;
    UIRefreshControl  *refreshControl;
    NSOperationQueue *sentMessageQueue;
    
    

}

@property (nonatomic, strong) MessageComposerView *messageComposerView;
@property (weak, nonatomic) IBOutlet UIBubbleTableView *bubbleTable;
@property (weak, nonatomic) IBOutlet HPGrowingTextView *txtViewMessagebox;
@property (weak, nonatomic) IBOutlet UIView *chatParentView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property(nonatomic,strong)NSArray *messgaArray;
@property(nonatomic,strong)NSTimer* myTimer;
@property(nonatomic,strong)NSSet *messgaArraySet;
@property(nonatomic,strong)NSMutableArray *allMessageArray;
@property(nonatomic,strong)NSOperationQueue *getMessageQueue;;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loaderView;
@property (weak, nonatomic) IBOutlet UIView *loaderViewV;


@end

@implementation ChatDtlViewController
@synthesize title;
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self loadChatTableDelegate];
    [self loadingDefaultsSetup];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[Service sharedEventController]setUserStatus:@"1"];
   
    isSentMessage=YES;
    self.allMessageArray=[NSMutableArray new];
    bubbleData = [[NSMutableArray alloc] init];
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        [self httpChatHistoryAndOldMessageRequest];
    }
    [self setTitleAndBackAction];
    
}

#pragma mark - Copy & Paste

-(void)viewDidAppear:(BOOL)animated
{
    [self getRecentMessageTimer];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[Service sharedEventController]setUserStatus:@"0"];
    if (self.myTimer && [self.myTimer isValid]) {
        [self.myTimer invalidate];
        self.myTimer=nil;
    }
}

#pragma mark - Title & Back Action
-(void)setTitleAndBackAction
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardDidShowNotification:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardDidHideNotification:) name:UIKeyboardWillHideNotification object:nil];
    
    __weak ChatDtlViewController *weekSelf=self;
    NSString *titleString =[[AppHelper nullCheck:self.groupDict[@"ac_title"] ]capitalizedString];
    
    [self.headerView.btnRight setImage:[UIImage imageNamed:@"ViewMembers"] forState:UIControlStateNormal];
    [self setUpHeaderWithTitle:titleString withLeftbtn:@"back" withRigthbtn:@"GroupInfo" WithComilation:^(int navigateValue) {
        
        if (navigateValue==1) {
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            
            GroupDetailViewC* objVerifyView=(GroupDetailViewC*) [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"GroupDetailViewC"];
            objVerifyView.groupDict=weekSelf.groupDict;
            [weekSelf.navigationController  pushViewController:objVerifyView animated:YES];

        }
    }];

   // [self.headerView.lblStatus setHidden:NO];
}
-(void)hideKeyBoardOntap
{
    [self.messageComposerView.messageTextView resignFirstResponder];
}



#pragma mark - Keyboard Notification
- (void)handleKeyboardDidShowNotification:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    _bubbleTable.contentInset = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0);
    [_bubbleTable scrollBubbleViewToBottomAnimated:NO];
}

- (void)handleKeyboardDidHideNotification:(NSNotification *)notification
{
    _bubbleTable.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [_bubbleTable scrollBubbleViewToBottomAnimated:NO];

}


-(void)loadingDefaultsSetup
{
    
    sentMessageQueue = [NSOperationQueue mainQueue];

    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyBoardOntap)];
    [self.view addGestureRecognizer:tap];
    
    
    [self bottomChatTextBoxSetup];
  
   }
#pragma mark - Send Text Messages and Delegate
-(void)bottomChatTextBoxSetup
{
    self.messageComposerView = [[MessageComposerView alloc] init];
    self.messageComposerView.delegate = self;
    self.messageComposerView.messagePlaceholder = @"Type a message";
    [self.view addSubview:self.messageComposerView];
    
}
- (void)messageComposerSendMessageClickedWithMessage:(NSString*)message{
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self messageBindParam:message];
    });
    
}


-(void)messageBindParam:(NSString*)message
{
   // if (isConnectionConnect) {
        NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
        NSString *messsageText = [message stringByTrimmingCharactersInSet:charSet];
        
        if ([messsageText length]>0)
            {
            
            isSentMessage=YES;
            
            NSDictionary *messageDict=[NSMutableDictionary new];
            [messageDict setValue:messsageText forKey:@"message"];
            [messageDict setValue:[AppHelper appDelegate].cureentUser.accountId forKey:@"member_id"];
                NSString *lastId=nil;
                if ([self.allMessageArray count]>0) {
                    NSDictionary *lastMessage=[self.allMessageArray lastObject];
                    lastId = lastMessage[@"id"];
                }
                else
                    lastId=@"0";
                
            [messageDict setValue:lastId forKey:@"lastId"];
            [messageDict setValue:self.groupDict[@"RoomId"] forKey:@"RoomId"];
            NSInvocationOperation *sentMessageOperation=[[NSInvocationOperation alloc]initWithTarget:self selector:@selector(httMessageSent:) object:messageDict];
            [sentMessageQueue addOperation:sentMessageOperation];
        }
}

#pragma mark - HTTP Request

-(void)httpChatRecentChatRequest
{
     if (self.getMessageQueue.operations.count==0) {
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"member_id"] = [AppHelper appDelegate].cureentUser.accountId;
 
        NSString *lastId=nil;
        if ([self.allMessageArray count]>0) {
            NSDictionary *lastMessage=[self.allMessageArray lastObject];
            lastId = lastMessage[@"id"];
        }
        else
            lastId=@"0";
        parameter[@"RoomId"]=self.groupDict[@"RoomId"];
        parameter[@"lastId"]=lastId;
        
        NSInvocationOperation *sentMessageOperation=[[NSInvocationOperation alloc]initWithTarget:self selector:@selector(getRecentMessage:) object:parameter];
        [sentMessageQueue addOperation:sentMessageOperation];

    }
}


-(void)getRecentMessage:(NSDictionary*)parameter
{
    
        isSentMessage=NO;
        if([AppHelper appDelegate].checkNetworkReachability)
        {
            
            dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                //if ([bubbleData count]==0) [[AppHelper sharedInstance]showIndicator];
                
                NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,KNewChatRecentMessages];
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    dispatch_async( dispatch_get_main_queue(), ^{
                        
                        isSentMessage=YES;
                        [[AppHelper sharedInstance]hideIndicator];
                        [refreshControl beginRefreshing];
                        
                        if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
                        {
                            if ([responseObject objectForKey:@"ChatHistory"] && [[responseObject objectForKey:@"ChatHistory"] isKindOfClass:[NSArray class]]) {
                                
                                if ([[responseObject objectForKey:@"ChatHistory"] count]>0) {
                                    [self.allMessageArray addObjectsFromArray:[responseObject objectForKey:@"ChatHistory"]];
                                    [self removeDuplicateMessageFromMessageArray];
                                    [self.bubbleTable reloadData];
                                    [self.bubbleTable scrollBubbleViewToBottomAnimated:NO];
                                }
                            }
                        }
                        
                    });
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    dispatch_async( dispatch_get_main_queue(), ^{
                        [[AppHelper sharedInstance]hideIndicator];
                    });
                }];
                
            });
            
        }
        
}

-(void)removeDuplicateMessageFromMessageArray
{
    NSMutableSet *keysSet = [[NSMutableSet alloc] init];
    NSMutableArray *filteredArray = [[NSMutableArray alloc]init];
    
    for (NSDictionary *msg in self.allMessageArray) {
        NSString *key = [NSString stringWithFormat:@"%@",msg[@"id"]];
        if (![keysSet containsObject:key]) {
            [filteredArray addObject:msg];
            [keysSet addObject:key];
        }
    }
    
    if ([self.allMessageArray count]>0) {
        
        [bubbleData removeAllObjects];
        [self.allMessageArray removeAllObjects];
    }
    self.allMessageArray = filteredArray;
    [self reloadMessageArray:self.allMessageArray];
    
}
-(void)httpChatHistoryAndOldMessageRequest
{
    if (isSentMessage) {
        
        isSentMessage=NO;
        if([AppHelper appDelegate].checkNetworkReachability)
        {
            
            dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                if ([bubbleData count]==0) [[AppHelper sharedInstance]showIndicator];
                
                NSMutableDictionary *parameter=[NSMutableDictionary new];
                parameter[@"RoomId"]=self.groupDict[@"RoomId"];
                
                NSString *lastId;
                if ([self.allMessageArray count]>0) {
                    NSDictionary *lastMessage=[self.allMessageArray firstObject];
                    lastId = lastMessage[@"id"];
                }
                else
                    lastId=@"0";
                
                parameter[@"RoomId"]=self.groupDict[@"RoomId"];
                parameter[@"lastId"]=lastId;

                
                NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,KNewChatHistory];
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    
                    dispatch_async( dispatch_get_main_queue(), ^{
                        
                        isSentMessage=YES;
                        [self.loaderView stopAnimating ];
                        [self.loaderViewV setHidden:YES];
                        [[AppHelper sharedInstance]hideIndicator];
                        [refreshControl beginRefreshing];
                        
                        if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
                        {
                            if ([responseObject objectForKey:@"ChatHistory"] && [[responseObject objectForKey:@"ChatHistory"] isKindOfClass:[NSArray class]]) {
                                
                                NSMutableArray *previousChat=[NSMutableArray arrayWithArray:[[responseObject objectForKey:@"ChatHistory"]mutableCopy]];
                                
                                if ([self.allMessageArray count]>0)
                                {
                                    [previousChat addObjectsFromArray:self.allMessageArray];
                                    [bubbleData removeAllObjects];
                                    [self.allMessageArray removeAllObjects];
                                    self.allMessageArray=[NSMutableArray arrayWithArray:previousChat];
                                    [self removeDuplicateMessageFromMessageArray];
                                    [self.bubbleTable reloadData];
                                    [self.bubbleTable scrollBubbleViewToPositionAnimated: NO load:nil];
                                }
                                else{
                                    
                                    [self.allMessageArray addObjectsFromArray:[responseObject objectForKey:@"ChatHistory"]];
                                    if ([self.allMessageArray count] > 0)
                                    {
                                        [self removeDuplicateMessageFromMessageArray];
                                        [self.bubbleTable reloadData];
                                    }
                                    [self.bubbleTable scrollBubbleViewToBottomAnimated:NO];
                                }
                            }
                        }
                        
                        });
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    dispatch_async( dispatch_get_main_queue(), ^{
                        
                        
                        [[AppHelper sharedInstance]hideIndicator];
                    });
                }];
            });
        }
    }
}


-(void)httMessageSent:(NSDictionary *)messageParam{
   
    if (isSentMessage) {
        isSentMessage=NO;
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,KNewChatSet];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:messageParam success:^(AFHTTPRequestOperation *operation, id responseObject) {
            isSentMessage=YES;
            if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            
        }];
    }
    
}


#pragma mark -
- (NSString *) timeStamp {
    return [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970] * 1000];
}

#pragma mark - Chat table Setup
-(void)loadChatTableDelegate{
    
    _bubbleTable.bubbleDataSource = self;
    _bubbleTable.snapInterval = 120;
    _bubbleTable.showAvatars = YES;
  }

-(void)loadMessageHistory
{
    if (isSentMessage) {
        
        if([AppHelper appDelegate].checkNetworkReachability){
            if (self.allMessageArray.count >0) {
               
                [self.loaderViewV setHidden:NO];
                [self.loaderView startAnimating];
                [self.loaderViewV setHidden:NO];
                [self httpChatHistoryAndOldMessageRequest];
            }
        }
        else{
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }
    }
   
}
- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    return [bubbleData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    return [bubbleData objectAtIndex:row];
}

-(void)copiedText:(NSIndexPath *)index
{
    NSDictionary *messageDict=[self.allMessageArray objectAtIndex:index.section];
    if( [AppHelper nullCheck:messageDict[@"message"]])
    {
        [[UIPasteboard generalPasteboard] setString:[AppHelper nullCheck:messageDict[@"message"]]];
    }
}
#pragma mark - Keyboard events

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = _chatParentView.frame;
        frame.origin.y -= kbSize.height;
        _chatParentView.frame = frame;
        
        frame = _bottomView.frame;
        frame.size.height -= kbSize.height;
        _bottomView.frame = frame;
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = _chatParentView.frame;
        //        frame.origin.y += kbSize.height;
        //        _chatParentView.frame = frame;
        frame = _bottomView.frame;
        frame.size.height += kbSize.height;
        _bottomView.frame = frame;
    }];
}


#pragma mark -


-(void)refreshTable {
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
          [refreshControl beginRefreshing];
           [self httpChatHistoryAndOldMessageRequest];
    }
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }

}


#pragma mark - Reload Chat
-(void)reloadMessageArray:(NSArray *)messageArray
{
    
    for (NSDictionary *dict in messageArray) {
        
        int userId= [dict[@"member_id"] intValue];
        int loginUserId = [[AppHelper appDelegate].cureentUser.accountId intValue];
        NSString *message= [AppHelper nullCheck:dict[@"message"]];
        NSString *name= [AppHelper nullCheck:dict[@"name"]];
        double stimeStamp=[dict[@"delivered_time"] doubleValue];
        NSDate *date = [self returnDateFromUnixTimeStampString:stimeStamp];
        
        if (userId==loginUserId) {
           
            NSBubbleData *sayBubble = [NSBubbleData dataWithText:message date:date type:BubbleTypeMine Username:name];
            sayBubble.avatar=nil;
            [bubbleData addObject:sayBubble];
        }
        else
        {
          
            NSBubbleData *otherBubble = [NSBubbleData dataWithText:message date:date type:BubbleTypeSomeoneElse Username:name];
            otherBubble.avatar=nil;
            [bubbleData addObject:otherBubble];
        }

    }
    
    
}

-(NSDate *)returnDateFromUnixTimeStampString:(double )unixTimeStampString
{
     NSDate*date= [NSDate dateWithTimeIntervalSince1970:unixTimeStampString];
     return [self getLocalDate:date];

}

-(NSDate *)getLocalDate:(NSDate *)localDate
{
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:localDate];
    NSDate *gmtDate = [localDate dateByAddingTimeInterval:timeZoneOffset];
    return gmtDate;
}
-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}


-(void)getRecentMessageTimer{
    if (!self.myTimer) {
        self.myTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target: self
                                                      selector: @selector(httpChatRecentChatRequest) userInfo: nil repeats: YES];
    }
}
@end
