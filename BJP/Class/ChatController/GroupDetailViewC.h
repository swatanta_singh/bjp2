//
//  GroupDetailViewC.h
//  BJP
//
//  Created by PDSingh on 10/21/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface GroupDetailViewC : BaseViewController
@property(nonatomic,strong)NSDictionary *groupDict;
@end
