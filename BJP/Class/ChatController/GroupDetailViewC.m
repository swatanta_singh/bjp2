//
//  GroupDetailViewC.m
//  BJP
//
//  Created by PDSingh on 10/21/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "GroupDetailViewC.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "UIImageView+AFNetworking.h"

@interface GroupDetailViewC ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSArray *groupListArray;
@property (weak, nonatomic) IBOutlet UIImageView *groupImage;
@property (weak, nonatomic) IBOutlet UILabel *groupName;
@property (weak, nonatomic) IBOutlet UILabel *groupCountLable;

@end

@implementation GroupDetailViewC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadDataOnView];
    [self httpGroupMembersRequest];
    
    __weak GroupDetailViewC *weekSelf=self;
    [self setUpHeaderWithTitle:@"Group Info" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.navigationController popViewControllerAnimated:YES];

    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -

-(void)loadDataOnView
{
    NSString *strImg=[AppHelper nullCheck:self.groupDict[@"ac_image"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [self.groupImage  setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    
    self.groupName.text=[self.groupDict[@"ac_title"]capitalizedString];
    self.groupCountLable.text=[NSString stringWithFormat:@"%@ members",self.groupDict[@"MemberCount"]];


}

-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}

-(void)httpGroupMembersRequest
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=[NSNumber numberWithInt:1];
        parameter[@"RoomId"]=self.groupDict[@"RoomId"];
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,KNewChatGroupInfo];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([responseObject objectForKey:@"Userlist"] && [[responseObject objectForKey:@"Userlist"] isKindOfClass:[NSArray class]]) {
                    self.groupListArray = [responseObject objectForKey:@"Userlist"];
                }
                
                [self.tableView reloadData];
            };
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}
#pragma mark - Update Table View


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.groupListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCell" forIndexPath:indexPath];
    
     NSDictionary *groupList=[self.groupListArray objectAtIndex:indexPath.row];
    UILabel *lblname = (UILabel*)[cell.contentView viewWithTag:1001];
    lblname.text=[groupList[@"name"]capitalizedString];
    UILabel *lblSublable = (UILabel*)[cell.contentView viewWithTag:1002];
    lblSublable.text=[groupList[@"constituency"]capitalizedString];
    
    UILabel *lblDistrict = (UILabel*)[cell.contentView viewWithTag:1004];
    lblDistrict.text=[groupList[@"district"]capitalizedString];
    UIImageView *imageVStaus = (UIImageView*)[cell.contentView viewWithTag:110];
    if([groupList[@"active_status"] integerValue]==0){
        imageVStaus.backgroundColor=[UIColor grayColor];
    }
    else{
         imageVStaus.backgroundColor=[UIColor greenColor];
    }
    UIImageView *imageV = (UIImageView*)[cell.contentView viewWithTag:1003];
    NSString *strImg=[AppHelper nullCheck:groupList[@"member_photo"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    NSInteger numOfSections = 0;
    if ([self.groupListArray count]>0 )
    {
        numOfSections  = 1;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"No Members Found";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ChatDtlViewController"];
    
    [self.navigationController  pushViewController:objVerifyView animated:YES];
    
}


@end
