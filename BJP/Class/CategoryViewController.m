//
//  CategoryViewController.m 
//  KINCT
//
//  Created by Ishu Gupta on 10/1/15.
//  Copyright © 2015 KINCT. All rights reserved.
//

#import "CategoryViewController.h"
#import "User.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "NewsCategoryVC.h"
#import "HomeeViewController.h"
#import "EventsViewController.h"
#import "UIImageView+AFNetworking.h"
#import "PVoiceVController.h"
#import "NotificationViewController.h"
#import "SettingsAppVController.h"
#import "WriteToUsViewController.h"
#import "ChatViewController.h"
#import "InfoGraphicesVC.h"
#import "MagzineViewC.h"
#import "ChatViewController.h"

@interface CategoryViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeightConstant;
@property (weak, nonatomic) IBOutlet UILabel *lableName;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicView;
@property (nonatomic,retain) NSMutableArray *otherOptionsArrayLocalIzation;
@property (nonatomic,retain) NSMutableArray *otherOptionsArray;

@property (nonatomic,retain) NSString *messageCount;

@end

@implementation CategoryViewController

- (void)viewDidLoad {
    
 
    [super viewDidLoad];
    
    [self UpdateDataOnTable];
    self.isSelecedMenu = NO;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.viewHeightConstant.constant = 140;
    
    if (!self.otherOptionsArray) {
        self.otherOptionsArray = [[NSMutableArray alloc] init];
        self.otherOptionsArrayLocalIzation = [[NSMutableArray alloc] init];
    }
    
    if ([self.otherOptionsArray count]==0) {
        
        NSInteger userType=0;
        if ([AppHelper userDefaultsForKey:USER_TYPE]) {
            userType =  [[AppHelper userDefaultsForKey:USER_TYPE]intValue];
        }
        
        if (userType==2) {
            [self.otherOptionsArray addObject:@"VoterInfo"];
            [self.otherOptionsArrayLocalIzation addObject:@"Voter"];
        }
        if (userType==2)
            [self.otherOptionsArray addObject:@"MyTask"];

        [self.otherOptionsArray addObject:@"News"];
        [self.otherOptionsArray addObject:@"Local Events"];
        [self.otherOptionsArray addObject:@"Infographics"];
        [self.otherOptionsArray addObject:@"Survey"];
        [self.otherOptionsArray addObject:@"Chat"];
        [self.otherOptionsArray addObject:@"Local Issues"];

   
            [self.otherOptionsArray addObject:@"Magazine"];
        
        if (userType==2)
            [self.otherOptionsArrayLocalIzation addObject:@"MyTask"];
        [self.otherOptionsArrayLocalIzation addObject:@"News"];
        [self.otherOptionsArrayLocalIzation addObject:@"Events"];
        [self.otherOptionsArrayLocalIzation addObject:@"Infographics"];
        [self.otherOptionsArrayLocalIzation addObject:@"Survey"];
        [self.otherOptionsArrayLocalIzation addObject:@"Chat"];
        [self.otherOptionsArrayLocalIzation addObject:@"local"];
        
        [self.otherOptionsArrayLocalIzation addObject:@"Magazine"];
        [self.otherOptionsArray addObject:@"Membership"];
        [self.otherOptionsArray addObject:@"Settings"];
        [self.otherOptionsArray addObject:@"Invite Friends"];
        [self.otherOptionsArray addObject:@"Write to us"];
        
        [self.otherOptionsArrayLocalIzation addObject:@"Membership"];
        [self.otherOptionsArrayLocalIzation addObject:@"Settings"];
        [self.otherOptionsArrayLocalIzation addObject:@"Invite"];
        [self.otherOptionsArrayLocalIzation addObject:@"Write"];
        
    }

   }

-(void)showOrganizerMemberMenuSet
{
    NSInteger userType=0;
    if ([AppHelper userDefaultsForKey:USER_TYPE]) {
         userType = [[AppHelper userDefaultsForKey:USER_TYPE]intValue];
    }
   
    if (userType==2) {
        
        if(![self.otherOptionsArray containsObject:@"VoterInfo"]){
            [self.otherOptionsArray insertObject:@"VoterInfo" atIndex:0];
            [self.otherOptionsArrayLocalIzation insertObject:@"Voter" atIndex:0];
        }
    }
    else{
        if([self.otherOptionsArray containsObject:@"VoterInfo"]){
            [self.otherOptionsArray removeObject:@"VoterInfo"];
            [self.otherOptionsArrayLocalIzation removeObject:@"Voter"];
            
        }
    }
    
}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}

-(void)UpdateDataOnTable{
    
    if([AppHelper appDelegate].cureentUser){
      
        if(![self.otherOptionsArray containsObject:@"Logout"]){
            [self.otherOptionsArray addObject:@"Logout"];
            [self.otherOptionsArrayLocalIzation addObject:@"Logout"];
        }
    }
    else{
        if([self.otherOptionsArray containsObject:@"Logout"]){
            [self.otherOptionsArray removeObject:@"Logout"];
            [self.otherOptionsArrayLocalIzation removeObject:@"Logout"];

        }
    }
    
    if([AppHelper appDelegate].cureentUser){
        NSURL *url2 = [NSURL URLWithString:[self normalizePath:[AppHelper appDelegate].cureentUser.normalPhotoUrl]];
        [self.profilePicView setImageWithURL:url2 placeholderImage:[UIImage imageNamed:@"userp"]];
         self.lableName.text = [[NSString stringWithFormat:@"%@",[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.fullname]]capitalizedString];
    }
    else{
          [self.profilePicView setImage:[UIImage imageNamed:@"userp"]];
           self.lableName.text =NSLocalizedString(@"Login", nil);

    }
    [tableView reloadData];
}

-(void)setUpMove{
    
    [self showOrganizerMemberMenuSet];
    [self setUserUnreadCount];
    
    self.lableName.text = [AppHelper appDelegate].cureentUser.fullname;

    self.view.frame=CGRectMake(-[UIScreen mainScreen].bounds.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    [[AppHelper navigationController].view addSubview:self.view];
    
    [self UpdateDataOnTable];
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
     
    }];
}

-(IBAction)swipeGesture:(id)sender{
    
   // NSLog(@"Swipe received.");
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame=CGRectMake(-[UIScreen mainScreen].bounds.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
        self.sourcView.frame=CGRectMake(0, self.sourcView.frame.origin.y, self.sourcView.frame.size.width, self.sourcView.frame.size.height);
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [[AppHelper sharedInstance]removeMenuViewController];
    }];

}
- (IBAction)tapAction:(id)sender {

    
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame=CGRectMake(-[UIScreen mainScreen].bounds.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
        self.sourcView.frame=CGRectMake(0, self.sourcView.frame.origin.y, self.sourcView.frame.size.width, self.sourcView.frame.size.height);
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
          [[AppHelper sharedInstance]removeMenuViewController];
    }];
}

-(IBAction)profileClicked:(id)sender
{
    if ([AppHelper appDelegate].cureentUser.accountId) {
        UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ProfileViewController"];
        [self tapAction:nil];
        [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
    }
    else
    {
        UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LoginViewController"];
        [self tapAction:nil];
        [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
    }
   
}

#pragma mark --------------------------------
#pragma mark UITableViewDataSource ----------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //if(section==1)
        return [self.otherOptionsArray count];
    
    //return [_services count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    NSString *titleText = NSLocalizedString([self.otherOptionsArrayLocalIzation objectAtIndex:indexPath.row], nil);
    
    UITableViewCell *cell = (UITableViewCell *)[tableView1 dequeueReusableCellWithIdentifier:cellIdentifier];

    
    if ([titleText isEqualToString:@"Chat"] ||[titleText isEqualToString:@"वार्तालाप"] ) {
        if(!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }

        [self updateTableViewCell:cell forCount:[self.messageCount intValue]];
        
    }
    else
    {
        if(!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
    }

    
    UIView * selectedBackgroundView = [[UIView alloc] init];
    [selectedBackgroundView setBackgroundColor:[UIColor colorWithRed:235/255.0f green:237.0f/239.0f blue:187.0f/255 alpha:0.3]];
    cell.selectedBackgroundView=selectedBackgroundView;
    
    if ([titleText isEqualToString:@"Party Task"] ||[titleText isEqualToString:@"पार्टी कार्य"] || [titleText isEqualToString:@"Voter Survey"] ||[titleText isEqualToString:@"मतदाता सर्वेक्षण"])
        cell.textLabel.textColor=[UIColor greenColor];
    else
        cell.textLabel.textColor=[UIColor whiteColor];
    [cell.textLabel setFont:[UIFont fontWithName:@"Roboto-Regular" size:18
                             ]];
    cell.textLabel.text=titleText;
    cell.backgroundColor=[UIColor clearColor];
    cell.imageView.image=[UIImage imageNamed:SETTING_ITEM[indexPath.row]];
    
     return cell;
    

   
}



#pragma mark --------------------------------
#pragma mark UITableViewDelegate ----------

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Logout"])
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@" Are you sure you want to logout?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO"];
          
        }
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"HOME"])
        {
            UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LoginViewController"];
                [self tapAction:nil];
            [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
            
        }
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Local Issues"])
        {
            if ([AppHelper appDelegate].cureentUser.accountId) {
                
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"IssueViewController"];
                [self tapAction:nil];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];            }
            else
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please login to use this feature." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
            
        }
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"MyTask"])
        {
            if ([AppHelper appDelegate].cureentUser.accountId) {
                
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"MyTaskViewController"];
                [self tapAction:nil];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];            }
            else
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please login to use this feature." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
          
            
        }
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"VoterInfo"])
        {
            if ([AppHelper appDelegate].cureentUser.accountId) {
                
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Voter" WithName:@"VoterInfoVController"];
                [self tapAction:nil];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];            }
            else
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please login to use this feature." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
            
        }

        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Membership"])
        {
            if([AppHelper appDelegate].cureentUser){
                      UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"MemberViewController"];
            [self tapAction:nil];
            [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
            }
            else{
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LoginViewController"];
                [self tapAction:nil];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
            }
        }
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Settings"])
        {
            UIViewController *viControlroller=nil;
            NSArray *allViewControllers = [[AppHelper navigationController] viewControllers];
            for (id aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[SettingsAppVController class]]) {
                    viControlroller=aViewController;
                }
            }
            
            if (!viControlroller) {
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Settings" WithName:@"SettingsAppVController"];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
            }
            else
            {
                [[AppHelper navigationController] popToViewController:viControlroller  animated:YES];
            }

            [self tapAction:nil];
            
        }
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Category News"])
        {
            UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"NewsCategoryVC"];
            [self tapAction:nil];
            [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
            
        }
    
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Write to us"])
        {
            
            if ([AppHelper appDelegate].cureentUser.accountId) {
              
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"WriteToUsViewController"];
                [self tapAction:nil];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
            }
            else
            {
               [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please login to use this feature." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        }
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"News"])
        {
            UIViewController *viControlroller=nil;
            NSArray *allViewControllers = [[AppHelper navigationController] viewControllers];
            for (id aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[HomeeViewController class]]) {
                    viControlroller=aViewController;
                }
            }
            
            if (!viControlroller) {
                
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"HomeeViewController"];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
            }
            else
            {
                [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
            }

         [self tapAction:nil];

        }
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Local Events"])
        {
            UIViewController *viControlroller=nil;
            NSArray *allViewControllers = [[AppHelper navigationController] viewControllers];
            for (id aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[EventsViewController class]]) {
                    viControlroller=aViewController;
                }
            }
            
            if (!viControlroller) {
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"EventsViewController"];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
            }
            else
            {
                [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
            }
            
            [self tapAction:nil];
        }
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Survey"])
        {
            UIViewController *viControlroller=nil;
            NSArray *allViewControllers = [[AppHelper navigationController] viewControllers];
            for (id aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[PVoiceVController class]]) {
                    viControlroller=aViewController;
                }
            }
            
            if (!viControlroller) {
                
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"PVoiceVController"];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
            }
            else
                [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
            [self tapAction:nil];
            
        }
    
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Infographics"])
        {
            UIViewController *viControlroller=nil;
            NSArray *allViewControllers = [[AppHelper navigationController] viewControllers];
            for (id aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[InfoGraphicesVC class]]) {
                    viControlroller=aViewController;
                }
            }
            
            if (!viControlroller) {
                
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"InfoGraphicesVC"];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
            }
            else
                [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
            [self tapAction:nil];
            
        }
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Magazine"])
        {
            UIViewController *viControlroller=nil;
            NSArray *allViewControllers = [[AppHelper navigationController] viewControllers];
            for (id aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[MagzineViewC class]]) {
                    viControlroller=aViewController;
                }
            }
            
            if (!viControlroller) {
                
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"MagzineViewC"];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
            }
            else
                [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
            [self tapAction:nil];
        }

    

        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Notifications"])
        {
            UIViewController *viControlroller=nil;
            NSArray *allViewControllers = [[AppHelper navigationController] viewControllers];
            for (id aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[NotificationViewController class]]) {
                    viControlroller=aViewController;
                }
            }
            
            if (!viControlroller) {
                
                UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"NotificationViewController"];
                [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
            }
            else
                [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
                [self tapAction:nil];
            
        }
        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Chat"])
        {
            if ([AppHelper appDelegate].cureentUser.accountId) {
                
                UIViewController *viControlroller=nil;
                NSArray *allViewControllers = [[AppHelper navigationController] viewControllers];
                for (id aViewController in allViewControllers) {
                    if ([aViewController isKindOfClass:[ChatViewController class]]) {
                        viControlroller=aViewController;
                    }
                }
                
                if (!viControlroller) {
                    
                    
                    UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ChatViewController"];
                    [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
                }
                else
                    [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
                [self tapAction:nil];

            }
            else
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please login to use this feature." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
            
        }


        else   if([[self.otherOptionsArray objectAtIndex:indexPath.row] isEqualToString:@"Invite Friends"])
      {
          NSString * message = @"UP BJP\n";
          UIImage *shareImage = [UIImage imageNamed:@"Kamala"];
          
          NSArray *excludeActivities = @[
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo];
          
          NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/app/up-bjp/id1148583016?ls=1&mt=8"];
          NSArray * shareItems = @[shareImage, message, url];
          UIActivityViewController * activityViewController = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
          activityViewController.excludedActivityTypes = excludeActivities;
          [[AppHelper navigationController] presentViewController:activityViewController animated:YES completion:nil];
          [self tapAction:nil];
    }
}


#pragma mark --------------------------------
#pragma mark UIAlertViewDelegate ------------
- (void)updateTableViewCell:(UITableViewCell *)cell forCount:(NSUInteger)count
{
    // Count > 0, show count
    if (count > 0) {
        // Create label
        CGFloat fontSize = 12;
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:fontSize];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor colorWithRed:250.0f/255.0f green:129.0f/255.0f blue:0 alpha:1.0];
        label.backgroundColor = [UIColor whiteColor];
        
        // Add count to label and size to fit
        label.text = [NSString stringWithFormat:@"%@", @(count)];
        [label sizeToFit];
        
        // Adjust frame to be square for single digits or elliptical for numbers > 9
        CGRect frame = label.frame;
        frame.size.height += (int)(0.4*fontSize);
        frame.size.width = (count <= 9) ? frame.size.height : frame.size.width + (int)fontSize;
        label.frame = frame;
        
        // Set radius and clip to bounds
        label.layer.cornerRadius = frame.size.height/2.0;
        label.clipsToBounds = true;
        
        // Show label in accessory view and remove disclosure
        cell.accessoryView = label;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
     else {
         cell.accessoryView = nil;
     }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0)
    {
        //[self.otherOptionsArray removeObject:@"LOGOUT"];
        [AppHelper appDelegate].cureentUser=nil;
        [AppHelper saveToUserDefaults:nil withKey:USER_ID];
        [AppHelper saveToUserDefaults:nil withKey:USER_TYPE];
        //[AppHelper saveToUserDefaults:nil withKey:USER_NAME];
        [AppHelper saveToUserDefaults:nil withKey:ACCESS_TOKEN];
        [self.view removeFromSuperview];
        [[AppHelper navigationController] popToRootViewControllerAnimated:YES];
        
        if([AppHelper appDelegate].checkNetworkReachability)
        {
            NSMutableDictionary *parameter=[NSMutableDictionary new];
            parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
            parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
            NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERSNewLogout];
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer = [AFJSONResponseSerializer serializer];
            [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      
                  }];
        }
    }
}
#pragma mark laout
-(void)logoutUser{
  
}

-(void)shareContent{
    
    NSString * message = @"UP BJP";
    UIImage *shareImage = [UIImage imageNamed:@"Kamala"];
    NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/app/up-bjp/id1148583016?ls=1&mt=8"];
    NSArray * shareItems = @[message, shareImage, url];
    UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    [self presentViewController:avc animated:YES completion:nil];
    
}


-(void)setUserUnreadCount
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"memberId"]=[AppHelper appDelegate].cureentUser.accountId;
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERSMESSAGECOUNT];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
         // NSLog(@"%@",responseObject);
          if (responseObject && [[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
               self.messageCount = [responseObject objectForKey:@"count"];
            }
            [tableView reloadData];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        }];
        
    }
    
}

@end
