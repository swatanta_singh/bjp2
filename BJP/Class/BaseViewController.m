//
//  BaseViewController.m
//  KINCT
//
//  Created by Ashish on 02/03/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import "BaseViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
@interface BaseViewController ()

@end
// 1 for left
// 2 for right
@implementation BaseViewController

- (void)viewDidLoad {
   
    [super viewDidLoad];
    self.headerView= (HeaderViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"HeaderViewController"];
    
    self.headerView.view.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 69);
    [self.view addSubview:self.headerView.view];
   
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpHeaderWithTitle:(NSString*)title withLeftbtn:(NSString*)leftimage withRigthbtn:(NSString*)Rightimage WithComilation:(CompilationHeader)navigatevalue{
  
       [self.headerView.btnLeft addTarget:self action:@selector(leftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView.btnRight addTarget:self action:@selector(rightButtonAction) forControlEvents:UIControlEventTouchUpInside];
   ;

    self.headerView.btnRight.hidden=YES;
    self.headerView.btnLeft.hidden=YES;
    
    self.navigatevalue=navigatevalue;
    self.headerView.lblTitle.text=[AppHelper nullCheck:title];
    [self.headerView.lblTitle setFont:[UIFont fontWithName:@"Roboto-Medium" size:18]];
  
    if(leftimage){
        [self.headerView.btnLeft setImage:[UIImage imageNamed:leftimage] forState:UIControlStateNormal];
        self.headerView.btnLeft.hidden=NO;
    }
    if(Rightimage){
     
            [self.headerView.btnRight setImage:[UIImage imageNamed:Rightimage] forState:UIControlStateNormal];
            self.headerView.btnRight.hidden=NO;
        
    }
    
}
-(void)leftButtonAction{
    self.navigatevalue(1);
}
-(void)rightButtonAction{
     self.navigatevalue(2);
}

-(void)rightSkipButtonAction{
    self.navigatevalue(3);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
