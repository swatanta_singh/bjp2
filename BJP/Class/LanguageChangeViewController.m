//
//  LanguageChangeViewController.m
//  BJP
//
//  Created by toyaj on 8/31/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "LanguageChangeViewController.h"
#import "AppHelper.h"
#import "PresentionHandler.h"
#import "DataPickerViewController.h"
#import "Defines.h"
#import "HomeeViewController.h"

#import <objc/runtime.h>

@interface CustomizedBundle : NSBundle
@end

@implementation CustomizedBundle
static const char kAssociatedLanguageBundle = 0;

-(NSString*)localizedStringForKey:(NSString *)key
                            value:(NSString *)value
                            table:(NSString *)tableName {
    
    NSBundle* bundle=objc_getAssociatedObject(self, &kAssociatedLanguageBundle);
    
    return bundle ? [bundle localizedStringForKey:key value:value table:tableName] :
    [super localizedStringForKey:key value:value table:tableName];
}

@end

@implementation NSBundle (Custom)

+ (void)setLanguage:(NSString*)language {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        object_setClass([NSBundle mainBundle], [CustomizedBundle class]);
    });
    
    objc_setAssociatedObject([NSBundle mainBundle], &kAssociatedLanguageBundle, language ?
                             [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:language ofType:@"lproj"]] : nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end


@interface LanguageChangeViewController ()
@property (strong, nonatomic) PresentionHandler *presentHandler;
@property (weak, nonatomic) IBOutlet UILabel *lblLaunge;
- (IBAction)submitButtonClick:(id)sender;
- (IBAction)dropdownClick:(id)sender;

@end

@implementation LanguageChangeViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if([[AppHelper userDefaultsForKey:APP_LANGAUGE] isEqualToString:@"hi"]){
        self.lblLaunge.text=@"Hindi";
    }
    else{
         self.lblLaunge.text=@"English";
    }
   
 
}




-(void)initialiseTheListViewWithDataSource:(NSArray *)dataSource withField:(UILabel*)txtField selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    [self.view endEditing:YES];
    // Initialise DataPickerViewController object with datasorce, selected datasource , isSigles selection criteria ,and selected array call back block -- >
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];
    
    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            txtField.text=[NSString stringWithFormat:@"%@",[selectedvalues firstObject]];
        }
    }];
    
    self.presentHandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:CGSizeMake(100, 150)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)submitButtonClick:(id)sender {
    
    if([self.lblLaunge.text isEqualToString:@"Hindi"]){
      [AppHelper saveToUserDefaults:@"hi" withKey:APP_LANGAUGE];
        [[NSUserDefaults standardUserDefaults] setObject:@"hi" forKey:APP_LANGAUGE];
    }
    else{
           [AppHelper saveToUserDefaults:@"en" withKey:APP_LANGAUGE];
        [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:APP_LANGAUGE];
    }
  
    [[NSUserDefaults standardUserDefaults] synchronize];
    [NSBundle setLanguage:[AppHelper userDefaultsForKey:APP_LANGAUGE]];
    [self dismissViewControllerAnimated:YES completion:nil];
  //  [self resetScreens];
    

    AppDelegate *object=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [object.loginObjct setlanguage  ];
    

}

-(void)resetScreens
{
    
    UIViewController *viControlroller=nil;
    NSArray *allViewControllers = [[AppHelper navigationController] viewControllers];
    for (id aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[HomeeViewController class]]) {
            viControlroller=aViewController;
        }
    }
    
    if (!viControlroller) {
        UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"HomeeViewController"];
        [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
    }
    else
    {
        [[AppHelper navigationController] popToViewController:viControlroller  animated:NO];
    }
    

}
+(void)setLangaugeUtils
{
     [NSBundle setLanguage:[AppHelper userDefaultsForKey:APP_LANGAUGE]];
}
- (IBAction)dropdownClick:(id)sender {
    NSArray *selectedArr = @[@"Hindi",@"English"];
        [self initialiseTheListViewWithDataSource:selectedArr withField:self.lblLaunge
                                    selectedArray:nil
                                  selectionChoice:YES
                                       senderRect:sender SelectionType:SEL_NONE];
}
@end
