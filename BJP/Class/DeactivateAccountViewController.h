//
//  DeactivateAccountViewController.h
//  BJP
//
//  Created by toyaj on 9/1/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface DeactivateAccountViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *text_Password;
- (IBAction)deactivation_Action:(id)sender;

@end
