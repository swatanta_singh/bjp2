//
//  SituationVoterViewController.m
//  BJP
//
//  Created by toyaj on 12/14/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "SituationVoterViewController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "DataPickerViewController.h"
#import "PresentionHandler.h"


@interface SituationVoterViewController ()
{
   
    NSMutableDictionary *nameAddress;
}
@property(nonatomic,strong)PresentionHandler *presnthandler;
@end

@implementation SituationVoterViewController
@synthesize arrData=arrData;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrData=[NSMutableArray new];
    self.dictData=[NSMutableDictionary new];
   // [self setUpData];
}
-(void)viewWillAppear:(BOOL)animated
{
}
-(void)setUpData{
    //  1 for button , 0 for text field
    for (int i=0; i<5; i++) {
        NSMutableDictionary *dict=[NSMutableDictionary new];
        switch (i) {
            case 0:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Economic Class ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                dict[@"id"]=@"";
            }
                break;
            case 1:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Religious ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                 dict[@"id"]=@"";
            }
                break;
            case 2:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Social Status ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                 dict[@"id"]=@"";
            }
                break;
            case 3:{
                
                dict[@"value"]=@"";
                dict[@"place"]=@"Social Activity ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                 dict[@"id"]=@"";
            }
                break;
            case 4:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Political view  ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                 dict[@"id"]=@"";
            }
                break;
                
            default:
                break;
        }
        [arrData addObject:dict];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrData.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *  myCell ;
   
    myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dict =[arrData objectAtIndex:indexPath.row];
    UILabel *lblName=(UILabel *)[myCell.contentView viewWithTag:1001];
    UITextField *txtFild=(UITextField *)[myCell.contentView viewWithTag:1002];
    UIButton *btnAction =(UIButton*)[myCell.contentView viewWithTag:1003];
    
    btnAction.hidden=YES;
   
    if ([dict[@"type"] isEqualToString:@"1"]) {
        btnAction.hidden=NO;
    }
   
    [btnAction addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];

    txtFild.enabled=NO;
    txtFild.text=[NSString stringWithFormat:@"%@",dict[@"value"]];

    lblName.text=dict[@"place"];
    myCell.tag=indexPath.row;
    return myCell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict =[arrData objectAtIndex:indexPath.row];
    
    if([dict[@"part"] isEqualToString:@"1"]){
        return CGSizeMake(SCREEN_WIDTH/2-10, 70);
    }
    else{
        CGSize size = CGSizeMake(SCREEN_WIDTH-5,70);
        return size;
    }
}


-(void)btnAction:(UIButton*)btn{
    
    UICollectionViewCell *cel=(UICollectionViewCell*)[btn.superview.superview superview];
    
    switch (cel.tag) {
        case 0:
            [self pickerForEconomicsClass:cel withsender:btn];
            break;
        case 1:
            [self pickerForReligious:cel withsender:btn];
            
            break;
        case 2:
            [self pickerForSocialStatus:cel withsender:btn];
            
            
            break;
        case 3:
            [self pickerForSocialActivity:cel withsender:btn];
            
            break;
            
        case 4:
            [self pickerForPoliticalView:cel withsender:btn];
            
            break;
            
        default:
            break;
    }
    
    
}

-(void)pickerForEconomicsClass:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
     if([[nameAddress valueForKey:@"economic_class"] count]>0)
     {
    [self pickerDataSource:nameAddress[@"economic_class"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_ECONOMIC];
     }
   
}

-(void)pickerForReligious:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    if([[nameAddress valueForKey:@"relegious"] count]>0)
    {
        
        [self pickerDataSource:nameAddress[@"relegious"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_RELIGIOUS];

    }

  }
-(void)pickerForSocialStatus:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    if([[nameAddress valueForKey:@"social_status"] count]>0)
    {
        
        [self pickerDataSource:nameAddress[@"social_status"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_SOCIAL_STATUS];
    }
   
}
-(void)pickerForSocialActivity:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    if([[nameAddress valueForKey:@"social_active"] count]>0)
    {
        [self pickerDataSource:nameAddress[@"social_active"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_SOCIAL_ACTIVITY];

    }
  }

-(void)pickerForPoliticalView:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    if([[nameAddress valueForKey:@"political_view"] count]>0)
    {
        [self pickerDataSource:nameAddress[@"political_view"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_POLITICAL];
    }
  
}

-(void)pickerDataSource:(NSArray *)dataSource withField:(UICollectionViewCell*)myCell selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    
    [self.view endEditing:YES];
    __weak SituationVoterViewController *weekSelf=self;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];
    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            
            if(type==SEL_ECONOMIC){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"economicclass"]];
                dir[@"id"]=[AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"id"]];
                [weekSelf.collectionView reloadData];
            }
            else  if(type==SEL_RELIGIOUS){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"relegious"]];
                dir[@"id"]=[AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"id"]];
                [weekSelf.collectionView reloadData];
            }
            else  if(type==SEL_SOCIAL_STATUS){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"socialstatus"]];
                dir[@"id"]=[AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"id"]];
                [weekSelf.collectionView reloadData];
            }
            else  if(type==SEL_SOCIAL_ACTIVITY){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"socialactive"]];
                dir[@"id"]=[AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"id"]];
                [weekSelf.collectionView reloadData];
            }
            else  if(type==SEL_POLITICAL){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"politicalview"]];
                dir[@"id"]=[AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"id"]];
                [weekSelf.collectionView reloadData];
            }
            else{
            
//            NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
//            NSString *value=[selectedvalues firstObject];
//            dir[@"value"] = value;
//            [weekSelf.collectionView reloadData];
            }
        }
    }];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];
}




-(NSMutableDictionary*)situationData{
    //    self.dictData[@"year"]=self.textField_year.text;
    //    self.dictData[@"month"]=self.textField_Month.text;
    //    self.dictData[@"rate"]=self.textField_hourly_Rate.text;
    
    return self.dictData;
}
-(void)setUpdataForStringtype:(NSString *)typeString keytype:(NSString*)keytype withField:(NSInteger)myCell{
    
    NSMutableDictionary *dir=[arrData objectAtIndex:myCell];
    if ([nameAddress valueForKey:typeString]>0) {
        NSArray *arrId = [nameAddress valueForKey:typeString];
        if (arrId.count>0) {
            
            NSPredicate *prd=[NSPredicate predicateWithFormat:@"selected == %d",1];
            NSArray *Arr=[arrId filteredArrayUsingPredicate:prd];
            if (Arr.count>0) {
                NSString *value=[[Arr valueForKey:keytype] lastObject];
                dir[@"value"] = value;
                dir[@"id"]=[[Arr valueForKey:@"id"] lastObject];

            }
            else
                dir[@"value"] = @"Select";
        }
        else
            dir[@"value"] = @"NA";
    }
    else
        dir[@"value"] = @"NA";
   
   
    
}
-(void)displayViewSetMEthod
{
    nameAddress = self.dicdataRecived;
     [self setUpData];
     [self setUpdataForStringtype:@"economic_class" keytype:@"economicclass" withField:0];
     [self setUpdataForStringtype:@"relegious" keytype:@"relegious" withField:1];
     [self setUpdataForStringtype:@"social_status" keytype:@"socialstatus" withField:2];
     [self setUpdataForStringtype:@"social_active" keytype:@"socialactive" withField:3];
     [self setUpdataForStringtype:@"political_view" keytype:@"politicalview" withField:4];
     [self.collectionView reloadData];
    
}

@end
