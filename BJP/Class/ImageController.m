//
//  ViewController.m
//  ScrollViews
//
//  Created by Matt Galloway on 29/02/2012.
//  Copyright (c) 2012 Swipe Stack Ltd. All rights reserved.
//

#import "ImageController.h"
@interface ImageController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer;
- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer;
@end

@implementation ImageController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.scrollView.minimumZoomScale = 1.0f;
    self.scrollView.maximumZoomScale = 3.0f;
    // Set a nice title
    self.title = @"Image";
    if (self.image) {
       self.imageView.image=self.image;
    }
    else{
  //  self.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_path]]];
    }
   
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    __weak ImageController *weekSelf=self;
      [self.headerView.headerImageView setImage:[UIImage imageNamed:@"RectangleFlip"]];
    [self setUpHeaderWithTitle:@"Preview" withLeftbtn:@"back" withRigthbtn:@"ShareImg" WithComilation:^(int navigateValue) {
        if(navigateValue==1){
        [  weekSelf dismissViewControllerAnimated:YES completion:nil];
        }
        else{
            if(self.dictImage!=nil){
            [weekSelf shareAction:weekSelf.dictImage];
            }
        }
    
 
    }];
   }

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    // Return the view that we want to zoom
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
  
}
#pragma mark - Reload News
-(void)shareAction:(NSDictionary *)dict {
        NSString * message = [[AppHelper nullCheck:dict[@"title"]]capitalizedString];;
    UIImage *shareImage =self.image;
    
    NSArray *excludeActivities = @[
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    //   activityVC.excludedActivityTypes = excludeActivities;
    // NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/app/up-bjp/id1148583016?ls=1&mt=8"];
    NSArray * shareItems = @[message, shareImage];
    UIActivityViewController * activityViewController = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    activityViewController.excludedActivityTypes = excludeActivities;
    __weak __typeof(self)weakSelf = self;
    
    [self presentViewController:activityViewController animated:YES completion:nil];
    [activityViewController setCompletionWithItemsHandler:
     ^(NSString *activityType, BOOL completed, NSArray* returnedItems, NSError *activityError) {
         if(completed){
             [weakSelf updateShareCount:dict[@"id"]];
         }
         
     }];
    
}
-(void)updateShareCount:(NSString*)strId{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=@"1";
        parameter[@"id"]=strId;
        // parameter[@"itemId"]=@"";
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kINFOGRAFICS_SHARE_COUNT];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Share Successfully" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Some error found." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}


@end
