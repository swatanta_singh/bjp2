//
//  CopyrightInfoViewController.m
//  BJP
//
//  Created by toyaj on 9/1/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "CopyrightInfoViewController.h"

@interface CopyrightInfoViewController ()

@end

@implementation CopyrightInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    __weak CopyrightInfoViewController *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"Copyright", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
            
        }
        else{
            [weekSelf.view endEditing:YES];
            
        }
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
