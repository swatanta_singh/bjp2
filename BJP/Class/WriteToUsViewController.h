//
//  WriteToUsViewController.h
//  BJP
//
//  Created by toyaj on 8/31/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface WriteToUsViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextView *text_content;
@property (weak, nonatomic) IBOutlet UITextField *text_email;
@property (weak, nonatomic) IBOutlet UITextField *text_department;
@property (weak, nonatomic) IBOutlet UITextField *text_subject;
- (IBAction)submit_Action:(id)sender;
- (IBAction)departmentListAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)Reply_List:(id)sender;

@end
