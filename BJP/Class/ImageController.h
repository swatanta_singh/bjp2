//
//  ViewController.h
//  ScrollViews
//
//  Created by Matt Galloway on 29/02/2012.
//  Copyright (c) 2012 Swipe Stack Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"

@interface ImageController : BaseViewController <UIScrollViewDelegate>
{
}

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, weak) NSString *pathtImg;
@property (nonatomic, weak) NSDictionary *dictImage;



@end
