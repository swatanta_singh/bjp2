//
//  CategoryViewController.h
//  KINCT
//
//  Created by Ishu Gupta on 10/1/15.
//  Copyright © 2015 KINCT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryViewController : UIViewController
{
    __weak IBOutlet UIButton *profileImageButton;
    __weak IBOutlet UILabel *emailLable;
    __weak IBOutlet UITableView *tableView;
}

@property(nonatomic)BOOL  isSelecedMenu;
@property (nonatomic,retain) NSMutableArray *primaryArray;
@property (nonatomic,retain) UIView *sourcView;

-(IBAction)tapAction:(id)sender;
-(void)setUpMove;
-(IBAction)profileClicked:(id)sender;
-(void)UpdateDataOnTable;
@end
