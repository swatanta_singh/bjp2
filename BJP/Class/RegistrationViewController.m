
#import "RegistrationViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "PresentionHandler.h"
#import "HomeeViewController.h"
#import "VerifyViewController.h"

@interface RegistrationViewController ()
{
    NSUInteger selectIndex;
}
@property (strong, nonatomic) PresentionHandler *presentHandler;

@end


@implementation RegistrationViewController

#pragma mark view cycle

- (void)viewDidLoad {
    
    
    [super viewDidLoad];

    numberformatter = [[ECPhoneNumberFormatter alloc] init];
    arrAccountsModelData=[NSMutableArray new];
   
    [self setUpData];
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    numberToolbar.items = @[flexibleItem,[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor darkGrayColor];
    [numberToolbar setBackgroundColor:[UIColor lightGrayColor]];
    self.tblView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    [self registerForKeyboardNotifications];
   
    
    float cellHgt;
    if(IS_IPHONE_4_OR_LESS){
        cellHgt= 40;
    }
   else if(IS_IPHONE_5){
        _constrantHeight.constant=20;
        cellHgt= 50;

    }
    else{
        _constrantHeight.constant=-15;
        cellHgt= 50;
    }
    float height=[[UIScreen mainScreen] bounds].size.height-100;
     self.tblHeightConstant.constant=MIN(height, cellHgt*arrAccountsModelData.count);
    }
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}
-(void)viewWillAppear:(BOOL)animated {
   
    //NSLocalizedString(@"login_message", nil)
    [super viewWillAppear:YES];
        self.automaticallyAdjustsScrollViewInsets = false;
    __weak RegistrationViewController *weekSelf=self;
    
    [self setUpHeaderWithTitle:NSLocalizedString(@"signup", nil) withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.btn_signup setTitle:NSLocalizedString(@"signup", nil) forState:UIControlStateNormal];
   
}
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
    
}
#pragma mark profile Data Source


-(void)setUpData{
    
    [arrAccountsModelData removeAllObjects];
      if (self.fbId || self.dictGoogle) {
          for (int i=0; i<2; i++) {
              NSMutableDictionary *dict=[NSMutableDictionary new];
              switch (i) {
                      break;
                  case 0:{
                      dict[@"value"]=self.fulname;//signup_fullname
                      dict[@"place"]=NSLocalizedString(@"signup_fullname", nil);
                  }
                      break;
                  case 1:{
                      dict[@"value"]=@"";
                      dict[@"place"]=NSLocalizedString(@"login_mobilenumber", nil);
                  }
                      break;
                      
                  default:
                      break;
              }
              [arrAccountsModelData addObject:dict];
          }

      }
      else{
          for (int i=0; i<4; i++) {
              NSMutableDictionary *dict=[NSMutableDictionary new];
              switch (i) {
                      break;
                  case 0:{
                      dict[@"value"]=@"";
                      dict[@"place"]=NSLocalizedString(@"signup_fullname", nil);
                  }
                      break;
                  case 1:{
                      dict[@"value"]=@"";
                      dict[@"place"]=NSLocalizedString(@"login_mobilenumber", nil);
                  }//signup_confirmPassword
                      break;
                  case 2:{
                      dict[@"value"]=@"";
                      dict[@"place"]=NSLocalizedString(@"signup_placeholderpas", nil);
                  }
                      break;
                  case 3:{
                      dict[@"value"]=@"";
                      dict[@"place"]=NSLocalizedString(@"signup_confirmPassword", nil);
                  }
                      
                      
                  default:
                      break;
              }
              [arrAccountsModelData addObject:dict];
          }

      }
   }
-( BOOL )validateUserInputs
{
    NSString *errorMessage=nil;
    bool IsValid=YES;
    
    for (short  i = 0; i <[arrAccountsModelData count]; i++ )
    {
        
        NSDictionary *dict=[arrAccountsModelData objectAtIndex:i];
        
        if ([dict[@"value"] length] == 0)
        {
            //please_enter
            errorMessage = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"please_enter", nil),[dict[@"place"] lowercaseString]];
            [AppHelper showAlertViewWithTag:i+1 title:APP_NAME message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            errorMessage=@"";
            IsValid= NO;
            break;
            
        }

        else if([dict[@"place"] isEqualToString:NSLocalizedString(@"login_mobilenumber", nil)])
        {
            if ([dict[@"value"] length]!=14)
            {
                
                errorMessage=@"Mobile number can't be less than 10 digits";
                [AppHelper showAlertViewWithTag:5 title:APP_NAME message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                errorMessage=@"";
                IsValid= NO;
                break;
            }

            
        }
        
    else if([dict[@"place"] isEqualToString:NSLocalizedString(@"signup_placeholderpas", nil)] )
        {
            NSDictionary *dict1=[arrAccountsModelData objectAtIndex:i+1];
            
            if ([dict[@"value"] length]< 8)
            {
                
                errorMessage=@"Password must be minimum 8 characters";
                [AppHelper showAlertViewWithTag:5 title:APP_NAME message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                errorMessage=@"";
                IsValid= NO;
                break;
            }
    else  if (![dict1[@"value"] isEqualToString:dict[@"value"]]) {
                
                errorMessage=@"Confirm password doesn't match with password";
                [AppHelper showAlertViewWithTag:5 title:APP_NAME message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                errorMessage=@"";
                IsValid= NO;
                break;
            }
               }
        
    }
    
   
    return IsValid;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UITableViewCell *nextCell=[self.tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:alertView.tag -1 inSection:0]];
    
    if(nextCell){
        
        UITextField *txt=(UITextField*)[nextCell.contentView viewWithTag:101];
        [txt becomeFirstResponder];
        
    }
    
}


- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0);
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
}




- (void)registerForKeyboardNotifications

{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
#pragma mark table View
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrAccountsModelData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(IS_IPHONE_4_OR_LESS){
        return 40;
    }
    else{
        return 50;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    NSDictionary *dict =[arrAccountsModelData objectAtIndex:indexPath.row];
    UITableViewCell *myCell=nil;
    myCell =[tableView dequeueReusableCellWithIdentifier:@"TextCell" forIndexPath:indexPath];
    myCell.tag=indexPath.row;
    myCell.contentView.tag=indexPath.section;
    UITextField *txtFild=(UITextField*)[myCell.contentView viewWithTag:101];
    
    txtFild.secureTextEntry=NO;
    txtFild.delegate = self;
    txtFild.autocapitalizationType=UITextAutocapitalizationTypeNone;
    txtFild.keyboardType=UIKeyboardTypeDefault;
    txtFild.text=dict[@"value"];
    txtFild.placeholder=dict[@"place"];
   
    
    if(self.fbId){
        txtFild.enabled=NO;
    }
    if (indexPath.row==1) {
        txtFild.keyboardType=UIKeyboardTypePhonePad;
        txtFild.inputAccessoryView = numberToolbar;
        txtFild.enabled=YES;
        txtFild.text = [numberformatter stringForObjectValue:dict[@"value"]];

    }
    else if (indexPath.row==2) {
        txtFild.secureTextEntry=YES;  
    }
    else if (indexPath.row==3) {
        txtFild.secureTextEntry=YES;
    }

    else{
        txtFild.autocapitalizationType=UITextAutocapitalizationTypeWords;
    }
    
    txtFild.returnKeyType=UIReturnKeyNext;

    return myCell;
}
#pragma mark textfieald
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField.tag!=102) {
        [AppHelper setupViewAtUp:0 toview:self.view];
        UITableViewCell *cell=(UITableViewCell*)[textField.superview superview];
        NSMutableDictionary *dict=[arrAccountsModelData objectAtIndex:cell.tag];
        dict[@"value"]=textField.text;
    }
  
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    UITableViewCell *cell=(UITableViewCell*)[textField.superview superview];
    NSIndexPath *path=[self.tblView indexPathForCell:cell];
    
    UITableViewCell *nextCell=[self.tblView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:path.row+1 inSection:path.section]];
    
    if(nextCell){
        
        UITextField *txt=(UITextField*)[nextCell.contentView viewWithTag:101];
        [txt becomeFirstResponder];
        
    }
    else{
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    UITableViewCell *cell=(UITableViewCell*)[textField.superview superview];
    
    NSIndexPath *path=[self.tblView indexPathForCell:cell];
    if(path.section==0){
        
        
        if (path.row==1) {
            if (textField.text.length >= 14 && range.length == 0)
            {
                return NO; // return NO to not change text
            }
        }
      //  else if (path.row==1)
//        {
//            if (textField.text.length >= 25 && range.length == 0)
//            {
//                return NO; // return NO to not change text
//            }
//        }
    }
    if (path.row == 1 ) {
        textField.text = [numberformatter stringForObjectValue:textField.text];
    }

    return YES;
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(IS_IPHONE_4_OR_LESS || IS_IPHONE_5)
    {
        UITableViewCell *cell=(UITableViewCell*)[textField.superview superview];
        if(cell.tag==3 ||cell.tag==4||cell.tag==5)
            [AppHelper setupViewAtUp:100 toview:self.view];
    }
    
         //  [self cancelPikcerViewAction:nil];
        return YES;
    
}

-(void)dimissKeyborad
{
    [self.view endEditing:YES];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
  
    if(IS_IPHONE_4_OR_LESS || IS_IPHONE_5)
    {
        UITableViewCell *cell=(UITableViewCell*)[textField.superview superview];
        if(cell.tag==3 ||cell.tag==4||cell.tag==5)
            [AppHelper setupViewAtUp:120 toview:self.view];
    }
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark ---------------------
#pragma mark service
-(IBAction)SignupAction:(id)sender{
   [self.view endEditing:YES];
    if(![self validateUserInputs])
        return;
    
    [self proceedSignUp];
}

-(NSString *)getPhoneNunbervalue:(NSString*)textField
{
    id objectValue;
    NSString *error;
    [numberformatter  getObjectValue:&objectValue forString:textField errorDescription:&error];
    return objectValue;
    // objectValue = @"12345678900"
}
-(void)proceedSignUp{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameters =[NSMutableDictionary new];
       
        
        parameters[@"clientId"]=@"1";
       parameters[@"gcm_regId"]=[AppHelper userDefaultsForKey:Device_Token];
        NSDictionary *dict=[arrAccountsModelData objectAtIndex:0];
        parameters[@"fullname"]=[AppHelper nullCheck:dict[@"value"]];
        dict=[arrAccountsModelData objectAtIndex:1];
        parameters[@"mobile"]=[self getPhoneNunbervalue:[AppHelper nullCheck:dict[@"value"]]];;
        parameters[@"device_type"]=@"iPhone";
        parameters[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        if (self.fbId) {
            parameters[@"facebookId"]=self.fbId;
            parameters[@"password"]=@"12345678";
        }
        else if(self.dictGoogle){
              parameters[@"google_id"]=self.dictGoogle[@"google_id"];
              parameters[@"email_id"]=self.dictGoogle[@"email_id"];
             parameters[@"password"]=@"12345678";
        }
        else{
            dict=[arrAccountsModelData objectAtIndex:2];
            parameters[@"password"]=[AppHelper nullCheck:dict[@"value"]];
 
        }
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kSIGN_UP];

        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
          
            [[AppHelper sharedInstance]hideIndicator];

            NSString *errorMsg=@"";
            if(responseObject==nil)
            {
                errorMsg=@"Server not responding";
           
            }
            else if ([[responseObject valueForKey:@"error_code"] integerValue]==200) // Handle the error.
            {
                   [AppHelper saveToUserDefaults:[responseObject objectForKey:@"accessToken"] withKey:ACCESS_TOKEN];
                NSDictionary *resultDict = [[responseObject objectForKey:@"account"]lastObject];
               // [AppHelper saveToUserDefaults:[AppHelper nullCheck:[responseObject objectForKey:@"accountId"]] withKey:USER_ID];
                 [AppHelper saveToUserDefaults:parameters[@"mobile"]withKey:USER_NAME];
                User *cuser=[[Service sharedEventController] parseUserData:[responseObject objectForKey:@"accountId"] withData:resultDict];
                //[AppHelper appDelegate].cureentUser=cuser;
                [[AppHelper appDelegate] saveContext];
                
                
//                NSArray *allViewControllers = [self.navigationController viewControllers];
//                for (UIViewController *aViewController in allViewControllers) {
//                    if ([aViewController isKindOfClass:[HomeeViewController class]]) {
//                        [self.navigationController popToViewController:aViewController  animated:YES];
//                    }
//                }
              
                
                VerifyViewController* objLanguageView= (VerifyViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"VerifyViewController"];
                objLanguageView.strPhn=parameters[@"mobile"];
                self.presentHandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objLanguageView fromPresentingViewController:self withStyle:UIModalPresentationCustom fromView:nil andPreferedSize:CGSizeMake(SCREEN_WIDTH-50, 280)];
            }
            else{
                 errorMsg=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
              
            }
            if(errorMsg.length>3){
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
         
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [AppHelper showMessageErrorCode:error];
            [[AppHelper sharedInstance]hideIndicator];
        }];
    }
    else{
        
        [[AppHelper sharedInstance]hideIndicator];
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}

-(void) httpPostWithCustomDelegate
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    NSMutableDictionary *parameters =[NSMutableDictionary new];
    parameters[@"clientId"]=@"1";
    parameters[@"gcm_regId"]=@"1234325534";
    parameters[@"mobile"]=@"9910723520";
    parameters[@"password"]=@"pdsingh";
    
    NSURL * url = [NSURL URLWithString:@"http://10.10.12.241/api/v1/method/account.newsignIn.inc"];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    //    NSString * params =@"name=Ravi&loc=India&age=31&submit=true";
    //    [urlRequest setHTTPMethod:@"POST"];
    //    [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    //    [request addValue:@"text/html" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"text/html" forHTTPHeaderField:@"Accept"];
    //
    //    [request setHTTPMethod:@"POST"];
    //
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer]; // if response JSON format
    [manager POST:@"http://10.10.12.241/api/v1/method/account.newsignIn.inc" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
    
    
    
    NSString * params =@"mobile=9910723520&clientId=1&password=pdsingh&gcm_regId=1234325534";
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:request
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                           NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                           if(error == nil)
                                                           {
                                                               NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                                               NSLog(@"Data = %@",text);
                                                           }
                                                           
                                                       }];
    [dataTask resume];
    
}

@end
