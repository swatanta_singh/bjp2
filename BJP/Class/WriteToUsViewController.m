//
//  WriteToUsViewController.m
//  BJP
//
//  Created by toyaj on 8/31/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "WriteToUsViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "PresentionHandler.h"
#import "DataPickerViewController.h"
#import "AFNetworking.h"
#import "LoginViewController.h"
#import "ReplyListViewController.h"
@interface WriteToUsViewController (){
    UIToolbar* numberToolbar;

}

@property (strong, nonatomic) PresentionHandler *presentHandler;
@end

@implementation WriteToUsViewController
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    numberToolbar.items = @[flexibleItem,[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor darkGrayColor];
    [numberToolbar setBackgroundColor:[UIColor lightGrayColor]];
    // Do any additional setup after loading the view.
    self.text_content.inputAccessoryView = numberToolbar;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if([AppHelper appDelegate].cureentUser){
        [self.btnSubmit setTitle:@"Submit" forState:UIControlStateNormal];
    }
    else{
        [self.btnSubmit setTitle:@"Login to Submit" forState:UIControlStateNormal];
    }
    __weak WriteToUsViewController *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"Write", nil);
    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];

        }
        else{
            [weekSelf.view endEditing:YES];
            
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)validValues{
    
    NSString *email=[self.text_email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *subject=[self.text_subject.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
     NSString *content=[self.text_content.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    if ([email length]==0) {
        
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please enter Email" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        return NO;
    }
    else if ([subject length]==0) {
        
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please enter subject" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        return NO;
    }
    else if ([content length]==0) {
        
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please enter Description" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        return NO;
    }
   
   
        NSString *errorMessage=nil;
            NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
            
            if(![emailTest evaluateWithObject:email])
            {
                errorMessage=@"Please enter valid email";
                [AppHelper showAlertViewWithTag:3 title:nil message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                errorMessage=@"";
               return NO;
            }
            
        
        
    

    
    return YES;
}



#pragma mark button action

- (IBAction)submit_Action:(id)sender {
     if([AppHelper appDelegate].cureentUser){
         BOOL isExist= [self validValues];
         if (!isExist)
             return;
         
         [self httpRequestWriteToUS];

     }
     else{
         LoginViewController *expandAndCollaps=(LoginViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LoginViewController"];
         [self.navigationController pushViewController:expandAndCollaps animated:YES];
        
     }
    
}

-(void)httpRequestWriteToUS
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        //clientId
        parameter[@"clientId"]=@"1";
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"type"]=@"1";
        parameter[@"email"]=_text_email.text;
        parameter[@"subject"]=_text_subject.text;
        parameter[@"detail"]=_text_content.text;
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kWriteToUS];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Thank you for sharing feedback." delegate:self cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
               
                
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 103)
            {
                [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Suggestions Limit Over" delegate:self cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                
                
            }
           else{
                  [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==100){
         [[AppHelper navigationController] popViewControllerAnimated:YES];
        
    }
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegext = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegext];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)departmentListAction:(id)sender {
    NSArray *selectedArr = @[@"Which department",@"Technical Team",@"Organization"];
        [self initialiseTheListViewWithDataSource:selectedArr withField:_text_department
                                    selectedArray:nil
                                  selectionChoice:YES
                                       senderRect:sender SelectionType:SEL_NONE];
}

-(void)initialiseTheListViewWithDataSource:(NSArray *)dataSource withField:(UITextField*)txtField selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    [self.view endEditing:YES];
    // Initialise DataPickerViewController object with datasorce, selected datasource , isSigles selection criteria ,and selected array call back block -- >

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];

    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            txtField.text=[NSString stringWithFormat:@"   %@",[selectedvalues firstObject]];
        }
    }];

    self.presentHandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];



}
#pragma mark text fild
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

/*
 
 UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"EventsViewController"];
 [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
 */
- (IBAction)Reply_List:(id)sender {
    UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ReplyListViewController"];
    [[AppHelper navigationController] pushViewController:objVerifyView animated:NO];
}
@end
