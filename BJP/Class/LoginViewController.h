//
//  LoginViewController.h
//  KINCT
//
//  Created by Ashish on 26/02/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageChangeViewController.h"

#import "BaseViewController.h"
#import "PresentionHandler.h"
typedef enum FieldTypeEmailOrPhone
{
    FieldTypeEmail,
    FieldTypePhone,
    FieldTypeNone
} FieldType;
@interface LoginViewController : BaseViewController<UITextFieldDelegate,UITextViewDelegate>{
    UITextField *emailTextField,*passwordTextField;
  
    UIToolbar*   numberToolbar;
    
    UIAlertController *alert;

}

- (IBAction)googlePlus:(id)sender;

- (IBAction)forgotPasswordButton:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthIcon;
@property(nonatomic,strong)NSString *FullName;
@property(nonatomic,strong)  NSString *fbId;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heigtIcon;

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblHeightConstant;
@property(nonatomic,strong)PresentionHandler *presnthandler;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@property (nonatomic, assign) BOOL usingArabic;
@property (weak, nonatomic) IBOutlet UIButton *btn_or;

@property (weak, nonatomic) IBOutlet UILabel *login_message;
@property (weak, nonatomic) IBOutlet UIButton *btn_signUP;
@property (weak, nonatomic) IBOutlet UIButton *btn_ggogle;
@property (weak, nonatomic) IBOutlet UIButton *btn_facebook;

-(IBAction)signInAction:(id)sender;
-(IBAction)forgotAction:(id)sender;

-(void)setlanguage;

@end
