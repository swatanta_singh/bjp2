//
//  SupportViewViewController.h
//  BJP
//
//  Created by toyaj on 12/14/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportViewViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic)  NSMutableDictionary *dictData;
-(NSMutableDictionary*)supportData;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionview;
@property(strong,nonatomic) NSMutableDictionary *dicdataRecived;
@property(strong,nonatomic)NSMutableArray *arrData;
@property (strong, nonatomic) NSString  *serveyId;

-(void)setViewFrameData;
@end
