//
//  BoothListVController.m
//  BJP
//
//  Created by PDSingh on 12/14/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "VoterlistVController.h"
#import "UIImageView+AFNetworking.h"

#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"

@interface VoterlistVController (){
    
    NSUInteger selectIndex;
    UIRefreshControl              *refreshControl;
    NSMutableArray *arrEventList;
    
    NSInteger selected;
    NSString* keyselected;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSArray *boothArrData;
- (IBAction)receiveTabButtonAction:(id)sender;
- (IBAction)sentTabButtonAction:(id)sender;
@property(nonatomic,strong)NSMutableArray *arrData;
@property (weak, nonatomic) IBOutlet UIButton *receiveTabButton;
@property (weak, nonatomic) IBOutlet UIButton *sentTabButton;
@property (strong, nonatomic) NSArray *menuArray;
@property (strong, nonatomic) PresentionHandler *presentHandler;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTopContarints;
@property (weak, nonatomic) IBOutlet UICollectionView *headerMenuCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *containerCollectionView;
@end

@implementation VoterlistVController

- (void)viewDidLoad {
    [super viewDidLoad];
    selectIndex=0;
    
    keyselected=@"received";
    self.menuArray = [NSArray arrayWithObjects:@"All",@"Maid",@"Gardener",@"Tutor",@"Cook",@"Babysitter",@"Coach", nil];
    self.tableView.tableFooterView = [[UIView alloc ]initWithFrame:CGRectZero];
    
    [self initilaize];
    [self selectReciverTab];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initilaize
{
    self.tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
}

-(void)selectReciverTab
{
    [self.receiveTabButton setTitleColor:[UIColor colorWithRed:18.0f/255.0f green:106.0f/255.0f blue:160.0f/255.0f alpha:1] forState:UIControlStateNormal ];
    [self.sentTabButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal ];
    [self.receiveTabButton setSelected:YES];
    [self.sentTabButton setSelected:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self menuModel];
    [super viewWillAppear:animated];
    __weak VoterlistVController *weekSelf=self;
    [self setUpHeaderWithTitle:@"Voter list" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if (navigateValue==1) {
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}


-(void)selectSentTab
{
    [self.sentTabButton setTitleColor:[UIColor colorWithRed:18.0f/255.0f green:106.0f/255.0f blue:160.0f/255.0f alpha:1] forState:UIControlStateNormal ];
    [self.receiveTabButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal ];
    [self.sentTabButton setSelected:YES];
    [self.receiveTabButton setSelected:NO];
}


-(void)menuModel
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERshowBoothUsers];
        NSMutableDictionary *parameters =[NSMutableDictionary new];
        parameters[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameters[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameters[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        parameters[@"boothid"]=@"1612";

        
        if ([self.receiveTabButton isSelected]) {
            
            if (arrEventList.count==0)[[AppHelper sharedInstance]showIndicator];;
            parameters[@"verificationstatus"]=@"0";
            selected=0;
            [self httpRequestGetEventStatus:@"0" url:baseURL param:parameters];
        }
        else
        {
            parameters[@"verificationstatus"]=@"1";
            selected=0;
            [self httpRequestGetEventStatus:@"0" url:baseURL param:parameters];
        }
    }
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)httpRequestGetEventStatus:(NSString *)status url:(NSString *)baseURL param:(NSDictionary *)parameters
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        //clientId
    
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERshowBoothUsers];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]])
                self.boothArrData = [responseObject valueForKey:@"data"];
               
            }
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
             [self.tableView reloadData];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
    }
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}
#pragma mark -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.boothArrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCell" forIndexPath:indexPath];
    
    UIView *view = (UIView*)[cell.contentView viewWithTag:6001];
    
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(1, 1);
    view.layer.shadowRadius = 2;
    view.layer.shadowOpacity = 0.5;
    
    NSDictionary *groupList=[self.boothArrData objectAtIndex:indexPath.row];
    UILabel *lblname = (UILabel*)[cell.contentView viewWithTag:1001];
    lblname.text=[[AppHelper nullCheck:groupList[@"booth"]]capitalizedString];
    
    UILabel *lblSublable = (UILabel*)[cell.contentView viewWithTag:1002];
    lblSublable.text=[NSString stringWithFormat:@"%@ Voters",groupList[@"totalVoter"]];
    UILabel *lastlable = (UILabel*)[cell.contentView viewWithTag:1003];
    lastlable.text=[AppHelper nullCheck:groupList[@"sector"]];
    UILabel *datelable = (UILabel*)[cell.contentView viewWithTag:1004];
    datelable.text=[AppHelper nullCheck:groupList[@"mandal"]];;
    
    UIImageView *imageV = (UIImageView*)[cell.contentView viewWithTag:5002];
    NSString *strImg=[AppHelper nullCheck:groupList[@"ac_image"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    
    //    UIButton *infoButton = (UIButton*)[cell.contentView viewWithTag:5001];
    //    infoButton.tag=indexPath.row;
    //    [infoButton addTarget:self action:@selector(infoButton:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(NSDate *)returnDateFromUnixTimeStampString:(double )unixTimeStampString
{
    NSDate*date= [NSDate dateWithTimeIntervalSince1970:unixTimeStampString];
    return [self getLocalDate:date];
    
}
-(NSDate *)getLocalDate:(NSDate *)localDate
{
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:localDate];
    NSDate *gmtDate = [localDate dateByAddingTimeInterval:timeZoneOffset];
    return gmtDate;
}
-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    NSInteger numOfSections = 0;
    if ([self.boothArrData count]>0 )
    {
        numOfSections  = 1;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"No data found";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
 NSDictionary *groupDict=[self.boothArrData objectAtIndex:indexPath.row];
    
//    ChatDtlViewController* objVerifyView=(ChatDtlViewController*) [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ChatDtlViewController"];
//    objVerifyView.groupDict=groupDict;
//    [self.navigationController  pushViewController:objVerifyView animated:YES];
    
}

#pragma mark - Action
- (IBAction)receiveTabButtonAction:(id)sender {
    
    [self selectReciverTab];
    keyselected=@"received";
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERshowBoothUsers];
        NSMutableDictionary *parameters =[NSMutableDictionary new];
        parameters[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameters[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameters[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        parameters[@"boothid"]=@"1612";
        
        if (arrEventList.count==0)[[AppHelper sharedInstance]showIndicator];;
        parameters[@"verificationstatus"]=@"0";
        selected=0;
        [self httpRequestGetEventStatus:@"0" url:baseURL param:parameters];
      
    }
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}

- (IBAction)sentTabButtonAction:(id)sender {
    
    [self selectSentTab];
    keyselected=@"send";
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERshowBoothUsers];
        NSMutableDictionary *parameters =[NSMutableDictionary new];
        parameters[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameters[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameters[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        parameters[@"boothid"]=@"1612";
        parameters[@"verificationstatus"]=@"1";
        selected=0;
        [self httpRequestGetEventStatus:@"0" url:baseURL param:parameters];
    }
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
}
@end
