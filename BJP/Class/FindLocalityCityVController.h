//
//  FindLocalityCityVController.h
//  KINCT
//
//  Created by PDSingh on 5/5/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

typedef void(^DidSelectLocationblock)(id address);

@interface FindLocalityCityVController : UIViewController
{
    NSMutableArray *selectedRows;
    NSMutableArray *mainDataSourceArray;
    UIToolbar*   numberToolbar;
}

@property(nonatomic,copy)DidSelectLocationblock address;
-(void)getAddressBlock:(DidSelectLocationblock)address;

@end
