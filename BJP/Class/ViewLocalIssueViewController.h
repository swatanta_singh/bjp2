//
//  ViewLocalIssueViewController.h
//  BJP
//
//  Created by swatantra on 12/16/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "BaseViewController.h"

@interface ViewLocalIssueViewController : UIViewController
@property(nonatomic,strong)NSDictionary *dictFilter;
-(void)refreshTable;
@end
