//
//  ShowTaskViewController.m
//  BJP
//
//  Created by swatantra on 11/12/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "ShowTaskViewController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "CreateTaskViewController.h"
#import "DateFormatters.h"
#import "TaskDTLViewController.h"

#import "ImageController.h"
#import "PresentionHandler.h"
@interface ShowTaskViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tbleView;
@property(nonatomic,strong)NSArray *arrData;
@end

@implementation ShowTaskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getAssignment];

    __weak ShowTaskViewController *weekSelf=self;
    [self setUpHeaderWithTitle:@"Show MyTask" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [  weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self.serviceType isEqualToString:@"2"])
        {
            return self.arrData.count;

        }
    else{
        return self.arrData.count;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *myCell;
     NSDictionary *dict=self.arrData[indexPath.row];
    if([self.serviceType isEqualToString:@"2"]){
        
        myCell=[tableView dequeueReusableCellWithIdentifier:@"MyTask" forIndexPath:indexPath];
        UIImageView *imgV = (UIImageView*)[myCell.contentView viewWithTag:100];
        UIImageView *imgVBase = (UIImageView*)[myCell.contentView viewWithTag:107];
        UILabel *lblName = (UILabel*)[myCell.contentView viewWithTag:101];
        UILabel *lblStart = (UILabel*)[myCell.contentView viewWithTag:102];
        UILabel *lblEnd = (UILabel*)[myCell.contentView viewWithTag:103];
        UILabel *lblSub = (UILabel*)[myCell.contentView viewWithTag:104];
        UILabel *lblDest = (UILabel*)[myCell.contentView viewWithTag:105];
        UILabel *lblStatus = (UILabel*)[myCell.contentView viewWithTag:106];
        lblStart.text=@"Start Date:  N/A";
        lblEnd.text=@"End Date:  N/A";

        lblName.text=[NSString stringWithFormat:@"From: %@",[AppHelper nullCheck:[[dict valueForKey:@"assignedBy"] valueForKey:@"name"]]];
        lblDest.text=[NSString stringWithFormat:@"%@",[AppHelper nullCheck:[[dict valueForKey:@"assignedBy"] valueForKey:@"desg_hi"]]];

        lblSub.text=[NSString stringWithFormat:@"Subject: %@",[AppHelper nullCheck:[dict valueForKey:@"subject"]]];
        lblStatus.text=[NSString stringWithFormat:@"%@",[AppHelper nullCheck:[dict valueForKey:@"status"]]];

        lblStart.text=[NSString stringWithFormat:@"Subject: %@",[AppHelper nullCheck:[dict valueForKey:@"subject"]]];
        NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];
        if([[AppHelper nullCheck:[dict valueForKey:@"end_date"]]length]>2){
            NSDate *strDate=[AppHelper returnDateFromUnixTimeStampString:[dict valueForKey:@"end_date"]];
            lblEnd.text=[NSString stringWithFormat:@"%@\n%@",@"End Date:",[formatter stringFromDate:strDate]];
        }
        if([[AppHelper nullCheck:[dict valueForKey:@"start_date"]]length]>2){
            NSDate *strDate=[AppHelper returnDateFromUnixTimeStampString:[dict valueForKey:@"start_date"]];
            lblStart.text=[NSString stringWithFormat:@"%@\n%@",@"Start Date:",[formatter stringFromDate:strDate]];
        }
        
        NSString *strImg=[AppHelper nullCheck:[dict valueForKey:@"img_url"]];
         if(strImg.length>2){
             NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
             [imgV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];

         }
           if([dict[@"status"] isEqualToString:@"Pending"]){//Expired
            imgVBase.backgroundColor=[UIColor colorWithRed:250.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1];
            lblStatus.backgroundColor=[UIColor colorWithRed:222.0/255.0 green:2.0/255.0 blue:39.0/255.0 alpha:1];

        }
        else   if([dict[@"status"] isEqualToString:@"Compleated"]){
            imgVBase.backgroundColor=[UIColor colorWithRed:255.0/255.0 green:220.0/255.0 blue:200.0/255.0 alpha:1];
            lblStatus.backgroundColor=[UIColor colorWithRed:255.0/255.0 green:120.0/255.0 blue:50.0/255.0 alpha:1];
        }
        else {
            imgVBase.backgroundColor=[UIColor colorWithRed:200.0/255.0 green:226.0/255.0 blue:204.0/255.0 alpha:1];
            lblStatus.backgroundColor=[UIColor colorWithRed:30.0/255.0 green:137.0/255.0 blue:56.0/255.0 alpha:1];

        }
    }
    else{
        myCell=[tableView dequeueReusableCellWithIdentifier:@"MyTask1" forIndexPath:indexPath];
        UIImageView *imgV = (UIImageView*)[myCell.contentView viewWithTag:100];
        UILabel *lblName = (UILabel*)[myCell.contentView viewWithTag:101];
        UILabel *lblDest = (UILabel*)[myCell.contentView viewWithTag:102];
        UILabel *lblStart = (UILabel*)[myCell.contentView viewWithTag:103];
        UILabel *lblEnd = (UILabel*)[myCell.contentView viewWithTag:104];
        UILabel *lblComplete = (UILabel*)[myCell.contentView viewWithTag:105];
        UILabel *lblOnGong = (UILabel*)[myCell.contentView viewWithTag:106];
        UILabel *lblPending = (UILabel*)[myCell.contentView viewWithTag:107];
        lblName.text=[NSString stringWithFormat:@"Subject: %@",[AppHelper nullCheck:[dict valueForKey:@"subject"]]];
        lblDest.text=[NSString stringWithFormat:@"Total Participants: %@",[AppHelper nullCheck:[dict valueForKey:@"TotalCount"]]];
        lblComplete.text=[NSString stringWithFormat:@"%@ \n%@",[AppHelper nullCheck:[dict valueForKey:@"compleated"]],@"Compleated"];
        lblOnGong.text=[NSString stringWithFormat:@"%@ \n%@",[AppHelper nullCheck:[dict valueForKey:@"Ongoing"]],@"Ongoing"];
        lblPending.text=[NSString stringWithFormat:@"%@ \n%@",[AppHelper nullCheck:[dict valueForKey:@"pending"]],@"Pending"];
        NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];

        if([[AppHelper nullCheck:[dict valueForKey:@"end_date"]]length]>2){
            NSDate *strDate=[AppHelper returnDateFromUnixTimeStampString:[dict valueForKey:@"end_date"]];
            lblEnd.text=[NSString stringWithFormat:@"%@%@",@"End Date:",[formatter stringFromDate:strDate]];
        }
        if([[AppHelper nullCheck:[dict valueForKey:@"start_date"]]length]>2){
            NSDate *strDate=[AppHelper returnDateFromUnixTimeStampString:[dict valueForKey:@"start_date"]];
            lblStart.text=[NSString stringWithFormat:@"%@%@",@"Start Date:",[formatter stringFromDate:strDate]];
        }
        NSString *strImg=[AppHelper nullCheck:[dict valueForKey:@"img_url"]];
        if(strImg.length>2){
            NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
            [imgV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
            
        }
        [self setLableText:lblComplete];
          [self setLableText:lblOnGong];
          [self setLableText:lblPending];
    }
  
    return myCell;
}
-(void)setLableText:(UILabel*)lblName{
    NSString *str =lblName.text;
    NSArray *arr=[str componentsSeparatedByString:@" "];
    if(arr.count>1){
        NSString *sub=[arr firstObject];
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc]initWithString:str];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [attrStr length])];
    [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue" size:16.0f] range:[str rangeOfString:sub]];
    //add alignment
    //NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    //[paragraphStyle setAlignment:NSTextAlignmentLeft];
   // [attrStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attrStr.length)];
        lblName.text=nil;
    lblName.attributedText = attrStr;
    }

}
-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
      if([self.serviceType isEqualToString:@"2"]){
          TaskDTLViewController *expandAndCollaps=(TaskDTLViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"TaskDTLViewController"];
          expandAndCollaps.dictData=self.arrData[indexPath.row];
          expandAndCollaps.serviceType=self.serviceType;
          [self.navigationController pushViewController:expandAndCollaps animated:YES];
      }
      else{
          TaskDTLViewController *expandAndCollaps=(TaskDTLViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"TaskDTLViewController"];
          expandAndCollaps.dictData=self.arrData[indexPath.row];
          expandAndCollaps.serviceType=self.serviceType;

          [self.navigationController pushViewController:expandAndCollaps animated:YES];
      }
  
    
}
#pragma mark service
-(void)getAssignment{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=@"1";
        NSString *baseURL;
        if([self.serviceType isEqualToString:@"2"]){
              parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;//@"6503";//
            baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_ASSIGNMNT_ME];
        }
        else{
            parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
            baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_ASSIGNMNT_BY];
        }
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
       // manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                self.arrData=[responseObject valueForKey:@"list"];
                [self.tbleView reloadData];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error){
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
             
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}

@end
