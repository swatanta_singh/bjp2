//
//  ReplyListViewController.m
//  BJP
//
//  Created by toyaj on 10/20/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "ReplyListViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "ReplyDetailViewController.h"

@interface ReplyListViewController ()

@end

@implementation ReplyListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self httpCategoryRequest];
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
       __weak ReplyListViewController *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"Replies", nil);
    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
            
        }
        else{
            [weekSelf.view endEditing:YES];
            
        }
    }];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.categoryArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ReplyList" forIndexPath:indexPath];
    
    
    UILabel *lbldescription = (UILabel*)[cell.contentView viewWithTag:101];
    UILabel *lblreply = (UILabel*)[cell.contentView viewWithTag:102];
    
    UILabel *lblDate = (UILabel*)[cell.contentView viewWithTag:104];
    
    
    lbldescription.text =[NSString stringWithFormat:@"%@",[AppHelper nullCheck:[[self.categoryArray objectAtIndex:indexPath.row] valueForKey:@"text"]]];
    
    double stimeStamp=[[[self.categoryArray objectAtIndex:indexPath.row] valueForKey:@"replyAt"] doubleValue];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd MMM, yyyy HH:mm";
    NSDate *sdate = [NSDate dateWithTimeIntervalSince1970:stimeStamp];
  lblDate.text=[dateFormatter stringFromDate:sdate];
    
    NSString *checkValue =[[self.categoryArray objectAtIndex:indexPath.row] valueForKey:@"reply"];

    
    if ([checkValue length]==0) {
        lblreply.text =@"No Reply";
        [lblreply setTextColor:[UIColor grayColor]];
    }
    else
    {
        lblreply.text =@"View Reply";
        
    }
    
    return cell;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)httpCategoryRequest
{
    
    NSMutableDictionary *parameter=[NSMutableDictionary new];
    parameter[@"clientId"]=@"1";
    parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
    parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@",BaseUrl,kGet_Reply];
    
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                    self.categoryArray = [responseObject objectForKey:@"Data"];
                
                if (self.categoryArray.count>0) {
                    [_listTableView reloadData];
                    
                }
                else
                {
                    [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"No Reply" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                }

                
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *checkValue =[[self.categoryArray objectAtIndex:indexPath.row] valueForKey:@"reply"];
    
    
    if ([checkValue length]==0) {
     
    }
    else
    {
        ReplyDetailViewController *expandAndCollaps=(ReplyDetailViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ReplyDetailViewController"];
        expandAndCollaps.categoryArrayList = [_categoryArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:expandAndCollaps animated:YES];    }
    

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
       return 1300.0f;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==100){
        
        [[AppHelper navigationController] popToRootViewControllerAnimated:YES];
        
    }
}
@end
