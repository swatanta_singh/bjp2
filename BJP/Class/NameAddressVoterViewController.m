
#import "NameAddressVoterViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "PresentionHandler.h"
#import "DataPickerViewController.h"


@interface NameAddressVoterViewController ()
{
  
    NSMutableArray *arrdataValue;
    NSMutableDictionary *nameAddress;
     NameAddressVoterViewController *sendDetails;
    
}
@property(nonatomic,strong)PresentionHandler *presnthandler;
@property(nonatomic,strong)NSArray *arrCategory;
@property(nonatomic,strong)NSMutableArray *selectedCatgory;

@property (weak, nonatomic) IBOutlet UITextField *lblCategory;
@end

@implementation NameAddressVoterViewController
@synthesize arrData=arrData;

#pragma mark - view did load

- (void)viewDidLoad {
    [super viewDidLoad];
    numberformatter = [[ECPhoneNumberFormatter alloc] init];
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    numberToolbar.items = @[flexibleItem,[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor darkGrayColor];
    [numberToolbar setBackgroundColor:[UIColor lightGrayColor]];
    
    arrData=[NSMutableArray new];
    arrdataValue =[NSMutableArray new];
    self.dictData=[NSMutableDictionary new];
    nameAddress=[NSMutableDictionary new];
    
    self.selectedCatgory=[NSMutableArray new];
     [self registerForKeyboardNotifications];

}
 #pragma mark - view Will Appear

-(void)viewWillAppear:(BOOL)animated
{
    [self.selectedCatgory removeAllObjects];
     [self displayDataOnView];
}

-(void)setUpdataForStringtype:(NSString *)typeString keytype:(NSString*)keytype withField:(NSInteger)myCell{
    
    NSMutableDictionary *dir=[arrData objectAtIndex:myCell];

    if ([nameAddress valueForKey:typeString]>0) {
        NSArray *arrId = [nameAddress valueForKey:typeString];
        
        if (arrId.count>0) {
            NSPredicate *prd=[NSPredicate predicateWithFormat:@"selected == %d",1];
            NSArray *Arr=[arrId filteredArrayUsingPredicate:prd];
            if (Arr.count>0) {
                NSString *value=[[Arr valueForKey:keytype] lastObject];
                dir[@"value"] =value;
                dir[@"id"]=[[Arr valueForKey:@"id"] lastObject];
                
            }
            else
                dir[@"value"] = @"Select";
        }
        else dir[@"value"] = @"NA";
    }
    else
    dir[@"value"] = @"NA";
}

#pragma mark - set up View

-(void)setUpData{
       //  1 for button , 0 for text field
    for (int i=0; i<21; i++) {
        NSMutableDictionary *dict=[NSMutableDictionary new];
        switch (i) {
            case 0:{
                dict[@"value"]=[AppHelper nullCheck:[nameAddress valueForKey:@"disrtict"]];
                dict[@"place"]=@"District ";
                dict[@"type"]=@"0";
                dict[@"part"]=@"0";
                dict[@"id"]=@"";
            }
                break;
            case 1:{
                dict[@"value"]= [AppHelper nullCheck:nameAddress[@"booth_name"]];
                dict[@"place"]=@"Booth Name ";
                dict[@"type"]=@"0";
                dict[@"part"]=@"0";
                dict[@"id"]=[AppHelper nullCheck:[nameAddress valueForKey:@"booth"]];;
            }
                break;
            case 2:{
       
                dict[@"value"]=[AppHelper nullCheck:[nameAddress valueForKey:@"ac_name"]];
                dict[@"place"]=@"Assembly constituency ";
                dict[@"type"]=@"0";
                dict[@"part"]=@"0";
                dict[@"id"]=[AppHelper nullCheck:[nameAddress valueForKey:@"ac"]];;
            }
                break;
            case 3:{
                dict[@"value"]=[AppHelper nullCheck:[nameAddress valueForKey:@"name"]];
                dict[@"place"]=@"Name";
                dict[@"type"]=@"0";
                dict[@"part"]=@"0";
                 dict[@"id"]=@"";
            }
                break;
            case 4:{
                
                dict[@"value"]=[AppHelper nullCheck:[nameAddress valueForKey:@"fathers_name"]];
                dict[@"place"]=@"Father name";
                dict[@"type"]=@"0";
                dict[@"part"]=@"0";
                 dict[@"id"]=@"";
            }
                break;
            case 5:{
                dict[@"value"]=[AppHelper nullCheck:[nameAddress valueForKey:@"voter_no"]];
                dict[@"place"]=@"Voter No";
                dict[@"type"]=@"0";
                dict[@"part"]=@"1";
                 dict[@"id"]=@"";
            }
                break;
            case 6:{
                dict[@"value"]=[AppHelper nullCheck:[nameAddress valueForKey:@"voter_id"]];
                dict[@"place"]=@"Voter Id";
                dict[@"type"]=@"0";
                dict[@"part"]=@"1";
                 dict[@"id"]=@"";
            }
                break;
            case 7:{
                dict[@"value"]=[AppHelper nullCheck:[nameAddress valueForKey:@"age"]];
                dict[@"place"]=@"Age";
                dict[@"type"]=@"0";
                dict[@"part"]=@"1";
                 dict[@"id"]=@"";
            }
                break;
            case 8:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Gender";
                dict[@"type"]=@"1";
                dict[@"part"]=@"1";
                 dict[@"id"]=@"";
            }
                break;
            case 9:{
                dict[@"value"]=[AppHelper nullCheck:[nameAddress valueForKey:@"house_no"]];
                dict[@"place"]=@"House No";
                dict[@"type"]=@"0";
                dict[@"part"]=@"0";
                 dict[@"id"]=@"";
            }
                break;
                
            case 10:{
                dict[@"value"]=[AppHelper nullCheck:nameAddress[@"phoneno"]];
                dict[@"place"]=@"Mobile Number";
                dict[@"type"]=@"0";
                dict[@"part"]=@"0";
                dict[@"id"]=@"";
                
            }
                break;
            case 11:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Availability ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                 dict[@"id"]=@"";
                
            }
                break;
            case 12:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Residential Status ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                 dict[@"id"]=@"";
                
            }
                break;
            case 13:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Education ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                 dict[@"id"]=@"";
                
            }
                break;
            case 14:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Occupation";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                 dict[@"id"]=@"";
                
            }
                break;
                
            case 15:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Income Group(Monthly)";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                 dict[@"id"]=@"";
                
            }
                break;
            case 16:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Religion";
                dict[@"type"]=@"1";
                dict[@"part"]=@"1";
                 dict[@"id"]=@"";
                
            }
                break;
            case 17:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Category";
                dict[@"type"]=@"1";
                dict[@"part"]=@"1";
                 dict[@"id"]=@"";
                
            }
                break;
            case 18:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Cast";
                dict[@"type"]=@"1";
                dict[@"part"]=@"1";
                 dict[@"id"]=@"";
                
            }
                break;
            case 19:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Head Of Family";
                dict[@"type"]=@"1";
                dict[@"part"]=@"1";
                 dict[@"id"]=@"";
                
            }
                break;
            case 20:{
                
                dict[@"value"]=[AppHelper nullCheck:[nameAddress valueForKey:@"family_members"]];
                dict[@"place"]=@"No Of dependents";
                dict[@"type"]=@"0";
                dict[@"part"]=@"0";
                dict[@"id"]=@"";
                
            }
                break;
            default:
                break;
        }
        [arrData addObject:dict];
    }
}

#pragma mark - uicollectionview View

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [arrData count];
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *  myCell ;
    myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dict =[arrData objectAtIndex:indexPath.row];
    UILabel *lblName=(UILabel *)[myCell.contentView viewWithTag:1001];
    UITextField *txtFild=(UITextField *)[myCell.contentView viewWithTag:1002];
    UIView *view =(UIView*)[myCell.contentView viewWithTag:9001];

  
    UIButton *btnAction =(UIButton*)[myCell.contentView viewWithTag:1003];
    btnAction.hidden=YES;
    
    if ([dict[@"type"] isEqualToString:@"1"]) {
        btnAction.hidden=NO;
    }
   
    [btnAction addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    txtFild.text=[NSString stringWithFormat:@"%@",dict[@"value"]];
    txtFild.delegate = self;
   
    [view setBackgroundColor:[UIColor whiteColor]];

    if (self.serveyId) {
        txtFild.enabled=NO;
    }
    else if(indexPath.row==0||indexPath.row==1||indexPath.row==2)
    {
        txtFild.enabled=NO;
        [view setBackgroundColor:[UIColor colorWithRed:227.0f/255.0f green:227.0f/255.0f  blue:227.0f/255.0f  alpha:0.8]];
    }
    else{
        txtFild.enabled=YES;
    }
    
    lblName.text=dict[@"place"];
    myCell.tag=indexPath.row;
    if ([dict[@"place"] isEqualToString:@"No Of dependents"]  || [dict[@"place"] isEqualToString:@"Mobile Number"]) {
        txtFild.keyboardType=UIKeyboardTypePhonePad;
        txtFild.inputAccessoryView = numberToolbar;
        txtFild.enabled = YES;
        txtFild.text = [numberformatter stringForObjectValue:dict[@"value"]];
        
    }
    
    if ([dict[@"place"] isEqualToString:@"Voter No"] || [dict[@"place"] isEqualToString:@"Age"]) {
        txtFild.keyboardType=UIKeyboardTypePhonePad;
        txtFild.inputAccessoryView = numberToolbar;
        txtFild.text = [numberformatter stringForObjectValue:dict[@"value"]];
        
    }
    
    if ([dict[@"place"] isEqualToString:@"Father name"]  || [dict[@"place"] isEqualToString:@"Name"]  || [dict[@"place"] isEqualToString:@"Voter Id"]) {
        txtFild.keyboardType=UIKeyboardTypeAlphabet;
        txtFild.inputAccessoryView = numberToolbar;
               
    }

    txtFild.inputAccessoryView = numberToolbar;
    return myCell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict =[arrData objectAtIndex:indexPath.row];
    
    if([dict[@"part"] isEqualToString:@"1"]){
        return CGSizeMake(SCREEN_WIDTH/2-10, 70);
    }
    else{
        CGSize size = CGSizeMake(SCREEN_WIDTH-5,70);
        return size;
    }
}

#pragma mark - UIBUTTON Action

-(void)btnAction:(UIButton*)btn{
    
    UICollectionViewCell *cel=(UICollectionViewCell*)[btn.superview.superview superview];
    
    switch (cel.tag) {
        case 8:
            [self pickerForGender:cel withsender:btn];
            break;
        case 11:
            [self pickerForAvailability:cel withsender:btn];
            
            break;
        case 12:
            [self pickerForResidentialStatus:cel withsender:btn];
            
            
            break;
        case 13:
            [self pickerForEducation:cel withsender:btn];
            
            break;
            
        case 14:
            [self pickerForOccupation:cel withsender:btn];
            
            break;
        case 15:
            [self pickerForIncomeGroup:cel withsender:btn];
            
            break;
        case 16:
            [self pickerForReligious:cel withsender:btn];
            break;
        case 17:
            [self pickerForCategories:cel withsender:btn];
            break;
        case 18:
            
            [self pickerForCast:cel withsender:btn];
            
            
            break;
        case 19:
            
            [self pickerForHeadOFFamily:cel withsender:btn];
            
            
            break;
            
            
        default:
            break;
    }
    
    
}
#pragma mark - Picker Action

-(void)pickerForAvailability:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    
    [self pickerDataSource:nameAddress[@"avalability"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_AVAIL];
}

-(void)pickerForGender:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    
    [self pickerDataSource:nameAddress[@"gender"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_SERVEY];
}
-(void)pickerForResidentialStatus:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    
    [self pickerDataSource:nameAddress[@"residential_status"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_RESI];
}
-(void)pickerForEducation:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    
    [self pickerDataSource:nameAddress[@"education"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_EDU];
}

-(void)pickerForOccupation:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    [self pickerDataSource:nameAddress[@"occupation"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_OCCU];
}

-(void)pickerForIncomeGroup:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    
    [self pickerDataSource:nameAddress[@"income_group"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_INCOM];
}
-(void)pickerForReligious:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    [self pickerDataSource:nameAddress[@"religion"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_RELIGI];
}
-(void)pickerForCategories:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    
    [self pickerDataSource:nameAddress[@"category"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_CATEGORIES];
}
-(void)pickerForCast:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    [self pickerDataSource:nameAddress[@"cast"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_CAST];
}

-(void)pickerForHeadOFFamily:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    if ([nameAddress[@"head_of_family"] count]>0) {
        [self pickerDataSource:nameAddress[@"head_of_family"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_HEADOFFAMILY];
    }
}

-(void)pickerDataSource:(NSArray *)dataSource withField:(UICollectionViewCell*)myCell selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    
    [self.view endEditing:YES];
    __weak NameAddressVoterViewController *weekSelf=self;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];
    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            
            
            if(type==SEL_SERVEY){

                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [[selectedvalues firstObject]valueForKey:@"gender"];
                dir[@"id"]=[[selectedvalues firstObject]valueForKey:@"id"];
            }
            else  if(type==SEL_AVAIL){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [[selectedvalues firstObject]valueForKey:@"availability"];
                dir[@"id"]=[[selectedvalues firstObject]valueForKey:@"id"];
            }
            else  if(type==SEL_RESI){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [[selectedvalues firstObject]valueForKey:@"residential"];
                dir[@"id"]=[[selectedvalues firstObject]valueForKey:@"id"];
            }
            else  if(type==SEL_EDU){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [[selectedvalues firstObject]valueForKey:@"education"];
                dir[@"id"]=[[selectedvalues firstObject]valueForKey:@"id"];
            }
            else  if(type==SEL_OCCU){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [[selectedvalues firstObject]valueForKey:@"occupation"];
                dir[@"id"]=[[selectedvalues firstObject]valueForKey:@"id"];
            }
            else  if(type==SEL_INCOM){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [[selectedvalues firstObject]valueForKey:@"income"];
                dir[@"id"]=[[selectedvalues firstObject]valueForKey:@"id"];
            }
            else  if(type==SEL_RELIGI){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [[selectedvalues firstObject]valueForKey:@"religion"];
                dir[@"id"]=[[selectedvalues firstObject]valueForKey:@"id"];
            }
            else  if(type==SEL_CATEGORIES){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [[selectedvalues firstObject]valueForKey:@"casteCategory"];
                dir[@"id"]=[[selectedvalues firstObject]valueForKey:@"id"];
            }
            else  if(type==SEL_CAST){
                
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [[selectedvalues firstObject]valueForKey:@"caste"];
                dir[@"id"]=[[selectedvalues firstObject]valueForKey:@"id"];
            }
            else  if(type==SEL_HEADOFFAMILY){
              
                NSMutableDictionary *dir=[arrData objectAtIndex:myCell.tag];
                dir[@"value"] = [[selectedvalues firstObject]valueForKey:@"type"];
                dir[@"id"]=[[selectedvalues firstObject]valueForKey:@"id"];
            }
            else{
            
            }
            
             [weekSelf.collectionView reloadData];
        }
    }];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];
}



#pragma mark textfield action

-(void)textFieldDidEndEditing:(UITextField *)textField{
    

    UICollectionViewCell *cell=(UICollectionViewCell*)[textField.superview.superview superview];
    NSIndexPath *path=[self.collectionView indexPathForCell:cell];
    NSMutableDictionary *dict=[arrData objectAtIndex:cell.tag];

    dict[@"value"]=textField.text;

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    UICollectionViewCell *cell=(UICollectionViewCell*)[textField.superview.superview superview];
    
    NSIndexPath *path=[self.collectionView indexPathForCell:cell];
    if (path.row == 10 ) {
        
        if (textField.text.length >= 14 && range.length == 0)
        {
            return NO;
        }
    }
    if (path.row == 10 ) {
        textField.text = [numberformatter stringForObjectValue:textField.text];
    }
    if (path.row==7) {
        if (textField.text.length>=3 &&range.length==0) {
            return NO;
        }
    }
    
    return YES;
    
}

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//    
//
//        UICollectionViewCell *cell=(UICollectionViewCell*)[textField.superview superview];
//        if(cell.tag==10 )
//            [AppHelper setupViewAtUp:100 toview:self.view];
//    else if(cell.tag==19)
//        [AppHelper setupViewAtUp:210 toview:self.view];
// 
//    
//    return YES;
//    
//}


//- (void)textFieldDidBeginEditing:(UITextField *)textField{
//    
////    if(IS_IPHONE_4_OR_LESS || IS_IPHONE_5)
////    {
//        UICollectionViewCell *cell=(UICollectionViewCell*)[textField.superview.superview superview];
//        if(cell.tag==10 ||cell.tag==19||cell.tag==5)
//            [AppHelper setupViewAtUp:120 toview:self.view];
//  //  }
//    
//
//    
//}


-(void)dimissKeyborad
{
    [self.view.superview endEditing:YES];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(NSMutableDictionary*)nameAddressData{
    
    
    return self.dictData;
}

#pragma mark - Get API call

-(void)getList{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        parameter[@"surveyId"]=@"2";
        
        //   parameter[@"from_memberId"]=[AppHelper appDelegate].cureentUser.accountId;
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kformSurvey];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        // manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                nameAddress = [responseObject [@"nameaddress"] lastObject] ;
//                _namevalue = [responseObject [@"nameaddress"] lastObject];
                
                
                
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}

#pragma mark - Display data after get method

-(void)displayDataOnView
{
    
    nameAddress=self.dicdataRecived;
    [self setUpData];
    
    [self setUpdataForStringtype:@"gender" keytype:@"gender" withField:8];
    [self setUpdataForStringtype:@"avalability" keytype:@"availability" withField:11];
    
    [self setUpdataForStringtype:@"residential_status" keytype:@"residential" withField:12];
    [self setUpdataForStringtype:@"education" keytype:@"education" withField:13];
    [self setUpdataForStringtype:@"occupation" keytype:@"occupation" withField:14];
    [self setUpdataForStringtype:@"income_group" keytype:@"income" withField:15];
    [self setUpdataForStringtype:@"religion" keytype:@"religion" withField:16];
    [self setUpdataForStringtype:@"category" keytype:@"casteCategory" withField:17];
    [self setUpdataForStringtype:@"cast" keytype:@"caste" withField:18];
    [self setUpdataForStringtype:@"head_of_family" keytype:@"type" withField:19];
    
    [self.collectionView reloadData];

}


#pragma mark - Hide Keyboard Method

-(void)doneWithNumberPad
{
    [self.view endEditing:YES];

    

}
/*
 
 //- (BOOL)textFieldShouldReturn:(UITextField *)textField{
 //
 //
 //    UICollectionViewCell *cell=(UICollectionViewCell*)[textField.superview superview];
 //    NSIndexPath *path=[self.collectionView indexPathForCell:cell];
 //
 //    UICollectionViewCell *nextCell=[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:path.row+1 inSection:path.section]];
 //
 //    if(nextCell){
 //
 //        UITextField *txt=(UITextField*)[nextCell.contentView viewWithTag:101];
 //        [txt becomeFirstResponder];
 //
 //    }
 //    else{
 //        [textField resignFirstResponder];
 //    }
 //
 //    return YES;
 //}

 */

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0);
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)registerForKeyboardNotifications

{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

@end
