//
//  NotificationVC.m
//  BJP
//
//  Created by PDSingh on 9/1/16.
//  Copyright © 2016 swatantra. All rights reserved.
//
#import "PVoiceVController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "NotificationViewController.h"

@interface NotificationViewController ()
@property(nonatomic,strong)NSArray *notificationArray;
@property (weak, nonatomic) IBOutlet UITableView *tableViewNews;
@end

@implementation NotificationViewController

- (void)viewDidLoad {
    
    self.tableViewNews.tableFooterView = [[UIView alloc ]initWithFrame:CGRectZero];
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self httpNotificationRequest];
    
    __weak NotificationViewController *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"Notifications", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"menu" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [[[AppHelper sharedInstance]menuViewController] setUpMove];
        }
        else{
            [weekSelf.view endEditing:YES];
        }
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([self.notificationArray count]>0 )
    {
        numOfSections  = 1;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        
        
        noDataLabel.text             =  NSLocalizedString(@"NONotifications", nil);
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.notificationArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cellYouAvailable ;
    NSDictionary * notiD = [self.notificationArray objectAtIndex:indexPath.row];

    cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"NotificationTableCell" forIndexPath:indexPath];
    UILabel *title=[cellYouAvailable.contentView viewWithTag:1001];
    NSString *strImg=[NSString stringWithFormat:@"Comment to reply"];
    title.text = [strImg capitalizedString];

    UILabel *beforeTittle=[cellYouAvailable.contentView viewWithTag:1002];
    NSString *timeAgo= [AppHelper nullCheck:notiD[@"fromUserFullname"]];
    beforeTittle.text=timeAgo;
    
    NSString *eventsDesc=[AppHelper nullCheck:notiD[@"timeAgo"]];

    //double etimeStamp=[notiD[@"createAt"] doubleValue];
//    
//    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
//    dateFormatter.dateFormat = @"dd MMM, yyyy HH:mm";
//    NSDate *sdate = [NSDate dateWithTimeIntervalSince1970:etimeStamp];
    UILabel *eventsDescLable=[cellYouAvailable.contentView viewWithTag:1003];
    eventsDescLable.text=eventsDesc;

    return cellYouAvailable;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([AppHelper appDelegate].cureentUser.accountId) {
//        
//        
//    }
//    else
//    {
//        UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LoginViewController"];
//        [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
//    }
//}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   // return UITableViewAutomaticDimension;
     return 60.0f;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 130.0f;
}


-(void)httpNotificationRequest
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        
        
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        
//        parameter[@"accountId"]=@"4";
//        parameter[@"accessToken"]=@"55416e6b0e7664aa5a0db2ef27915ebc";
       
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_NOTIFICATION];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            NSString *errorMsg;
            if (!responseObject)
            {
                //[AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
               // return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([responseObject objectForKey:@"notifications"] && [[responseObject objectForKey:@"notifications"] isKindOfClass:[NSArray class]]) {
                    self.notificationArray  = [responseObject objectForKey:@"notifications"];
                }
                
                //if (self.notificationArray.count==0)
                //    errorMsg=@"No Notification Found";
                
                [self.tableViewNews reloadData];
            }
            else{
                
                NSString *messge=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
                if ([messge length] && [messge isEqualToString:@"Error authorization."]) {
                    errorMsg=@"Please login in app";
                    }
                   else
                   {
                    errorMsg=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
                   }
            }
            
            if(errorMsg.length>3){
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}


@end
