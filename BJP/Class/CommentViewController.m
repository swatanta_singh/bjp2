//
//  CommentViewController.m
//  BJP
//
//  Created by swatantra on 8/31/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "CommentViewController.h"
#import "Service.h"
#import "AppHelper.h"
#import "Defines.h"
#import "LoginViewController.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"

@interface CommentViewController (){
    UIToolbar* numberToolbar;

}

@property (weak, nonatomic) IBOutlet UITextView *txtView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnComment;
@property (weak, nonatomic) IBOutlet UILabel *lblheader;
@property (strong, nonatomic) IBOutlet NSArray *arrComment;
- (IBAction)commentButtonClick:(id)sender;
@end

@implementation CommentViewController
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
    
}
- (void)viewDidLoad {
 
    [super viewDidLoad];
    
     self.lblheader.text=[[AppHelper nullCheck:self.servicDict[@"itemTitle"]]capitalizedString];
    
    [self getCommentData];
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    numberToolbar.items = @[flexibleItem,[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor darkGrayColor];
    [numberToolbar setBackgroundColor:[UIColor lightGrayColor]];    // Do any additional setup after loading the view.
    self.txtView.inputAccessoryView = numberToolbar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    if([AppHelper appDelegate].cureentUser){
        [self.btnComment setTitle:@"Submit" forState:UIControlStateNormal];
    }
    __weak CommentViewController *weekSelf=self;
    //[self.headerView.headerImageView setHidden:YES];
    
    NSString *titleText = NSLocalizedString(@"Comment", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

#pragma mark table life cycle
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /* Return an estimated height or calculate
     * estimated height dynamically on information
     * that makes sense in your case.
     */
    return 300.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrComment.count;
}

-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *  myCell = [tableView dequeueReusableCellWithIdentifier:@"DetailNews" forIndexPath:indexPath];
    UILabel *lblname = (UILabel*)[myCell.contentView viewWithTag:101];
     UIImageView *imgUser = (UIImageView*)[myCell.contentView viewWithTag:100];
    UILabel *lblnameD = (UILabel*)[myCell.contentView viewWithTag:102];
        UILabel *lblCommet = (UILabel*)[myCell.contentView viewWithTag:103];
       UIButton *btnReply = (UIButton*)[myCell.contentView viewWithTag:104];
      [btnReply addTarget:self action:@selector(buttonReplly:) forControlEvents:UIControlEventTouchUpInside];
    NSDictionary *newsDictionary=self.arrComment[indexPath.row];
    NSString *strImg2=[AppHelper nullCheck:newsDictionary[@"fromUserPhotoUrl"]];
    NSURL *url2 = [NSURL URLWithString:[self normalizePath:strImg2]];
    [imgUser setImageWithURL:url2 placeholderImage:[UIImage imageNamed:@"userp"]];
    
    lblname.text=[AppHelper nullCheck:newsDictionary[@"fromUserFullname"]];
     lblnameD.text=[AppHelper nullCheck:newsDictionary[@"timeAgo"]];
    lblCommet.text=[AppHelper nullCheck:newsDictionary[@"comment"]];

    
    return myCell;
}

#pragma mark button action
-(void)buttonReplly:(UIButton*)btn{
    
}
- (IBAction)commentButtonClick:(id)sender {
      if([AppHelper appDelegate].cureentUser){
          if(self.txtView.text.length>0){
              [self.view endEditing:YES];
              [self sendCommentData];
          }
          else{
                 [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please enter a comment." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK"];
           
          }
      }
      else{
          LoginViewController *expandAndCollaps=(LoginViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LoginViewController"];
          [self.navigationController pushViewController:expandAndCollaps animated:YES];

      }
}
#pragma mark service
-(void)getCommentData
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_COMMENT];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"clientId"]=@"1";
        parameter[@"itemId"]=self.servicDict[@"id"];
    
        NSDictionary *dict=nil;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:nil];
        NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        dict = @{@"body":json};
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
             else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                self.arrComment=[[responseObject valueForKey:@"comments"] valueForKey:@"comments"];
            }
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            [self.tableView reloadData];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}
-(void)sendCommentData
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kSEND_COMMENT];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"clientId"]=@"1";
        parameter[@"itemId"]=self.servicDict[@"id"];
        parameter[@"commentText"]=self.txtView.text;
           parameter[@"replyToUserId"]=@"";
        
        NSDictionary *dict=nil;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:nil];
        NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        dict = @{@"body":json};
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                self.txtView.text=nil;
                [self getCommentData];
               // self.arrComment=[[responseObject valueForKey:@"comments"] valueForKey:@"comments"];
            }
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            [self.tableView reloadData];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}

@end
