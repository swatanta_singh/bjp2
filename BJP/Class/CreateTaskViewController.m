//
//  CreateTaskViewController.m
//  BJP
//
//  Created by swatantra on 11/12/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "CreateTaskViewController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "SearchContactViewController.h"
#import "ImageController.h"
#import "DateFormatters.h"
#import "DataPickerViewController.h"
#import "PresentionHandler.h"
#import "DatePickerViewController.h"
@interface CreateTaskViewController (){
    UIToolbar*   numberToolbar;
  NSMutableArray *arrData;
}
@property(nonatomic,strong)PresentionHandler *presnthandler;

@property (weak, nonatomic) IBOutlet UIButton *btnEnd;
- (IBAction)endButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
- (IBAction)startButtonActon:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblEndDate;
@property (weak, nonatomic) IBOutlet UIButton *btnAttacmnt;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITextView *txtView;
- (IBAction)submitbuttonClick:(id)sender;
- (IBAction)addContactButtonClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
- (IBAction)attachmentButtonClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnAddContct;
@property (weak, nonatomic) IBOutlet UITextField *txtField;
@end

@implementation CreateTaskViewController
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    UICollectionViewFlowLayout *flow = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
//    flow.estimatedItemSize = CGSizeMake(100.0f, 50.0f);
    arrData=[NSMutableArray new];
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    numberToolbar.items = @[flexibleItem,[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor darkGrayColor];
    [numberToolbar setBackgroundColor:[UIColor lightGrayColor]];
    
    if(self.dictData){
        self.lblEndDate.text=@"";
        self.lblStartDate.text=@"";
        self.txtView.editable=NO;
        self.txtField.enabled=NO;
        self.btnSubmit.hidden=YES;
        self.btnAttacmnt.hidden=YES;
        self.btnAddContct.hidden=YES;
        self.btnEnd.hidden=YES;
        self.btnStart.hidden=YES;
        self.txtView.text=[AppHelper nullCheck:self.dictData[@"message"]];
        self.txtField.text=[AppHelper nullCheck:self.dictData[@"subject"]];
        if([[AppHelper nullCheck:[self.dictData valueForKey:@"imgurl"]]length]>2){
            NSURL *url2 = [NSURL URLWithString:[self normalizePath:self.dictData[@"imgurl"]]];
            [self.imgView setImageWithURL:url2 placeholderImage:[UIImage imageNamed:@"userp"]];
        }
        
        [arrData addObjectsFromArray:self.dictData[@"assigned_to"]];
        NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];
        
        if([[AppHelper nullCheck:[self.dictData valueForKey:@"start_date"]]length]>2){
            NSDate *strDate=[AppHelper returnDateFromUnixTimeStampString:[self.dictData valueForKey:@"start_date"]];
            self.lblStartDate.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:strDate]];
        }
        if([[AppHelper nullCheck:[self.dictData valueForKey:@"end_date"]]length]>2){
            NSDate *strDate=[AppHelper returnDateFromUnixTimeStampString:[self.dictData valueForKey:@"end_date"]];
            self.lblEndDate.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:strDate]];
        }
    }
    else{
        NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];
        self.lblEndDate.text=[formatter stringFromDate:[NSDate date]];
        self.lblStartDate.text=[formatter stringFromDate:[NSDate date]];

    }
    // Do any additional setup after loading the view.
 }
-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    __weak CreateTaskViewController *weekSelf=self;
    [self setUpHeaderWithTitle:@"Create Task" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [  weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

#pragma mark - uicollectionview View
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [arrData count];
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    UILabel *lblName=(UILabel *)[cell.contentView viewWithTag:101];
     UIButton *btnD=(UIButton *)[cell.contentView viewWithTag:102];
    if(self.dictData){
        btnD.hidden=YES;
    }
    [btnD addTarget:self action:@selector(delteContact:) forControlEvents:UIControlEventTouchUpInside];
    lblName.text=[NSString stringWithFormat:@" %@ ",[[arrData objectAtIndex:indexPath.row] valueForKey:@"name"]];
    cell.tag=indexPath.row;
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
        NSString *string_size=[NSString stringWithFormat:@" %@ ",[[arrData objectAtIndex:indexPath.row] valueForKey:@"name"]];
        CGSize size = CGSizeMake([self getLabelSize:string_size].width+50, self.collectionView.frame.size.width);
        return size;
    }
- (CGSize)getLabelSize:(NSString *)string {
    UIFont *myFont = [UIFont fontWithName:@"Roboto-Regular" size:15];
    
    CGSize maximumSize = [string sizeWithAttributes:@{NSFontAttributeName : myFont}];
    
    return maximumSize;
}
#pragma mark - button action
-(void)openDatePicker:(UILabel*)lbl button:(UIButton*)btn{
    // NSLog(@"%d",btn.tag);
    [self.view endEditing:YES];
    __weak CreateTaskViewController *weekSelf=self;
    
    DatePickerViewController *datePickerViewController = (DatePickerViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"DatePickerViewController"];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:datePickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:btn andPreferedSize:DropDownSize];
    NSDate *minDate=[NSDate date] ;
    NSDate *maxDate=nil ;
    NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];
    
    [datePickerViewController initDatePickerWithDatePickerMode:UIDatePickerModeDate minDate:minDate maxDate:maxDate andDefaultSelectedDate:minDate withReturnedDate:^(NSDate *selectedDate) {
        if (selectedDate) {
            NSString *strDatte= [formatter stringFromDate:selectedDate];
            lbl.text =  strDatte;
        }
    }];
    
}

- (IBAction)startButtonActon:(UIButton*)sender {
    [self openDatePicker:self.lblStartDate button:sender];
}
- (IBAction)endButtonAction:(UIButton*)sender {
      [self openDatePicker:self.lblEndDate button:sender];
}
-(void)delteContact:(UIButton*)btn{
    UICollectionViewCell *cel=(UICollectionViewCell*)[btn.superview superview];
  //  NSLog(@"%ld",(long)cel.tag);
   // NSLog(@"%@",[NSArray arrayWithObject:[NSIndexPath indexPathForItem:cel.tag inSection:0]]);
    [arrData removeObjectAtIndex:cel.tag];
//    [self.collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:cel.tag inSection:0]]];
//
//    [self.collectionView.collectionViewLayout invalidateLayout];
    [self.collectionView reloadData];
}
-(IBAction)imgButtonClick:(id)sender{
    if(self.imgView.image){
    ImageController *expandAndCollaps=(ImageController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ImageController"];
    expandAndCollaps.image = self.imgView.image;
    [self presentViewController:expandAndCollaps animated:YES completion:nil];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==100){
        [[AppHelper navigationController] popViewControllerAnimated:YES];
        
    }
}
- (IBAction)submitbuttonClick:(id)sender {
    NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];
    NSDate *endD=[formatter dateFromString:self.lblEndDate.text];
    NSDate *strtd=[formatter dateFromString:self.lblStartDate.text];

    if(arrData.count==0){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Please select assign to." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else if(self.txtField.text.length==0){
      [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Please enter subject." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else if(self.txtView.text.length==0){
       [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Please enter message." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else if([strtd timeIntervalSinceDate:endD]>0.0){
           [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Start date should be less from end date." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else{
    [self createuAssignment];
    }
}

- (IBAction)addContactButtonClick:(id)sender {
    SearchContactViewController *expandAndCollaps=(SearchContactViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"SearchContactViewController"];
    [expandAndCollaps igetSelectedContact:^(NSArray *selectedContact) {
      //  NSLog(@"se==%@",selectedContact);
        [arrData addObjectsFromArray:selectedContact];
        [self.collectionView reloadData];
    }];
    [self.navigationController pushViewController:expandAndCollaps animated:YES];
}
- (IBAction)attachmentButtonClick:(id)sender {
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
            // There is not a camera on this device, so don't show the camera button.
            
        }
        else{
            [AppHelper showAlertViewWithTag:10 title:APP_NAME message:@"Camera not available." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        }
        // Camera button tapped.
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Library button tapped.
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        // [self launchSpecialController];
        
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}
- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    // imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}
#pragma mark - Text Field
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Text view
- (void)textViewDidBeginEditing:(UITextView *)textView{
    [AppHelper setupViewAtUp:160 toview:self.view];
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    [AppHelper setupViewAtUp:0 toview:self.view];
}
#pragma mark UIImagePickerControllerDelegate ------------


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    self.imgView.image=image;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma  mark service
-(void)createuAssignment{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];

    NSMutableDictionary *parameters=[NSMutableDictionary new];
    parameters[@"clientId"]=@"1";
    parameters[@"subject"]=self.txtField.text;
    parameters[@"message"]=self.txtView.text;
    parameters[@"assign_to"]=[arrData valueForKeyPath:@"member_id"];
    parameters[@"end_date"]=[AppHelper unixDateString:[formatter dateFromString:self.lblEndDate.text]];
    parameters[@"start_date"]=[AppHelper unixDateString:[formatter dateFromString:self.lblStartDate.text]];
    parameters[@"from_memberId"]=[AppHelper appDelegate].cureentUser.accountId;//[AppHelper appDelegate].cureentUser.accountId;
    NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_NEWASSIGNMNT];
    [[AppHelper sharedInstance]showIndicator];


    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
        NSData *imageData = UIImageJPEGRepresentation(self.imgView.image, 0.5);

    AFHTTPRequestOperation *op = [manager POST:baseURL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        if(imageData)
        [formData appendPartWithFileData:imageData name:@"uploaded_file" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[AppHelper sharedInstance]hideIndicator];

        if (!responseObject)
        {
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            return ;
        }
        else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
        {
          [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Submitted Successfully.." delegate:self cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }
        
        else{
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       // NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        [[AppHelper sharedInstance]hideIndicator];
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];

    }];
        
    [op start];
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
}



@end
