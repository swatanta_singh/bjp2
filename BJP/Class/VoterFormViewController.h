
#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ECPhoneNumberFormatter.h"

@interface VoterFormViewController : BaseViewController
{
    ECPhoneNumberFormatter *numberformatter;
}
@property (weak, nonatomic) IBOutlet UIButton *submitButton;


@property (strong, nonatomic) NSString  *serveyId;
@property(strong,nonatomic) NSString *status;
@property (strong, nonatomic) NSString  *boothId;
@property (strong,nonatomic)NSString *requestType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submittButtonHieghtConstraints;


@end
