//
//  ReplyListViewController.h
//  BJP
//
//  Created by toyaj on 10/20/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ReplyListViewController : BaseViewController
@property(nonatomic,strong)NSArray *categoryArray;
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@end
