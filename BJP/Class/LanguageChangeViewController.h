//
//  LanguageChangeViewController.h
//  BJP
//
//  Created by toyaj on 8/31/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"

@interface LanguageChangeViewController : UIViewController

@property(strong, nonatomic) UIButton *radiobutton1;
@property(strong, nonatomic) UIButton *radiobutton2;



@property (weak, nonatomic) IBOutlet UIButton *radio_Hindi;
@property (weak, nonatomic) IBOutlet UIButton *radio_English;
//- (IBAction)radio_Hindi:(id)sender;
//- (IBAction)radio_English:(id)sender;
//-(void)radiobuttonSelected:(id)sender;
+(void)setLangaugeUtils;
@end
