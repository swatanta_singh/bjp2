//
//  DetlsViewController.h
//  BJP
//
//  Created by swatantra on 8/29/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#define ZOOMPOINT -60
#import "Service.h"
#import "AppHelper.h"
#import "Defines.h"
#import "BaseViewController.h"
@interface DetlsViewController : BaseViewController
@property(weak,nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *tableHeader;
@property(nonatomic,strong)NSMutableDictionary *servicDict;
@property (strong,nonatomic) IBOutlet UIImageView *iv;
@property (strong,nonatomic)IBOutlet NSLayoutConstraint *topCon;
@end
