//
//  MagzineViewC.m
//  BJP
//
//  Created by PDSingh on 9/15/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "MagzineViewC.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "ContentViewController.h"
#import "UIImageView+AFNetworking.h"
#import "ContactMenuCollectionViewCell.h"
#import "ContactContainCollectionViewCell.h"
#import "PresentionHandler.h"

@interface MagzineViewC ()<UIDocumentInteractionControllerDelegate>
{
    NSUInteger selectIndex;
}

@property (strong, nonatomic) PresentionHandler *presentHandler;
@property (strong, nonatomic)NSArray *menuArray;
@property (strong, nonatomic)NSURL *fileURLPath;
@property(nonatomic,strong)NSArray *newsArray;
@property(nonatomic,strong)UIDocumentInteractionController *documentController;
@property (weak, nonatomic) IBOutlet UICollectionView *headerMenuCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *containerCollectionView;

@end

@implementation MagzineViewC

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
        [super viewWillAppear:animated];
    
        [self httpRequestForCategoryCompletionHandler:^(id result) {
            NSDictionary *tempDict=[self.menuArray firstObject];
            [self httpNewsRequest:tempDict[@"id"]];
        }];
   
    
    __weak MagzineViewC *weekSelf = self;
    NSString *titleText = NSLocalizedString(@"Magazine", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"menu" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
             [[AppHelper sharedInstance]setLoaderText:@"Please"];
            [[[AppHelper sharedInstance]menuViewController] setUpMove];
        }
        else{
           // [weekSelf.view endEditing:YES];
        }
    }];
}


#pragma mark - UICollection view
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.menuArray.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView.tag==1001)
    {
        ContactMenuCollectionViewCell *cell = (ContactMenuCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ContactMenuCollectionViewCell" forIndexPath:indexPath];
        cell.menuLable.tag = indexPath.row;
        
        NSDictionary *catagryDict=[self.menuArray objectAtIndex:indexPath.row];
        [cell.menuLable setText:[AppHelper nullCheck:catagryDict[@"title"]]];

        
        if (indexPath.row == selectIndex) {
            
            [cell.selectedLable setHidden:NO];
//            cell.menuLable.textColor=[UIColor colorWithRed:246.0f/255.0f green:129.0f/255.0f blue:33.0f/255.0f alpha:1.0];
//            cell.selectedLable.backgroundColor =[UIColor colorWithRed:246.0f/255.0f green:129.0f/255.0f blue:33.0f/255.0f alpha:1.0];
            
            cell.menuLable.textColor=[UIColor whiteColor];
            cell.selectedLable.backgroundColor =[UIColor whiteColor];

        }
        else
        {
            cell.menuLable.textColor=[UIColor darkGrayColor];
            [cell.selectedLable setHidden:YES];
        }
        return cell;
   }
    if (collectionView.tag==100)
    {
        ContactContainCollectionViewCell *cell = (ContactContainCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"MenuCollectionViewCell" forIndexPath:indexPath];
             return cell;
    }
    else
    {
//          ContactContainCollectionViewCell *cell = (ContactContainCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ContactContainCollectionViewCell" forIndexPath:indexPath];
//          UICollectionView *collectionView = (UICollectionView*)[cell.contentView viewWithTag:100];
//          [collectionView reloadData];
        ContactContainCollectionViewCell *cell = (ContactContainCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ContactContainCollectionViewCell" forIndexPath:indexPath];
        UITableView *tempTable = (UITableView*)[cell.contentView viewWithTag:100];
        tempTable.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
        [tempTable reloadData];

        return cell;

    }
    
}

//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
//{
////    if(kind == UICollectionElementKindSectionHeader)
////    {
////        Header *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerTitle" forIndexPath:indexPath];
////        //modify your header
////        return header;
////    }
////    
////    else
////    {
//    
//        UICollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footter" forIndexPath:indexPath];
//        //modify your footer
//        return footer;
//   // }
//}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView.tag==1001)
    {
      return CGSizeMake(_headerMenuCollection.frame.size.width/2, _headerMenuCollection.frame.size.height);
    }
    else if (collectionView.tag==100)
        return CGSizeMake(_containerCollectionView.frame.size.width/3-10
                          , _containerCollectionView.frame.size.height/3);
        else
        return CGSizeMake(_containerCollectionView.frame.size.width, _containerCollectionView.frame.size.height);
    
}

- (CGSize)widthOfString:(NSString *)string withFont:(UIFont *)font {
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size];
}


- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (_headerMenuCollection == collectionView)
    {
        if (selectIndex != indexPath.row)
        {
            selectIndex = indexPath.row;
            [_containerCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
            [self menuModel];
            
        }
        [_headerMenuCollection scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        
    }
//    else
//    {
//        if([AppHelper appDelegate].checkNetworkReachability)
//        {
//            [[AppHelper sharedInstance]showIndicator];
//           // [self downloadPDF];
//        }
//    }
//    
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (selectIndex<self.menuArray.count) {
        
        if (_containerCollectionView == scrollView)
        {
            NSArray *arr=[self.containerCollectionView indexPathsForVisibleItems];
            
            if(arr.count){
                NSIndexPath *path=[arr firstObject];
                if (selectIndex != path.row)
                {
                    selectIndex = path.row;
                    
                    [self menuModel];
                    [_headerMenuCollection scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
                }
            }
        }
        
    }
}

#pragma mark- TableView
#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([self.newsArray count]>0 )
    {
        numOfSections  = 1;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"No PDF documents to display.";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [self.newsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"NewsCell" forIndexPath:indexPath];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cellYouAvailable setSelectedBackgroundView:bgColorView];

    NSDictionary *newsDictionary=[self.newsArray objectAtIndex:indexPath.row];
    UIImageView *imageV=[cellYouAvailable.contentView viewWithTag:1001];
    
    NSString *strImg=[AppHelper nullCheck:newsDictionary[@"lowPhotoUrl"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    
    
    UILabel *newsType=[cellYouAvailable.contentView viewWithTag:1002];
    UILabel *newsTitle=[cellYouAvailable.contentView viewWithTag:1003];
    UILabel *newsDate=[cellYouAvailable.contentView viewWithTag:1004];
    UIButton *viewPDFFileButton=[cellYouAvailable.contentView viewWithTag:1005];
    viewPDFFileButton.tag=indexPath.row;
    [viewPDFFileButton addTarget:self action:@selector(openPDF:) forControlEvents:UIControlEventTouchUpInside ];


    newsType.text=[[AppHelper nullCheck:newsDictionary[@"magazine_name"]]capitalizedString];
    newsTitle.text=[AppHelper nullCheck:newsDictionary[@"description"]];
    
    double timeStamp=[newsDictionary[@"createAt"] doubleValue];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd MMM, yyyy HH:mm";
    NSDate *sdate = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    newsDate.text=[NSString stringWithFormat:@"Published on: %@",[dateFormatter stringFromDate:sdate]];

    return cellYouAvailable;
}


-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
-(NSString *)normalizePDfPath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if([AppHelper appDelegate].checkNetworkReachability)
//    {
//        [[AppHelper sharedInstance]showIndicator];
//        NSDictionary *newsDictionary=[self.newsArray objectAtIndex:indexPath.row];
//        [self downloadPDF:[self normalizePDfPath:newsDictionary[@"pdf_url"]] index:indexPath.row];
//    }
//    
//}

-(void)openPDF:(UIButton *)sender{
    
    NSDictionary *newsDictionary=[self.newsArray objectAtIndex:sender.tag];
    NSString *url=[self normalizePDfPath:[AppHelper nullCheck:newsDictionary[@"pdf_url"]]];
    NSString *filePath = [self filePathUrl:url];
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath]){
       
        if([AppHelper appDelegate].checkNetworkReachability){
            [[AppHelper sharedInstance]setLoaderText:@"Download"];
            [[AppHelper sharedInstance]showIndicator];
            [self downloadPDF:url];
        }

    }
    else
    {
        self.fileURLPath = [NSURL fileURLWithPath:filePath];
        if (self.fileURLPath) {
            self.documentController = [UIDocumentInteractionController interactionControllerWithURL:self.fileURLPath];
            [self.documentController setDelegate:self];
            [self.documentController presentPreviewAnimated:YES];
            
        }
        else{
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Invalid url resource found" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }
        
    }
    
}

#pragma mark - Reload News

-(void)menuModel
{
    [UIView animateWithDuration:0.1
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
    [self.headerMenuCollection reloadData];
    [self.containerCollectionView reloadData];
    if (self.menuArray && [self.menuArray count]>0) {
        NSDictionary *tempDict=[self.menuArray objectAtIndex:selectIndex];
        [self httpNewsRequest:tempDict[@"id"]];
    }
}

-(void)httpNewsRequest:(NSString *)categoryId
{
    
    if([AppHelper appDelegate].checkNetworkReachability){
        
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=@"1" ;
        parameter[@"categoryId"] = categoryId;
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kNewsMagazine];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            self.newsArray=nil;
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject){
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200){
                if ([responseObject objectForKey:@"list"] && [[responseObject objectForKey:@"list"] isKindOfClass:[NSArray class]]) {
                    self.newsArray = [responseObject objectForKey:@"list"];
                }
            };
            [self.containerCollectionView reloadData];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            self.newsArray=nil;
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            [self.containerCollectionView reloadData];
        }];
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}
#pragma mark - NKJPagerViewDelegate

-(void)httpRequestForCategoryCompletionHandler:(void (^)(id result))completionHandler
{
    
    if([AppHelper appDelegate].checkNetworkReachability){
        [[AppHelper sharedInstance]showIndicator];
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@",BaseUrl,kCategoryMagazine];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=@"1";
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([responseObject objectForKey:@"list"] && [[responseObject objectForKey:@"list"] isKindOfClass:[NSArray class]]) {
                    self.menuArray = [responseObject objectForKey:@"list"];
                }
                
                [self.headerMenuCollection reloadData];
                completionHandler(self.menuArray);
            }
            [self.containerCollectionView reloadData];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [self.containerCollectionView reloadData];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
}


#pragma mark - Download PDF

-(void)downloadPDF:(NSString *)urlString {
    
    urlString =  [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
   
    NSURL *url=nil;
    if ([urlString.lowercaseString hasPrefix:@"http://"]) {
        url = [NSURL URLWithString:urlString];
    } else {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",urlString]];
    }
    
    [self saveDocuments:urlString withResourceURl:url];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
            
            NSURL *documentsDirectoryPath = [NSURL fileURLWithPath:[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject]];
            self.fileURLPath = [documentsDirectoryPath URLByAppendingPathComponent:[response suggestedFilename]];
            return self.fileURLPath;
            
        } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (self.fileURLPath) {
               
                self.documentController = [UIDocumentInteractionController interactionControllerWithURL:self.fileURLPath];
                [self.documentController setDelegate:self];
                [self.documentController presentPreviewAnimated:YES];
            }
            else
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Invalid url resource found" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
        }];
        [downloadTask resume];

}

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return self;
}

-(void)saveDocuments:(NSString *)filePathUrl withResourceURl:(NSURL *)url
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *filePath = [self filePathUrl:filePathUrl];
        NSData *pdfData = [[NSData alloc] initWithContentsOfURL:url];
        [pdfData writeToFile:filePath atomically:YES];
    });
}

-(NSString *)filePathUrl:(NSString *)url
{
    NSArray *parts = [url componentsSeparatedByString:@"/"];
    NSString *filename = [parts lastObject];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *fileName=filename;
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    return filePath ;
}


@end
