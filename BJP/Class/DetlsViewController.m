//
//  DetlsViewController.m
//  BJP
//
//  Created by swatantra on 8/29/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "DetlsViewController.h"
#import "UIImageView+AFNetworking.h"
#import "CommentViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "UILabel+HTMLText.h"
#import "AFNetworking.h"


@interface DetlsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *shadowImage;

- (IBAction)commentButtonClick:(id)sender;
- (IBAction)likeButtonClick:(id)sender;
- (IBAction)shareButtonClick:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *dateLable;
@property (weak, nonatomic) IBOutlet UILabel *likeLable;
@property (weak, nonatomic) IBOutlet UILabel *NewsType;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic)  NSMutableAttributedString *strTest;
@property(nonatomic,strong)NSMutableDictionary *detailsDict;

@end

@implementation DetlsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
//    self.topCon = [NSLayoutConstraint constraintWithItem:self.tableHeader attribute:NSLayoutAttributeTop relatedBy:0 toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:ZOOMPOINT/2.0];
//    [self.tableHeader addConstraint:[NSLayoutConstraint constraintWithItem:self.tableHeader attribute:NSLayoutAttributeHeight relatedBy:0 toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:self.tableHeader.frame.size.height - ZOOMPOINT*1.5]];
//    [self.view addConstraint:self.topCon];
//    [self.view layoutIfNeeded];
    
  
}

-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self httpDetailsRequest];
    
    [self.likeButton setSelected:[_servicDict[@"myLike"] boolValue]];
    NSString *strImg=[AppHelper nullCheck:_servicDict[@"imgUrl"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [self.iv setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    
  
    [self updateLikeButton];
    self.NewsType.text=[AppHelper nullCheck:_servicDict[@"categoryTitle"]];
    self.dateLable.text=[AppHelper nullCheck:_servicDict[@"date"]];
    
    [super viewWillAppear:animated];
    __weak DetlsViewController *weekSelf=self;
    //[self.headerView.headerImageView setHidden:YES];
     [self.headerView.headerImageView setImage:[UIImage imageNamed:@"RectangleFlip"]];
    
    NSString *titleText = NSLocalizedString(@"Details", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

-(void)updateLikeButton
{
    if ([_servicDict[@"likesCount"]intValue]==0)
        self.likeLable.text=@"";
    else
        self.likeLable.text=[NSString stringWithFormat:@"👍 Likes %@",_servicDict[@"likesCount"]];
}
#pragma mark table view
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //NSLog(@"%f",scrollView.contentOffset.y );
   // if (scrollView.contentOffset.y < 0 && scrollView.contentOffset.y > ZOOMPOINT) {
//        self.topCon.constant = ZOOMPOINT/2.0 - scrollView.contentOffset.y/2.0;
//     if (scrollView.contentOffset.y <= ZOOMPOINT) {
//        self.iv.transform = CGAffineTransformMakeScale(1 - (scrollView.contentOffset.y - ZOOMPOINT)/200, 1 - (scrollView.contentOffset.y - ZOOMPOINT)/200);
//    }
//    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;//SCREEN_HEIGHT-50;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    return 300.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *  myCell = [tableView dequeueReusableCellWithIdentifier:@"DetailNews" forIndexPath:indexPath];
    UILabel *lblname = (UILabel*)[myCell.contentView viewWithTag:101];
    UILabel *lblnameD = (UILabel*)[myCell.contentView viewWithTag:102];
   
    UIWebView *webView = (UIWebView*)[myCell.contentView viewWithTag:110];
    lblname.text=nil;
    lblnameD.text=nil;
    webView.hidden=YES;
    
    lblname.text = [[AppHelper nullCheck:self.servicDict[@"itemTitle"]]capitalizedString];
    [lblnameD setHTMLString: [self.detailsDict objectForKey:@"itemContent"]];
    return myCell;
}


#pragma mark button Action

-(void)httpDetailsRequest
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=[NSNumber numberWithInt:1];
        parameter[@"itemId"]=self.servicDict[@"id"];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kNESWDETAILS];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            if (!responseObject)
            {
                //[AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
               // return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([responseObject objectForKey:@"items"] && [[responseObject objectForKey:@"items"] isKindOfClass:[NSDictionary class]]) {
                    self.detailsDict = [responseObject objectForKey:@"items"];
                    [self.tableView reloadData];
                }
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}



- (IBAction)commentButtonClick:(id)sender {
    
    CommentViewController *expandAndCollaps=(CommentViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"CommentViewController"];
    expandAndCollaps.servicDict = self.servicDict;
    [self.navigationController pushViewController:expandAndCollaps animated:YES];
}

- (IBAction)likeButtonClick:(id)sender {
    
    if ([AppHelper appDelegate].cureentUser.accountId) {
         [self httpSubmitRequest];
    }
    else
    {
        UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LoginViewController"];
        [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
    }

}

-(void)httpSubmitRequest
{

    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"itemId"]=[NSNumber numberWithInt:[self.servicDict[@"id"]intValue]];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGet_Like_Submit];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
      
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            NSString *errorMsg=@"";
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                [self.likeButton setSelected:[[responseObject objectForKey:@"myLike"] boolValue]];
                _servicDict[@"likesCount"]=[responseObject objectForKey:@"likesCount"];
                [self updateLikeButton];
            }
            else{
                errorMsg=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
            }
            if(errorMsg.length>3){
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}


- (IBAction)shareButtonClick:(id)sender {
    
    
    NSString * message = [[AppHelper nullCheck:self.servicDict[@"itemTitle"]]capitalizedString];;
    // UIImage *shareImage = [UIImage imageNamed:@"AppIcon"];
    
    NSArray *excludeActivities = @[
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    //   activityVC.excludedActivityTypes = excludeActivities;
    NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/app/up-bjp/id1148583016?ls=1&mt=8"];
    NSArray * shareItems = @[message, url];
    UIActivityViewController * activityViewController = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    activityViewController.excludedActivityTypes = excludeActivities;
    [self presentViewController:activityViewController animated:YES completion:nil];

}

//- (void)webViewDidFinishLoad:(UIWebView *)webView stringWithHTMLText:(NSString *)htmlNewsDetailsString
//{
//    NSString *string = [webView stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"body\").offsetHeight;"];
//    CGFloat height = [string floatValue] + 8;
//    CGRect frame = [webView frame];
//    frame.size.height = height;
//    [webView setFrame:frame];
//    
//}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView
{
    CGFloat height = [[aWebView stringByEvaluatingJavaScriptFromString:@"document.height"] floatValue];
    CGFloat width = [[aWebView stringByEvaluatingJavaScriptFromString:@"document.width"] floatValue];
    aWebView.frame = CGRectMake(aWebView.frame.origin.x,aWebView.frame.origin.y,width, height);
}
@end
