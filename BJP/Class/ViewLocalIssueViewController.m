//
//  ViewLocalIssueViewController.m
//  BJP
//
//  Created by swatantra on 12/16/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "ViewLocalIssueViewController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "ImageController.h"
#import "DateFormatters.h"
#import "DataPickerViewController.h"
#import "PresentionHandler.h"
#import "DatePickerViewController.h"
#import "FilterViewController.h"
#import "IssueDtlViewController.h"
@interface ViewLocalIssueViewController ()
@property(nonatomic,strong)NSArray *arrData;
@property (weak, nonatomic) IBOutlet UITableView *tbleView;
@property (strong, nonatomic) PresentionHandler *presentHandler;


@end

@implementation ViewLocalIssueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self getCategory];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   
}
-(void)refreshTable {
    
   self.arrData=nil;
    [self getCategory];
   }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)filterButtonAction:(UIButton*)sender {
    
    FilterViewController* objVerifyView=(FilterViewController*) [AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"FilterViewController"];
    objVerifyView.providerData=self;
    //
    float height=450;
    self.presentHandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objVerifyView fromPresentingViewController:self withStyle:UIModalPresentationCustom fromView:sender andPreferedSize:CGSizeMake(SCREEN_WIDTH-50, height)];
    
    [objVerifyView getFilterDataWithComilation:self.dictFilter withCompile:^(NSDictionary *dictData) {
        if(dictData)
            self.dictFilter=dictData;
        else
            self.dictFilter=nil;
      
        [self getCategory];
    }];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *myCell=[tableView dequeueReusableCellWithIdentifier:@"MyTask" forIndexPath:indexPath];
    UILabel *lblName=(UILabel *)[myCell.contentView viewWithTag:101];
     UILabel *lblCat=(UILabel *)[myCell.contentView viewWithTag:102];
     UILabel *lblLOC=(UILabel *)[myCell.contentView viewWithTag:103];
    UILabel *lblDist=(UILabel *)[myCell.contentView viewWithTag:104];
    UILabel *lblCreted=(UILabel *)[myCell.contentView viewWithTag:105];
    UIImageView *imageV=(UIImageView *)[myCell.contentView viewWithTag:100];
    NSDictionary *dict=[self.arrData objectAtIndex:indexPath.row];
    lblName.text=[AppHelper nullCheck:[[dict valueForKey:@"createdBy"] valueForKey:@"fullName"]];
      lblCat.text=[AppHelper nullCheck:[dict valueForKey:@"category"]];
      lblLOC.text=[AppHelper nullCheck:[dict valueForKey:@"address"]];
    NSString *strImg=[AppHelper nullCheck:[[dict valueForKey:@"createdBy"] valueForKey:@"imgUrl"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];

    NSDate *strDate=[AppHelper returnDateFromUnixTimeStampString:[self.arrData[indexPath .row]valueForKey:@"createAt"]];
    lblCreted.text=[NSString stringWithFormat:@"Created Date: %@",[formatter stringFromDate:strDate]];
    lblDist.text =[NSString stringWithFormat:@"Distance: %@ KM                                                                                                                                                                                                                                                                                                                                                                                                                             ",[dict valueForKey:@"distance"]];
    return myCell;
}
-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    IssueDtlViewController *expandAndCollaps=(IssueDtlViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"IssueDtlViewController"];
    expandAndCollaps.dictIssues =self.arrData[indexPath.row];
    [[AppHelper navigationController] pushViewController:expandAndCollaps animated:YES];
}
#pragma mark services
-(void)getCategory{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        //clientId
        parameter[@"clientId"]=@"1";
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
         parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"cat_id"]=@"0";
      
        if([AppHelper nullCheck:[AppHelper userDefaultsForKey:K_LATITUDE]]){
            parameter[@"latitude"]=[AppHelper userDefaultsForKey:K_LATITUDE];
            parameter[@"longitude"]=[AppHelper userDefaultsForKey:K_LONGITUDE];
        }
        if(self.dictFilter){
            parameter[@"cat_id"]=self.dictFilter[@"cat_id"];;
            parameter[@"min_distance"]=self.dictFilter[@"min_distance"];
            parameter[@"max_distance"]=self.dictFilter[@"max_distance"];;
            if([self.dictFilter[@"max_distance"]intValue]==51){
                 parameter[@"max_distance"]=@"1000";
            }
            parameter[@"latitude"]=self.dictFilter[@"latitude"];;
            parameter[@"longitude"]=self.dictFilter[@"longitude"];;
            if ([self.dictFilter[@"byMe"]integerValue]==1){
                 parameter[@"byMe"]=[AppHelper appDelegate].cureentUser.accountId;
            }
            
        }
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_ISSUELIST];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                  self.arrData=nil;
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                self.arrData=[responseObject valueForKey:@"list"];
                 [self.tbleView reloadData];
            }
            
            else{
                self.arrData=nil;
                [self.tbleView reloadData];
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}

@end
