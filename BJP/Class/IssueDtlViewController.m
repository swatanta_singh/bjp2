//
//  IssueDtlViewController.m
//  BJP
//
//  Created by swatantra on 12/20/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "IssueDtlViewController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "ImageController.h"
#import "DateFormatters.h"
#import "DataPickerViewController.h"
#import "PresentionHandler.h"
#import "DatePickerViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface IssueDtlViewController (){
    UIToolbar*   numberToolbar;
    NSMutableArray *arrData;
    NSMutableDictionary *dictLocation;
}
@property(nonatomic,strong)NSArray *arrCategory;
@property(nonatomic,strong)PresentionHandler *presnthandler;
@property (weak, nonatomic) IBOutlet UIButton *btnAttacmnt;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITextView *txtView;
- (IBAction)submitbuttonClick:(id)sender;
- (IBAction)attachmentButtonClick:(id)sender;
- (IBAction)CategoryButtonClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@end

@implementation IssueDtlViewController
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    dictLocation=[NSMutableDictionary new];
    [self getLocationdata];
    // Do any additional setup after loading the view.
    arrData=[NSMutableArray new];
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    numberToolbar.items = @[flexibleItem,[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor darkGrayColor];
    [numberToolbar setBackgroundColor:[UIColor lightGrayColor]];
    self.txtView.inputAccessoryView = numberToolbar;
    if(self.dictIssues){
        self.btnSubmit.hidden=YES;
        self.btnAttacmnt.hidden=YES;
        self.txtView.editable=NO;
        self.lblCategory.text=[AppHelper nullCheck:self.dictIssues[@"category"]];
        self.txtView.text=[AppHelper nullCheck:self.dictIssues[@"description"]];
    }
    [arrData addObjectsFromArray:self.dictIssues[@"media"]];
    
    self.lblUserName.text=[AppHelper nullCheck:[[self.dictIssues valueForKey:@"createdBy"] valueForKey:@"fullName"]];
    NSString *strImg=[AppHelper nullCheck:[[self.dictIssues valueForKey:@"createdBy"] valueForKey:@"imgUrl"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [self.imgUser setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
        __weak IssueDtlViewController *weekSelf=self;
        [self setUpHeaderWithTitle:@"Local Issue" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
            if(navigateValue==1){
                [  weekSelf.navigationController popViewControllerAnimated:YES];
            }
        }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - uicollectionview View
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [arrData count];
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    UIImageView *imgeView=(UIImageView *)[cell.contentView viewWithTag:100];
    UIButton *btnD=(UIButton *)[cell.contentView viewWithTag:102];
    btnD.hidden=YES;
    [btnD addTarget:self action:@selector(delteContact:) forControlEvents:UIControlEventTouchUpInside];
    cell.tag=indexPath.row;
    NSString *strImg=[AppHelper nullCheck:[[arrData objectAtIndex:indexPath.row] valueForKey:@"img_url"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [imgeView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    //imgeView.image=arrData[indexPath.row];
    return cell;
}
-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell=[collectionView cellForItemAtIndexPath:indexPath];
    UIImageView *imgeView=(UIImageView *)[cell.contentView viewWithTag:100];

    ImageController *expandAndCollaps=(ImageController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ImageController"];
    expandAndCollaps.image =imgeView.image;
    [self presentViewController:expandAndCollaps animated:YES completion:nil];
}

#pragma mark - button action
-(IBAction)CategoryButtonClick:(UIButton*)sender{
    [self initialiseTheListViewWithDataSource:self.arrCategory withField:self.lblCategory
                                selectedArray:nil
                              selectionChoice:YES
                                   senderRect:sender SelectionType:SEL_CATEGORY];
}
-(void)delteContact:(UIButton*)btn{
    UICollectionViewCell *cel=(UICollectionViewCell*)[btn.superview superview];

    [arrData removeObjectAtIndex:cel.tag];
    [self.collectionView reloadData];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==100){
        [[AppHelper navigationController] popViewControllerAnimated:YES];
        
    }
}
- (IBAction)submitbuttonClick:(id)sender {
    
    
    if([self.lblCategory.text isEqualToString:@"All"]){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Please enter Category." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else if(self.txtView.text.length==0){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Please enter details." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else if(arrData.count==0){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Please select attachment." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    else{
        [self createuAssignment];
    }
}
- (IBAction)attachmentButtonClick:(id)sender {
    if(arrData.count==5){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"you can upload maximum 5 photos." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        return;
    }
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
            // There is not a camera on this device, so don't show the camera button.
            
        }
        else{
            [AppHelper showAlertViewWithTag:10 title:APP_NAME message:@"Camera not available." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        }
        // Camera button tapped.
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Library button tapped.
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        // [self launchSpecialController];
        
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}
- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    // imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}
#pragma mark - Text view
- (void)textViewDidBeginEditing:(UITextView *)textView{
    [AppHelper setupViewAtUp:120 toview:self.view];
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    [AppHelper setupViewAtUp:0 toview:self.view];
}
#pragma mark UIImagePickerControllerDelegate ------------


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    [arrData addObject:image];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.collectionView reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma  mark service
-(void)createuAssignment{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        NSMutableDictionary *parameters=[NSMutableDictionary new];
        [parameters addEntriesFromDictionary:dictLocation];
        parameters[@"clientId"]=@"1";
        parameters[@"description"]=self.txtView.text;
        parameters[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameters[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;//[AppHelper appDelegate].cureentUser.accountId;
        parameters[@"latitude"]=[AppHelper userDefaultsForKey:K_LATITUDE];
        parameters[@"longitude"]=[AppHelper userDefaultsForKey:K_LONGITUDE];
        parameters[@"category_id"]=[NSString stringWithFormat:@"%ld",(long)self.lblCategory.tag];
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kSET_LOCALISSUE];
        [[AppHelper sharedInstance]showIndicator];
        
        
        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
        
        AFHTTPRequestOperation *op = [manager POST:baseURL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //do not put image inside parameters dictionary as I did, but append it!
            int i=1;
            for(UIImage *img in arrData){
                NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
                [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"uploaded_file%d",i] fileName:@"photo.jpg" mimeType:@"image/jpeg"];
                i++;
            }
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Submitted Successfully.." delegate:self cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
        [op start];
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}

-(void)initialiseTheListViewWithDataSource:(NSArray *)dataSource withField:(UILabel*)txtField selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    [self.view endEditing:YES];
    // Initialise DataPickerViewController object with datasorce, selected datasource , isSigles selection criteria ,and selected array call back block -- >
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];
    
    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            txtField.text=[NSString stringWithFormat:@"%@",[[selectedvalues firstObject] valueForKey:@"title"]];
            txtField.tag=[[[selectedvalues firstObject] valueForKey:@"id"] integerValue];
        }
    }];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];
    
}

#pragma mark location data
-(void)getLocationdata{
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:[self.dictIssues[@"latitude"] floatValue] longitude:[self.dictIssues[@"longitude"] floatValue]]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  if (placemark) {
                      
                      
                      self.lblLocation.text= [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      
                      dictLocation[@"address"]=[AppHelper nullCheck:placemark.subLocality];
                      dictLocation[@"zip_code"]=[AppHelper nullCheck:placemark.postalCode];
                      dictLocation[@"city"]=[AppHelper nullCheck:placemark.locality];
                      dictLocation[@"district"]=[AppHelper nullCheck:placemark.subAdministrativeArea];
                      dictLocation[@"state"]=[AppHelper nullCheck:placemark.administrativeArea];
                      
                  }
                  else {
                  }
              }
     ];
}
@end
