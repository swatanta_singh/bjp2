//
//  FilterViewController.h
//  BJP
//
//  Created by swatantra on 12/19/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewLocalIssueViewController.h"
typedef void(^FilterDataValue)(NSDictionary *dictData);

@interface FilterViewController : UIViewController
@property (nonatomic,copy)FilterDataValue filterData;
-(void)getFilterDataWithComilation:(NSDictionary*)dictDat withCompile :(FilterDataValue)filterData;
@property (weak, nonatomic) ViewLocalIssueViewController *providerData;
@end
