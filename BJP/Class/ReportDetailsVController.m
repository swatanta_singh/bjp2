//
//  ReportDetailsVController.m
//  BJP
//
//  Created by PDSingh on 1/3/17.
//  Copyright © 2017 swatantra. All rights reserved.
//

#import "ReportDetailsVController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "PresentionHandler.h"
#import "DataPickerViewController.h"


@interface ReportDetailsVController ()
{
    NSDictionary *nameAddress;
    NSDictionary *selectedAddress;

    BOOL isSelectedBaseMenu;
}
@property (weak, nonatomic) IBOutlet UILabel *baseDropDownMenuButtons;
@property (weak, nonatomic) IBOutlet UILabel *locationName;
@property (weak, nonatomic) IBOutlet UILabel *countPending;
@property (weak, nonatomic) IBOutlet UILabel *countCompleted;
@property (weak, nonatomic) IBOutlet UILabel *countTotalLable;
@property (weak, nonatomic) IBOutlet UILabel *dropDownLable;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabelTotal;
@property (weak, nonatomic) IBOutlet UILabel *bottomTitleLable;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabelVerfied;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabelNotVerfied;
@property (weak, nonatomic) IBOutlet UILabel *bottomTitleDetailsLable;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic,strong) PresentionHandler *presnthandler;

@property (strong,nonatomic)NSMutableArray *arrData;
@property (strong,nonatomic)NSArray *arrTopLevel;
@property (strong,nonatomic)NSMutableArray *arrDisplayLevel;
@property (strong,nonatomic)NSArray *subMenuArray;


- (IBAction)dropDowonSelectButton:(id)sender;

@end

@implementation ReportDetailsVController


- (void)viewDidLoad {
    [super viewDidLoad];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    isSelectedBaseMenu =YES;
    [self setValueOnCompletePending];
    [super viewWillAppear:animated];
    __weak ReportDetailsVController *weekSelf = self;
    
   // NSString *titleText = NSLocalizedString(@"Booth", nil);
    [self setUpHeaderWithTitle:@"Voter Survey Report" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if (navigateValue==1) {
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
     self.locationName.text = _locationValue[@"location"];
}


#pragma mark - UICollectionview View

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.self.arrDisplayLevel  count];
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *  myCell ;
    myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    myCell.tag=indexPath.row;
    NSDictionary *dict =[self.arrDisplayLevel  objectAtIndex:indexPath.row];
    UILabel *lblName=(UILabel *)[myCell.contentView viewWithTag:1001];

    UIButton *btnAction =(UIButton*)[myCell.contentView viewWithTag:1002];
    [btnAction addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    lblName.text=[NSString stringWithFormat:@"%@",dict[@"title"]];
    
    UILabel *lblPlaceHolderLable=(UILabel *)[myCell.contentView viewWithTag:1003];
    NSDictionary *dictPlace =[self.arrTopLevel  objectAtIndex:indexPath.row];
    lblPlaceHolderLable.text=[NSString stringWithFormat:@"Select %@",dictPlace[@"title"]];

    return myCell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize size = CGSizeMake(SCREEN_WIDTH-10,55);
    return size;
}


-(void)btnAction:(UIButton*)btn{
    
    UICollectionViewCell *cel=(UICollectionViewCell*)[btn.superview.superview superview];
    [self pickerDataSource:cel withsender:btn];
}

#pragma mark -  Drop down

-(void)pickerDataSource:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    if([self.arrDisplayLevel count]>0)
    {
        
        if (mycell.tag > 0)
        {
            nameAddress = self.arrDisplayLevel[mycell.tag-1];
            if (![nameAddress[@"selected"]boolValue]) {
                  [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[NSString stringWithFormat:@"Please select %@",nameAddress[@"title"]] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return;
            }
        }
        else
             nameAddress = self.locationValue;
        
        isSelectedBaseMenu=NO;
        selectedAddress = self.arrDisplayLevel [mycell.tag];
        
         self.bottomTitleDetailsLable.text=@"";
        [self getSubFilterLableSender:mycell fromObejct:nameAddress toDictionary:selectedAddress];
    }
    
}

- (IBAction)dropDowonSelectButton:(id)sender {
    
    if([self.arrTopLevel count]>0)
    {
        isSelectedBaseMenu=YES;
        [self pickerDataSource:self.arrTopLevel withField:sender selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_REPORTLEVEL];
    }

}

-(void)pickerDataSource:(NSArray *)dataSource withField:(UICollectionViewCell*)myCell selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    
    [self.view endEditing:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];
    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            
            [self resetLable];
            if (!isSelectedBaseMenu)
            {
                NSMutableDictionary *temp=[[selectedvalues firstObject] mutableCopy];
                temp[@"selected"]=[NSNumber numberWithBool:YES];
                [self.arrDisplayLevel replaceObjectAtIndex:myCell.tag withObject:temp];
                self.bottomTitleLable.text=[AppHelper nullCheck:temp[@"title"]];
                [self.collectionView reloadData];
                
                if (myCell.tag == [self.arrDisplayLevel count]-1) {
                    NSDictionary *selectDictionary=[self.arrDisplayLevel lastObject];
                   
                    if ([selectDictionary[@"selected"]boolValue]) {
                        [self getBottomDetailsLable:selectDictionary];
                    }
                }
            }
            else
            {
                self.dropDownLable.text = [[selectedvalues  firstObject]valueForKey:@"title"];
                nameAddress =[selectedvalues firstObject];
                [self getFilterLable];
            }
            
        }
    }];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];
}





#pragma mark - HTTP REQUEST

-(void)getFilterLable{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        NSString *baseURL=nil;
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        
        parameter[@"frm_level_id"] = self.locationValue[@"frm_level_id"];
        parameter[@"to_location_id"] = self.locationValue[@"to_location_id"];
        parameter[@"to_level_id"] = nameAddress[@"to_level_id"];

        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        baseURL = [NSString stringWithFormat:@"%@%@",BaseUrl,KSelectNewTaskLevels];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if (responseObject [@"level_list"] ) {
                    self.arrDisplayLevel = [responseObject [@"level_list"] mutableCopy];
                    for (NSDictionary *object in self.arrDisplayLevel )
                    {
                        NSMutableDictionary *tempDict=[object mutableCopy];
                        tempDict[@"selected"]=[NSNumber numberWithBool:NO];
                    }
                    
                
                }
            }
            [self.collectionView reloadData];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}

-(void)getBottomDetailsLable:(NSDictionary *)lastObjectToDisplayDetails{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        NSString *baseURL=nil;
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        
        parameter[@"frm_level_id"] = lastObjectToDisplayDetails[@"frm_level_id"];
        parameter[@"to_location_id"] = lastObjectToDisplayDetails[@"to_location_id"];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        baseURL = [NSString stringWithFormat:@"%@%@",BaseUrl,KNewVoterSurveyDetail];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if (responseObject [@"data"] ) {
                    NSDictionary *serveyData=responseObject [@"data"];
                    self.bottomLabelNotVerfied.text=[NSString stringWithFormat:@"%@",serveyData [@"notverified"]];
                    self.bottomLabelVerfied.text=[NSString stringWithFormat:@"%@",serveyData [@"verified"]];
                    self.bottomLabelTotal.text=[NSString stringWithFormat:@"%@",serveyData [@"total"]];
                }
               
            }
            else
            {
                self.bottomTitleDetailsLable.text=[NSString stringWithFormat:@"%@",[AppHelper nullCheck:responseObject [@"error_description"]]];
            }

            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}

-(void)resetLable
{
    self.bottomLabelNotVerfied.text=@"0";
    self.bottomLabelVerfied.text=@"0";
    self.bottomLabelTotal.text=@"0";
}
-(void)getSubFilterLableSender:(UICollectionViewCell *)sender  fromObejct:(NSDictionary *)fromObject toDictionary:(NSDictionary *)toObject{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        NSString *baseURL=nil;
        
      //  [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        
        parameter[@"frm_level_id"] = fromObject[@"frm_level_id"];
        parameter[@"to_location_id"] = fromObject[@"to_location_id"];
        
        
        if ([toObject[@"to_level_id"]length]>0) {
            parameter[@"to_level_id"] = toObject[@"to_level_id"];
        }
        else
            parameter[@"to_level_id"] = toObject[@"frm_level_id"];
            
        
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        baseURL = [NSString stringWithFormat:@"%@%@",BaseUrl,KTaskReportLevel];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([responseObject [@"list"] count]>0) {
                    
                    self.subMenuArray = responseObject [@"list"];
                    [self pickerDataSource:self.subMenuArray withField:sender selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_REPORTLEVEL];
                }
            }
            else
            {
                self.bottomTitleDetailsLable.text=[NSString stringWithFormat:@"%@",[AppHelper nullCheck:responseObject [@"error_description"]]];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}

-(void)setValueOnCompletePending
{
    if (self.locationValue ) {
        
        if ([self.locationValue [@"levels"]count]>0)
        {
            [self.baseDropDownMenuButtons setHidden:NO];
            self.arrTopLevel=self.locationValue [@"levels"];
             nameAddress = [self.arrTopLevel firstObject];
            self.dropDownLable.text=@"Select level";
        }
        else
        {
            [self.baseDropDownMenuButtons setHidden:YES];
           self.dropDownLable.text=@"NA";
        }
        
        
        self.countPending.text=[NSString stringWithFormat:@"%@",self.locationValue [@"notverified"]];
        self.countCompleted.text=[NSString stringWithFormat:@"%@",self.locationValue [@"verified"]];
        self.countTotalLable.text=[NSString stringWithFormat:@"%@",self.locationValue [@"total"]];
    }
}
@end
