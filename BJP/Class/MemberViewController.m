//
//  MemberViewController.m
//  BJP
//
//  Created by swatantra on 9/19/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "MemberViewController.h"
#import "Service.h"
#import "AppHelper.h"
#import "Defines.h"
#import "DataPickerViewController.h"
#import "PresentionHandler.h"
#import "DatePickerViewController.h"
#import "DateFormatters.h"
#import "AFNetworking.h"
#import "ECPhoneNumberFormatter.h"
#import "UIImageView+AFNetworking.h"
#import "FindLocalityCityVController.h"

@interface MemberViewController (){
    
    NSMutableArray *arrData;
    UIToolbar* numberToolbar;
    ECPhoneNumberFormatter *numberformatter;
    NSInteger is_member;
}
@property(nonatomic,strong)PresentionHandler *presnthandler;
@property(nonatomic,strong)NSDictionary *dictData;
@property(weak,nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@end

@implementation MemberViewController
#pragma mark  view
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
    
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.contentInset.top, 0, keyboardSize.height, 0);
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.contentInset.top, 0, 0, 0);
}
- (void)viewDidLoad {
   
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    numberformatter = [[ECPhoneNumberFormatter alloc] init];
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    numberToolbar.items = @[flexibleItem,[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor darkGrayColor];
    [numberToolbar setBackgroundColor:[UIColor lightGrayColor]];
    self.btnSave.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    arrData=[NSMutableArray new];
    [self getDataProfile];

}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    __weak MemberViewController *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"Membership", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
        else{
           // [weekSelf coverPhotoChange:nil];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpData{
    
    [arrData removeAllObjects];
// for persnol info
    NSMutableArray *arrPersnaol=[NSMutableArray new];
    NSMutableArray *arrContact=[NSMutableArray new];
    NSMutableArray *arrAddres=[NSMutableArray new];
    for (int i=0; i<3; i++) {
        NSMutableDictionary *dict=[NSMutableDictionary new];
        switch (i) {
            case 0:{
                dict[@"value"]=[AppHelper nullCheck:self.dictData[@"fullname"]];
                dict[@"place"]=@"NAME : ";
                dict[@"type"]=@"2";
            }break;
            case 1:{
                if([[AppHelper nullCheck:self.dictData[@"sex"]] isEqualToString:@"2"]){
                    dict[@"value"]=@"Male";
                }
                else     if([[AppHelper nullCheck:self.dictData[@"sex"]] isEqualToString:@"1"]){
                    dict[@"value"]=@"Female";
                }
                
                dict[@"place"]=@"GENDER : ";
                dict[@"type"]=@"1";
            }break;
            case 2:{
                dict[@"value"]=[AppHelper nullCheck:self.dictData[@"dateOfBirth"]];
                dict[@"place"]=@"DATE OF BIRTH : ";
                dict[@"type"]=@"1";
            }break;
                
            default:
                break;
        }
        [arrPersnaol addObject:dict];
    }
    
    //contactInfo
    for (int i=0; i<3; i++) {
        NSMutableDictionary *dict=[NSMutableDictionary new];
        switch (i) {
            case 0:{
                dict[@"value"]=[AppHelper nullCheck:self.dictData[@"mobile"]];
                dict[@"place"]=@"MOBILE 1 : ";
                dict[@"type"]=@"2";

            }break;
            case 1:{
                dict[@"value"]=[AppHelper nullCheck:self.dictData[@"mobile"]];
                dict[@"place"]=@"MOBILE 2 : ";
                dict[@"type"]=@"0";
            }break;
            case 2:{
                dict[@"value"]=[AppHelper nullCheck:self.dictData[@"email"]];
                dict[@"place"]=@"EMAIL : ";
                dict[@"type"]=@"0";
            }break;
                
            default:
                break;
        }
        [arrContact addObject:dict];
    }

    //Adress
    //contactInfo
    for (int i=0; i<5; i++) {
        NSMutableDictionary *dict=[NSMutableDictionary new];
        switch (i) {
            case 0:{
                dict[@"value"]=[AppHelper nullCheck:self.dictData[@"pincode"]];
                dict[@"place"]=@"PINCODE :";
                dict[@"type"]=@"0";
                 dict[@"pincode_id"]=[AppHelper nullCheck:self.dictData[@"pincode_id"]];
                
            }break;
            case 1:{
                dict[@"value"]=[AppHelper nullCheck:self.dictData[@"district"]];
                dict[@"place"]=@"DIST :";
                dict[@"type"]=@"2";
            }break;
            case 2:{
                dict[@"value"]=[AppHelper nullCheck:self.dictData[@"state"]];
                dict[@"place"]=@"STATE :";
                dict[@"type"]=@"2";
            }break;
            case 3:{
                dict[@"value"]=[AppHelper nullCheck:self.dictData[@"house_no"]];
                dict[@"place"]=@"PLOT / FLAT  NUMBER :";
                dict[@"type"]=@"0";
            }break;
            case 4:{
                dict[@"value"]=[AppHelper nullCheck:self.dictData[@"locality"]];
                 dict[@"lat"]=[AppHelper nullCheck:self.dictData[@"latitude"]];
                 dict[@"long"]=[AppHelper nullCheck:self.dictData[@"longitude"]];
                dict[@"place"]=@"LOCALITY :";
                dict[@"type"]=@"1";
            }break;
            default:
                break;
        }
        [arrAddres addObject:dict];
    }
    [arrData addObject:arrPersnaol];
    [arrData addObject:arrContact];
    [arrData addObject:arrAddres];
    [self.tableView reloadData];
}

#pragma mark table view
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  30.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init];
    
    NSString *strR;
    switch (section) {
        case 0:
            strR= @"PERSONAL INFO";
            break;
        case 1:
            strR= @"CONTACT INFO";
            break;
        case 2:
            strR= @"ADDRESS";
            break;
        default:
            break;
    }

    myLabel.frame = CGRectMake(10, 8, 320, 20);
    myLabel.font = [UIFont boldSystemFontOfSize:15];
    myLabel.text = [[self tableView:tableView titleForHeaderInSection:section]capitalizedString];
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];
    
    return headerView;
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *strR;
    switch (section) {
        case 0:
            strR= @"PERSONAL INFO";
            break;
        case 1:
            strR= @"CONTACT INFO";
            break;
        case 2:
            strR= @"ADDRESS";
            break;
        default:
            break;
    }
    return strR;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if ([view isKindOfClass:[UITableViewHeaderFooterView class]]) {
        UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)view;
        headerView.contentView.backgroundColor = [UIColor clearColor];
        headerView.backgroundView.backgroundColor = [UIColor clearColor];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return arrData.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[arrData objectAtIndex:section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *  myCell ;
  NSDictionary *dict = [[arrData objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        myCell = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
        UITextField *txtFild = (UITextField*)[myCell.contentView viewWithTag:101];
        UILabel *lblname = (UILabel*)[myCell.contentView viewWithTag:102];
        UIButton *btnPick = (UIButton*)[myCell.contentView viewWithTag:103];
        [btnPick addTarget:self action:@selector(pickerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        txtFild.delegate=self;
        txtFild.enabled=NO;
        btnPick.hidden=YES;
        lblname.alpha=1;
        btnPick.enabled=YES;
        txtFild.inputAccessoryView = numberToolbar;
        
        txtFild.keyboardType=UIKeyboardTypeEmailAddress;
        txtFild.autocapitalizationType = UITextAutocapitalizationTypeWords;
        txtFild.text = [AppHelper nullCheck:dict[@"value"]];
        lblname.text = [AppHelper nullCheck:dict[@"place"]];
        
        if ([[AppHelper nullCheck:dict[@"place"]] isEqualToString:@"MOBILE 1 : "] || [[AppHelper nullCheck:dict[@"place"]] isEqualToString:@"MOBILE 2 : "])
        {
            txtFild.enabled=NO;
            txtFild.text = [numberformatter stringForObjectValue:[AppHelper nullCheck:dict[@"value"]]];
            txtFild.keyboardType=UIKeyboardTypePhonePad;
        }
    if ([[AppHelper nullCheck:dict[@"place"]] isEqualToString:@"PINCODE :"])
    {
        txtFild.keyboardType=UIKeyboardTypeNumberPad;
    }
    
    
    if([dict[@"type"] isEqualToString:@"1"] ){
        btnPick.hidden=NO;
    }
    else if([dict[@"type"] isEqualToString:@"0"]){
             txtFild.enabled=YES;
    }
    
    myCell.tag=indexPath.row;
      myCell.contentView.tag=indexPath.section;
    if(is_member==1){
        txtFild.enabled=NO;;
         btnPick.hidden=YES;
    }
    return myCell;
}
#pragma mark textfield
- (void)textViewDidEndEditing:(UITextView *)textView{
    // [AppHelper setupViewAtUp:0 toview:self.view];
    UITableViewCell *mycell=(UITableViewCell*)[textView.superview superview];
    
    NSMutableDictionary *dict=[[arrData objectAtIndex:mycell.contentView.tag] objectAtIndex:mycell.tag];
    dict[@"value"]=textView.text;
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    UITableViewCell *mycell=(UITableViewCell*)[textField.superview superview];
    NSMutableDictionary *dict=[[arrData objectAtIndex:mycell.contentView.tag] objectAtIndex:mycell.tag];
    dict[@"value"]=textField.text;
    if([[AppHelper nullCheck:dict[@"place"]] isEqualToString:@"PINCODE :"]){
        if(textField.text.length==6){
            [self getPincode:textField.text withCell:mycell];
        }
        else{
            NSPredicate *prd=[NSPredicate predicateWithFormat:@"place == %@",@"DIST :"];
            NSArray *Arr=[[arrData objectAtIndex:mycell.contentView.tag] filteredArrayUsingPredicate:prd];
            if(Arr.count){
              dict[@"value"]=@"";
            }
            prd=[NSPredicate predicateWithFormat:@"place == %@",@"STATE :"];
           Arr=[[arrData objectAtIndex:mycell.contentView.tag] filteredArrayUsingPredicate:prd];
            if(Arr.count){
                dict[@"value"]=@"";
            }

            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Pincode is not vailid." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        }
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    UITableViewCell *cell=(UITableViewCell*)[textField.superview superview];
    
    NSIndexPath *path=[self.tableView indexPathForCell:cell];
    if(path.section==2 && path.row==0){
        if (textField.text.length >= 6 && range.length == 0)
            {
                
                return NO; // return NO to not change text
            }
     
    }
    else   if(path.section==1 && path.row==0){
        if (textField.text.length >= 14 && range.length == 0)
        {

            return NO; // return NO to not change text
        }
        
    }
    else   if(path.section==1 && path.row==1){
        if (textField.text.length >= 14 && range.length == 0)
        {
            
            return NO; // return NO to not change text
        }
        
    }
    if ((path.section==1 && path.row==0) || (path.section==1 && path.row==1)) {
        textField.text = [numberformatter stringForObjectValue:textField.text];
    }

       return YES;
    
}

#pragma mark Button Action
-( BOOL )validateUserInputs
{
    NSString *errorMessage=nil;
    bool IsValid=YES;
    NSDictionary  *dict=[[arrData objectAtIndex:1] objectAtIndex:2];
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];

    // personal info
    if([[[[arrData objectAtIndex:0] objectAtIndex:1] valueForKey:@"value"] length]<=1){
        [AppHelper showAlertViewWithTag:0 title:nil message:@"Please select the gender." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        IsValid= NO;

    }
    else if([[[[arrData objectAtIndex:0] objectAtIndex:2] valueForKey:@"value"] length]<=1){
        [AppHelper showAlertViewWithTag:0 title:nil message:@"Please select the date of birth." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        IsValid= NO;

    }
//    else if([[[[arrData objectAtIndex:1] objectAtIndex:2] valueForKey:@"value"] length]<=1){
//        [AppHelper showAlertViewWithTag:0 title:nil message:@"Please enter a valid email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        IsValid= NO;
//
//    }
//    else  if(![emailTest evaluateWithObject:dict[@"value"]])
//    {
//        errorMessage=@"Please enter valid email";
//        [AppHelper showAlertViewWithTag:3 title:nil message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        errorMessage=@"";
//        IsValid= NO;
//    }
    else if([[[[arrData objectAtIndex:2] objectAtIndex:0] valueForKey:@"pincode_id"] length]<=1){
        [AppHelper showAlertViewWithTag:0 title:nil message:@"Please enter a valid pincode." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        IsValid= NO;
        
    }
//    else if([[[[arrData objectAtIndex:2] objectAtIndex:3] valueForKey:@"value"] length]<=1){
//        [AppHelper showAlertViewWithTag:0 title:nil message:@"Please enter a Plot / Flat number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        IsValid= NO;
//        
//    }
//    else if([[[[arrData objectAtIndex:2] objectAtIndex:4] valueForKey:@"value"] length]<=1){
//        [AppHelper showAlertViewWithTag:0 title:nil message:@"Please enter a Locality." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        IsValid= NO;
//        
//    }
    
    
    return IsValid;
}

-(IBAction)saveButtonAction:(UIButton*)sender{
    
    if(is_member==0)
    {
       if(![self validateUserInputs])
          return;
       else{
        [self uyploadDataProfile];
        [self.tableView reloadData];
        }
     
    }
}
-(void)pickerButtonAction:(UIButton*)sender{
        UITableViewCell *mycell=(UITableViewCell*)[sender.superview superview];
        switch (mycell.tag) {
            case 1:
                [self pickerForGender:mycell withsender:sender];
                break;
            case 2:
                [self openDatePicker:mycell button:sender];
                break;
                case 4:
                [self setLocality:mycell];
                break;
            default:
                break;
        }
    
}
-(void)setLocality:(UITableViewCell*)myCell{
    NSMutableDictionary *dict=[[arrData objectAtIndex:myCell.contentView.tag] objectAtIndex:myCell.tag];
    __weak MemberViewController *weekSelf=self;

        FindLocalityCityVController* objVerifyView= (FindLocalityCityVController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"FindLocalityCityVController"];
        [objVerifyView getAddressBlock:^(id address) {
            dict[@"value"]=address;
            [weekSelf.tableView reloadData];
        }];
        
        self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objVerifyView fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:myCell andPreferedSize:DropDownSize];
    
}
-(void)openDatePicker:(UITableViewCell*)myCell button:(UIButton*)btn{

    
    [self.view endEditing:YES];
    __weak MemberViewController *weekSelf=self;
    
    DatePickerViewController *datePickerViewController = (DatePickerViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"DatePickerViewController"];
    NSMutableDictionary *dict=[[arrData objectAtIndex:myCell.contentView.tag] objectAtIndex:myCell.tag];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:datePickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:btn andPreferedSize:DropDownSize];
    NSDate *minDate=[NSDate dateWithTimeIntervalSinceNow:-60*365*24*60*60] ;
    NSDate *maxDate=[NSDate dateWithTimeIntervalSinceNow:-18*365*24*60*60] ;
    NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];
    
    [datePickerViewController initDatePickerWithDatePickerMode:UIDatePickerModeDate minDate:minDate maxDate:maxDate andDefaultSelectedDate:maxDate withReturnedDate:^(NSDate *selectedDate) {
        if (selectedDate) {
            NSString *strDatte = [formatter stringFromDate:selectedDate];
            dict[@"value"]   =  strDatte;
        }
        [weekSelf.tableView reloadData];
    }];
    
}

-(void)pickerForGender:(UITableViewCell*)mycell withsender:(UIButton*)sender{
   
    NSArray *selectedArr = @[@"Male",@"Female"];
    [self pickerDataSource:selectedArr withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_NONE];
}
-(void)pickerDataSource:(NSArray *)dataSource withField:(UITableViewCell*)myCell selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
   
    [self.view endEditing:YES];
    __weak MemberViewController *weekSelf=self;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];
    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            
                NSMutableDictionary *dir=[[arrData objectAtIndex:myCell.contentView.tag] objectAtIndex:myCell.tag];
                NSString *value=[selectedvalues firstObject];
                dir[@"value"] = value;
               [weekSelf.tableView reloadData];
        }
    }];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];
}

#pragma mark services
-(void)getDataProfile
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_MEMBER];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        NSDictionary *dict=nil;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:nil];
        NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        dict = @{@"body":json};
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
           
            NSDictionary *dict=(NSDictionary *)responseObject;
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            else if ([[responseObject valueForKey:@"error_code"] integerValue]==200)

            {
                self.dictData=[responseObject valueForKey:@"data"];
                is_member=[[responseObject valueForKey:@"is_member"] integerValue];
                if([[responseObject valueForKey:@"is_member"] integerValue]==0){
                      [self.btnSave setTitle:NSLocalizedString(@"Submit", nil) forState:UIControlStateNormal];
                }
                else{
        [self.btnSave setTitle:[NSString stringWithFormat:@"Membership request Id : %@",self.dictData[@"membership_req_id"]] forState:UIControlStateNormal];
                }
                [self setUpData];

            }
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}
-(void)getPincode:(NSString*)pincCode withCell:(UITableViewCell*)myCell
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_PINCODE];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
//        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
//        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"pincode"]=pincCode;
        NSDictionary *dict=nil;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:nil];
        NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        dict = @{@"body":json};
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            NSDictionary *dict=(NSDictionary*)responseObject;
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            else if ([[responseObject valueForKey:@"error_code"] integerValue]==200)
            {
                NSDictionary*dict=[responseObject valueForKey:@"data"];
                NSMutableDictionary *dictData=[[arrData objectAtIndex:myCell.contentView.tag] objectAtIndex:myCell.tag];
                dictData[@"pincode_id"]=[AppHelper nullCheck:dict[@"pincodeId"]];
                dictData=[[arrData objectAtIndex:myCell.contentView.tag] objectAtIndex:myCell.tag+1];
                dictData[@"value"]=[AppHelper nullCheck:dict[@"district"]];
                dictData=[[arrData objectAtIndex:myCell.contentView.tag] objectAtIndex:myCell.tag+2];
                dictData[@"value"]=[AppHelper nullCheck:dict[@"state"]];
                [self.tableView reloadData];
            }
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}
- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==100){
        [[AppHelper navigationController] popViewControllerAnimated:YES];
        
    }
}
-(void)uyploadDataProfile
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUPLOADMEMBER];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"clientId"]=@"1";
        parameter[@"dob"]= [AppHelper nullCheck:[[[arrData objectAtIndex:0] objectAtIndex:2] valueForKey:@"value"]];
        parameter[@"mobile"]= [AppHelper nullCheck:[[[arrData objectAtIndex:1] objectAtIndex:1] valueForKey:@"value"]];
        parameter[@"email"]= [AppHelper nullCheck:[[[arrData objectAtIndex:1] objectAtIndex:2] valueForKey:@"value"]];
        
        parameter[@"pincode"]= [AppHelper nullCheck:[[[arrData objectAtIndex:2] objectAtIndex:0] valueForKey:@"value"]];
        parameter[@"pincodeId"]= [AppHelper nullCheck:[[[arrData objectAtIndex:2] objectAtIndex:0] valueForKey:@"pincode_id"]];
        parameter[@"house_no"]= [AppHelper nullCheck:[[[arrData objectAtIndex:2] objectAtIndex:3] valueForKey:@"value"]];
        parameter[@"locality"]= [AppHelper nullCheck:[[[arrData objectAtIndex:2] objectAtIndex:4] valueForKey:@"value"]];
        
        CLLocationCoordinate2D homeCordinate=[self geoCodeUsingAddress:[AppHelper nullCheck:[[[arrData objectAtIndex:2] objectAtIndex:4] valueForKey:@"value"]]];
        parameter[@"latitude"]=[NSString stringWithFormat:@"%f",homeCordinate.latitude];
        parameter[@"longitude"]=[NSString stringWithFormat:@"%f",homeCordinate.longitude];
        parameter[@"sex"]=@"1";
        if([[AppHelper nullCheck:[[[arrData objectAtIndex:0] objectAtIndex:1] valueForKey:@"value"]] isEqualToString:@"Male"]){
            parameter[@"sex"]=@"2";
        }

        NSDictionary *dict=nil;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:nil];
        NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        dict = @{@"body":json};
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            else if ([[responseObject valueForKey:@"error_code"] integerValue]==200)
            {
               [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Submitted successfully." delegate:self cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}

@end
