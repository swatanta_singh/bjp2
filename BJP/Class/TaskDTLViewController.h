//
//  TaskDTLViewController.h
//  BJP
//
//  Created by swatantra on 11/16/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "BaseViewController.h"

@interface TaskDTLViewController : BaseViewController
@property(nonatomic,strong)NSDictionary *dictData;
@property(nonatomic,strong)NSString *serviceType;

@end
