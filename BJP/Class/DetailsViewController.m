//
//  DetailsViewController.m
//  BJP
//
//  Created by swatantra on 12/29/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()
@property (weak, nonatomic) IBOutlet UITextView *txtview;

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.txtview.text=self.strDtl;
    
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
 
        self.txtview.text=nil;
        
        NSString * htmlString = self.strDtl;;
        NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [attrStr length])];
        [attrStr addAttribute:NSFontAttributeName value:self.txtview.font range:NSMakeRange(0, [attrStr length])];
        //add alignment
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setAlignment:NSTextAlignmentLeft];
        [attrStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attrStr.length)];
        self.txtview.attributedText = attrStr;
  

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
