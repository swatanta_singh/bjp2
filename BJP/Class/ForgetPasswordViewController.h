//
//  ForgetPasswordViewController.h
//  3O Seconds
//
//  Created by Swatantra Singh on 18/01/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ForgetPasswordViewController : BaseViewController{
    
   
}
@property(nonatomic,strong)NSString* mobilenumber;
- (IBAction)doneButtonClick:(id)sender;

@end
