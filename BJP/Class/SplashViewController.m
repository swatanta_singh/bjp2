//
//  SplashViewController.m
//  BJP
//
//  Created by swatantra on 9/1/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "SplashViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+animatedGIF.h"
#import "AppHelper.h"
#import "Defines.h"
#import "Service.h"

@interface SplashViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblBJP;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSplas;
@property (weak, nonatomic) IBOutlet UIImageView *imgAnimated;

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"asdf" withExtension:@"gif"];
    self.imgAnimated.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    // Do any additional setup after loading the view.
    [UIView animateWithDuration:2 animations:^{
    } completion:^(BOOL finished) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.5* NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //   UIViewController *aViewController2=nil;
                      [self.view removeFromSuperview];
        });
    }];
    // Do any additional setup after loading the view.
}
-(void )finalMethod{
    [self.imgViewSplas.layer removeAllAnimations];
}
-(void)viewWillAppear:(BOOL)animated{
    //UIViewKeyframeAnimationOptionAutoreverse
     /* self.lblBJP.alpha=0;
        [UIView animateWithDuration:3 delay:1 options: 0 animations:^{
            self.imgViewSplas.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5);
             self.lblBJP.alpha=1;
        } completion:^(BOOL finished) {
                    //  self.imgViewSplas.transform = CGAffineTransformIdentity;
                     [self.view removeFromSuperview];
            }];
*/
  //[self performSelector:@selector(finalMethod) withObject:nil afterDelay:<#(NSTimeInterval)#>]
//    [UIView animateWithDuration:0.3/1.5 animations:^{
//        self.imgViewSplas.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.3/2 animations:^{
//             self.imgViewSplas.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
//        } completion:^(BOOL finished) {
//            [UIView animateWithDuration:0.3/2 animations:^{
//                 self.imgViewSplas.transform = CGAffineTransformIdentity;
//            }];
//        }];
//    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
