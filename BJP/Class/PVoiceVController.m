//
//  PVoiceVController.m
//  BJP
//
//  Created by PDSingh on 8/30/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "PVoiceVController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "EventsViewController.h"
#import "DetlsViewController.h"
#import "VoicedetailsVC.h"

@interface PVoiceVController ()
@property(nonatomic,strong)NSArray *eventsArray;
@property (weak, nonatomic) IBOutlet UITableView *tableViewNews;
@end


@implementation PVoiceVController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.tableViewNews.tableFooterView = [[UIView alloc ]initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self httpServeyRequest];

    __weak PVoiceVController *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"Survey", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"menu" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [[[AppHelper sharedInstance]menuViewController] setUpMove];
        }
        else{
            [weekSelf.view endEditing:YES];
        }
    }];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([self.eventsArray count]>0 )
    {
        numOfSections  = 1;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"No update survey";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.eventsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"NewsCell" forIndexPath:indexPath];
    NSDictionary *newsDictionary=[self.eventsArray objectAtIndex:indexPath.row];
    UIImageView *imageV=[cellYouAvailable.contentView viewWithTag:1001];
    UILabel *evntType=[cellYouAvailable.contentView viewWithTag:1003];
    UILabel *eventsTittle=[cellYouAvailable.contentView viewWithTag:1004];
    UILabel *stateNews = [cellYouAvailable.contentView viewWithTag:1010];
    
    NSString *strImg=[AppHelper nullCheck:newsDictionary[@"imgUrl"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    
    evntType.text=[AppHelper nullCheck:newsDictionary[@"surveyState"]];
    eventsTittle.text=[AppHelper nullCheck:newsDictionary[@"surveyTitle"]];
    if ([newsDictionary[@"surveyState"] isEqualToString:@"Present"]) {
        stateNews.text=@"Active";
       // stateNews.textColor=[UIColor colorWithRed:(0/255.f) green:(122/255.f) blue:(255/255.f) alpha:1.0];
    }
    else
    {
         stateNews.text=@"Expired";
    }
    stateNews.textColor= [UIColor colorWithRed:(246/255.f) green:(129/255.f) blue:(33/255.f) alpha:1.0];

    return cellYouAvailable;
}

-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VoicedetailsVC *voicedetailsVC=(VoicedetailsVC*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"VoicedetailsVC"];
    voicedetailsVC.servicDict = [self.eventsArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:voicedetailsVC animated:YES];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 253.0f;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//   // return 253.0f;
//    return UITableViewAutomaticDimension;
//
//}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    /* Return an estimated height or calculate
//     * estimated height dynamically on information
//     * that makes sense in your case.
//     */
//    return 253.0f;
//}

-(void)httpServeyRequest
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=[NSNumber numberWithInt:1];
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];

        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGet_P_Voice];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            NSString *errorMsg;
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([responseObject objectForKey:@"surveys"] && [[responseObject objectForKey:@"surveys"] isKindOfClass:[NSArray class]]) {
                    self.eventsArray = [responseObject objectForKey:@"surveys"];
                }
                
                if (self.eventsArray.count==0)
                    errorMsg=@"No Survey Found";
                
                [self.tableViewNews reloadData];
            }
            else{
                
                NSString *messge=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
                if ([messge length] && [messge isEqualToString:@"Error authorization."]) {
                    errorMsg=@"Please login in app";
                }
                else
                {
                    errorMsg=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
                    
                }
            }
            
            if(errorMsg.length>3){
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}

@end
