//
//  VoicedetailsVC.m
//  BJP
//
//  Created by PDSingh on 8/30/16.
//  Copyright © 2016 swatantra. All rights reserved.
//
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "EventsViewController.h"
#import "DetlsViewController.h"
#import "EventsDetailsVC.h"
#import "DateFormatters.h"
#import "VoicedetailsVC.h"

@interface VoicedetailsVC ()
{
    NSInteger rowId;
}
@property (weak, nonatomic) IBOutlet UIImageView *headerImageV;
@property (weak, nonatomic) IBOutlet UILabel *eventsTitleLable;
@property (weak, nonatomic) IBOutlet UILabel *eventsStatusLable;
@property(nonatomic,strong) NSMutableArray *serveyArray;
@property (weak, nonatomic) IBOutlet UIButton *SybmitButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeightConstraints;
@property (weak, nonatomic) IBOutlet UITableView *tableViewNews;
@property (weak, nonatomic) IBOutlet UILabel *submittedLable;
- (IBAction)submitEevntesDetails:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lableActive;

@property (weak, nonatomic) IBOutlet UILabel *lableDateActive;
@end

@implementation VoicedetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableViewNews.tableFooterView = [[UIView alloc ]initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}

-(void)loadEeventsDetails
{
    self.serveyArray=[NSMutableArray new];
    
    NSString *option1=[AppHelper nullCheck:self.servicDict[@"option1"]];
    NSString *option2=[AppHelper nullCheck:self.servicDict[@"option2"]];
    NSString *option3=[AppHelper nullCheck:self.servicDict[@"option3"]];
    NSString *option4=[AppHelper nullCheck:self.servicDict[@"option4"]];
    NSString *option5=[AppHelper nullCheck:self.servicDict[@"option5"]];
    NSString *option6=[AppHelper nullCheck:self.servicDict[@"option6"]];
    NSString *option7=[AppHelper nullCheck:self.servicDict[@"option7"]];
    NSString *option8=[AppHelper nullCheck:self.servicDict[@"option8"]];
    NSString *option9=[AppHelper nullCheck:self.servicDict[@"option9"]];
    NSString *option10=[AppHelper nullCheck:self.servicDict[@"option10"]];
    
  // [ self.serveyArray addObject:[AppHelper nullCheck:self.servicDict[@"surveyTitle"]]];
    
    if ([option1 length]>0)[self.serveyArray addObject:option1];
    if ([option2 length]>0)[self.serveyArray addObject:option2];
    if ([option3 length]>0)[self.serveyArray addObject:option3];
    if ([option4 length]>0)[self.serveyArray addObject:option4];
    if ([option5 length]>0)[self.serveyArray addObject:option5];
    if ([option6 length]>0)[self.serveyArray addObject:option6];
    if ([option7 length]>0)[self.serveyArray addObject:option7];
    if ([option8 length]>0)[self.serveyArray addObject:option8];
    if ([option9 length]>0)[self.serveyArray addObject:option9];
    if ([option10 length]>0)[self.serveyArray addObject:option10];

    [ self.serveyArray addObject:[AppHelper nullCheck:self.servicDict[@"surveyDesc"]]];

//    NSString *strImg2=[AppHelper nullCheck:self.servicDict[@"previewImgUrl"]];
//    NSURL *url2 = [NSURL URLWithString:[self normalizePath:strImg2]];
//    [self.headerImageV setImageWithURL:url2 placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    NSString *strImg=[AppHelper nullCheck:self.servicDict[@"imgUrl"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [self.headerImageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    
    self.eventsStatusLable.text=[AppHelper nullCheck:self.servicDict[@"surveyState"]];
    if ([[AppHelper nullCheck:self.servicDict[@"surveyState"]] isEqualToString:@"Present"]) {
        self.lableActive.text=@"Active";
       }
    else
    {
        self.lableActive.text=@"Expired";
    }
    self.eventsTitleLable.text=[AppHelper nullCheck:self.servicDict[@"surveyTitle"]];
    self.lableDateActive.text=[AppHelper nullCheck:self.servicDict[@"date"]];
    
    if ([self.servicDict[@"userSubmitted"]intValue ]==1) {
        [self.submittedLable setHidden:NO];
    }
    else
    {
        [self.submittedLable setHidden:YES];
    }
    
    rowId=[self.servicDict[@"optionId"]intValue]-1;
    [self.tableViewNews reloadData];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self loadEeventsDetails];
    __weak VoicedetailsVC *weekSelf=self;
    
  //  NSString *title=[[AppHelper nullCheck:self.servicDict[@"surveyTitle"]]capitalizedString];
    [self setUpHeaderWithTitle:@"Survey" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
        else{
            [weekSelf.view endEditing:YES];
            
        }
    }];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.serveyArray count];
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cellYouAvailable ;
    
        cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"DetailsCell" forIndexPath:indexPath];
        UILabel *eventsDesc=[cellYouAvailable.contentView viewWithTag:1001];
    
    UILabel *tickLable=[cellYouAvailable.contentView viewWithTag:1002];

    if (rowId == indexPath.row) {
        [tickLable setHidden:NO];
    }
    else
         [tickLable setHidden:YES];

        NSString * htmlString = [self.serveyArray objectAtIndex:indexPath.row];
        
        NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [attrStr length])];
        [attrStr addAttribute:NSFontAttributeName value:eventsDesc.font range:NSMakeRange(0, [attrStr length])];
        //add alignment
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setAlignment:NSTextAlignmentLeft];
        [attrStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attrStr.length)];
        eventsDesc.attributedText = attrStr;
    
      UIImageView *imge=[cellYouAvailable.contentView viewWithTag:2001];
      if (indexPath.row==[self.serveyArray count]-1) {
         [imge setHidden:YES];
      }
      else
      [imge setHidden:NO];
      return cellYouAvailable;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row !=[self.serveyArray count]-1) {
    
     if ([AppHelper appDelegate].cureentUser.accountId) {
         
         if ([self.servicDict[@"userSubmitted"]intValue ]!=1)
         {
             if ( [[AppHelper nullCheck:self.servicDict[@"surveyState"]]isEqualToString:@"Present"])
             {
                 
                 UIAlertController *alertController= [UIAlertController
                                                      alertControllerWithTitle:@"Survey"
                                                      message:@"Are you sure, you want to submit your poll?"
                                                      preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action){
                                                                
                                                                NSString *key=[NSString stringWithFormat:@"%zd",indexPath.row+1];
                                                                [self httpSubmitRequest:key];
                                                                
                                                            }];
                 
                 UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel
                                                                  handler:^(UIAlertAction * action){
                                                                      
                                                                  }];
                 [alertController addAction:noButton];
                 [alertController addAction:ok];
                 [self presentViewController:alertController animated:YES completion:nil];
             }

         }
        else
        {
                 [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Already Submitted" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }
     }
     else
     {
         UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LoginViewController"];
         [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
     }
  }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100.0f;
}
-(void)httpSubmitRequest:(NSString *)key
{

    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=[NSNumber numberWithInt:1];
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"surveyId"]=self.servicDict[@"id"];
        parameter[@"surveyOption"] = key;
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@",BaseUrl,kGet_P_Voice_Submit];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            NSString *errorMsg=@"";
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                errorMsg=@"Submitted Successfully";
            }
            else{
                errorMsg=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
            }
            if(errorMsg.length>3){
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    
    
}


@end
