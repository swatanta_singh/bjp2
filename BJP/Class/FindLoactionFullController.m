//
//  FindLoactionFullController.m
//  KINCT
//
//  Created by PDSingh on 6/20/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import "Defines.h"
#import "AppHelper.h"
#import "FindLoactionFullController.h"

#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"


@interface FindLoactionFullController ()
{
    NSMutableArray *searchLocationData;
    NSMutableArray *googlePlaceArrayLocation;
    
    SPGooglePlacesAutocompleteQuery *query;
    
}

@property (weak, nonatomic) IBOutlet UITableView *searchPlaceTable;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation FindLoactionFullController

- (void)viewDidLoad {
    
    [self initilaizeGoolePlaceApi  ];
    [self.searchBar becomeFirstResponder];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated {
   
    [super viewWillAppear:YES];
    self.automaticallyAdjustsScrollViewInsets = false;
    //__weak FindLoactionFullController *weekSelf=self;
    
//    [self setUpHeaderWithTitle:@"Search Address" withLeftbtn:@"back" withRigthbtn:@"currentlocation" WithComilation:^(int navigateValue) {
//        if (navigateValue==1) {
//        }
//        else
//        {
//            weekSelf.address(@"Current Location");
//            [weekSelf.searchBar resignFirstResponder];
//        }
//        [weekSelf dismissViewControllerAnimated:YES completion:nil];
//
//    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//-(void)viewWillAppear:(BOOL)animated {
//
//    [super viewWillAppear:YES];
//
//     self.automaticallyAdjustsScrollViewInsets = false;
//    __weak FindLocalityCityVController *weekSelf=self;
//
//    [self setUpHeaderWithTitle:@"Find Locality/City" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
//        [weekSelf.navigationController popViewControllerAnimated:YES];
//    }];
//
//}

-(void)initilaizeGoolePlaceApi
{
    query = [[SPGooglePlacesAutocompleteQuery alloc] initWithApiKey:KGOOGLE_PLACE_API_KEY];
    query.radius =3000.0;   // search addresses close to user
    query.types = SPPlaceTypeGeocode;
}


- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([searchLocationData count]) {
        return [searchLocationData objectAtIndex:indexPath.row];
    }
    else
        return  nil;
    
}


#pragma mark- UITable View Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([searchLocationData count]>0) {
        return [searchLocationData count];
    }
    else
        return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell.textLabel setFont:[UIFont fontWithName:@"Proxima Nova" size:18]];
    cell.textLabel.text = [self placeAtIndexPath:indexPath].name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *address = [self placeAtIndexPath:indexPath].name;
    self.address(address);
    [self.searchBar resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



#pragma mark - Search Bar Delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar
{
    
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)theSearchBar
{
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    if ([theSearchBar text]>0)
    {
        //[self performSelectorOnMainThread:@selector(getAddress:) withObject:theSearchBar.text waitUntilDone:YES];
    }
}


//NEW - to handle filtering
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    query.input=searchBar.text;
    [query fetchPlaces:^(NSArray *places, NSError *error)
     {
         searchLocationData=[[NSMutableArray alloc]initWithArray:places];
         [self.searchPlaceTable reloadData];
     }];
}


#pragma mark - Block
-(void)getAddressBlock:(DidSelectLocationblock)address
{
    self.address=address;
}
@end
