//
//  FilterViewController.m
//  BJP
//
//  Created by swatantra on 12/19/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "FilterViewController.h"
#import "TTRangeSlider.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "DataPickerViewController.h"
#import "FindLoactionFullController.h"
@interface FilterViewController (){
    NSMutableDictionary *dictData;
    double latV;
    double longV;
}

@property(nonatomic,strong)NSArray *arrCategory;
@property(nonatomic,strong)PresentionHandler *presnthandler;

- (IBAction)addressButtonClick:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
- (IBAction)categoryButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnAvaiblity;

@property (weak, nonatomic) IBOutlet TTRangeSlider *sliderDistance;
@property (strong, nonatomic)  NSDictionary *dictLocal;
- (IBAction)btnAvailability:(id)sender;
@end

@implementation FilterViewController
#pragma mark filter action
-(void)setInitialLoadingSlider:(TTRangeSlider *)slider min:(float)minValue max:(float)maxValue selMin:(float)smin selMax:(float)smax
{
    slider.delegate = self;
    slider.minValue = minValue;
    slider.maxValue = maxValue;
    slider.selectedMinimum = smin;
    slider.selectedMaximum = smax;
    slider.maxLabel.string =[NSString stringWithFormat:@"%d",(int)smax];
    if(smax==51){
   slider.maxLabel.string =@"max";
    }
}
-(void)getFilterDataWithComilation:(NSDictionary*)dictDat withCompile :(FilterDataValue)filterData{
    
    self.filterData=filterData;
    self.dictLocal = dictDat;
    if(self.dictLocal){
       // self.sliderDistance.selectedMinimum=[self.dictLocal[@"min_distance"]integerValue];
//self.sliderDistance.selectedMaximum=[self.dictLocal[@"max_distance"]integerValue];
        
        [self setInitialLoadingSlider:self.sliderDistance min:0 max:51 selMin:[self.dictLocal[@"min_distance"]intValue] selMax:[self.dictLocal[@"max_distance"]intValue]];
        latV=[self.dictLocal[@"latitude"]doubleValue];
        longV=[self.dictLocal[@"longitude"]doubleValue];
         self.lblCategory.tag=[self.dictLocal[@"cat_id"]integerValue];
        if ([self.dictLocal[@"byMe"]integerValue]==0)
        {
            self.btnAvaiblity.selected=NO;
        }
        else{
            self.btnAvaiblity.selected=YES;
        }
    }
    else{
        [self setInitialLoadingSlider:self.sliderDistance min:0 max:51 selMin:0 selMax:10];
         latV=[[AppHelper userDefaultsForKey:K_LATITUDE]doubleValue];
         longV=[[AppHelper userDefaultsForKey:K_LONGITUDE]doubleValue];
    }
       [self getLocationlData];
}
-(void)getLocationlData{
   
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLLocation *locatoin=[[CLLocation alloc]initWithLatitude:latV longitude:longV];
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:locatoin completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             address =  [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             // NSLog(@"%@",placemark.addressDictionary);
             if ([address length]>0) {
                 self.lblAdd.text =address;
             }
         }
         
     }
     ];

}
#pragma mark view action
- (void)viewDidLoad {
    [super viewDidLoad];
    dictData=[NSMutableDictionary new];
   [self getCategory];
    self.sliderDistance.tag=99;
    // Do any additional setup after loading the view.
}
-(void)viewWillDisappear:(BOOL)animated{
    if(self.providerData.dictFilter==nil){
        [self.providerData refreshTable];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark button action
- (IBAction)applyButtonAction:(id)sender {
   
    dictData[@"max_distance"]=[NSString stringWithFormat:@"%0.1f",self.sliderDistance.selectedMaximum];
    dictData[@"min_distance"]=[NSString stringWithFormat:@"%0.1f",self.sliderDistance.selectedMinimum];
    dictData[@"latitude"]=[NSString stringWithFormat:@"%f",latV];
    dictData[@"longitude"]=[NSString stringWithFormat:@"%0.9f",longV];
    dictData[@"cat_id"]=[NSString stringWithFormat:@"%ld",(long)self.lblCategory.tag];
    
    if(self.btnAvaiblity.selected){
         dictData[@"byMe"]=@"1";
    }
    else{
        dictData[@"byMe"]=@"0";
        
    }
    
    self.filterData(dictData);
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)resetButtonAction:(id)sender {
    self.providerData.dictFilter=nil;
    self.btnAvaiblity.selected=NO;
    latV = [[AppHelper userDefaultsForKey:K_LATITUDE]doubleValue];
    longV = [[AppHelper userDefaultsForKey:K_LONGITUDE]doubleValue];
    [self getLocationlData];
    self.lblCategory.tag=0;
    self.lblCategory.text=@"All";

 [self setInitialLoadingSlider:self.sliderDistance min:0 max:51 selMin:0 selMax:10];
}
- (IBAction)btnAvailability:(id)sender{
    if(self.btnAvaiblity.selected){
          self.btnAvaiblity.selected=NO;
    }
    else{
        self.btnAvaiblity.selected=YES;
    }
}
-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
    sender.maxLabel.string = [NSString stringWithFormat:@"%d",(int)selectedMaximum];

        if (sender.selectedMaximum==51.00){
           // NSLog(@"Standard slider updated. Min Value: %.0f Max Value: %.0f", selectedMinimum, selectedMaximum);
            sender.maxLabel.string =@"max";
        }
    //    else if (sender == self.rangeSliderCurrency) {
    //        NSLog(@"Currency slider updated. Min Value: %.0f Max Value: %.0f", selectedMinimum, selectedMaximum);
    //    }
    //    else if (sender == self.rangeSliderCustom){
    //        NSLog(@"Custom slider updated. Min Value: %.0f Max Value: %.0f", selectedMinimum, selectedMaximum);
    //    }
}

- (IBAction)categoryButtonAction:(id)sender {
    [self initialiseTheListViewWithDataSource:self.arrCategory withField:self.lblCategory
                                selectedArray:nil
                              selectionChoice:YES
                                   senderRect:sender SelectionType:SEL_CATEGORY];
}
- (IBAction)addressButtonClick:(id)sender {
    FindLoactionFullController* objVerifyView= (FindLoactionFullController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"FindLoactionFullController"];
    [objVerifyView getAddressBlock:^(id address) {
       
            CLLocationCoordinate2D coordinate = [self geoCodeUsingAddress:address];
            
            self.lblAdd.text = address;
        latV=coordinate.latitude;
        longV=coordinate.longitude;
        
           }];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objVerifyView fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];


}
- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}

#pragma mark service
-(void)initialiseTheListViewWithDataSource:(NSArray *)dataSource withField:(UILabel*)txtField selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    [self.view endEditing:YES];
    // Initialise DataPickerViewController object with datasorce, selected datasource , isSigles selection criteria ,and selected array call back block -- >
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];
    
    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            txtField.text=[NSString stringWithFormat:@"%@",[[selectedvalues firstObject] valueForKey:@"title"]];
            txtField.tag=[[[selectedvalues firstObject] valueForKey:@"id"] integerValue];
        }
    }];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];
    
}

-(void)getCategory{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        //clientId
        parameter[@"clientId"]=@"1";
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_Category];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                self.arrCategory=[responseObject valueForKey:@"list"];
                NSPredicate *prd=[NSPredicate predicateWithFormat:@"id == %@",[NSString stringWithFormat:@"%ld",(long)self.lblCategory.tag]];

                NSArray *Arr=[self.arrCategory filteredArrayUsingPredicate:prd];
                NSLog(@"%@",Arr);
                if(Arr.count){
                    self.lblCategory.text=[[Arr firstObject] valueForKey:@"title"];
                }
                // [self.tbleView reloadData];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}

@end
