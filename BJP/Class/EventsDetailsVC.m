//
//  EventsDetailsVC.m
//  BJP
//
//  Created by PDSingh on 8/29/16.
//  Copyright © 2016 swatantra. All rights reserved.
//
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "EventsViewController.h"
#import "DetlsViewController.h"
#import "EventsDetailsVC.h"
#import "DateFormatters.h"
#import "UIImageView+AFNetworking.h"
#import "UILabel+HTMLText.h"

@interface EventsDetailsVC ()
{
    BOOL isGoing;
    BOOL notGoing;
}
@property (weak, nonatomic) IBOutlet UIImageView *headerImageV;
@property (weak, nonatomic) IBOutlet UILabel *eventsTitleLable;
@property (weak, nonatomic) IBOutlet UILabel *eventsStatusLable;
@property(nonatomic,strong) NSArray *eventsArray;
- (IBAction)submitEevntesDetails:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *SybmitButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeightConstraints;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonBottomConstraints;
@property (weak, nonatomic) IBOutlet UITableView *tableViewNews;
@end

@implementation EventsDetailsVC
-(void)viewWillAppear:(BOOL)animated{
    
   
    [super viewWillAppear:animated];
    
    isGoing=NO;
    notGoing=YES;
    
    if ((![self.servicDict[@"eventState"] isEqualToString:@"Present"])&&(![self.servicDict[@"eventState"] isEqualToString:@"Future"]))
    {
        [self.SybmitButton setHidden:YES];
        self.buttonBottomConstraints.constant=0;
        self.buttonHeightConstraints.constant=0;
    }
    
    
    [self loadEeventsDetails];
    
    __weak EventsDetailsVC *weekSelf=self;
    [self setUpHeaderWithTitle:@"Events" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
        else{
            [weekSelf.view endEditing:YES];
        }
    }];
}

- (void)viewDidLoad {
    
    [self.SybmitButton setTitle:NSLocalizedString(@"Submit", nil) forState:UIControlStateNormal];

    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)loadEeventsDetails
{

    NSString *strImg=[AppHelper nullCheck:self.servicDict[@"imgUrl"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [self.headerImageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    
    NSString *state= [AppHelper nullCheck:self.servicDict[@"eventState"]];
    if ([state isEqualToString:@"Past"])
        self.eventsStatusLable.text=@"Expired";
    else  if ([state isEqualToString:@"Present"])
        self.eventsStatusLable.text=@"Ongoing";
    else
        self.eventsStatusLable.text=@"Upcoming";
    self.eventsTitleLable.text=[[AppHelper nullCheck:self.servicDict[@"eventTitle"]]capitalizedString];

}

-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}

#pragma mark - Table View

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 if ((![self.servicDict[@"eventState"] isEqualToString:@"Present"])&&(![self.servicDict[@"eventState"] isEqualToString:@"Future"]))
    return 3;
    else
    return 4;
        
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cellYouAvailable ;
    if (indexPath.row==0 ) {
      
        cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"DetailsCell" forIndexPath:indexPath];
        NSString * contents = [AppHelper nullCheck:self.servicDict[@"eventTitle"]];
        [self configureDetailCell:cellYouAvailable WithString:contents];
      
    }
    else  if (indexPath.row==1) {
        
        cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"TimeCell" forIndexPath:indexPath];
        double stimeStamp=[self.servicDict[@"startDate"] doubleValue];
        double etimeStamp=[self.servicDict[@"endDate"] doubleValue];

        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd MMM, yyyy HH:mm";
        NSDate *sdate = [NSDate dateWithTimeIntervalSince1970:stimeStamp];
        NSDate *edate = [NSDate dateWithTimeIntervalSince1970:etimeStamp];

        UILabel *sevntD=[cellYouAvailable.contentView viewWithTag:1001];
        sevntD.text=[dateFormatter stringFromDate:sdate];
        
        UILabel *sevntt=[cellYouAvailable.contentView viewWithTag:2001];
        sevntt.text=[dateFormatter stringFromDate:edate];

        UILabel *venueTittle=[cellYouAvailable.contentView viewWithTag:3001];
        venueTittle.text =[AppHelper nullCheck:self.servicDict[@"eventPlace"]];

    }
    
   else  if (((![self.servicDict[@"eventState"] isEqualToString:@"Present"])&&(![self.servicDict[@"eventState"] isEqualToString:@"Future"])) || (indexPath.row==3))
    {
        
            cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"DetailsCell" forIndexPath:indexPath];
            NSString * contents = [AppHelper nullCheck:self.servicDict[@"eventDesc"]];
           [self configureDetailCell:cellYouAvailable WithString:contents];
        
        
    }
   else if (indexPath.row==2)
   {
       if (([self.servicDict[@"eventState"] isEqualToString:@"Present"])||([self.servicDict[@"eventState"] isEqualToString:@"Future"]))
       {
           
           cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"RadioCell" forIndexPath:indexPath];
           UIButton *on=[cellYouAvailable.contentView viewWithTag:3001];
           [on setSelected:isGoing];
           [on addTarget:self action:@selector(clickRadioButton:) forControlEvents:UIControlEventTouchUpInside];
           if ([on isSelected])
               [on setImage:[UIImage imageNamed:@"On"] forState:UIControlStateNormal];
           else
               [on setImage:[UIImage imageNamed:@"Off"] forState:UIControlStateNormal];
           
           UIButton *off=[cellYouAvailable.contentView viewWithTag:3002];
           [off setSelected:notGoing];
           [off addTarget:self action:@selector(clickRadioButton:) forControlEvents:UIControlEventTouchUpInside];
           
           if ([off isSelected])
               [off setImage:[UIImage imageNamed:@"On"] forState:UIControlStateNormal];
           else
               [off setImage:[UIImage imageNamed:@"Off"] forState:UIControlStateNormal];
       }
     }

    return cellYouAvailable;
}


-(void)configureDetailCell:(UITableViewCell *)cellYouAvailable WithString:(NSString *)contents
{
    UILabel *eventsDesc=[cellYouAvailable.contentView viewWithTag:1001];
    [eventsDesc setHTMLString: contents];
  
}
//#pragma mark - Utils Methods
//-(NSMutableAttributedString *)mutableStringFromString:(NSString *)htmlString lableObject:(UILabel *)lable
//{
//    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
//    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [attrStr length])];
//    [attrStr addAttribute:NSFontAttributeName value:lable.font range:NSMakeRange(0, [attrStr length])];
//    //add alignment
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    [paragraphStyle setAlignment:NSTextAlignmentLeft];
//    [attrStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attrStr.length)];
//    return attrStr;
//
//}
-(void)clickRadioButton:(UIButton*)sender
{
    if ([sender tag]==3001) {
        if (![sender isSelected]) {
            isGoing=YES;
            notGoing=NO;
        }
        
    }
    else if ([sender tag]==3002) {
        if (![sender isSelected]) {
            isGoing = NO;
            notGoing = YES;
        }
    }
    
    [self.tableViewNews reloadData];
}




-(void)httpSubmitRequest
{
    
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=[NSNumber numberWithInt:1];
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"eventId"]=self.servicDict[@"id"];
        
        if (isGoing) {
            parameter[@"participation"]=@"0";
        }
        if (notGoing) {
            parameter[@"participation"]=@"1";
        }
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGet_Submit_Events];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            NSString *errorMsg=@"";

            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
               errorMsg=@"Submitted Successfully";
            }
            else{
                errorMsg=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
            }
            if(errorMsg.length>3){
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}


- (IBAction)submitEevntesDetails:(id)sender {
    
    if ([AppHelper appDelegate].cureentUser.accountId) {
          [self httpSubmitRequest];
    }
    else
    {
        UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LoginViewController"];
        [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
    }
  
}
@end
