//
//  PresentionHandler.h
//  ZoomableScrollViewDemoApp
//
//  Created by Manoj  on 20/05/15.
//  Copyright (c) 2015 Manoj . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class CustomPresentationController;
@interface PresentionHandler : NSObject <UIAdaptivePresentationControllerDelegate,UIPopoverPresentationControllerDelegate,UIViewControllerTransitioningDelegate>

@property (nonatomic,assign) UIModalPresentationStyle presentationStyle;
@property (nonatomic,strong) UIViewController       * presentedViewController;
@property (nonatomic,strong) UIViewController       * presentingViewController;
@property (nonatomic,strong) CustomPresentationController       * pcm;


@property (nonatomic,assign) CGSize preferedSize;
@property (nonatomic,strong) id     senderView;


-(instancetype)initWithViewControllerToBePresented:(UIViewController *)VC fromPresentingViewController:(UIViewController *)pVC withStyle:(UIModalPresentationStyle)style fromView:(id)view andPreferedSize:(CGSize)size;

@end
