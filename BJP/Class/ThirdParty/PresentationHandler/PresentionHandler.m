//
//  PresentionHandler.m
//  ZoomableScrollViewDemoApp
//
//  Created by Manoj  on 20/05/15.
//  Copyright (c) 2015 Manoj . All rights reserved.
//

#import "PresentionHandler.h"

// Custome presentation controller to manage presentation style and presented content size.
@interface CustomPresentationController : UIPresentationController {
    
}

@property(nonatomic,strong)     UIView   *dimmingView; // faded view
@property(nonatomic,strong)     UIView   *pView;
@property(nonatomic ,assign)    CGSize    preferedSize;  // prefered content size. view over diming view
@property(nonatomic,strong)   UIButton *btnClose;    // close btn.

@property(nonatomic ,assign) CGFloat widthPercentage;
@property(nonatomic ,assign) CGFloat heighPercentage;

- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController withPreferedSize:(CGSize)pSize;

@end

@implementation CustomPresentationController

- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController withPreferedSize:(CGSize)pSize {
    
    if (self == [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController]) {
        
        // set prefered size of content view
        self.preferedSize = pSize;
        self.widthPercentage = self.preferedSize.width/[UIScreen mainScreen].bounds.size.width;
        self.heighPercentage = self.preferedSize.height/[UIScreen mainScreen].bounds.size.height;
        // set up dimming view and close button
        self.pView=presentedViewController.view;
    
    }
    //return self after initialisation
    return self;
}

- (void)prepareDimmingView
{
    self.dimmingView   = [[UIView alloc] init];
    [[self dimmingView] setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.4]];
 
    [UIView animateWithDuration:1 animations:^{
           [self.dimmingView setAlpha:1.0];
    }];
     self.btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnClose setTitle:@"✕" forState:UIControlStateNormal];
    self.btnClose.backgroundColor=[UIColor whiteColor];
    self.btnClose.layer.cornerRadius=10.0f;
    self.btnClose.clipsToBounds=YES;
    [self.btnClose setTitleColor:[UIColor colorWithRed:9.0f/255 green:22.0f/255.0 blue:144.0f/255 alpha:1.0] forState:UIControlStateNormal  ];
    //[self.btnClose setImage:[UIImage imageNamed:@"closebtn"] forState:UIControlStateNormal];
    [self.pView addSubview:self.btnClose];
    
    [self.btnClose addTarget:self action:@selector(dimmingViewTapped:) forControlEvents:UIControlEventTouchUpInside];
      [self.containerView insertSubview:self.dimmingView atIndex:0];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dimmingViewTapped:)];
//    [[self dimmingView] addGestureRecognizer:tap];
}


-(void)dimmingViewTapped:(UIButton *)sender {
    [UIView animateWithDuration:0.1 animations:^{
       [self.dimmingView setAlpha:0.0];
    } completion:^(BOOL finished) {
     
          [[self presentedViewController] dismissViewControllerAnimated:YES completion:NULL];
    }];
  
}


- (CGRect)frameOfPresentedViewInContainerView {

    float width  = [UIScreen mainScreen].bounds.size.width*self.widthPercentage;
    float height = [UIScreen mainScreen].bounds.size.height*self.heighPercentage;
    
    return CGRectMake(self.containerView.center.x-width/2, self.containerView.center.y-height/2, width,height);
    
    //return CGRectMake(self.containerView.center.x-self.preferedSize.width/2, self.containerView.center.y-self.preferedSize.height/2, self.preferedSize.width,self.preferedSize.height);
}

- (void)containerViewWillLayoutSubviews
{
    // Before layout, make sure our dimmingView and presentedView have the correct frame
    [self.dimmingView     setFrame:[[self containerView] bounds]];
    [[self presentedView]   setFrame:[self frameOfPresentedViewInContainerView]];
    CGRect rect = [self     frameOfPresentedViewInContainerView];
    [self.btnClose setFrame:CGRectMake(rect.size.width-25,5, 20, 20)];
    self.btnClose.layer.cornerRadius=10.0f;

}

- (void)presentationTransitionWillBegin
{
     [self prepareDimmingView];
}

- (void)dismissalTransitionWillBegin
{
  
}

@end

/////////**********

@implementation PresentionHandler 



-(instancetype)initWithViewControllerToBePresented:(UIViewController *)VC fromPresentingViewController:(UIViewController *)pVC withStyle:(UIModalPresentationStyle)style fromView:(id)view andPreferedSize:(CGSize)size {
    
    if (self = [super init]) {
        
        self.presentationStyle        = style;
        self.presentedViewController  = VC;
        self.presentingViewController = pVC;
        self.senderView               = view;
        self.preferedSize             = size;
        [self preparePresntedViewController];
    }
    return self;
}

-(void)preparePresntedViewController {
    
    self.presentedViewController.modalPresentationStyle               = self.presentationStyle;
    self.presentedViewController.transitioningDelegate      = self;
    
    if (self.presentationStyle== UIModalPresentationPopover) {
        
        UIPopoverPresentationController *obj    = self.presentedViewController.popoverPresentationController;
        obj.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Header"]];
        obj.delegate                            = self;
        obj.sourceView                          = [self.senderView  superview];
        obj.sourceRect                          = [self.senderView  frame];
        obj.permittedArrowDirections            = UIPopoverArrowDirectionAny;
        
        if (!CGSizeEqualToSize(self.preferedSize, CGSizeZero)) {
            self.presentedViewController.preferredContentSize = self.preferedSize;
        }
    
    }else {
        
         UIPresentationController *obj           = self.presentedViewController.presentationController;
         obj.delegate                            = self;
        self.presentedViewController.view.layer.cornerRadius = 5.0f;
        
    }
   
    [self.presentingViewController presentViewController:self.presentedViewController animated:YES completion:nil];
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    
    if (self.presentationStyle== UIModalPresentationPopover) {
        return UIModalPresentationNone;
    }
    return self.presentationStyle;
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    return [[CustomPresentationController alloc] initWithPresentedViewController:self.presentedViewController presentingViewController:self.presentingViewController withPreferedSize:self.preferedSize] ;
}

- (void)popoverPresentationController:(UIPopoverPresentationController *)popoverPresentationController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView **)view{
    UIButton *btn=(UIButton*)self.senderView;
    *rect=btn.frame;
    *view=[self.senderView superview];
}

@end
