//
//  SMWDataPickerViewController.m
//  DynamicForm
//
//  Created by Manoj  on 31/03/14.
//  Copyright (c) 2014 Manoj . All rights reserved.
//

#import "DataPickerViewController.h"
#import "AppHelper.h"
#import "DateFormatters.h"

// macros for max height in orietation modes
@interface DataPickerViewController ()
{
    NSMutableArray *selectedRows;
    NSMutableArray *mainDataSourceArray;
    SelectionOptions selectionType;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightToolbar;
@property (strong, nonatomic)NSArray *dataSourceArray;
@property (weak, nonatomic) IBOutlet UITextField *txtfieldSerch;
@property (weak, nonatomic) IBOutlet UIView *viewForheader;
@end

@implementation DataPickerViewController

#pragma mark  - SMWDataPickerViewController intialiser

- (void)initWitSingleSelectionCriteriaType:(BOOL)isSingleSelection dataSource:(NSArray *)dataSource withSelectedDataSourceRows:(NSArray *)selectedDataSourceRows selectionType:(SelectionOptions)type andSelectedValuesCallBack:(DidSelectItems)selectedItemsCallBack
{
    
    // initialise the instance variables
    self.selectItems             = selectedItemsCallBack;
    self.dataSourceArray              = dataSource;
    mainDataSourceArray              = [[NSMutableArray alloc] initWithArray:dataSource];
   selectedRows                 = [[NSMutableArray alloc] initWithArray:selectedDataSourceRows];
    //previousSelectedRows         = [[NSMutableArray alloc] initWithArray:selectedDataSourceRows];
    self.singleSelectionCriteria = isSingleSelection;
    selectionType = type;
}

#pragma mark  - View life cycle
-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
        
    self.dataTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    [_dataTableView reloadData];
    if(self.singleSelectionCriteria){
        self.heightToolbar.constant=0;
    }
    else{
        self.heightToolbar.constant=44;
    }
    if(selectionType==SEL_MEMB){
        if(self.dataSourceArray.count){
        NSMutableDictionary *dict=[NSMutableDictionary new];
        dict[@"fullname"]=@"All Members";
         dict[@"desg_hi"]=@"All Members";
         dict[@"desg_en"]=@"All Members";
        [mainDataSourceArray insertObject:dict atIndex:0];
        }
        self.dataSourceArray=[NSArray arrayWithArray:mainDataSourceArray];
        
         [_dataTableView reloadData];
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    if(self.singleSelectionCriteria){
        self.heightToolbar.constant=0;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.viewForheader.hidden=YES;
    // manage size of table based on content/rows --- >
    
    // reload table after datasource is initialised -- >
    _dataTableView.delegate      = self;
    _dataTableView.dataSource    = self;
    [_dataTableView reloadData];
    if(self.singleSelectionCriteria){
        self.heightToolbar.constant=0;
    }

    // REMOVE all object object
    if (selectedRows.count && selectedRows.count==1 && [selectedRows containsObject:@""]) {
        [selectedRows removeAllObjects];
    }
    
    
}


#pragma mark  - Table view datasorce

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataSourceArray .count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"DynamicTemplateCell";
    UITableViewCell *cell           = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        [cell setTintColor:[UIColor blackColor]];
        cell.textLabel.numberOfLines=4;
        //cell.selectionStyle=UITableViewCellSelectionStyleDefault;
    }
    
    if(SEL_ON==selectionType){
        //  BioData *master=[self.dataSourceArray  objectAtIndex:indexPath.row];
        //   cell.textLabel.text =[NSString stringWithFormat:@"%@ (%@)",master.bD_NAME,master.bD_RANK_NAME];
    }
      
     else if (selectionType==SEL_COUNTRY)
    {
        cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"title"];
        
    }
     else if (selectionType==SEL_CATEGORY)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]valueForKey:@"title"];
         
     }
    //SEL_HEADOFFAMILY
     else if (selectionType==SEL_HEADOFFAMILY)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]valueForKey:@"type"];
         
     }
     else if (selectionType==SEL_DEST)
     {
         cell.textLabel.text =[NSString stringWithFormat:@"%@ - %@",[[self.dataSourceArray objectAtIndex:indexPath.row] valueForKey:@"desingation"],[[self.dataSourceArray objectAtIndex:indexPath.row] valueForKey:@"location"]];//[[self.dataSourceArray objectAtIndex:indexPath.row]valueForKey:@"desingation"];
         
     }
     else if (selectionType==SEL_MEMB)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]valueForKey:@"fullname"];
         
     }
     else if (selectionType==SEL_OPTLVL)
     {
         cell.textLabel.text =[AppHelper nullCheck:[[self.dataSourceArray objectAtIndex:indexPath.row]valueForKey:@"title"]];
         
     }
    //SEL_SERVEY
     else if (selectionType==SEL_SERVEY)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]valueForKey:@"gender"];
         
     }
    
     else if (selectionType==SEL_DESIGNATION)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]valueForKey:@"location"];
         
     }
    
     else if (selectionType==SEL_LVL)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]valueForKey:@"title"];
         
     }

     else if (selectionType==SEL_STATE)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"state_title"];
         
    }
     else if (selectionType==SEL_DIST)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"district_title"];
         
     }
     else if (selectionType==SEL_CONSRTANCY)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"cons_title"];
         
     }
     else if (selectionType==SEL_BLOCK)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"block_title"];
         
     }
//////////new changes//
    
     else if (selectionType==SEL_AVAIL)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]valueForKey:@"availability"];
         
     }
     else if (selectionType==SEL_RESI)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"residential"];
         
     }
     else if (selectionType==SEL_EDU)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"education"];
         
     }
     else if (selectionType==SEL_OCCU)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"occupation"];
         
     }
     else if (selectionType==SEL_INCOM)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"income"];
         
     }
     else if (selectionType==SEL_RELIGI)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"religion"];
         
     }
     else if (selectionType==SEL_CATEGORIES)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"casteCategory"];
         
     }
     else if (selectionType==SEL_CAST)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"caste"];
         
     } else if (selectionType==SEL_NO_OF_DEPENDENT)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"block_title"];
         
     }
     else if (selectionType==SEL_PREFERENCES)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"partyPrefrence"];
         
     }
     else if (selectionType==SEL_WILL_VOTE)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"votepercentage"];
         
     }
     else if (selectionType==SEL_ISSUE)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"partyIssue"];
         
     }
     else if (selectionType==SEL_ECONOMIC)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"economicclass"];
         
     }
     else if (selectionType==SEL_RELIGIOUS)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"relegious"];
         
     }
     else if (selectionType==SEL_SOCIAL_STATUS)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"socialstatus"];
         
     }
     else if (selectionType==SEL_SOCIAL_ACTIVITY)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"socialactive"];
         
     }
     else if (selectionType==SEL_POLITICAL)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"politicalview"];
         
     }
     else if (selectionType==SEL_REPORTLEVEL)
     {
         cell.textLabel.text =[[self.dataSourceArray objectAtIndex:indexPath.row]objectForKey:@"title"];
         
     }
    
    
    ////////////////////////////////
    else {
        
        cell.textLabel.text =[self.dataSourceArray  objectAtIndex:indexPath.row];
    }
    
    
    if ([selectedRows containsObject:[self.dataSourceArray  objectAtIndex:indexPath.row]]) {
        
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else if ([[selectedRows firstObject] isKindOfClass:[NSString class]] && ([[selectedRows firstObject] isEqualToString:cell.textLabel.text])) {
        
        [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }
    else
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    
    
    return cell;
}


#pragma mark  - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // check if single selection
    // if yes then remove all ojcet from the selected rows
    // and add the current selected one
    // dict[@"fullname"]=@"All Members";
    if (self.isSingleSlection) {
        
        [selectedRows removeAllObjects];
        [selectedRows addObject:[self.dataSourceArray  objectAtIndex:indexPath.row]];
        self.selectItems(selectedRows);
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        if(selectionType==SEL_MEMB){
            if([[[self.dataSourceArray  objectAtIndex:indexPath.row]valueForKey:@"fullname"] isEqualToString:@"All Members"]){
                [selectedRows removeAllObjects];
            }
            else{
                NSArray *ArrD=   [selectedRows valueForKeyPath:@"fullname"];
                if([ArrD containsObject:@"All Members"]){
                    [selectedRows removeAllObjects];
                }
            }
       
        }
        // select and deselect data - >
        [selectedRows containsObject:[self.dataSourceArray  objectAtIndex:indexPath.row]] ? [selectedRows removeObject:[self.dataSourceArray  objectAtIndex:indexPath.row]] :  [selectedRows addObject:[self.dataSourceArray  objectAtIndex:indexPath.row]];
    }
    
    // reload table to update changes
    [tableView reloadData];
    
}

#pragma mark  - Done or cancel tool bar buttion action

// Basically send call back in done method  ---  - >
- (IBAction)doneSelecting:(UIBarButtonItem *)sender {
    
    // send callback to the viewcontroller asociated to ths block -- >
    self.selectItems(selectedRows);
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Resume the previous state of selection and send previous slected rows --- >
- (IBAction)cancelSelecting:(UIBarButtonItem *)sender {
    
    //self.selectItems(previousSelectedRows);
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
