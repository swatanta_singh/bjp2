//
// DataPickerViewController.h
//  DynamicForm
//
//  Created by swat  on 31/03/15.
//  Copyright (c) 2014 Manoj . All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    SEL_NONE            = 0,
    SEL_COUNTRY        = 1,
     SEL_STATE       = 3,
     SEL_DIST       = 4,
       SEL_CONSRTANCY       = 5,
       SEL_BLOCK       = 6,
    SEL_ON        = 2,
    SEL_CATEGORY            = 7,
    SEL_DEST             = 8,
     SEL_LVL              = 9,
    SEL_OPTLVL              = 19,
    SEL_MEMB               = 10,
    SEL_SERVEY              =20,
    SEL_AVAIL              =21,
    SEL_RESI              =22,
    SEL_EDU              =23,
    SEL_OCCU              =24,
    SEL_INCOM              =25,
    SEL_RELIGI              =26,
    SEL_CATEGORIES              =27,
    SEL_CAST              =28,
    SEL_NO_OF_DEPENDENT              =29,
    SEL_PREFERENCES              =30,
    SEL_WILL_VOTE              =31,
    SEL_ISSUE              =32,
    SEL_ECONOMIC              =33,
    SEL_RELIGIOUS              =34,
    SEL_SOCIAL_STATUS              =35,
    SEL_SOCIAL_ACTIVITY              =36,
    SEL_POLITICAL              =37,
    SEL_HEADOFFAMILY              =38,
    SEL_DESIGNATION               =39,
    SEL_REPORTLEVEL              =40,


}SelectionOptions;

// typedef for the callback block
typedef void(^DidSelectItems)(NSArray *selectedvalues);

@interface DataPickerViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate>{
  
}

// tableView Object - >
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;

// Action for selcting and canceling - >
- (IBAction)doneSelecting:(UIBarButtonItem *)sender;
- (IBAction)cancelSelecting:(UIBarButtonItem *)sender;

// Get Callback for the selected items in the required controller
@property (nonatomic,copy)DidSelectItems selectItems;
// for slection criteria
@property(nonatomic,assign, getter = isSingleSlection) BOOL singleSelectionCriteria;

// Get uipopover object


// This method initialise SMWDataPickerViewController object with datasorce, selected datasource , isSigles selection criteria ,and selected array call back block -- >

- (void)initWitSingleSelectionCriteriaType:(BOOL)isSingleSelection dataSource:(NSArray *)dataSource withSelectedDataSourceRows:(NSArray *)selectedDataSourceRows selectionType:(SelectionOptions)type andSelectedValuesCallBack:(DidSelectItems)selectedItemsCallBack;


@end
