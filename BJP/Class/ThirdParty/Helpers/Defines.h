/*********************ENUM TAG *****************/
#define CURRENCY_SYMBOL [[NSLocale currentLocale] objectForKey:NSLocaleCurrencySymbol]
#define KGOOGLE_PLACE_API_KEY    @"AIzaSyDeo1X7-H6e6NQihwy88r7MpebE8SbesBI"

#define Notification_updateReminder   @"Notification_updateReminder"
#define Notification_Login_User       @"Notification_Login_User"
#define GOOGLE_SCHEME  @"com.googleusercontent.apps.320254151112-4c2n293qf4lui2hpd27s5jelvb3b96og"
#define FACEBOOK_SCHEME  @"fb1781145965483841"
typedef enum optionTag {
    cusineTag = 101,
    genderTag,
    educationTag,
    carTag,
    serviceTag,
    professionalTag,
    subjectTag,
    relisionTag,
    EthnicityTag,
    professionTag,
    MotherTongueTag,
    FamilyStatusTag,
    InterestTag,
    AgeRangeTag,
    SalaryRangeTag,
    MoverService,
    MoverVechile,
} optionTag;

typedef enum MenuItemEnum {
    connectMenu,
    starMenu,
    detailMenu,
    deleteMenu
} MenuItem;

typedef enum MenuEventItemEnum {
    showEventMenu,
    deleteEventMenu,
    updateEventMenu,
} MenuEventItem;


/************************************PORT &  BASE URL  ************************/

#define MIXPANEL_TOKENID @"0198c88807f27a1e8efc6037c985b365"
#define Device_Token @"Device_Token"

//http://www.bjp-up.com
#define BaseUrl           @"http://www.bjp-up.com/api/v1/method/"
//#define BaseUrl           @ "http://10.10.12.77:8081/api/v1/method/"
//#define BaseUrl           @ "http://10.10.13.127:8081/api/v1/method/"
//#define BaseUrl           @ "http://10.10.99.25:8081/api/v1/method/"
//#define BaseUrl           @ "http://10.10.12.77:8081/api/v1/method/"


/************************************ NETWORK ALERT  ************************/

#define ERROR_INTERNET          @"No network connection! please try again"
#define ERROR_INVAILID          @"Invailid User."
#define TIMEOUT_INTERVAL		60
#define NSSTRING_HAS_DATA(_x)   (((_x) != nil) && ( [(_x) length] > 0 ))

/************************************BASIC PROFILE************************/
#define kGET_ISSUELIST    @"NewlocalissuesNearBy.get.inc"
#define kGET_Category     @"NewIssuesCategory.get.inc"
#define kSET_LOCALISSUE   @"Newlocalissues.set.inc"



#define kGET_MENU        @"NewTaskMenu.get.inc"
//new
#define kGET_DESIGNATION    @"NewSelectDesignation.get.inc"
#define kGET_TASKLVL    @"NewTaskLevels.get.inc"
#define kGET_TASKMENU    @"NewTaskLevelMenu.get.inc"
#define kGET_TASKREPORTLVL    @"NewTaskReportLevel.get.inc"
#define kGET_ASSIGNDEST    @"NewAssignToDesignation.get.inc"
#define kGET_ASSIGNMEMBER    @"NewSelectMember.get.inc"
#define kGET_NEWASSIGNMNT    @"NewCreateAssignment.set.inc"
#define kADD_REPORT    @"NewTaskReportSubmit.set.inc"
#define kGET_ASSIGNMNT_ME    @"NewTaskToMe.get.inc"
#define kGET_REPORT    @"NewTaskReportDetail.get.inc"
#define kGET_ASSIGNMNT_BY    @"NewTaskByMe.get.inc"
#define kGET_MEMBER_TASK    @"NewTaskByMeDetail.get.inc"
#define kUPDATE_REPORT    @"NewTaskReportUpdate.set.inc"









#define kGET_CONTCT      @"NewOrgMemberList.get.inc"
#define kGOOGLE_SIGN     @"NewsignInByGooglePlus.inc"
#define kGet_Categrories @"newcategories.get.inc"
#define kGet_Categroy    @"newcategoryV2.get.inc"
#define kGet_Reply       @"support.getReply.inc.php"
#define kChange_Password @"NewsetPassword.inc"
#define kWriteToUS       @"Newsupport.sendTicket.inc.php"

#define kDeactivate_Account @"Newdeactivate.inc"
#define kSIGN_IN       @"NewsignIn.inc"
#define kSIGN_UP       @"NewsignUp.inc"
#define kSIGN_IN_FB    @"NewsignInByFacebook.inc"
#define kPROFILEPIC_UPLOAD    @"profile.NewuploadPhoto.inc.php"
#define kPROFILECOVER_UPLOAD  @"profile.NewuploadCover.inc.php"

#define kRecent_News    @"Newstream.get.inc"
#define kRecent_events  @"Neweventlist.get.inc"
#define kGet_Country    @"NewCountries.get.inc"
#define kGet_State      @"NewState.get.inc"
#define kGet_DIST       @"NewDistrict.get.inc"
#define kGet_CONSTNCY   @"NewConstituency.get.inc"
#define kGet_BLOCK      @"NewBlocks.get.inc"
#define kGet_PROFILE    @"Newprofile.get.inc"
#define kDetails_events @"Newevent.get.inc"
#define kGet_Submit_Events @"Newevents.participation.inc"
#define kGet_P_Voice       @"Newsurveylist.get.inc"
#define kGet_P_Voice_Submit  @"Newsurvey.submit.inc"
#define kGet_Like_Submit     @"Newitems.like.inc"
#define kUPLOADPROFILE    @"NewsaveSettings.inc"
#define kGET_COMMENT      @"NewitemV1.get.inc"
#define kGET_NOTIFICATION @"Newnotifications.get.inc"
#define kSEND_COMMENT     @"Newcomments.new.inc"
#define kSendOTP          @"NewOtpverify.get.inc.php"
#define kRegenerateOTP    @"NewOtpgen.set.inc"
#define kNewFavorites     @"newfavorites.get.inc"
#define kINFOGRAFICS      @"NewInfographicsPhotoV2.get.inc"
#define kINFOGRAFICS_SHARE_COUNT @"NewInfograpicsShareCount.set.inc"
#define kNewsetGcmRegId @"NewsetGcmRegId"
#define kNewsMagazine   @"NewMagazine.get.inc"
#define kGET_PINCODE    @"NewGetpincodedata.inc"
#define kGET_MEMBER     @"NewGetMembershipProfile.inc"
#define kCategoryMagazine @"mgzncategories.get.inc"
#define kUPLOADMEMBER     @"NewMemebershipRequest.inc"
#define kNESWDETAILS      @"NewItemdetailV1.get.inc"
#define kForgotPassword      @"NewOtpgenForgotPassword.set.inc"
#define kNewOtpverifyForgotPassword      @"NewOtpverifyForgotPassword.get.inc"
#define kNewChatGroupList      @"NewChatGroupList.get.inc"
#define KNewChatHistory @"NewChatHistory.get.inc"
#define KNewChatSet     @"NewChatHistory.set.inc"
#define KNewChatGroupInfo @"NewChatGroupInfo.get.inc"
#define KNewChatRecentMessages @"NewChatRecentMessages.get.inc"
#define kUSERSTATUS      @"NewChatStatus.set.inc"
#define kUSERSMESSAGECOUNT @"NewChatNotificationCount.get.inc"
#define kUSERSNotificationSTOP @"NewNotification.set.inc.php"
#define kUSERSGETNotificationSTOP @"NewNotification.get.inc"
#define kUSERSNewLogout @"NewLogout.set.inc"
#define kUSERviewButton @"viewButton.get.inc"
#define kUSERShowBooth @"showBooth.inc"
#define kUSERshowBoothUsers @"showBoothUsers.inc"
#define kSearchSurveyVoter @"searchSurveyVoter.inc"
#define kformSurvey     @"formSurvey.inc"
#define KupdateVoterServey @"updateVoterServey.set.inc.php"
#define KInsertVoterDropDownGet @"addVoterSurvey.inc"
#define KSelectDesignation   @"NewSelectDesignationVoter.get.inc"
#define KSelectNewTaskLevels   @"NewTaskLevelMenu.get.inc"
#define KTaskReportLevel   @"NewTaskReportLevel.get.inc"
#define KNewVoterSurveyDetail   @"NewVoterSurveyDetail.get.inc"



/************************************EMAIL REGEX************************/

#define emailRegex @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"

/***************************ALERT ******************************/

#pragma mark -
#pragma mark Alert title and messages
#define Alert_Cancel_Button       @"Cancel"
#define Alert_Ok_Button           @"OK"
#define screenSize    CGSizeMake([[UIScreen mainScreen] bounds].size.width , [[UIScreen mainScreen] bounds].size.height)


//#define SETTING_ITEM @[@"ABOUT",@"USER",@"BLOCK",@"padlock",@"CALENDER",@"PROX",@"SPEAK",@"NOTI"]
#define SETTING_ITEM @[@"Recent News",@"Local Events",@"Infographics",@"Infographics",@"Infographics",@"People's Voice",@"Local Events",@"Magazine",@"Membership",@"Settings",@"Settings",@"Invite Friends",@"Write",@"Logout"]

#define SETTING_NEW_ITEM @[@"Language",@"Push Notification",@"Change Password",@"Deactivate Account",@"Social",@"Terms of Use",@"Acknowledgement",@"Copyright Info",@"Write to Us"]
//padlock

#define SETTING_GENERAL_ITEM @[@"Language",@"Change Password",@"Deactivate Account",@"Notifications"]

#define SETTING_ABOUT_ITEM @[@"Terms of Use",@"Acknowledgement",@"Copyright Info"]

/***********************DEVICE SCREEN SIZE*************************
 **********/

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

/***********************OTHER***********************************/

#define APP_NAME                             @"UP BJP"
#define USER_ID                              @"UserId"
#define USER_TYPE                             @"UserType"

#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)
#define Notification_Facebook_Did_Login      @"Notification_Facebook_Did_Login"
#define USER_NAME                            @"Username"
#define ACCESS_TOKEN     @"accessToken"
#define APP_LANGAUGE     @"APP_LANGAUGE"
#define USER_PASS                            @"password"
#define BGGrayColor  [UIColor colorWithRed:69/255.0 green:70/255.0 blue:70/255.0 alpha:1.0]
#define HeaderHeight  60
#define MenuWidth  208
#define K_LATITUDE @"K_LATITUDE"
#define K_LONGITUDE @"K_LONGITUDE"


#define DatePickerSize  CGSizeMake(260,300)
#define DropDownSize CGSizeMake(260,300)
#define MenuDropDownSize CGSizeMake(150,180)
#define MenuDropDownNotiSize CGSizeMake(150,160)

#define EVENT_MenuDropDownSize CGSizeMake(SCREEN_WIDTH-100,40)
#define EVENT_Info_MenuDropDownSize CGSizeMake(SCREEN_WIDTH-250,40)

#define KGOOGLECLIENT_KEY  @"320254151112-4c2n293qf4lui2hpd27s5jelvb3b96og.apps.googleusercontent.com";


