//
//  AppHelper.h
//  IntroduceThem
//
//  Created by Aravind Kumar on 13/06/11.
//  Copyright 2011 Tata Consultant Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "CategoryViewController.h"
@interface AppHelper : NSObject {
MBProgressHUD *HUD;
    
}
@property (strong, nonatomic)CategoryViewController *leftContainer;
- (CategoryViewController *)menuViewController;
- (void )removeMenuViewController;
+(NSString *)arrayString:(NSArray *)array;
+ (UINavigationController *)navigationController;
+(NSString *)getErrorMessage:(NSString *)errorCode;
+ (void)saveImage:(UIImage *)image withImageName:(NSString *)imageName;
+ (NSString*)getDocumentPathImageName:(NSString *)imageName ;
+ (AppHelper *)sharedInstance;
- (MBProgressHUD *)getCurrentHud;
- (void )showIndicator;
+(void)popToViewContoleer:(NSString*)viewC;
- (void )hideIndicator;
+ (id)alloc;
+(NSArray *)allPropertyNamesOfClass:(Class)class;
+ (AppDelegate*) appDelegate;
+(void)saveToUserDefaults:(id)value withKey:(NSString*)key;
+(NSString*)userDefaultsForKey:(NSString*)key;
+(void)removeFromUserDefaultsWithKey:(NSString*)key;

+ (void) showAlertViewWithTag:(NSInteger)tag title:(NSString*)title message:(NSString*)msg delegate:(id)delegate
            cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles;
+ (NSString *)getCurrentLanguage;
+(UIImage *)normalizedImage:(UIImage *)image;
-(void)initPopoverWithViewController:(UIViewController*)vc direction:(UIPopoverArrowDirection)arrowDirection senderRect:(CGRect)rect inView:(UIView *)inView;
//make color code
+(UIColor *)colorFromHexString:(NSString *)hexString ;
+(NSString *)hexFromUIColor:(UIColor *)color;
+(BOOL)emailValidate:(NSString *)checkString;
+(UIViewController *)intialiseViewControllerFromMainStoryboard:(NSString *)storyBoard WithName:(NSString *)vcName;
+(NSString *)nullCheck:(id)str;
-(NSDate*)getNextYearDate:(NSDate*)date year:(NSString*)year ;
-(NSDate*)getDate:(NSDate*)date year:(NSString*)year month:(NSString*)month;
-(void)addlayerOnObject:(id)viewObj withRadius:(float)radius color:(UIColor*)color;
+(void)setupViewAtUp:(int)value toview:(UIView*)viewD;
+(void)addMenuView:(UIButton*)send withdata:(NSArray*)arrData withAction:(SEL)selectorName;
+(void)saveUserProfilePics;
+(void)showMessageErrorCode:(NSError *)error;
-(void)setLoaderText:(NSString *)message;
+ (NSString *)unixDateString:(NSDate *)date;
+(NSDate *)returnDateFromUnixTimeStampString:(NSString *)unixTimeStampString;
+(NSString *)nullCheckWithNA:(id)str;
+(NSString*)getDistanceOfUserWithLocation:(NSString*)lat longT:(NSString*)longt;
@end
