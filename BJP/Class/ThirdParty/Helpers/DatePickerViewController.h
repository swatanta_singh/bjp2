//
//  SMWDatePickerViewController.h
//  SMW
//
//  Created by Sandeep Patidar on 7/17/14.
//  Copyright (c) 2014 Smart Utility Systems. All rights reserved.
//

#import <UIKit/UIKit.h>



typedef void(^DidSelectedDate)(NSDate *selectedDate);

@interface DatePickerViewController : UIViewController {
    
    __weak IBOutlet UIBarButtonItem *btnDone;
    __weak IBOutlet UIBarButtonItem *btnCancel;
    __weak IBOutlet UIDatePicker    *datePicker;
}

@property(copy,nonatomic)DidSelectedDate selectedDateBlock;

- (IBAction)doneDatePicking:(id)sender;
- (IBAction)cancelDatePicking:(id)sender;

- (void)initDatePickerWithDatePickerMode:(UIDatePickerMode)mode minDate:(NSDate *)minDate maxDate:(NSDate *)maxDate andDefaultSelectedDate:(NSDate *)defSelDate withReturnedDate:(DidSelectedDate)selectedDate;

@end
