//
//  AppHelper.m
//  IntroduceThem
//
//  Created by Aravind Kumar on 13/06/11.
//  Copyright 2011 Tata Consultant Services. All rights reserved.
//

#import "AppHelper.h"
#import <objc/runtime.h>
#import <QuartzCore/QuartzCore.h>
#import "Defines.h"
@implementation AppHelper {
    UIPopoverController * popoverController;
}

static AppHelper *utility = nil;
static dispatch_once_t onceToken;
static AppHelper *sharedMyManager = nil;

+ (AppHelper *)sharedInstance {
  
    
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
    

}
-(instancetype)init
{
    @synchronized(self) {
    if (!utility){
       utility = [super init];
        UINavigationController *navi=[AppHelper navigationController];
        HUD = [[MBProgressHUD alloc] initWithView:navi.view];
        
        NSString *titleText = NSLocalizedString(@"Please", nil);
        HUD.labelText = titleText;
        HUD.minSize = CGSizeMake(100.f, 100.f);
        [[AppHelper appDelegate].window addSubview:HUD];

      }
    }
    return utility;
}

-(void)setLoaderText:(NSString *)message;
{
    NSString *titleText = NSLocalizedString(message, nil);
    HUD.labelText=titleText;
}

- (CategoryViewController *)menuViewController{
    if(self.leftContainer==nil)
        self.leftContainer= (CategoryViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"CategoryViewController"];
    return  self.leftContainer;
}
- (void )removeMenuViewController{
    self.leftContainer=nil;
}
+(NSString *)arrayString:(NSArray *)array
{
    NSString *arrStr=@"[";
    
    for(int i=0;i<[array count];i++)
    {
        if(i==0)
            arrStr=[NSString stringWithFormat:@"%@\"%@\"",arrStr,[array objectAtIndex:i]];
        else
            arrStr=[NSString stringWithFormat:@"%@,\"%@\"",arrStr,[array objectAtIndex:i]];
    }
    arrStr=[NSString stringWithFormat:@"%@]",arrStr];
    return arrStr;
}

+(NSString *)nullCheck:(id)str {
    
    if (str == nil) {
        return @"";
    }
    return (str ==[NSNull null])?@"":str;
}

+(NSString *)nullCheckWithNA:(id)str {
    
    if (str == nil) {
        return @"NA";
    }
    return ((str ==[NSNull null]) || [str isEqualToString:@""])?@"NA":str;
}
-(NSDate*)getNextYearDate:(NSDate*)date year:(NSString*)year {
    NSCalendar *calnder=[NSCalendar currentCalendar];
    NSDateComponents *compnet=[calnder components:NSCalendarUnitYear |NSCalendarUnitMonth |NSCalendarUnitDay fromDate:date];
    NSDateComponents *refCmnt=[[NSDateComponents alloc]init];
    [refCmnt setDay:compnet.day];
      [refCmnt setMonth:compnet.month];
    [refCmnt setYear:compnet.year+ [year intValue]];
   // NSLog(@"%@",[calnder dateFromComponents:refCmnt]);
    return [calnder dateFromComponents:refCmnt];
    
}
-(NSDate*)getDate:(NSDate*)date year:(NSString*)year month:(NSString*)month {
    NSCalendar *calnder=[NSCalendar currentCalendar];
    NSDateComponents *compnet=[calnder components:NSCalendarUnitYear |NSCalendarUnitMonth |NSCalendarUnitDay fromDate:date];
    NSDateComponents *refCmnt=[[NSDateComponents alloc]init];
    [refCmnt setDay:7];
    [refCmnt setMonth:[month intValue]];
    [refCmnt setYear:compnet.year+ [year intValue]];
   // NSLog(@"%@",[calnder dateFromComponents:refCmnt]);
    return [calnder dateFromComponents:refCmnt];
    
}
+(NSArray *)allPropertyNamesOfClass:(Class)class{
    unsigned count;
    objc_property_t *properties = class_copyPropertyList(class, &count);
    
    NSMutableArray *rv = [NSMutableArray array];
    
    unsigned i;
    for (i = 0; i < count; i++)
    {
        objc_property_t property = properties[i];
        NSString *name = [NSString stringWithUTF8String:property_getName(property)];
        [rv addObject:name];
    }
    
    free(properties);
    
    return rv;
}
+(void)popToViewContoleer:(NSString*)viewC{
    for(UIViewController *viewController in [[AppHelper navigationController] viewControllers]){
        if([viewController.title isEqualToString:viewC ]){
            [[AppHelper navigationController] popToViewController:viewController animated:YES];
        }
    }
}

+ (UINavigationController *)navigationController
{
    return (UINavigationController *)[[[AppHelper appDelegate] window] rootViewController];
}
-(void )showIndicator{
    [[AppHelper appDelegate].window bringSubviewToFront:HUD];
       [HUD show:YES];

}

-(void )hideIndicator{
   // [HUD removeFromSuperview];
    [HUD hide:YES];
}
+ (AppDelegate*) appDelegate {
    return (AppDelegate*)[[UIApplication sharedApplication]  delegate];
}

+(UIViewController *)intialiseViewControllerFromMainStoryboard:(NSString *)storyBoard WithName:(NSString *)vcName
{
    return  [[UIStoryboard storyboardWithName:storyBoard bundle:nil] instantiateViewControllerWithIdentifier:vcName];
}
-(void)initPopoverWithViewController:(UIViewController*)vc direction:(UIPopoverArrowDirection)arrowDirection senderRect:(CGRect)rect inView:(UIView *)inView  {
    
    
    //if (!popoverController) {
    popoverController         = [[UIPopoverController alloc] initWithContentViewController:vc];
    //    }
    //    else {
    //        [popoverController setContentViewController:vc];
    //    }
    //
    popoverController.popoverContentSize     = vc.view.frame.size;
    [popoverController presentPopoverFromRect:rect inView:inView permittedArrowDirections:arrowDirection animated:YES];
}
-(MBProgressHUD *)getCurrentHud {
    
    return HUD?HUD:nil;
}

-(UIPopoverController *)curruntPopOver {
    
    return popoverController?popoverController:nil;
}
//normalize image
+ (void)saveImage:(UIImage *)image withImageName:(NSString *)imageName {
   
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:imageName];
    
    NSData *imageData = UIImageJPEGRepresentation(image,0.1);
    [imageData writeToFile:savedImagePath atomically:NO];
}

- (void)removeImage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
      //  UIAlertView *removedSuccessFullyAlert = [[UIAlertView alloc] initWithTitle:@"Congratulations:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
      //  [removedSuccessFullyAlert show];
    }
    else
    {
        //NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
}

+ (NSString*)getDocumentPathImageName:(NSString *)imageName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:imageName];
    
    return savedImagePath;
}
+(UIImage *)normalizedImage:(UIImage *)image
{
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    [image drawInRect:(CGRect){0, 0, image.size}];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return normalizedImage;
}

+(void)saveToUserDefaults:(id)value withKey:(NSString*)key
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
	if (standardUserDefaults) {
		[standardUserDefaults setObject:value forKey:key];
		[standardUserDefaults synchronize];
	}
}

+(NSString*)userDefaultsForKey:(NSString*)key
{
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	NSString *val = nil;
	
	if (standardUserDefaults)
		val = [standardUserDefaults objectForKey:key];
	
	return val;
}
+(void)removeFromUserDefaultsWithKey:(NSString*)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults removeObjectForKey:key];
    [standardUserDefaults synchronize];
}


////----- show a alert massage
+ (void) showAlertViewWithTag:(NSInteger)tag title:(NSString*)title message:(NSString*)msg delegate:(id)delegate
            cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:delegate
										  cancelButtonTitle:CbtnTitle otherButtonTitles:otherBtnTitles, nil];
    alert.tag = tag;
	[alert show];
}


+(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


+(NSString *) hexFromUIColor:(UIColor *)color
{
    if (CGColorGetNumberOfComponents(color.CGColor) < 4) {
        const CGFloat *components = CGColorGetComponents(color.CGColor);
        color = [UIColor colorWithRed:components[0] green:components[0] blue:components[0] alpha:components[1]];
    }
    if (CGColorSpaceGetModel(CGColorGetColorSpace(color.CGColor)) != kCGColorSpaceModelRGB) {
        return [NSString stringWithFormat:@"#FFFFFF"];
    }
    return [NSString stringWithFormat:@"#%02X%02X%02X", (int)((CGColorGetComponents(color.CGColor))[0]*255.0), (int)((CGColorGetComponents(color.CGColor))[1]*255.0), (int)((CGColorGetComponents(color.CGColor))[2]*255.0)];
}

#pragma mark Email Validation
+(BOOL)emailValidate:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegexT = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegexT];
    return [emailTest evaluateWithObject:checkString];
}
#pragma add Layer
-(void)addlayerOnObject:(id)viewObj withRadius:(float)radius color:(UIColor*)color{
    [[viewObj layer] setCornerRadius:radius];
    [viewObj layer].borderColor = color.CGColor;
    [viewObj layer].borderWidth = 2.0f;
    [[viewObj layer] setMasksToBounds:YES];
}
+(void)setupViewAtUp:(int)value toview:(UIView*)viewD{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.25];
    CGRect rect = [viewD frame];
    if(value==0){
       rect.origin.y = value;
    }
    else{
         rect.origin.y -= value;
    }
    [viewD setFrame:rect];
    [UIView commitAnimations];
}
+(NSString *)getErrorMessage:(NSString *)errorCode
{
    
    NSString *errorMsgStr;
    NSInteger errorInt = [[errorCode substringFromIndex:1] integerValue];
    switch (errorInt) {
        case 401:
            errorMsgStr=@"Invalid input data";
            break;
        case 402:
            errorMsgStr=@"Facebook ID and User email ID are NULL.";
            break;
        case 403:
            errorMsgStr=@"User Email ID/ Phone number/ Facebook ID already exist.";
            break;
        case 404:
            errorMsgStr=@"User registration ID does not exist.";
            break;
        case 405:
            errorMsgStr=@"Invalid UserID/Password.";
            break;
        case 406:
            errorMsgStr=@"Some error has occured in new token generate process.";
            break;
        case 407:
            errorMsgStr=@"Invalid OTP";
            break;
        case 408:
            errorMsgStr=@"Invalid User's OTP.";
            break;
        default:
            break;
    }
    return errorMsgStr;
}

#pragma mark addMenu Controller
+(void)addMenuView:(UIButton*)send withdata:(NSArray*)arrData withAction:(SEL)selectorName{
    
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    CGRect buttonFrame=CGRectMake(send.frame.origin.x, send.frame.origin.y-30, send.frame.size.width, send.frame.size.height);
    NSMutableArray *arrMenu=[NSMutableArray new];
    for(NSString *str in arrData){
          UIMenuItem *menu1 = [[UIMenuItem alloc] initWithTitle:str action:selectorName];
        [arrMenu addObject:menu1];
    }
    [menuController setMenuItems:arrMenu];
    [menuController setArrowDirection:UIMenuControllerArrowUp];
    [menuController setTargetRect:buttonFrame inView:[send.superview superview]];
    [[UIMenuController sharedMenuController] setMenuVisible:YES animated:YES];
}

+(void)saveUserProfilePics{
    
//    NSString *baseURL = [NSString stringWithFormat:@"%@%@%@/%@", BaseUrl,kBasicPort,@"/Image/UserProfileImage",  [AppHelper appDelegate].cureentUser.profileImageURL];
//    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:baseURL]];;//
//    if(data){
//        UIImage *image = [UIImage imageWithData:data];
//        
//        NSString *imageNameString=[AppHelper appDelegate].cureentUser.userRegdID;
//        [AppHelper saveImage:image withImageName:[NSString stringWithFormat:@"%@.png",imageNameString]];
//    }
    
}


+(void)showMessageErrorCode:(NSError *)error
{
    if (error.code == -1005)
    {
        [AppHelper showAlertViewWithTag:11 title:@"The network connection was lost" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else if (error.code == -1001)
    {
        [AppHelper showAlertViewWithTag:11 title:@"Request time out!" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else
    {
        [AppHelper showAlertViewWithTag:11 title:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }

}
+ (NSString *)unixDateString:(NSDate *)date
{
    date = [AppHelper getUTCFormateDate:date];
    double unixTime = (double)([date timeIntervalSince1970] );
    return [NSString stringWithFormat:@"%f",unixTime];
}
+(NSDate* )getUTCFormateDate:(NSDate *)localDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    NSString *dateString = [formatter stringFromDate:localDate];
    return [formatter dateFromString:dateString];
}
+(NSDate *)returnDateFromUnixTimeStampString:(NSString *)unixTimeStampString
{
    double dDate=[unixTimeStampString doubleValue];
        NSDate*date= [NSDate dateWithTimeIntervalSince1970:dDate];
        return [AppHelper getLocalDate:date];
}
+(NSDate* )getLocalDate:(NSDate *)localDate
{
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:localDate];
    NSDate *gmtDate = [localDate dateByAddingTimeInterval:timeZoneOffset];
    return gmtDate;
}
#pragma mark Get Distgance
+(NSString*)getDistanceOfUserWithLocation:(NSString*)lat longT:(NSString*)longt{
    
    NSString *strDistce = @"0.0";
    NSString *latUser = @"0.0";
    NSString* longUser = @"0.0";
    
    
        latUser=[AppHelper userDefaultsForKey:K_LATITUDE];
        longUser=[AppHelper userDefaultsForKey:K_LONGITUDE];
    
    CLLocation *current = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[longt doubleValue]];
    CLLocation *itemLoc = [[CLLocation alloc] initWithLatitude:[latUser doubleValue] longitude:[longUser doubleValue]];
    
    CLLocationDistance itemDist = [itemLoc distanceFromLocation:current];
   // NSLog(@"Distance: %f", itemDist);
  
            strDistce=[NSString stringWithFormat:@"%0.2f %@",(itemDist/1000.0),@"Km"];
    
    return strDistce;
}

@end
