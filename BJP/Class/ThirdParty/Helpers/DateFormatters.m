//
//  DateFormatters.m
//  deleteme14
//
//  Created by Robert Ryan on 6/6/13.
//  Copyright (c) 2013 Robert Ryan. All rights reserved.
//

#import "DateFormatters.h"

@interface DateFormatters ()
@property (nonatomic, strong) NSCache *cache;
@end

@implementation DateFormatters

+ (id)sharedManager
{
    static id sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        _cache = [[NSCache alloc] init];
        _cache.name = @"Date Formatters";
    }
    return self;
}

- (NSDateFormatter *)formatterForString:(NSString *)dateFormat
{
    NSDateFormatter *formatter = [_cache objectForKey:dateFormat];

    if (formatter)
        return formatter;

    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:dateFormat];
   // NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
  //  [formatter setTimeZone:timeZone];
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];

    [_cache setObject:formatter forKey:dateFormat];

    return formatter;
}
@end