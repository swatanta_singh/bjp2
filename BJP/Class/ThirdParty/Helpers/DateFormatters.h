//
//  DateFormatters.h
//  deleteme14
//
//  Created by Robert Ryan on 6/6/13.
//  Copyright (c) 2013 Robert Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateFormatters : NSObject

+ (id)sharedManager;
- (NSDateFormatter *)formatterForString:(NSString *)dateFormat;

@end
