    //
//  SMWDatePickerViewController.m
//  SMW
//
//  Created by Sandeep Patidar on 7/17/14.
//  Copyright (c) 2014 Smart Utility Systems. All rights reserved.
//

#import "DatePickerViewController.h"
#import "AppHelper.h"
@interface DatePickerViewController ()

@end

@implementation DatePickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)initDatePickerWithDatePickerMode:(UIDatePickerMode)mode minDate:(NSDate *)minDate maxDate:(NSDate *)maxDate andDefaultSelectedDate:(NSDate *)defSelDate withReturnedDate:(DidSelectedDate)selectedDate {
    
    datePicker.minimumDate      = minDate;
    datePicker.maximumDate      = maxDate;
    datePicker.datePickerMode   = mode;
    datePicker.date             = defSelDate;
    datePicker.locale           = [NSLocale currentLocale];
    
    self.selectedDateBlock      = selectedDate;
    if(mode==UIDatePickerModeTime && minDate!=nil){
          datePicker.date             = minDate;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [btnDone setTitle:@"Done"];
    [btnCancel setTitle:@"Cancel"];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)doneDatePicking:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    self.selectedDateBlock(datePicker.date);
}

- (IBAction)cancelDatePicking:(id)sender {
    
       [self dismissViewControllerAnimated:YES completion:nil];
    // at receiver end check for not nil to get value
    self.selectedDateBlock(nil);
}

@end
