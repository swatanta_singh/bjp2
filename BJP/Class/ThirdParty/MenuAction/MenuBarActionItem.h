//
//  MenuBarActionItem.h
//  KINCT
//
//  Created by PDSingh on 5/18/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^menuActionblock)(id actionName);
@interface MenuBarActionItem : NSObject

@property(nonatomic,strong)NSMutableArray *menuArray;
@property(nonatomic,copy)menuActionblock actionName;
-(void)addMenuView:(UIButton*)send withdata:(NSArray*)arrData containerObject:(UITableView *)senderObjectView  withobject:(id)object WithCompilationBlock:(menuActionblock)actionName;
+ (MenuBarActionItem *)sharedInstance;
@end
