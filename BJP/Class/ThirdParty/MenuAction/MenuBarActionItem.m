//
//  MenuBarActionItem.m
//  KINCT
//
//  Created by PDSingh on 5/18/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuBarActionItem.h"

@implementation MenuBarActionItem

static dispatch_once_t onceToken;
static MenuBarActionItem *sharedMyManager = nil;

+ (MenuBarActionItem *)sharedInstance {
    
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
    
    
}
-(void)addMenuView:(UIButton*)send withdata:(NSArray*)arrData containerObject:(UITableView *)senderObjectTable withobject:(id)object WithCompilationBlock:(menuActionblock)actionName;
{
    [senderObjectTable canBecomeFirstResponder];

     self.actionName = actionName;
    UIMenuController *menuController = [UIMenuController sharedMenuController];
 //   CGRect buttonFrame=CGRectMake(send.frame.origin.x, send.frame.origin.y-30, send.frame.size.width, send.frame.size.height);
    
    if (!self.menuArray) {
        self.menuArray = [NSMutableArray new];
    }
    else
        [self.menuArray removeAllObjects];
   
    for(NSString *str in arrData){
        UIMenuItem *menu = [[UIMenuItem alloc] initWithTitle:str action:@selector(callAction:)];
        [self.menuArray addObject:menu];
    }
    
    [menuController setMenuItems:self.menuArray];
    [menuController setArrowDirection:UIMenuControllerArrowUp];
    [menuController setTargetRect:[senderObjectTable rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] inView:senderObjectTable];
    [[UIMenuController sharedMenuController] setMenuVisible:YES animated:YES];
    
}


- (BOOL) canPerformAction:(SEL)selector withSender:(id) sender
{
    for (NSString *action in self.menuArray) {
        
        NSString *actionStringName=[action stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *actionWithSelector=[NSString stringWithFormat:@"%@:",actionStringName];
        SEL actionStrinName = NSSelectorFromString(actionWithSelector);
        
        if (selector == actionStrinName)
        {
            return YES;
        }
    }
    
    return YES;
}


-(BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)callAction:(id) sender
{
    self.actionName(sender);
}



@end
