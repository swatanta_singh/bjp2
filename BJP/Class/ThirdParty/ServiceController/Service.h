//
//  Service.h
//
//  Created by Ansh Kumar on 22/08/12.
//  Copyright (c) 2012 RingBell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "User.h"

typedef void(^CompilationService)(BOOL isComplete);
typedef void (^utilityListFetchedCompletionHandler)();

@interface Service: NSObject
{
    NSManagedObjectContext *_managedObjectContext;

}
- (User*)parseUserData:(NSString*)User_ID withData:(NSDictionary*)dictionary;
+ (Service*) sharedEventController;
- (User *)getLoginDetailsWith:(NSString *)User_ID;
- (void)setUserStatus:(NSString*)sataus;

@property (nonatomic,copy)CompilationService completeValue;
@end
