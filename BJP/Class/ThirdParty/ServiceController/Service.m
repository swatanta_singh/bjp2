
//
//  Service.m
//
//  Created by Ansh Kumar on 22/08/12.
//  Copyright (c) 2012 RingBell. All rights reserved.
//

#import "Service.h"
#import "AppHelper.h"
#import "Defines.h"
#import "User.h"
#import "AFNetworking.h"
#import "DateFormatters.h"

@implementation Service


+ (Service*) sharedEventController
{
    static Service* singleton;
    
    if (!singleton)
    {
        singleton = [[Service alloc] init];
        
    }
    return singleton;
}

- (id)init
{
    self = [super init];
    if (self) {
        _managedObjectContext = [[AppHelper appDelegate] managedObjectContext];
    }
    
    return self;
}

- (NSEntityDescription *) makeEntityDescription: (NSString *)name
{
    NSEntityDescription* descr = [NSEntityDescription entityForName:name
                                             inManagedObjectContext:_managedObjectContext];
    return descr;
    
}


- (NSFetchRequest *)getBasicRequestForEntityName: (NSString *)entityName
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:_managedObjectContext];
    [request setEntity:entity];
    
    return request;
}

-(User*)parseUserData:(NSString*)User_ID withData:(NSDictionary*)dictionary{
    User *curentList=[self getLoginDetailsWith:User_ID];
   
    if (!curentList) {
        curentList = [NSEntityDescription
                      insertNewObjectForEntityForName:@"User"
                      inManagedObjectContext:_managedObjectContext];
    }
       curentList.accountId = [NSString stringWithFormat:@"%@",User_ID];
        NSArray *proparArr=[AppHelper allPropertyNamesOfClass:[User class]];
        NSArray *arr=dictionary.allKeys;
        for(NSString *data in proparArr){
            if([arr containsObject:data]){
                    [curentList setValue:[AppHelper nullCheck:[dictionary valueForKey:data ]] forKey:data];
            }
    
        }
     [AppHelper appDelegate].cureentUser=curentList;
     [_managedObjectContext save:nil];
    return curentList;
}
- (User *)getLoginDetailsWith:(NSString *)User_ID{
   
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"User"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"accountId == %@", User_ID];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    User  * addItemData  = nil;
    if (!error && [results count] > 0) {
        
        addItemData = [results objectAtIndex:0];
    }
    return addItemData;
}


#pragma mark user status
-(void)setUserStatus:(NSString *)sataus
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=[NSNumber numberWithInt:1];
        parameter[@"memberId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"status"] = sataus;
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERSTATUS];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
        
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   }];
        
    }
    
    
}


@end
