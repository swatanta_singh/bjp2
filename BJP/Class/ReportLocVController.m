//
//  ReportLocVController.m
//  BJP
//
//  Created by PDSingh on 1/3/17.
//  Copyright © 2017 swatantra. All rights reserved.
//

#import "ReportLocVController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "PresentionHandler.h"
#import "ReportLocVController.h"
#import "DataPickerViewController.h"
#import "ReportDetailsVController.h"


@interface ReportLocVController ()
{
    NSDictionary *nameAddress;
}

@property (weak, nonatomic) IBOutlet UILabel *locationTypeLable;
@property(nonatomic,strong)PresentionHandler *presnthandler;
@property(strong,nonatomic)NSMutableArray *arrData;
- (IBAction)submitAction:(id)sender;
- (IBAction)serveyLocationButton:(id)sender;

@end

@implementation ReportLocVController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    
  //  [self getboothList];
    [super viewWillAppear:animated];
    __weak ReportLocVController *weekSelf = self;
    
    NSString *titleText = NSLocalizedString(@"Booth", nil);
    [self setUpHeaderWithTitle:@"Voter Survey Report" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if (navigateValue==1) {
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    
    [self getList];
}


- (IBAction)submitAction:(id)sender {
    
    ReportDetailsVController *reportLocVController = (ReportDetailsVController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Voter" WithName:@"ReportDetailsVController"];
    reportLocVController.locationValue =nameAddress;
    [self.navigationController pushViewController:reportLocVController animated:YES];
}



-(void)getList{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        NSString *baseURL=nil;
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        baseURL = [NSString stringWithFormat:@"%@%@",BaseUrl,KSelectDesignation];
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if (responseObject [@"designation_list"] ) {
                    nameAddress = [responseObject[@"designation_list"]firstObject]  ;
                    self.arrData = responseObject [@"designation_list"];
                    if ([responseObject [@"designation_list"]firstObject]) {
                        self.locationTypeLable.text=[NSString stringWithFormat:@"%@ - %@",[AppHelper nullCheck:[responseObject [@"designation_list"]firstObject][@"desingation"]],[AppHelper nullCheck:[responseObject [@"designation_list"]firstObject][@"location"]]];
                    }
                }
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}


-(void)pickerDataSource:(NSArray *)dataSource withField:(UICollectionViewCell*)myCell selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    
    [self.view endEditing:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];
    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            if(type==SEL_DESIGNATION){
                
                NSDictionary *data=[selectedvalues  lastObject];
                self.locationTypeLable.text =[NSString stringWithFormat:@"%@ - %@",[AppHelper nullCheck:data[@"desingation"]],data[@"location"]];
                nameAddress =[selectedvalues firstObject];
            }
        }
    }];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];
}




- (IBAction)serveyLocationButton:(id)sender {
    if(nameAddress)
    {
        [self pickerDataSource:[NSArray arrayWithArray:self.arrData] withField:sender selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_DESIGNATION];
    }
}


@end
