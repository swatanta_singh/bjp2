//
//  CreateTaskViewController.h
//  BJP
//
//  Created by swatantra on 11/12/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "BaseViewController.h"

@interface CreateTaskViewController : BaseViewController
@property(nonatomic,strong)NSString *isEdit;
@property(nonatomic,strong)NSDictionary *dictData;
@end
