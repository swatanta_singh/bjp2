//
//  VerifyViewController.h
//  KINCT
//
//  Created by Ishu Gupta on 9/23/15.
//  Copyright © 2015 KINCT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface VerifyViewController : UIViewController
{
    __weak IBOutlet UITextField *codeTextField;
    __weak IBOutlet UIButton *verifyButton,*codeButton;
    __weak IBOutlet UILabel *sendLabel;
}
@property(nonatomic,strong)NSString *strPhn;

@property(nonatomic,strong)NSString* screenFrom;
-(IBAction)verifyAction:(id)sender;
-(IBAction)regenrateCodeAction:(id)sender;
-(IBAction)backAction:(id)sender;
@end
