//
//  FindLoactionFullController.h
//  KINCT
//
//  Created by PDSingh on 6/20/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

typedef void(^DidSelectLocationblock)(id address);

@interface FindLoactionFullController : UIViewController
{
    NSMutableArray *selectedRows;
    NSMutableArray *mainDataSourceArray;
}

@property(nonatomic,copy)DidSelectLocationblock address;
-(void)getAddressBlock:(DidSelectLocationblock)address;

@end
