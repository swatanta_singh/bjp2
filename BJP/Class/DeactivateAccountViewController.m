
//
//  DeactivateAccountViewController.m
//  BJP
//
//  Created by toyaj on 9/1/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "DeactivateAccountViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"

@interface DeactivateAccountViewController ()
@property (weak, nonatomic) IBOutlet UIButton *submittButton;

@end

@implementation DeactivateAccountViewController

- (void)viewDidLoad {
    
    
    [self.submittButton setTitle:NSLocalizedString(@"Submit", nil) forState:UIControlStateNormal];

    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    __weak DeactivateAccountViewController *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"Deactivate", nil);
    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
        else{
            [weekSelf.view endEditing:YES];
            
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)deactivation_Action:(id)sender {
    
     NSString *password=[self.text_Password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ([password length]==0){
        
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please enter old Password" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    
    }
    else{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        /*
         AppHelper appDelegate].cureentUser.accountId;
         parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
         */
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"currentPassword"]=_text_Password.text;

        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kDeactivate_Account];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                 [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Deactivated successfully" delegate:self cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
            }
            else
                
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Password Not Match" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            
            
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==100){
        [AppHelper appDelegate].cureentUser=nil;
        [AppHelper saveToUserDefaults:nil withKey:USER_ID];
        [AppHelper saveToUserDefaults:nil withKey:USER_NAME];
        [AppHelper saveToUserDefaults:nil withKey:ACCESS_TOKEN];
        [[AppHelper navigationController] popToRootViewControllerAnimated:YES];
    }
}
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}
@end
