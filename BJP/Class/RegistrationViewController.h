//
//  RegistrationViewController.h
//  KINCT
//
//  Created by Ashish on 26/02/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PresentionHandler.h"
#import "ECPhoneNumberFormatter.h"

@interface RegistrationViewController : BaseViewController<UITextFieldDelegate >{
  
    UIToolbar*   numberToolbar;
    NSMutableArray *arrAccountsModelData;
    ECPhoneNumberFormatter *numberformatter;

}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constrantHeight;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblHeightConstant;
@property(nonatomic,strong)PresentionHandler *presnthandler;
@property(nonatomic,strong)NSString *fbId;
@property(nonatomic,strong)NSString *fulname;
@property(nonatomic,strong)NSDictionary *dictGoogle;
@property (weak, nonatomic) IBOutlet UIButton *btn_signup;


- (IBAction)SignupAction:(id)sender;

@end
