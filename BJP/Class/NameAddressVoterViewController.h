//
//  NameAddressVoterViewController.h
//  BJP
//
//  Created by toyaj on 12/14/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECPhoneNumberFormatter.h"

@interface NameAddressVoterViewController : UIViewController<UITextFieldDelegate>
{
    UIToolbar*   numberToolbar;
       ECPhoneNumberFormatter *numberformatter;
}
@property (strong, nonatomic)  NSMutableDictionary *dictData;
-(NSMutableDictionary*)nameAddressData;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(strong,nonatomic) NSMutableDictionary *dicdataRecived;
@property(strong,nonatomic)NSString *status;
@property(strong,nonatomic)NSMutableArray *arrData;
@property (strong, nonatomic) NSString  *serveyId;

-(void)displayDataOnView;

@end
