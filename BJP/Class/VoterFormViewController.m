
#import "VoterFormViewController.h"
#import "NameAddressVoterViewController.h"
#import "SupportViewViewController.h"
#import "SituationVoterViewController.h"
#import "AppHelper.h"
#import "NameAddressVoterViewController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "DataPickerViewController.h"
#import "PresentionHandler.h"


@interface VoterFormViewController ()
{
    NSMutableDictionary *nameAddress;
    SupportViewViewController *supportView;
    NameAddressVoterViewController *sendDetails;
    SituationVoterViewController *situationView;
}

@property (weak, nonatomic) IBOutlet UIView *viewContainer;

- (IBAction)submitButtion:(id)sender;

@end

@implementation VoterFormViewController



- (void)viewDidLoad {
   
    
    [super viewDidLoad];
    
    self.submitButton.hidden=YES;
    numberformatter = [[ECPhoneNumberFormatter alloc] init];
 
    sendDetails =(NameAddressVoterViewController *)[AppHelper intialiseViewControllerFromMainStoryboard:@"Voter" WithName:@"NameAddressVoterViewController"];
    sendDetails.serveyId=self.serveyId;
    
    supportView=(SupportViewViewController *)[AppHelper intialiseViewControllerFromMainStoryboard:@"Voter" WithName:@"SupportViewViewController"];
    supportView.serveyId = self.serveyId;

    situationView=(SituationVoterViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Voter" WithName:@"SituationVoterViewController"];
    situationView.serveyId = self.serveyId;
    
    
    [self.viewContainer addSubview:sendDetails.view];
    sendDetails.view.translatesAutoresizingMaskIntoConstraints = NO;
    supportView.view.translatesAutoresizingMaskIntoConstraints = NO;
    situationView.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self setUpFrame:self.viewContainer with:sendDetails.view];
    [self addChildViewController:sendDetails];
    [sendDetails didMoveToParentViewController:self];
    
  
}
- (void)setUpFrame:(UIView*)containerView with:(UIView*)newSubview
{
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeTop
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeLeading
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeLeading
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeTrailing
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeTrailing
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    
    [containerView layoutIfNeeded];
    
    
    
   
}


-(void)getList{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        NSString *baseURL=nil;
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        baseURL = [NSString stringWithFormat:@"%@%@",BaseUrl,kformSurvey];

        if (self.serveyId)
        {
            parameter[@"surveyId"]=self.serveyId;
            parameter[@"verificationstatus"]=_status;

        }
        else
        {
            parameter[@"boothId"]=self.boothId;
            parameter[@"verificationstatus"]=@"0";
        }
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                 return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([responseObject [@"nameaddress"] lastObject] ) {
                    nameAddress = [responseObject [@"nameaddress"] lastObject] ;
                    if (nameAddress ) {
                        sendDetails.dicdataRecived = nameAddress;
                        supportView.dicdataRecived = nameAddress;
                        [supportView setViewFrameData];
                        situationView.dicdataRecived=nameAddress;
                        [situationView displayViewSetMEthod];
                        [sendDetails displayDataOnView];
                    }
                }
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}



- (IBAction)segmentMethod:(id)sender {
    
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
     

        self.submitButton.hidden=YES;
        self.submittButtonHieghtConstraints.constant=0;
        
        [supportView willMoveToParentViewController:nil];
        [supportView removeFromParentViewController];
        [supportView.view  removeFromSuperview];
        
        [situationView willMoveToParentViewController:nil];
        [situationView removeFromParentViewController];
        [situationView.view  removeFromSuperview];
        
        
        [self.viewContainer addSubview:sendDetails.view];
        [self setUpFrame:self.viewContainer with:sendDetails.view];
        
        [self addChildViewController:sendDetails];
        [sendDetails didMoveToParentViewController:self];
        
    
       
    }
    else if(selectedSegment == 1){
      
        self.submitButton.hidden=YES;
        self.submittButtonHieghtConstraints.constant=0;
        
        [sendDetails willMoveToParentViewController:nil];
        [sendDetails removeFromParentViewController];
        [sendDetails.view  removeFromSuperview];
        
        [situationView willMoveToParentViewController:nil];
        [situationView removeFromParentViewController];
        [situationView.view  removeFromSuperview];
        
        
        [self.viewContainer addSubview:supportView.view];
        [self setUpFrame:self.viewContainer with:supportView.view];
        
        [self addChildViewController:supportView];
        [supportView didMoveToParentViewController:self];
    }
    else
    {
        self.submitButton.hidden=NO;
        self.submittButtonHieghtConstraints.constant=40;
        
        [supportView willMoveToParentViewController:nil];
        [supportView removeFromParentViewController];
        [supportView.view  removeFromSuperview];
        
        [situationView willMoveToParentViewController:nil];
        [situationView removeFromParentViewController];
        [situationView.view  removeFromSuperview];
        
        
        [self.viewContainer addSubview:situationView.view];
        [self setUpFrame:self.viewContainer with:situationView.view];
        
        [self addChildViewController:situationView];
        [situationView didMoveToParentViewController:self];
    }
}


- (void)cycleFromViewController: (UIViewController*) oldVC
               toViewController: (UIViewController*) newVC {
    // Prepare the two view controllers for the change.
    [oldVC willMoveToParentViewController:nil];
    [self addChildViewController:newVC];
    
    // Get the start frame of the new view controller and the end frame
    // for the old view controller. Both rectangles are offscreen.
    newVC.view.frame = CGRectMake(80.0, 84.0, 160.0, 40.0);
    CGRect endFrame = CGRectMake(80.0, 210.0, 160.0, 40.0);
    
    // Queue up the transition animation.
    [self transitionFromViewController: oldVC toViewController: newVC
                              duration: 0.25 options:0
                            animations:^{
                                // Animate the views to their final positions.
                                newVC.view.frame = oldVC.view.frame;
                                oldVC.view.frame = endFrame;
                            }
                            completion:^(BOOL finished) {
                                // Remove the old view controller and send the final
                                // notification to the new view controller.
                                [oldVC removeFromParentViewController];
                                [newVC didMoveToParentViewController:self];
                            }];
}


-(void)viewWillAppear:(BOOL)animated

{
    [super viewWillAppear:animated];
    
    self.submittButtonHieghtConstraints.constant=0;
    __weak VoterFormViewController *weekSelf=self;
    
    NSString *titleText=nil;
    if (self.serveyId)
        titleText = NSLocalizedString(@"Voter", nil);
    else
        titleText = NSLocalizedString(@"VoterNew", nil);
    
    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if (navigateValue==1) {
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    [self getList];
  }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) shouldAutomaticallyForwardAppearanceMethods {
    return NO;
}

- (IBAction)submitButtion:(id)sender {
    
    
    if (![self validateUserInputs])
        return;
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        
        parameter[@"id"]=self.serveyId;
        parameter[@"ac"]=[[sendDetails.arrData objectAtIndex:2]valueForKey:@"id"];
        parameter[@"booth"]=[[sendDetails.arrData objectAtIndex:1]valueForKey:@"id"];
        parameter[@"voter_no"]=[[sendDetails.arrData objectAtIndex:5]valueForKey:@"value"];
        parameter[@"voter_id"]=[[sendDetails.arrData objectAtIndex:6]valueForKey:@"value"];
        parameter[@"name"]=[[sendDetails.arrData objectAtIndex:3]valueForKey:@"value"];
        parameter[@"fathers_name"]=[[sendDetails.arrData objectAtIndex:4]valueForKey:@"value"];
        parameter[@"house_no"]=[[sendDetails.arrData objectAtIndex:9]valueForKey:@"value"];
        parameter[@"avalability"]=[[sendDetails.arrData objectAtIndex:11]valueForKey:@"id"];
        parameter[@"residential_status"]=[[sendDetails.arrData objectAtIndex:12]valueForKey:@"id"];
        parameter[@"age"]=[[sendDetails.arrData objectAtIndex:7]valueForKey:@"value"];
        parameter[@"gender"]=[[sendDetails.arrData objectAtIndex:8]valueForKey:@"id"];
        parameter[@"education"]=[[sendDetails.arrData objectAtIndex:13]valueForKey:@"id"];
        parameter[@"occupation"]=[[sendDetails.arrData objectAtIndex:14]valueForKey:@"id"];
        parameter[@"income_group"]=[[sendDetails.arrData objectAtIndex:15]valueForKey:@"id"];
        parameter[@"religion"]=[[sendDetails.arrData objectAtIndex:16]valueForKey:@"id"];
        parameter[@"category"]=[[sendDetails.arrData objectAtIndex:17]valueForKey:@"id"];
        parameter[@"cast"]=[[sendDetails.arrData objectAtIndex:18]valueForKey:@"id"];
        parameter[@"head_of_family"]=[[sendDetails.arrData objectAtIndex:19]valueForKey:@"id"];
        parameter[@"family_members"]=[[sendDetails.arrData objectAtIndex:20]valueForKey:@"value"];
       //situationView.arrData
        parameter[@"economic_class"]=[[situationView.arrData objectAtIndex:0]valueForKey:@"id"];
        parameter[@"relegious"]=[[situationView.arrData objectAtIndex:1]valueForKey:@"id"];
        parameter[@"social_status"]=[[situationView.arrData objectAtIndex:2]valueForKey:@"id"];
        parameter[@"social_active"]=[[situationView.arrData objectAtIndex:3]valueForKey:@"id"];
        parameter[@"political_view"]=[[situationView.arrData objectAtIndex:4]valueForKey:@"id"];

        parameter[@"party_preference1"]=[[supportView.arrData[0] objectAtIndex:0]valueForKey:@"id"];
        parameter[@"party_preference2"]=[[supportView.arrData[0] objectAtIndex:1]valueForKey:@"id"];
        parameter[@"party_preference3"]=[[supportView.arrData[0] objectAtIndex:2]valueForKey:@"id"];
        parameter[@"party_preference4"]=[[supportView.arrData[0] objectAtIndex:3]valueForKey:@"id"];
        
        parameter[@"vote_bjp"]=[[supportView.arrData[0] objectAtIndex:4]valueForKey:@"id"];
        parameter[@"local_issues1"]=[[supportView.arrData[1] objectAtIndex:0]valueForKey:@"id"];
        parameter[@"local_issues2"]=[[supportView.arrData[1] objectAtIndex:1]valueForKey:@"id"];
        parameter[@"local_issues3"]=[[supportView.arrData[1] objectAtIndex:2]valueForKey:@"id"];
        parameter[@"local_issues4"]=[[supportView.arrData[1] objectAtIndex:3]valueForKey:@"id"];
        parameter[@"local_issues5"]=[[supportView.arrData[1] objectAtIndex:4]valueForKey:@"id"];
        parameter[@"requesttype"]=self.requestType;
        
        //[self getPhoneNunbervalue:[AppHelper nullCheck:dict[@"value"]]];
        parameter[@"phoneno"]=[self getPhoneNunbervalue:[AppHelper nullCheck:[[sendDetails.arrData objectAtIndex:10]valueForKey:@"value"]]];
        
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,KupdateVoterServey];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
              
                UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:APP_NAME  message:[responseObject valueForKey:@"error_description"]  preferredStyle:UIAlertControllerStyleAlert];
                [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    [self dismissViewControllerAnimated:YES completion:nil];
                }]];
                [self presentViewController:alertController animated:YES completion:nil];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
    }
    
    
}

-(NSString *)getPhoneNunbervalue:(NSString*)textField
{
    id objectValue;
    NSString *error;
    [numberformatter  getObjectValue:&objectValue forString:textField errorDescription:&error];
    return objectValue;
 
}

-(BOOL)validateUserInputs
{
   
    NSString *message = @"";
    NSString *name = [[sendDetails.arrData objectAtIndex:3]valueForKey:@"value"];
    name=[name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    NSString *fname = [[sendDetails.arrData objectAtIndex:4]valueForKey:@"value"];
    fname=[fname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSInteger age=[[[sendDetails.arrData objectAtIndex:7]valueForKey:@"value"] integerValue];
    bool IsValid=(age>=18 && age<=200);

    NSString *gender = [[sendDetails.arrData objectAtIndex:8]valueForKey:@"id"];
    NSString *house_no = [[sendDetails.arrData objectAtIndex:9]valueForKey:@"value"];
    house_no = [house_no stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *religion=[[sendDetails.arrData objectAtIndex:16]valueForKey:@"id"];
    NSString *bjp =[[supportView.arrData[0] objectAtIndex:4]valueForKey:@"id"];
    NSString *party_preference1=[[supportView.arrData[0] objectAtIndex:0]valueForKey:@"id"];
    NSString *party_preference2=[[supportView.arrData[0] objectAtIndex:1]valueForKey:@"id"];
    NSString *party_preference3=[[supportView.arrData[0] objectAtIndex:2]valueForKey:@"id"];
    NSString *party_preference4=[[supportView.arrData[0] objectAtIndex:3]valueForKey:@"id"];
    
    NSString *local_issues1=[[supportView.arrData[1] objectAtIndex:0]valueForKey:@"id"];
    NSString *local_issues2=[[supportView.arrData[1] objectAtIndex:1]valueForKey:@"id"];
    NSString *local_issues3=[[supportView.arrData[1] objectAtIndex:2]valueForKey:@"id"];
    NSString *local_issues4=[[supportView.arrData[1] objectAtIndex:3]valueForKey:@"id"];
    NSString *local_issues5=[[supportView.arrData[1] objectAtIndex:4]valueForKey:@"id"];
    NSString *category =[[sendDetails.arrData objectAtIndex:17]valueForKey:@"id"];

    
    name=[name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
   if ([name length]==0 )message=@"Don't forget voter name";
   else if ([fname length]==0 ) message=@"Don't forget father name";
   else if (!IsValid ) message = @"Voter age eligibility must be 18+";
   else if ([gender length]==0 )  message=@"Please select gender";
   else if ([gender length]==0 )  message=@"Please select gender";
   else if ([house_no length]==0 )message=@"Please select house no";
   else if ([religion length]==0 )message=@"Please select religion";
   else if ([category length]==0 ) message=@"Please select category";
   else if ([bjp length]==0 ) message=@"Please select option vote for bjp";
   else if (([party_preference1 length]==0)|| ([party_preference2 length]==0)||([party_preference3 length]==0)||([party_preference4 length]==0) ) message=@"Please select all party preferences";
   else if (([local_issues1 length]==0)|| ([local_issues2 length]==0)||([local_issues3 length]==0)||([local_issues4 length]==0)||([local_issues5 length]==0) ) message=@"Please select all your issues";
    
    if ([message length]>0) {
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return NO;
    }
  


    return YES;
}




@end
