//
//  BoothListVController.m
//  BJP
//
//  Created by PDSingh on 12/14/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "BoothListVController.h"
#import "UIImageView+AFNetworking.h"
#import "VoterlistVController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"

@interface BoothListVController (){
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSArray *boothArrData;

@end

@implementation BoothListVController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [self getboothList];
    [super viewWillAppear:animated];
    __weak BoothListVController *weekSelf = self;
    
    NSString *titleText = NSLocalizedString(@"Booth", nil);
    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if (navigateValue==1) {
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)getboothList{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];

        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];

        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERShowBooth];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]])
                self.boothArrData = [responseObject valueForKey:@"data"];
               
            }
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                 self.boothArrData=nil;
            }
             [self.tableView reloadData];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}
#pragma mark -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.boothArrData count];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    NSInteger numOfSections = 0;
    if ([self.boothArrData count]>0 )
    {
        numOfSections  = 1;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        NSString *dataNot = NSLocalizedString(@"nodata", nil);
        noDataLabel.text             = dataNot;
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCell" forIndexPath:indexPath];
    
    NSDictionary *boothItem=[self.boothArrData objectAtIndex:indexPath.row];
    UILabel *lblname = (UILabel*)[cell.contentView viewWithTag:1001];
    lblname.text=[NSString stringWithFormat:@"Booth code : %@",[AppHelper nullCheck:boothItem[@"booth_code"]]];
    
    UILabel *lblSublable = (UILabel*)[cell.contentView viewWithTag:1002];
    lblSublable.text=[NSString stringWithFormat:@"Booth name : %@",boothItem[@"booth"]];
    
    UILabel *lastlable = (UILabel*)[cell.contentView viewWithTag:1003];
    NSString *subTitle=[NSString stringWithFormat:@"Assembly : %@\nSector: %@\nMandal : %@",[AppHelper nullCheck:boothItem[@"assembly"]],[AppHelper nullCheck:boothItem[@"sector"]],[AppHelper nullCheck:boothItem[@"mandal"]]];
    
    lastlable.text=subTitle;
    UILabel *datelable = (UILabel*)[cell.contentView viewWithTag:1004];
    datelable.text=[NSString stringWithFormat:@"District : %@\nTotal Voters : %@\nPending : %@ \nCompleted : %@",[AppHelper nullCheck:boothItem[@"district"]],boothItem[@"totalVoter"],boothItem[@"totalNonVerifiedUser"],boothItem[@"totalVerifiedUser"]];

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
       NSString *strVale=[[self.boothArrData objectAtIndex:indexPath.row] valueForKey:@"id"];
       VoterlistVController *voterlistVController=(VoterlistVController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Voter" WithName:@"VoterlistVController"];
       voterlistVController.boothId = strVale;
       [self.navigationController pushViewController:voterlistVController animated:YES];
}

-(NSDate *)returnDateFromUnixTimeStampString:(double )unixTimeStampString
{
    NSDate*date= [NSDate dateWithTimeIntervalSince1970:unixTimeStampString];
    return [self getLocalDate:date];
    
}
-(NSDate *)getLocalDate:(NSDate *)localDate
{
    NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:localDate];
    NSDate *gmtDate = [localDate dateByAddingTimeInterval:timeZoneOffset];
    return gmtDate;
}
-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}

@end
