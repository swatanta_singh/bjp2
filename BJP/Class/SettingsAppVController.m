//
//  SettingsAppVController.m
//  KINCT
//
//  Created by PDSingh on 3/21/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import "SettingsAppVController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "PresentionHandler.h"
#import "LanguageChangeViewController.h"
#import "ChangedPasswordViewController.h"
#import "DeactivateAccountViewController.h"
#import "TermsOfUSeViewController.h"
#import "AcknowledgeViewController.h"
#import "CopyrightInfoViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "DataPickerViewController.h"

@interface SettingsAppVController ()
{
    NSMutableArray *arrData;
    NSMutableArray *localArrData;
    UISwitch *switchview ;

}

@property (strong, nonatomic) PresentionHandler *presentHandler;
@property (nonatomic) BOOL isON;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SettingsAppVController

- (void)viewDidLoad {
  
    
    arrData=[NSMutableArray new];
    localArrData =[NSMutableArray new];

    
    NSMutableDictionary *localStringdict1=[NSMutableDictionary new];
    localStringdict1[@"lable"]=NSLocalizedString(@"About", nil);
    localStringdict1[@"value"]=@[NSLocalizedString(@"TermsofUse", nil), NSLocalizedString(@"Acknowledgement", nil), NSLocalizedString(@"CopyrightInfo", nil)];
    
    NSMutableDictionary *dict1=[NSMutableDictionary new];
    dict1[@"lable"]=@"ABOUT";
    dict1[@"value"]=@[@"Terms of Use", @"Acknowledgement", @"Copyright Info"];
    
    if([AppHelper appDelegate].cureentUser){
        
       
        NSMutableDictionary *dict=[NSMutableDictionary new];
        dict[@"lable"]=@"GENERAL";
        dict[@"value"]=@[@"Language", @"Change Password", @"Deactivate Account",@"Notification"];
        
        NSMutableDictionary *dict1=[NSMutableDictionary new];
        dict1[@"lable"]=@"ABOUT";
        dict1[@"value"]=@[@"Terms of Use", @"Acknowledgement", @"Copyright Info"];
        
        
        NSMutableDictionary *dictLocal=[NSMutableDictionary new];
        dictLocal[@"lable"]=NSLocalizedString(@"General", nil);
        dictLocal[@"value"]=@[NSLocalizedString(@"Language", nil), NSLocalizedString(@"Password", nil), NSLocalizedString(@"Deactivate", nil),NSLocalizedString(@"Notifications", nil)];
        
        
          [arrData addObject:dict];
          [arrData addObject:dict1];
        
        [localArrData addObject:dictLocal];
        [localArrData addObject:localStringdict1];
        }
    else{
      
        NSMutableDictionary *dict=[NSMutableDictionary new];
        dict[@"lable"]=@"GENERAL";
        dict[@"value"]=@[@"Language"];
        
        NSMutableDictionary *dictLocal=[NSMutableDictionary new];
        dictLocal[@"lable"]=NSLocalizedString(@"General", nil);
        dictLocal[@"value"]=@[NSLocalizedString(@"Language", nil)];
       
        [arrData addObject:dict];
        [arrData addObject:dict1];
        
        [localArrData addObject:dictLocal];
        [localArrData addObject:localStringdict1];
        
    }
   
      self.tableView.tableFooterView = [[UIView alloc ]initWithFrame:CGRectZero];

  //  [[UITableViewHeaderFooterView appearance] setTintColor:[UIColor clearColor]];
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setTextColor:[UIColor orangeColor]];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    __weak SettingsAppVController *weekSelf=self;
    
    [self getNotifictaion];
    
    NSString *titleText = NSLocalizedString(@"Settings", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
        
    }];
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[localArrData objectAtIndex:section] valueForKey:@"lable"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arrData count];
;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   // return [self.settingsArray count];
    NSArray *Arr=[[arrData objectAtIndex:section] valueForKey:@"value"];

  
    return [Arr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView_ cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    NSArray *Arr=[[localArrData objectAtIndex:indexPath.section] valueForKey:@"value"];
    NSString *animal = [Arr objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = (UITableViewCell *)[tableView_ dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        if ([animal isEqualToString:@"अधिसूचना"] || [animal isEqualToString:@"Notification"]) {
            switchview = [[UISwitch alloc] initWithFrame:CGRectZero];
            cell.accessoryType=UITableViewCellAccessoryNone;
            cell.accessoryView = switchview;
        }
        else
        {  // cell.accessoryView=nil;
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        }
       

    }

    [switchview setOn:self.isON];
    [switchview addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];

    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.textLabel.text = animal;
    if (indexPath.section==0) {
        cell.imageView.image = [UIImage imageNamed:SETTING_GENERAL_ITEM[indexPath.row]];
    }
   else
        cell.imageView.image = [UIImage imageNamed:SETTING_ABOUT_ITEM[indexPath.row]];
    

    
    return cell;
}

- (void)changeSwitch:(UISwitch *)sender{
   
// if (sender.isOn)
        self.isON = sender.isOn;
//    else
//        self.isON = NO;
    
    [self.tableView reloadData];
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        NSMutableDictionary *parameter=[NSMutableDictionary new];

        parameter[@"clientId"]=@"1";
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"status"]=[NSString stringWithFormat:@"%d",!self.isON];

        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERSNotificationSTOP];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([[responseObject objectForKey:@"status"] intValue]==0)
                    self.isON=YES;
                else
                    self.isON=NO;
            }
            else
            {
               self.isON=YES;
            }
            
            [self.tableView reloadData];
        }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  [[AppHelper sharedInstance]hideIndicator];
                  [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                  
              }];
    }

}

-(void)getNotifictaion
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        
        parameter[@"clientId"]=@"1";
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERSGETNotificationSTOP];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([[responseObject objectForKey:@"status"] intValue]==0)
                    self.isON=YES;
                else
                    self.isON=NO;
            }
            else
                self.isON=YES;
            
             [self.tableView reloadData];
        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  self.isON=YES;
                  [[AppHelper sharedInstance]hideIndicator];
                  [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                  
              }];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *Arr=[[arrData objectAtIndex:indexPath.section] valueForKey:@"value"];
    NSString *strVale = [Arr objectAtIndex:indexPath.row];
    if([strVale isEqualToString:@"Language"]){
        LanguageChangeViewController* objLanguageView= (LanguageChangeViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LanguageChangeViewController"];
        
        self.presentHandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objLanguageView fromPresentingViewController:self withStyle:UIModalPresentationCustom fromView:nil andPreferedSize:CGSizeMake(SCREEN_WIDTH-50, 280)];
    }
    else   if([strVale isEqualToString:@"Push Notification"]){
        
    }
    else   if([strVale isEqualToString:@"Change Password"]){
        ChangedPasswordViewController *changePassword=(ChangedPasswordViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ChangedPasswordViewController"];
        
        [self.navigationController pushViewController:changePassword animated:YES];

    }
    else   if([strVale isEqualToString:@"Deactivate Account"]){
        DeactivateAccountViewController *deactivateAccount=(DeactivateAccountViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"DeactivateAccountViewController"];
        
        [self.navigationController pushViewController:deactivateAccount animated:YES];
    }
    else   if([strVale isEqualToString:@"Terms of Use"]){
        
       TermsOfUSeViewController *termsofuse=(TermsOfUSeViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"TermsOfUSeViewController"];
        
        [self.navigationController pushViewController:termsofuse animated:YES];
        
    }
    else   if([strVale isEqualToString:@"Acknowledgement"]){
        AcknowledgeViewController *acknowledge=(AcknowledgeViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"AcknowledgeViewController"];
        
        [self.navigationController pushViewController:acknowledge animated:YES];
        
    }
    else   if([strVale isEqualToString:@"Copyright Info"]){
        CopyrightInfoViewController *copyriteInfo=(CopyrightInfoViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"CopyrightInfoViewController"];
        
        [self.navigationController pushViewController:copyriteInfo animated:YES];
        
    }

    
   
   
}



-(void)initialiseTheListViewWithDataSource:(NSArray *)dataSource withField:(UITextField*)txtField selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    [self.view endEditing:YES];
    

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];

    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            txtField.text = [NSString stringWithFormat:@"   %@",[selectedvalues firstObject]];
        }
    }];

    self.presentHandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];



}

-(void)shareContent{
    
//    NSString * message = @"Great, Enjoy with KINCT App.";
//    
//    UIImage *shareImage = [UIImage imageNamed:@"shareimage"];
//    NSURL *url = [NSURL URLWithString:@"http://www.apple.com/in/itunes/download/"];
//    
//    NSArray * shareItems = @[message, shareImage, url];
//    
//    UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
//    
//    [self presentViewController:avc animated:YES completion:nil];
    
}

@end
