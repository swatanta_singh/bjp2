//
//  MyTaskViewController.m
//  BJP
//
//  Created by swatantra on 11/12/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "MyTaskViewController.h"
#import "BaseViewController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "ShowTaskViewController.h"
@interface MyTaskViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tbleView;
@property(nonatomic,strong)NSArray *arrData;
@end

@implementation MyTaskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
      [self getSearchList];
    self.tbleView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
 
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    __weak MyTaskViewController *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"MyTask", nil);
    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [  weekSelf.navigationController popViewControllerAnimated:YES];
        }
        }];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        UITableViewCell *myCell=[tableView dequeueReusableCellWithIdentifier:@"MyTask" forIndexPath:indexPath];
    UILabel *lblName=(UILabel *)[myCell.contentView viewWithTag:101];
       lblName.text=[AppHelper nullCheck:[[self.arrData objectAtIndex:indexPath.row] valueForKey:@"level"]];
        return myCell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

//    "id": "0",
//    "level": "Assign by me"
//     "id": "1",
//    "level": "Assign to me
    //[ self playBtnPressed:@"https://www.youtube.com/watch?v=wtQrl35l26Y"];
    int value=[[[self.arrData objectAtIndex:indexPath.row] valueForKey:@"id"] intValue];
     switch (value) {
        case 0:{
            ShowTaskViewController *expandAndCollaps=(ShowTaskViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"ShowTaskViewController"];
            expandAndCollaps.serviceType=@"1";
            [self.navigationController pushViewController:expandAndCollaps animated:YES];
        }
            break;
         case 1:{
            ShowTaskViewController *expandAndCollaps=(ShowTaskViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"ShowTaskViewController"];
              expandAndCollaps.serviceType=@"2";
            [self.navigationController pushViewController:expandAndCollaps animated:YES];
         }
            break;
         case 2:{
            UIViewController *expandAndCollaps=[AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"CreateNewTaskVC"];
            [self.navigationController pushViewController:expandAndCollaps animated:YES];
         }
            break;
            
        default:
            break;
    }

}

#pragma mark service
#pragma  mark service
-(void)getSearchList{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        //clientId
        parameter[@"clientId"]=@"1";
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_MENU];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                self.arrData=[responseObject valueForKey:@"Menu"];
                [self.tbleView reloadData];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}

@end
