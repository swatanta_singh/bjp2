//
//  ReplyDetailViewController.h
//  BJP
//
//  Created by toyaj on 10/21/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ReplyDetailViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic,strong)NSArray *categoryArrayList;

@end
