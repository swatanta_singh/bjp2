//
//  ProfileViewController.m
//  BJP
//
//  Created by PDSingh on 8/27/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "ProfileViewController.h"
#define ZOOMPOINT -60
#import "Service.h"
#import "AppHelper.h"
#import "Defines.h"
#import "DataPickerViewController.h"
#import "PresentionHandler.h"
#import "DatePickerViewController.h"
#import "DateFormatters.h"
#import "AFNetworking.h"
#import "ECPhoneNumberFormatter.h"
#import "UIImageView+AFNetworking.h"

@interface ProfileViewController (){
    NSMutableArray *arrData;
    UIToolbar* numberToolbar;
    ECPhoneNumberFormatter *numberformatter;
    int imageChange;
}

@property(nonatomic,strong)PresentionHandler *presnthandler;

@property(weak,nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *tableHeader;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProfile;
@property (strong,nonatomic) IBOutlet UIImageView *iv;
@property (strong,nonatomic) NSLayoutConstraint *topCon;
- (IBAction)coverPhotoChange:(id)sender;
- (IBAction)profileBttonChange:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblNmae;
@property (strong,nonatomic) NSArray *arrCountry;
@end

@implementation ProfileViewController
#pragma mark  view
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
    
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.contentInset.top, 0, keyboardSize.height, 0);
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.contentInset.top, 0, 0, 0);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    numberformatter = [[ECPhoneNumberFormatter alloc] init];

    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    numberToolbar.items = @[flexibleItem,[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor darkGrayColor];
    [numberToolbar setBackgroundColor:[UIColor lightGrayColor]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    arrData=[NSMutableArray new];
    [self setUpData];
    //    self.iv.contentMode = UIViewContentModeScaleToFill; //UIViewContentModeScaleAspectFill;
    //    self.iv.translatesAutoresizingMaskIntoConstraints = NO;
    //    self.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view.
//    self.topCon = [NSLayoutConstraint constraintWithItem:self.iv attribute:NSLayoutAttributeTop relatedBy:0 toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:ZOOMPOINT/2.0];
//    [self.iv addConstraint:[NSLayoutConstraint constraintWithItem:self.iv attribute:NSLayoutAttributeHeight relatedBy:0 toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:self.tableHeader.frame.size.height - ZOOMPOINT*1.5]];
//    [self.view addConstraint:self.topCon];
//    [self.view layoutIfNeeded];
    
    self.topCon = [NSLayoutConstraint constraintWithItem:self.tableHeader attribute:NSLayoutAttributeTop relatedBy:0 toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:ZOOMPOINT/2.0];
    [self.tableHeader addConstraint:[NSLayoutConstraint constraintWithItem:self.tableHeader attribute:NSLayoutAttributeHeight relatedBy:0 toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:self.tableHeader.frame.size.height - ZOOMPOINT*1.5]];
    [self.view addConstraint:self.topCon];
    [self.view layoutIfNeeded];
   
    [self getDataProfile];
    
}

-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
-(void)setUpData{
    
    [arrData removeAllObjects];
        NSURL *url = [NSURL URLWithString:[self normalizePath:[AppHelper appDelegate].cureentUser.normalCoverUrl]];
        [self.iv setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
       NSURL *url2 = [NSURL URLWithString:[self normalizePath:[AppHelper appDelegate].cureentUser.normalPhotoUrl]];
      [self.imgViewProfile setImageWithURL:url2 placeholderImage:[UIImage imageNamed:@"userp"]];
     self.lblNmae.text=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.fullname];
    for (int i=0; i<11; i++) {
        NSMutableDictionary *dict=[NSMutableDictionary new];
        switch (i) {
            case 0:{
                dict[@"value"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.fullname];
                dict[@"place"]=@"NAME: ";
                dict[@"type"]=@"0";
            }
                break;
            case 1:{
                dict[@"value"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.email];
                dict[@"place"]=@"EMAIL: ";
                dict[@"type"]=@"0";
            }
                break;
            case 2:{
                dict[@"value"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.mobile];
                dict[@"place"]=@"MOBILE NO: ";
                dict[@"type"]=@"0";
            }
                break;
            case 3:{
                if([[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.sex] isEqualToString:@"2"]){
                 dict[@"value"]=@"Male";
                }
                else if([[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.sex] isEqualToString:@"1"]){
              dict[@"value"]=@"Female";
                }
              
                dict[@"place"]=@"GENDER: ";
                dict[@"type"]=@"1";
                dict[@"enable"]=@"1";
            }
                break;
            case 4:{
                dict[@"value"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.dateOfBirth];
                dict[@"place"]=@"DATE OF BIRTH: ";
                dict[@"type"]=@"1";
                  dict[@"enable"]=@"1";
            }
                break;
            case 5:{
                dict[@"value"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.country_name];
                dict[@"place"]=@"COUNTRY: ";
                dict[@"type"]=@"0";
                  dict[@"enable"]=@"1";
                 dict[@"id"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.countryId];
            }
                  break;
            case 6:{
                dict[@"value"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.state_name];
                dict[@"place"]=@"STATE: ";
                dict[@"type"]=@"1";
                  dict[@"enable"]=@"1";
                  dict[@"id"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.stateId];
            }
                break;
            case 7:{
                dict[@"value"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.district_name];
                dict[@"place"]=@"DISTRICT: ";
                dict[@"type"]=@"1";
                  dict[@"enable"]=@"0";
                   dict[@"id"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.districtId];
                if(   [dict[@"id"] integerValue]>0){
                     dict[@"enable"]=@"1";
                }
            }
                break;
            case 8:{
                dict[@"value"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.constituency_name];
                dict[@"place"]=@"CONSTITUENCY: ";
                dict[@"type"]=@"1";
                  dict[@"enable"]=@"0";
                    dict[@"id"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.constituencyId];
                if(   [dict[@"id"] integerValue]>0){
                    dict[@"enable"]=@"1";
                }
            }
                break;
            case 9:{
                dict[@"value"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.block_name];
                dict[@"place"]=@"BLOCK: ";
                dict[@"type"]=@"1";
                  dict[@"enable"]=@"0";
                   dict[@"id"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.blockId];
                if(   [dict[@"id"] integerValue]>0){
                    dict[@"enable"]=@"1";
                }
            }
                break;
            case 10:{
                dict[@"value"]=[AppHelper nullCheck:[AppHelper appDelegate].cureentUser.about];
                dict[@"place"]=@"ABOUT: ";
                dict[@"type"]=@"2";
                
            }
                break;
            default:
                break;
        }
        [arrData addObject:dict];
    }
    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
   
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    __weak ProfileViewController *weekSelf=self;
    [self.headerView.headerImageView setImage:[UIImage imageNamed:@"RectangleFlip"]];
    
    
    NSString *titleText = NSLocalizedString(@"Profile", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:@"Camera" WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
        else{
            [weekSelf coverPhotoChange:nil];
        }
    }];
}
#pragma mark textfield
- (void)textViewDidEndEditing:(UITextView *)textView{
    // [AppHelper setupViewAtUp:0 toview:self.view];
    UITableViewCell *mycell=(UITableViewCell*)[textView.superview superview];
    
    NSMutableDictionary *dict=[arrData objectAtIndex:mycell.tag];
    dict[@"value"]=textView.text;
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    UITableViewCell *mycell=(UITableViewCell*)[textField.superview superview];
    NSMutableDictionary *dict=[arrData objectAtIndex:mycell.tag];
    dict[@"value"]=textField.text;
}
#pragma mark table view
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    if (scrollView.contentOffset.y < 0 && scrollView.contentOffset.y > ZOOMPOINT) {
//        self.topCon.constant = ZOOMPOINT/2.0 - scrollView.contentOffset.y/2.0;\
//    }else if (scrollView.contentOffset.y <= ZOOMPOINT) {
//        self.iv.transform = CGAffineTransformMakeScale(1 - (scrollView.contentOffset.y - ZOOMPOINT)/200, 1 - (scrollView.contentOffset.y - ZOOMPOINT)/200);
//    }
    
    self.topCon.constant = ZOOMPOINT/2.0 - scrollView.contentOffset.y/2.0;
    if (scrollView.contentOffset.y <= ZOOMPOINT) {
        self.iv.transform = CGAffineTransformMakeScale(1 - (scrollView.contentOffset.y - ZOOMPOINT)/200, 1 - (scrollView.contentOffset.y - ZOOMPOINT)/200);
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict = [arrData objectAtIndex:indexPath.row];
    if([dict[@"type"] isEqualToString:@"2"]){
        return 100.0;
    }
    else{
        return 45.0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *  myCell ;
    NSDictionary *dict = [arrData objectAtIndex:indexPath.row];
    if([dict[@"type"] isEqualToString:@"2"]){
        myCell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        UITextView *txtFild = (UITextView*)[myCell.contentView viewWithTag:101];
        UILabel *lblname = (UILabel*)[myCell.contentView viewWithTag:102];
        
        txtFild.delegate=self;
        txtFild.editable=NO;
        txtFild.text = [AppHelper nullCheck:dict[@"value"]];
        lblname.text = [AppHelper nullCheck:dict[@"place"]];
        if(self.btnSave.selected){
            txtFild.editable=YES;
        }
           txtFild.inputAccessoryView = numberToolbar;
    }
    else{
        myCell = [tableView dequeueReusableCellWithIdentifier:@"cell1" forIndexPath:indexPath];
        UITextField *txtFild = (UITextField*)[myCell.contentView viewWithTag:101];
        UILabel *lblname = (UILabel*)[myCell.contentView viewWithTag:102];
        UIButton *btnPick = (UIButton*)[myCell.contentView viewWithTag:103];
        [btnPick addTarget:self action:@selector(pickerButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        txtFild.delegate=self;
        txtFild.enabled=NO;
        btnPick.hidden=YES;
        lblname.alpha=1;
        btnPick.enabled=YES;
        txtFild.inputAccessoryView = numberToolbar;

        txtFild.keyboardType=UIKeyboardTypeDefault;
        txtFild.autocapitalizationType = UITextAutocapitalizationTypeWords;
        txtFild.text = [AppHelper nullCheck:dict[@"value"]];
        lblname.text = [AppHelper nullCheck:dict[@"place"]];
       
            if(self.btnSave.selected){
              txtFild.enabled=YES;
            if([dict[@"type"] isEqualToString:@"1"] ){
                btnPick.hidden=NO;
                if([dict[@"enable"] isEqualToString:@"0"] ){
                    lblname.alpha=0.5;
                    btnPick.enabled=NO;
                }
            }

        }
        if ([[AppHelper nullCheck:dict[@"place"]] isEqualToString:@"MOBILE NO: "])
        {
            txtFild.enabled=NO;
            txtFild.text = [numberformatter stringForObjectValue:[AppHelper nullCheck:dict[@"value"]]];
            txtFild.keyboardType=UIKeyboardTypePhonePad;
        }
        if ([[AppHelper nullCheck:dict[@"place"]] isEqualToString:@"COUNTRY: "])
        {
            txtFild.enabled=NO;
         
        }
      
    }
    //@"COUNTRY: "
    myCell.tag=indexPath.row;
  
    return myCell;
}
#pragma mark Button Action
- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    // imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}
-(void)cameraAction{{
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
            // There is not a camera on this device, so don't show the camera button.
            
        }
        else{
            [AppHelper showAlertViewWithTag:10 title:APP_NAME message:@"Camera not available." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        }
        // Camera button tapped.
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Library button tapped.
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        // [self launchSpecialController];
        
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}
    
}
- (IBAction)coverPhotoChange:(id)sender {
    imageChange=1;
    [self cameraAction];
}

- (IBAction)profileBttonChange:(id)sender {
    imageChange=2;
    [self cameraAction];

}

-(void)pickerButtonAction:(UIButton*)sender{
      if(self.btnSave.selected){
          UITableViewCell *mycell=(UITableViewCell*)[sender.superview superview];
          switch (mycell.tag) {
              case 3:
                  [self pickerForGender:mycell withsender:sender];
                  break;
              case 4:
                  [self openDatePicker:mycell button:sender];
                  break;
              case 5:
                  [self pickerForCountry:mycell withsender:sender];
                  break;
              case 6:
                  [self pickerForState:mycell withsender:sender];
                  break;
              case 7:
                  [self pickerForDistrict:mycell withsender:sender];
                  break;
              case 8:
                  [self pickerForConsituencty:mycell withsender:sender];
                  break;
              case 9:
                  [self pickerForBloack:mycell withsender:sender];
                  break;
              default:
                  break;
          }
      }
}
-(IBAction)saveButtonAction:(UIButton*)sender{
    [self.view endEditing:YES];
    if(self.btnSave.selected){
        if(![self validateUserInputs]){
            return;
        }
        else{
            self.btnSave.selected=NO;

        [self uyploadDataProfile];
              [self.tableView reloadData];
        }
    }
    else{
          self.btnSave.selected=YES;
          [self.tableView reloadData];
    }
  
}
#pragma mark picker method
-(void)pickerForGender:(UITableViewCell*)mycell withsender:(UIButton*)sender{
    NSArray *selectedArr = @[@"Male",@"Female"];
    [self pickerDataSource:selectedArr withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_NONE];
}

-(void)pickerForCountry:(UITableViewCell*)mycell withsender:(UIButton*)sender{
    NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGet_Country];
    if(self.arrCountry.count){
        
          [self pickerDataSource:self.arrCountry withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_COUNTRY];
    }
    else{
    [self getDataForPicker:baseURL and:nil with:^(NSArray *data) {
         if(data.count){
          self.arrCountry=data;
          [self pickerDataSource:self.arrCountry withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_COUNTRY];
         }
    }];
    }
}
-(void)pickerForState:(UITableViewCell*)mycell withsender:(UIButton*)sender{
    NSPredicate *prd=[NSPredicate predicateWithFormat:@"place == %@",@"COUNTRY: "];
    NSArray *Arr=[arrData filteredArrayUsingPredicate:prd];
    if(Arr.count){
    NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGet_State];
    NSMutableDictionary *parameter=[NSMutableDictionary new];
       // parameter[@"countryId"]=[[Arr firstObject]valueForKey:@"id"];
    parameter[@"countryId"]=@"99";
        [self getDataForPicker:baseURL and:parameter with:^(NSArray *data) {
            if(data.count){
                [self pickerDataSource:data withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_STATE];
            }
        }];
    }
  }
-(void)pickerForDistrict:(UITableViewCell*)mycell withsender:(UIButton*)sender{
    
    NSPredicate *prd=[NSPredicate predicateWithFormat:@"place == %@",@"STATE: "];
    NSArray *Arr=[arrData filteredArrayUsingPredicate:prd];
    if(Arr.count){
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGet_DIST];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"stateId"]=[[Arr firstObject]valueForKey:@"id"];
        [self getDataForPicker:baseURL and:parameter with:^(NSArray *data) {
            if(data.count){
                [self pickerDataSource:data withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_DIST];
            }
        }];
    }
}
-(void)pickerForConsituencty:(UITableViewCell*)mycell withsender:(UIButton*)sender{
    
    NSPredicate *prd=[NSPredicate predicateWithFormat:@"place == %@",@"DISTRICT: "];
    NSArray *Arr=[arrData filteredArrayUsingPredicate:prd];
    if(Arr.count){
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGet_CONSTNCY];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"districtId"]=[[Arr firstObject]valueForKey:@"id"];
        [self getDataForPicker:baseURL and:parameter with:^(NSArray *data) {
            if(data.count){
                [self pickerDataSource:data withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_CONSRTANCY];
            }
        }];
    }
}
-(void)pickerForBloack:(UITableViewCell*)mycell withsender:(UIButton*)sender{
    
    NSPredicate *prd=[NSPredicate predicateWithFormat:@"place == %@",@"DISTRICT: "];
    NSArray *Arr=[arrData filteredArrayUsingPredicate:prd];
    if(Arr.count){
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGet_BLOCK];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"districtId"]=[[Arr firstObject]valueForKey:@"id"];
        [self getDataForPicker:baseURL and:parameter with:^(NSArray *data) {
            if(data.count){
                [self pickerDataSource:data withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_BLOCK];
            }
        }];
    }
}





-(void)pickerDataSource:(NSArray *)dataSource withField:(UITableViewCell*)myCell selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    [self.view endEditing:YES];
    __weak ProfileViewController *weekSelf=self;

      UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
      DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];
    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            if(type==SEL_COUNTRY){
                NSMutableDictionary *dict=[arrData objectAtIndex:myCell.tag];
                dict[@"value"]=[[selectedvalues firstObject] valueForKey:@"title"];
                 dict[@"id"]=[[selectedvalues firstObject] valueForKey:@"id"];
                [self setupForCountry:myCell];
               
            }
          else  if(type==SEL_STATE){
                NSMutableDictionary *dict=[arrData objectAtIndex:myCell.tag];
                dict[@"value"]=[[selectedvalues firstObject] valueForKey:@"state_title"];
                dict[@"id"]=[[selectedvalues firstObject] valueForKey:@"state_id"];
                [self setupForState:myCell];
                
            }
          else  if(type==SEL_DIST){
              NSMutableDictionary *dict=[arrData objectAtIndex:myCell.tag];
              dict[@"value"]=[[selectedvalues firstObject] valueForKey:@"district_title"];
              dict[@"id"]=[[selectedvalues firstObject] valueForKey:@"district_id"];
              [self setupForDist:myCell];
              
          }
          else  if(type==SEL_CONSRTANCY){
              NSMutableDictionary *dict=[arrData objectAtIndex:myCell.tag];
              dict[@"value"]=[[selectedvalues firstObject] valueForKey:@"cons_title"];
              dict[@"id"]=[[selectedvalues firstObject] valueForKey:@"cons_id"];
              [self setupForContency:myCell];
              
          }
          else  if(type==SEL_BLOCK){
              NSMutableDictionary *dict=[arrData objectAtIndex:myCell.tag];
              dict[@"value"]=[[selectedvalues firstObject] valueForKey:@"block_title"];
              dict[@"id"]=[[selectedvalues firstObject] valueForKey:@"block_id"];
              
          }
           
            else{
            NSMutableDictionary *dict=[arrData objectAtIndex:myCell.tag];
            dict[@"value"]=[selectedvalues firstObject];
            }
            
            [weekSelf.tableView reloadData];
        }
    }];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];
}
-(void)openDatePicker:(UITableViewCell*)myCell button:(UIButton*)btn{
    [self.view endEditing:YES];
    __weak ProfileViewController *weekSelf=self;

    DatePickerViewController *datePickerViewController = (DatePickerViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"DatePickerViewController"];
    NSMutableDictionary *dict=arrData[myCell.tag];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:datePickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:btn andPreferedSize:DropDownSize];
    NSDate *minDate=[NSDate dateWithTimeIntervalSinceNow:-60*365*24*60*60] ;
    NSDate *maxDate=[NSDate dateWithTimeIntervalSinceNow:-18*365*24*60*60] ;
    NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];
   
       [datePickerViewController initDatePickerWithDatePickerMode:UIDatePickerModeDate minDate:minDate maxDate:maxDate andDefaultSelectedDate:maxDate withReturnedDate:^(NSDate *selectedDate) {
        if (selectedDate) {
            NSString *strDatte= [formatter stringFromDate:selectedDate];
            dict[@"value"]   =  strDatte;
        }
           [weekSelf.tableView reloadData];
    }];
    
}

#pragma mark service
-( BOOL )validateUserInputs
{
    NSString *errorMessage=nil;
      bool IsValid=YES;
    NSPredicate*prd=[NSPredicate predicateWithFormat:@"place == %@",@"EMAIL: "];
    NSArray  *Arr=[arrData filteredArrayUsingPredicate:prd];
    if(Arr.count){
        NSDictionary *dict=[Arr firstObject];

        if([dict[@"value"] length]>0){
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        
        if(![emailTest evaluateWithObject:dict[@"value"]])
        {
            errorMessage=@"Please enter valid email";
            [AppHelper showAlertViewWithTag:3 title:nil message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            errorMessage=@"";
            IsValid= NO;
        }
        }
    }

  
    
    return IsValid;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==100){
        [[AppHelper navigationController] popViewControllerAnimated:YES];
        
    }
}
-(void)uyploadDataProfile
{
     if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUPLOADPROFILE];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
       //  parameter[@"bio"]=@"1";
         //parameter[@"countryId"]=@"99";
    //    parameter[@"assemblyId"]=@"2";
        //, assemblyId, orgMembershipId, sex, year, month, day
        //state
        NSPredicate *prd=[NSPredicate predicateWithFormat:@"place == %@",@"STATE: "];
        NSArray *Arr=[arrData filteredArrayUsingPredicate:prd];
        if(Arr.count){
            parameter[@"stateId"]=[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"id"]];
             parameter[@"state_name"]=[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"value"]];
        }
        //Country
        prd=[NSPredicate predicateWithFormat:@"place == %@",@"COUNTRY: "];
        Arr=[arrData filteredArrayUsingPredicate:prd];
        if(Arr.count){
            parameter[@"countryId"]=@"99";//[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"id"]];
            parameter[@"country_name"]=@"India";//[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"value"]];
        }
        //EMAIL
        prd=[NSPredicate predicateWithFormat:@"place == %@",@"EMAIL: "];
        Arr=[arrData filteredArrayUsingPredicate:prd];
        if(Arr.count){
            parameter[@"email"]=[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"value"]];
        }
        //DIST
        prd=[NSPredicate predicateWithFormat:@"place == %@",@"DISTRICT: "];
        Arr=[arrData filteredArrayUsingPredicate:prd];
        if(Arr.count){
            parameter[@"districtId"]=[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"id"]];
            parameter[@"district_name"]=[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"value"]];
        }
        //Const
        prd=[NSPredicate predicateWithFormat:@"place == %@",@"CONSTITUENCY: "];
        Arr=[arrData filteredArrayUsingPredicate:prd];
        if(Arr.count){
            parameter[@"constituencyId"]=[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"id"]];
            parameter[@"constituency_name"]=[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"value"]];
        }
        //BLOCk
        prd=[NSPredicate predicateWithFormat:@"place == %@",@"BLOCK: "];
        Arr=[arrData filteredArrayUsingPredicate:prd];
        if(Arr.count){
            parameter[@"blockId"]=[AppHelper nullCheck:[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"id"]]];
            parameter[@"block_name"]=[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"value"]];
        }
        //GEnder
        prd=[NSPredicate predicateWithFormat:@"place == %@",@"GENDER: "];
        Arr=[arrData filteredArrayUsingPredicate:prd];
        if(Arr.count){
             parameter[@"sex"]=@"1";
            if([[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"value"]] isEqualToString:@"Male"]){
               parameter[@"sex"]=@"2";
            }
        }
        //mobile
        prd=[NSPredicate predicateWithFormat:@"place == %@",@"MOBILE NO: "];
        Arr=[arrData filteredArrayUsingPredicate:prd];
        if(Arr.count){
            parameter[@"mobile"]=[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"value"]];
        }
        //full mname
        prd=[NSPredicate predicateWithFormat:@"place == %@",@"NAME: "];
        Arr=[arrData filteredArrayUsingPredicate:prd];
        if(Arr.count){
            parameter[@"fullname"]=[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"value"]];
        }
        //About
        prd=[NSPredicate predicateWithFormat:@"place == %@",@"ABOUT: "];
        Arr=[arrData filteredArrayUsingPredicate:prd];
        if(Arr.count){
            parameter[@"about"]=[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"value"]];
        }
//@"DATE OF BIRTH: "
        prd=[NSPredicate predicateWithFormat:@"place == %@",@"DATE OF BIRTH: "];
        Arr=[arrData filteredArrayUsingPredicate:prd];
        if(Arr.count){
            parameter[@"dateOfBirth"]=[AppHelper nullCheck:[[Arr firstObject]valueForKey:@"value"]];
        }
        NSDictionary *dict=nil;
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:nil];
        NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        dict = @{@"body":json};
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            NSDictionary *dict=(NSDictionary*)responseObject;
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            else if ([dict.allKeys count])
            {
                User *cuser=[[Service sharedEventController] parseUserData:[AppHelper appDelegate].cureentUser.accountId withData:dict];
                [AppHelper appDelegate].cureentUser=cuser;
                [self getDataProfile];
                 [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Profile updated successfully." delegate:self cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}

-(void)getDataProfile
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGet_PROFILE];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"profileId"]=[AppHelper appDelegate].cureentUser.accountId;
        NSDictionary *dict=nil;
      
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:nil];
            NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            dict = @{@"body":json};
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            NSDictionary *dict=(NSDictionary*)responseObject;
                    if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            else if ([dict.allKeys count])
            {
                User *cuser=[[Service sharedEventController] parseUserData:[AppHelper appDelegate].cureentUser.accountId withData:dict];
                [AppHelper appDelegate].cureentUser=cuser;
                [self setUpData];
            }
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}

-(void)getDataForPicker:(NSString *)baseURL and:(NSMutableDictionary*)parameter with:(void (^)(NSArray* data))block
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSDictionary *dict=nil;
        if(parameter){
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:nil];
        NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
         dict = @{@"body":json};
        }
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[AppHelper sharedInstance]hideIndicator];
        if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                block(nil);
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                      block([responseObject objectForKey:@"list"]);
                
            }
            else{
                  [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
          
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            block(nil);
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}
-(void)setupForCountry:(UITableViewCell*)myCell{
    // setUp
    NSMutableDictionary *dict1=[arrData objectAtIndex:myCell.tag+1];
    dict1[@"enable"]=@"1";
    dict1[@"value"]=@"";
    dict1[@"id"]=@"";
    // dist
    dict1=[arrData objectAtIndex:myCell.tag+2];
    dict1[@"enable"]=@"0";
    dict1[@"value"]=@"";
    dict1[@"id"]=@"";
    // cons
    dict1=[arrData objectAtIndex:myCell.tag+3];
    dict1[@"enable"]=@"0";
    dict1[@"value"]=@"";
    dict1[@"id"]=@"";
    // block
    dict1=[arrData objectAtIndex:myCell.tag+4];
    dict1[@"enable"]=@"0";
    dict1[@"value"]=@"";
    dict1[@"id"]=@"";
}
-(void)setupForState:(UITableViewCell*)myCell{
    // setUp
    NSMutableDictionary *dict1=[arrData objectAtIndex:myCell.tag+1];
    dict1[@"enable"]=@"1";
    dict1[@"value"]=@"";
    dict1[@"id"]=@"";
    // dist
    dict1=[arrData objectAtIndex:myCell.tag+2];
    dict1[@"enable"]=@"0";
    dict1[@"value"]=@"";
    dict1[@"id"]=@"";
    // cons
    dict1=[arrData objectAtIndex:myCell.tag+3];
    dict1[@"enable"]=@"0";
    dict1[@"value"]=@"";
    dict1[@"id"]=@"";
    // block
}
-(void)setupForDist:(UITableViewCell*)myCell{
    // setUp
    NSMutableDictionary *dict1=[arrData objectAtIndex:myCell.tag+1];
    dict1[@"enable"]=@"1";
    dict1[@"value"]=@"";
    dict1[@"id"]=@"";
    // dist
    dict1=[arrData objectAtIndex:myCell.tag+2];
    dict1[@"enable"]=@"0";
    dict1[@"value"]=@"";
    dict1[@"id"]=@"";
    // cons
   }
-(void)setupForContency:(UITableViewCell*)myCell{
    // setUp
    NSMutableDictionary *dict1=[arrData objectAtIndex:myCell.tag+1];
    dict1[@"enable"]=@"1";
    dict1[@"value"]=@"";
    dict1[@"id"]=@"";
}

#pragma mark UIImagePickerControllerDelegate ------------


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    if(imageChange==2){
    [self.imgViewProfile setImage:image];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kPROFILEPIC_UPLOAD];

        [self uploadPhoto:image with:baseURL];
    }
    else{
     [self.iv setImage:image];
          NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kPROFILECOVER_UPLOAD];
         [self uploadPhoto:image with:baseURL];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)uploadPhoto:(UIImage*)imageName with:(NSString*)baseUrl{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://www.bjp-up.com/api/v1/method/profile.NewuploadPhoto.inc"]];
    NSData *imageData = UIImageJPEGRepresentation(imageName, 0.5);
    NSDictionary *parameters = @{@"accountId": [AppHelper appDelegate].cureentUser.accountId, @"accessToken" : [AppHelper userDefaultsForKey:ACCESS_TOKEN]};
    AFHTTPRequestOperation *op = [manager POST:baseUrl parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        [formData appendPartWithFileData:imageData name:@"uploaded_file" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self getDataProfile];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
    [op start];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
