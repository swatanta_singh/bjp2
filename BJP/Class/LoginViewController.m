
//
//  LoginViewController.m
//  KINCT
//
//  Created by Ashish on 26/02/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import "LoginViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "User.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "RegistrationViewController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "ForgetPasswordViewController.h"
#import "HomeeViewController.h"
#import "VerifyViewController.h"
#import "HomeViewController.h"
#import "AppDelegate.h"
#import "LanguageChangeViewController.h"

@implementation LoginViewController


#pragma mark view cycle
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
    [alert dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)viewDidLoad {
  
    
    [super viewDidLoad];
    
     self.tblView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
     numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    numberToolbar.items = @[flexibleItem,[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor darkGrayColor];
    [numberToolbar setBackgroundColor:[UIColor lightGrayColor]];
    if(IS_IPHONE_4_OR_LESS ){
        self.tblHeightConstant.constant=100;
        self.heigtIcon.constant=120;
         self.widthIcon.constant=120;
    }
    else if( IS_IPHONE_5){
        self.tblHeightConstant.constant=100;
//        self.heigtIcon.constant=150;
//        self.widthIcon.constant=150;
    }
    
    
   // [GIDSignInButton class];
//    GIDSignIn *signIn = [GIDSignIn sharedInstance];
//    signIn.shouldFetchBasicProfile = YES;
//    signIn.delegate = self;
//    signIn.uiDelegate = self;
    
   // [self addGooglePlus];
    /*
    if([AppHelper  userDefaultsForKey:USER_ID]){
        [AppHelper appDelegate].cureentUser=[[Service sharedEventController] getLoginDetailsWith:[AppHelper userDefaultsForKey:USER_ID]];
        UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"HomeeViewController"];
        [self.navigationController pushViewController:objVerifyView animated:YES];
        }
    [self performSelector:@selector(finalMethoForLanguge) withObject:nil afterDelay:8];
    
    */

    AppDelegate *object=(AppDelegate *)[UIApplication sharedApplication].delegate;
    object.loginObjct=self;
}



-(void)finalMethoForLanguge{
    
    if([AppHelper nullCheck:  [AppHelper userDefaultsForKey:APP_LANGAUGE]].length==0){
        [AppHelper saveToUserDefaults:@"en" withKey:APP_LANGAUGE];
        LanguageChangeViewController* objLanguageView= (LanguageChangeViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LanguageChangeViewController"];
        
        self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objLanguageView fromPresentingViewController:self withStyle:UIModalPresentationCustom fromView:nil andPreferedSize:CGSizeMake(SCREEN_WIDTH-50, 280)];
      }
}

//-(void)addGooglePlus
//{
//    self.signInButton=[[GIDSignInButton alloc] initWithFrame:CGRectMake(10,self.view.frame.size.height/2-100,self.view.frame.size.width-20,50)];
//   // self.signInButton.backgroundColor=[UIColor redColor];
//    self.signInButton.layer.cornerRadius=15.0f;
//    [self.view addSubview:self.signInButton];
//
//}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.automaticallyAdjustsScrollViewInsets = false;
    
    [self.headerView.headerImageView setHidden:YES];
    __weak LoginViewController *weekSelf=self;
    [self setUpHeaderWithTitle:@"" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self setlanguage];
    //login_google
}

-(void)viewDidAppear:(BOOL)animated{
    
    if([AppHelper userDefaultsForKey:USER_NAME]){
        emailTextField.text=[AppHelper nullCheck:[AppHelper userDefaultsForKey:USER_NAME]];
    }
}

#pragma mark table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(IS_IPHONE_4_OR_LESS || IS_IPHONE_5){
        return 50;
    }
    else{
        return 65;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    UITableViewCell *myCell=[tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
    myCell.tag=indexPath.row;
    UITextField *txtFild=(UITextField*)[myCell.contentView viewWithTag:101];
    txtFild.delegate=self;
    if(indexPath.row==0){
        txtFild.inputAccessoryView = numberToolbar;
        txtFild.placeholder=NSLocalizedString(@"login_mobilenumber", nil);
        txtFild.keyboardType=UIKeyboardTypePhonePad;
        txtFild.returnKeyType=UIReturnKeyNext;
        txtFild.secureTextEntry=NO;
       
        emailTextField=txtFild;
    }//login_placeholderpas
    else{
        txtFild.inputAccessoryView = nil;
        txtFild.placeholder=NSLocalizedString(@"login_placeholderpas", nil);
        txtFild.secureTextEntry=YES;
        txtFild.keyboardType=UIKeyboardTypeEmailAddress;
        txtFild.returnKeyType=UIReturnKeyDone;
      
        passwordTextField=txtFild;
    }
    
    [txtFild setValue:[UIColor whiteColor]
           forKeyPath:@"_placeholderLabel.textColor"];
    return myCell;
}
#pragma mark -------------------------
#pragma mark IBActions ---------------
- (IBAction)registrationButtonAction:(id)sender {
    UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"RegistrationViewController"];
    [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
}
- (IBAction)faqceBookButtonAction:(id)sender {
  
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login logInWithReadPermissions: @[@"public_profile", @"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
         } else if (result.isCancelled) {
         } else {
               [self getFacebookProfileInfo];
         }
     }];

}

-(IBAction)forgotAction:(id)sender
{
    if (emailTextField.text.length>0) {
        [self.view endEditing:YES];
        ForgetPasswordViewController* objVerifyView=(ForgetPasswordViewController*) [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ForgetPasswordViewController"];
        objVerifyView.mobilenumber = emailTextField.text;
        [self presentViewController:objVerifyView animated:YES completion:nil];
    }
    else
    {
         [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please enter registered phone number."  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}


-(IBAction)signInAction:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *emailStr=@"";
    NSString *phoneStr=@"";
    
    FieldType type = [self validateEmailOrPhoneValue:emailTextField];
    
    if(type == FieldTypeNone)
        return;
    
    if(type == FieldTypePhone){
        phoneStr=emailTextField.text;
    }
    else{
        emailStr=emailTextField.text;
    }
    
    if([[passwordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
    {
        UIAlertView *alerts = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alerts.tag=112;
        [alerts show];
    }
    else{
        [self signUpVerification:emailStr withPhn:phoneStr];
   }
    
}
-(void)signUpVerification:(NSString*)email withPhn:(NSString*)phn{
  
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameters =[NSMutableDictionary new];
        parameters[@"gcm_regId"]=[AppHelper userDefaultsForKey:Device_Token];
        parameters[@"clientId"]=[NSNumber numberWithInteger:1];
        parameters[@"mobile"]=phn;
        parameters[@"password"]=passwordTextField.text;
        parameters[@"device_type"]=@"iPhone";
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kSIGN_IN];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer]; // if response JSON format
    [manager POST:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
           [[AppHelper sharedInstance]hideIndicator];
   
        NSString *errorMsg=@"";
        
        if(responseObject==nil)
        {
            errorMsg=@"Server not responding";
            }
        else if ([[responseObject valueForKey:@"error_code"] integerValue]==453)
        {
            VerifyViewController* objLanguageView= (VerifyViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"VerifyViewController"];
            objLanguageView.strPhn=[responseObject valueForKey:@"mobile"];
            self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objLanguageView fromPresentingViewController:self withStyle:UIModalPresentationCustom fromView:nil andPreferedSize:CGSizeMake(SCREEN_WIDTH-50, 280)];
        }
        else if ([[responseObject valueForKey:@"error_code"] integerValue]==200)
        {
            [AppHelper saveToUserDefaults:[responseObject objectForKey:@"accessToken"] withKey:ACCESS_TOKEN];
               [AppHelper saveToUserDefaults:parameters[@"mobile"] withKey:USER_NAME];
            
            NSDictionary *resultDict = [[responseObject objectForKey:@"account"]lastObject];
            [AppHelper saveToUserDefaults:[AppHelper nullCheck:[responseObject objectForKey:@"accountId"]] withKey:USER_ID];
            
            [AppHelper saveToUserDefaults:[[responseObject[@"account"] lastObject] objectForKey:@"user_type"] withKey:USER_TYPE];

            User *cuser=[[Service sharedEventController] parseUserData:[responseObject objectForKey:@"accountId"] withData:resultDict];
            [AppHelper appDelegate].cureentUser=cuser;
            [[AppHelper appDelegate] saveContext];
            NSArray *allViewControllers = [self.navigationController viewControllers];
            for (UIViewController *aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[HomeeViewController class]]) {
                    [self.navigationController popToViewController:aViewController  animated:YES];
                }
            }
        }
        else{
            errorMsg=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
        }
        if(errorMsg.length>3){
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [AppHelper showMessageErrorCode:error];
        [[AppHelper sharedInstance]hideIndicator];
    }];
    }
    else{
        
        [[AppHelper sharedInstance]hideIndicator];
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
}


#pragma mark ----------------------------
#pragma mark UITextFieldDelegate --------
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==emailTextField || textField.tag==5001){
     
            if (textField.text.length >= 10 && range.length == 0)
            {
                return NO; // return NO to not change text
            }
        
    }
     return YES;
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
        case 111:
            [emailTextField becomeFirstResponder];
            break;
        case 112:
            [passwordTextField becomeFirstResponder];
            break;
            
            break;
        default:
            break;
    }
}

-(FieldType)validateEmailOrPhoneValue:(UITextField*)txtField
{
    
    if([[txtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter mobile no." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag=111;
        [alert show];
        return FieldTypeNone;
    }
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    BOOL isValidEmail = [emailTest evaluateWithObject:txtField.text];
    
    if(!isValidEmail)
    {
        NSString *phoneStr=txtField.text;
        NSCharacterSet *numberSet = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
        numberSet = [numberSet invertedSet];
        NSRange r = [phoneStr rangeOfCharacterFromSet:numberSet];
        if(r.location != NSNotFound  ||  [phoneStr length]!=10){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid  mobile no." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alert.tag=111;
            [alert show];
            
            return FieldTypeNone;
        }
        else
            return FieldTypePhone;
    }
    else
        return FieldTypeEmail;
    
    return FieldTypeNone;
}




-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    if(emailTextField==textField){
        [passwordTextField becomeFirstResponder];
        
    }
    else{
        [self.view endEditing:YES];
        [self signInAction:nil];
        
    }
    
    return YES;
}



#pragma mark ---------------------
#pragma mark FBSDKLoginButtonDelegate --------
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark FBSDKLoginButtonDelegate --------
-(void)getFacebookProfileInfo {
    
    NSDictionary *dict=[NSDictionary dictionaryWithObject:@"id, name, first_name, last_name, picture.type(large), email,gender,birthday" forKey:@"fields"];
    
    FBSDKGraphRequest *requestMe = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:dict];
    
    FBSDKGraphRequestConnection *connection = [[FBSDKGraphRequestConnection alloc] init];
    
    [connection addRequest:requestMe completionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        
        NSString *responseString = [connection.URLResponse description];
        if(result)
        {
          //  NSMutableDictionary *parameters =[NSMutableDictionary new];
            NSDictionary *dict=(NSDictionary*)result;
            NSArray *arr=[dict allKeys];
                if([arr containsObject:@"name"]){
                self.FullName=[AppHelper nullCheck:dict[@"name"]];
            }
            if([arr containsObject:@"id"]){
             self.fbId=[AppHelper nullCheck:dict[@"id"]];
            }
                [self loginWithFb];
        }
        
    }];
    
    [connection start];
}



-(void)loginWithFb{
 
    NSMutableDictionary *parameters =[NSMutableDictionary new];
    parameters[@"clientId"]=@"1";
    parameters[@"gcm_regId"]=[AppHelper userDefaultsForKey:Device_Token];
    parameters[@"facebookId"]=self.fbId;
    parameters[@"device_type"]=@"iPhone";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kSIGN_IN_FB];

    manager.responseSerializer = [AFJSONResponseSerializer serializer]; // if response JSON format
    [manager POST:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [[AppHelper sharedInstance]hideIndicator];
        
        NSString *errorMsg=@"";
        
        if(responseObject==nil)
        {
            errorMsg=@"Server not responding";
        }
        else if ([[responseObject valueForKey:@"error_code"] integerValue]==453)
        {
            VerifyViewController* objLanguageView= (VerifyViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"VerifyViewController"];
            objLanguageView.strPhn=[responseObject valueForKey:@"mobile"];
            self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objLanguageView fromPresentingViewController:self withStyle:UIModalPresentationCustom fromView:nil andPreferedSize:CGSizeMake(SCREEN_WIDTH-50, 280)];
        }
        else  if([[responseObject valueForKey:@"error_code"] integerValue]==404){
            RegistrationViewController* objVerifyView= (RegistrationViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"RegistrationViewController"];
            objVerifyView.fbId=self.fbId;
            objVerifyView.fulname=self.FullName;
            [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
            
        }
        else if ([[responseObject valueForKey:@"error_code"] integerValue]==200)
        {
            [AppHelper saveToUserDefaults:parameters[@"mobile"] withKey:USER_NAME];
            [AppHelper saveToUserDefaults:[responseObject objectForKey:@"accessToken"] withKey:ACCESS_TOKEN];
            NSDictionary *resultDict = [[responseObject objectForKey:@"account"]lastObject];
            [AppHelper saveToUserDefaults:[AppHelper nullCheck:[responseObject objectForKey:@"accountId"]] withKey:USER_ID];
            User *cuser=[[Service sharedEventController] parseUserData:[responseObject objectForKey:@"accountId"] withData:resultDict];
            [AppHelper appDelegate].cureentUser=cuser;
            [[AppHelper appDelegate] saveContext];
            NSArray *allViewControllers = [self.navigationController viewControllers];
            for (UIViewController *aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[HomeeViewController class]]) {
                    [self.navigationController popToViewController:aViewController  animated:YES];
                }
            }
        }
        else{
            errorMsg=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
        }
        if(errorMsg.length>3){
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }

      
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Service error" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];

        
    }];
}

#pragma mark google

-(void)serviceGoogleSignIn:(NSDictionary*)parameters{
    
  
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGOOGLE_SIGN];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer]; // if response JSON format
    [manager POST:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        [[AppHelper sharedInstance]hideIndicator];
        
        NSString *errorMsg=@"";
        
        if(responseObject==nil)
        {
            errorMsg=@"Server not responding";
        }
        else if ([[responseObject valueForKey:@"error_code"] integerValue]==453)
        {
            VerifyViewController* objLanguageView= (VerifyViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"VerifyViewController"];
            objLanguageView.strPhn=[responseObject valueForKey:@"mobile"];
            self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objLanguageView fromPresentingViewController:self withStyle:UIModalPresentationCustom fromView:nil andPreferedSize:CGSizeMake(SCREEN_WIDTH-50, 280)];
        }
        else  if([[responseObject valueForKey:@"error_code"] integerValue]==404){
            RegistrationViewController* objVerifyView= (RegistrationViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"RegistrationViewController"];
            objVerifyView.dictGoogle=parameters;
            objVerifyView.fulname=self.FullName;
            [[AppHelper navigationController] pushViewController:objVerifyView animated:YES];
            
        }
        else if ([[responseObject valueForKey:@"error_code"] integerValue]==200)
        {
            [AppHelper saveToUserDefaults:parameters[@"mobile"] withKey:USER_NAME];
            [AppHelper saveToUserDefaults:[responseObject objectForKey:@"accessToken"] withKey:ACCESS_TOKEN];
            NSDictionary *resultDict = [[responseObject objectForKey:@"account"]lastObject];
            [AppHelper saveToUserDefaults:[AppHelper nullCheck:[responseObject objectForKey:@"accountId"]] withKey:USER_ID];
            User *cuser=[[Service sharedEventController] parseUserData:[responseObject objectForKey:@"accountId"] withData:resultDict];
            [AppHelper appDelegate].cureentUser=cuser;
            [[AppHelper appDelegate] saveContext];
            NSArray *allViewControllers = [self.navigationController viewControllers];
            for (UIViewController *aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[HomeeViewController class]]) {
                    [self.navigationController popToViewController:aViewController  animated:YES];
                }
            }
        }
        else{
            errorMsg=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
        }
        if(errorMsg.length>3){
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
         [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Service error" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }];
}




- (IBAction)googlePlus:(id)sender {
    
//    [[GIDSignIn sharedInstance] signOut];
//    [GIDSignIn sharedInstance].delegate = self;
//    [GIDSignIn sharedInstance].uiDelegate=self;
//    [[GIDSignIn sharedInstance] signIn];
}

- (IBAction)forgotPasswordButton:(id)sender {
    
  [self showForgotPasswordAlert];
}

-(void)showForgotPasswordAlert
{
    alert = [UIAlertController alertControllerWithTitle:@"Forgot Password"
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
      
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.placeholder =@"Enter mobile";
        textField.text= emailTextField.text;
        textField.keyboardType = UIKeyboardTypePhonePad;
        textField.returnKeyType = UIReturnKeyNext;
        textField.inputAccessoryView = numberToolbar;
        textField.tag = 5001;
        textField.delegate = self;
        
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"SUBMIT"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         UITextField *textField = [alert.textFields firstObject];
                                                         if (textField.text.length>0) {
                                                             [self generateNewPasswod:textField.text];
                                                             
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }
                                                         else
                                                         {
                                                             [self showForgotPasswordAlert];
                                                         }
                                                     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
    
    [alert addAction:cancelAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

//- (void)signIn:(GIDSignIn *)signIn
//didSignInForUser:(GIDGoogleUser *)user
//     withError:(NSError *)error {
//    if(error==nil){
//    // Perform any operations on signed in user here.
//    NSString *userId = user.userID;                  // For client-side use only!
////    NSString *idToken = user.authentication.idToken; // Safe to send to the server
//    self.FullName = user.profile.name;
////    NSString *givenName = user.profile.givenName;
////    NSString *familyName = user.profile.familyName;
//    NSString *email = user.profile.email;
//    
//    NSMutableDictionary *parameters =[NSMutableDictionary new];
//        parameters[@"clientId"]=@"1";
//        parameters[@"gcm_regId"]=[AppHelper userDefaultsForKey:Device_Token];
//        parameters[@"email_id"]=[AppHelper nullCheck:email];
//         parameters[@"google_id"]=[AppHelper nullCheck:userId];
//        [self serviceGoogleSignIn:parameters];
//    }
//    else{
//        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"There is some problem to login google account" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//    }
//    // ...
//}

//- (void)signIn:(GIDSignIn *)signIn
//didDisconnectWithUser:(GIDGoogleUser *)user
//     withError:(NSError *)error {
//    // Perform any operations when the user disconnects from app here.
//    // ...
//}
//
//- (void)signIn:(GIDSignIn *)signIn
//presentViewController:(UIViewController *)viewController {
//    [self presentViewController:viewController animated:YES completion:nil];
//}
//
//- (void)signIn:(GIDSignIn *)signIn
//dismissViewController:(UIViewController *)viewController {
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
//
//- (IBAction)didTapSignOut:(id)sender {
//    [[GIDSignIn sharedInstance] signOut];
//}

#pragma mark localization

- (void)localizeTexts {
//    self.label.text = NSLocalizedString(@"explanation_message", nil);
//    [self.button setTitle:NSLocalizedString(@"language_switch_title", nil) forState:UIControlStateNormal];
}

-(void)setlanguage
{
    self.login_message.text = NSLocalizedString(@"login_message", nil);
    [self.btnLogin setTitle:NSLocalizedString(@"language_switch_title", nil) forState:UIControlStateNormal];
    
    [self.btn_signUP setTitle:NSLocalizedString(@"login_signup", nil) forState:UIControlStateNormal];
    
    [self.btn_ggogle setTitle:NSLocalizedString(@"login_google", nil) forState:UIControlStateNormal];
    [self.btn_facebook setTitle:NSLocalizedString(@"login_facebook", nil) forState:UIControlStateNormal];
    [self.btn_or setTitle:NSLocalizedString(@"login_or", nil) forState:UIControlStateNormal];
    [_tblView reloadData];
}


-(void)generateNewPasswod:(NSString *)forgotSrtring
{
        if([AppHelper appDelegate].checkNetworkReachability)
        {
            [[AppHelper sharedInstance]showIndicator];
            NSMutableDictionary *parameters =[NSMutableDictionary new];
            parameters[@"mobile"]=forgotSrtring;
            parameters[@"clientId"]=[NSNumber numberWithInteger:1];
           
            NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kForgotPassword];
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer = [AFJSONResponseSerializer serializer]; // if response JSON format
            [manager POST:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [[AppHelper sharedInstance]hideIndicator];
                
                NSString *errorMsg=@"";
                
                if(responseObject==nil)
                {
                    errorMsg=@"Server not responding";
                }
               
                else if ([[responseObject valueForKey:@"error_code"] integerValue]==200)
                {
                   
                    ForgetPasswordViewController* objVerifyView=(ForgetPasswordViewController*) [AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ForgetPasswordViewController"];
                    objVerifyView.mobilenumber = forgotSrtring;
                    [self presentViewController:objVerifyView animated:NO completion:nil];
                }
                else{
                    errorMsg=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
                }
                if(errorMsg.length>3){
                    [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                [AppHelper showMessageErrorCode:error];
                [[AppHelper sharedInstance]hideIndicator];
            }];
        }
}
@end
