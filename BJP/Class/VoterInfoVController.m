//
//  VoterInfoVController.m
//  BJP
//
//  Created by PDSingh on 12/13/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "VoterInfoVController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "ReportLocVController.h"
#import "BoothListVController.h"

@interface VoterInfoVController ()
{
    NSMutableArray *arrData;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation VoterInfoVController

- (void)viewDidLoad {
    
    self.tableView.tableFooterView = [[UIView alloc ]initWithFrame:CGRectZero];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)viewWillAppear:(BOOL)animated {
    
    [self getMenu];
   
    [super viewWillAppear:animated];
    __weak VoterInfoVController *weekSelf=self;
    NSString *titleText = NSLocalizedString(@"Voter", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"menu" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [[[AppHelper sharedInstance]menuViewController] setUpMove];
        }
        else{
            [weekSelf.view endEditing:YES];
        }
    }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)getMenu{
 
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        //clientId
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];

        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kUSERviewButton];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([[responseObject valueForKey:@"menu"] isKindOfClass:[NSArray class]])
                arrData=[responseObject valueForKey:@"menu"];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            [self.tableView reloadData];

            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([arrData count]>0 )
    {
        numOfSections  = 1;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"Data not found";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView_ cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *cellIdentifier = @"Cell";
    NSString *title =[AppHelper nullCheck: [[arrData objectAtIndex:indexPath.row]objectForKey:@"title"]];
    UITableViewCell *cell = (UITableViewCell *)[tableView_ dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    cell.textLabel.text = title;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSString *strVale=[[arrData objectAtIndex:indexPath.row] valueForKey:@"type"];
   
    if([strVale isEqualToString:@"Report"]){
        
       

        ReportLocVController *reportLocVController = (ReportLocVController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Voter" WithName:@"ReportLocVController"];
        [self.navigationController pushViewController:reportLocVController animated:YES];

    }
    else if([strVale isEqualToString:@"Entry"]){
        
        BoothListVController *changePassword=(BoothListVController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Voter" WithName:@"BoothListVController"];
        [self.navigationController pushViewController:changePassword animated:YES];
    }
    
}



@end
