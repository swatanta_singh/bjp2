//
//  CreateNewTaskVC.m
//  BJP
//
//  Created by swatantra on 12/26/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "CreateNewTaskVC.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "SearchContactViewController.h"
#import "ImageController.h"
#import "DateFormatters.h"
#import "DataPickerViewController.h"
#import "PresentionHandler.h"
#import "DatePickerViewController.h"
@interface CreateNewTaskVC (){
    UIToolbar*   numberToolbar;
    NSMutableArray *arrMain;
}
@property (weak, nonatomic) IBOutlet UIView *viewForFoter;
@property (weak, nonatomic) IBOutlet UITextView *txtView;
@property(nonatomic,strong)NSArray *arrDesination;
@property(nonatomic,strong)NSArray *arrArea;
@property (weak, nonatomic) IBOutlet UITextField *txtField;

@property(nonatomic,strong)PresentionHandler *presnthandler;
@property (weak, nonatomic) IBOutlet UIButton *btnAttacmnt;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UILabel *lblArea;
@property (weak, nonatomic) IBOutlet UILabel *lblDegniation;
-(IBAction)endButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
- (IBAction)startButtonActon:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblEndDate;
- (IBAction)desginationButtonClick:(id)sender;
-(IBAction)imgButtonClick:(id)sender;
- (IBAction)submitbuttonClick:(id)sender;
- (IBAction)areaButtonClick:(id)sender;
- (IBAction)attachmentButtonClick:(id)sender;
@end

@implementation CreateNewTaskVC
#pragma mark view life
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
    
}
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.tblView.contentInset = UIEdgeInsetsMake(self.tblView.contentInset.top, 0, keyboardSize.height, 0);
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.tblView.contentInset = UIEdgeInsetsMake(self.tblView.contentInset.top, 0, 0, 0);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    arrMain=[NSMutableArray new];
    [self getDesination];
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320,40)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor blackColor];
    [numberToolbar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Header"]]];
    self.txtView.inputAccessoryView = numberToolbar;
  
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    __weak CreateNewTaskVC *weekSelf=self;
    [self setUpHeaderWithTitle:@"Create Task" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [  weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}
#pragma mark - Text view
- (void)textViewDidBeginEditing:(UITextView *)textView{
    [self.tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:arrMain.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}
- (void)textViewDidEndEditing:(UITextView *)textView{
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark setupData
-(void)setUpData:(NSArray*)arr{
    [arrMain  removeAllObjects];

    for(int i=0 ;i<arr.count;i++){
        NSMutableDictionary *dict=[NSMutableDictionary new];
        [dict addEntriesFromDictionary:[arr objectAtIndex:i]];
        dict[@"arrValue"]=[NSArray new];
         dict[@"arrValueS"]=[NSMutableArray new];
        [arrMain addObject:dict];
    }
    // for user desgination
    NSMutableDictionary *dict=[NSMutableDictionary new];
    dict[@"arrValue"]=[NSArray new];
    dict[@"arrValueS"]=[NSMutableArray new];
    dict[@"to_level_id"]=@"9999";
    dict[@"title_hi"]=@"Assign to Designation";
    dict[@"multiple_status"]=@"1";
    dict[@"title"]=@"Assign to Designation";
     [arrMain addObject:dict];
    // for member
    dict=[NSMutableDictionary new];
    dict[@"arrValue"]=[NSArray new];
    dict[@"arrValueS"]=[NSMutableArray new];
    dict[@"to_level_id"]=@"99999";
    dict[@"title_hi"]=@"Assign to Member";
    dict[@"multiple_status"]=@"1";
    dict[@"title"]=@"Assign to Member";
      [arrMain addObject:dict];
    [self.tblView reloadData];
}
#pragma mark - uicollectionview View
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    UITableViewCell *myCell=(UITableViewCell *)[collectionView.superview.superview superview];
    NSMutableDictionary *dict=[arrMain objectAtIndex:myCell.tag];
    NSArray *Arr=dict[@"arrValueS"];
    return [Arr count];
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    UILabel *lblName=(UILabel *)[cell.contentView viewWithTag:101];
    //UIButton *btnD=(UIButton *)[cell.contentView viewWithTag:102];
    UITableViewCell *myCell=(UITableViewCell *)[collectionView.superview.superview superview];
    NSMutableDictionary *dict=[arrMain objectAtIndex:myCell.tag];
      if([dict[@"to_level_id"] isEqualToString:@"99999"]){
          NSArray *Arr=dict[@"arrValueS"];
          NSMutableDictionary *dictD=[Arr objectAtIndex:indexPath.row];
          lblName.text=[NSString stringWithFormat:@" %@ ",[dictD valueForKey:@"fullname"]];

      }
      else{
          NSArray *Arr=dict[@"arrValueS"];
          NSMutableDictionary *dictD=[Arr objectAtIndex:indexPath.row];
          lblName.text=[NSString stringWithFormat:@" %@ ",[AppHelper nullCheck:[dictD valueForKey:@"title"]]];

      }
       cell.tag=indexPath.row;
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *myCell=(UITableViewCell *)[collectionView.superview.superview superview];
    NSMutableDictionary *dict=[arrMain objectAtIndex:myCell.tag];
    NSString *string_size;
    if([dict[@"to_level_id"] isEqualToString:@"99999"]){
        NSArray *Arr=dict[@"arrValueS"];
        NSMutableDictionary *dictD=[Arr objectAtIndex:indexPath.row];
        
        string_size=[NSString stringWithFormat:@" %@ ",[dictD valueForKey:@"fullname"]];
    }
    else{
        NSArray *Arr=dict[@"arrValueS"];
        NSMutableDictionary *dictD=[Arr objectAtIndex:indexPath.row];
        
        string_size=[NSString stringWithFormat:@" %@ ",[AppHelper nullCheck:[dictD valueForKey:@"title"]]];
 
    }
        CGSize size = CGSizeMake([self getLabelSize:string_size].width+50, collectionView.frame.size.height);
    return size;
}
- (CGSize)getLabelSize:(NSString *)string {
    UIFont *myFont = [UIFont fontWithName:@"Roboto-Regular" size:15];
    
    CGSize maximumSize = [string sizeWithAttributes:@{NSFontAttributeName : myFont}];
    
    return maximumSize;
}

#pragma mark - Table View
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 68.0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
      self.viewForFoter.hidden=YES;
    if(arrMain.count){
          self.viewForFoter.hidden=NO;
    }
    return arrMain.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary*dict=arrMain[indexPath.row];
    UITableViewCell *myCell;
    //arrValueS
    if([[dict valueForKey:@"multiple_status"] intValue]==0){
        myCell=[tableView dequeueReusableCellWithIdentifier:@"MyTask" forIndexPath:indexPath];
        UILabel *lblV=(UILabel *)[myCell.contentView viewWithTag:101];
        UIButton *btnAdd=(UIButton *)[myCell.contentView viewWithTag:102];
        [btnAdd addTarget:self action:@selector(selectOptionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        NSArray *arr=dict[@"arrValueS"];
         lblV.text=nil;
        if(arr.count){
        lblV.text=[AppHelper nullCheck:[[arr firstObject] valueForKey:@"title"]];
        }


    }
    else{
      myCell=[tableView dequeueReusableCellWithIdentifier:@"MyTask1" forIndexPath:indexPath];
        UIButton *btnAdd=(UIButton *)[myCell.contentView viewWithTag:102];
        [btnAdd addTarget:self action:@selector(addOptionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        UICollectionView *collView=(UICollectionView *)[myCell.contentView viewWithTag:101];
        [collView setDelegate:self];
        [collView setDataSource:self];
        [collView reloadData];
    }
    UILabel *lblName=(UILabel *)[myCell.contentView viewWithTag:100];
    lblName.text=[AppHelper nullCheck:[dict valueForKey:@"title"]];
    myCell.tag=indexPath.row;
    return myCell;
}

#pragma mark button Action
-(void)openDatePicker:(UILabel*)lbl button:(UIButton*)btn{
    // NSLog(@"%d",btn.tag);
    [self.view endEditing:YES];
    
    DatePickerViewController *datePickerViewController = (DatePickerViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"DatePickerViewController"];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:datePickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:btn andPreferedSize:DropDownSize];
    NSDate *minDate=[NSDate date] ;
    NSDate *maxDate=nil ;
    NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];
    
    [datePickerViewController initDatePickerWithDatePickerMode:UIDatePickerModeDate minDate:minDate maxDate:maxDate andDefaultSelectedDate:minDate withReturnedDate:^(NSDate *selectedDate) {
        if (selectedDate) {
            NSString *strDatte= [formatter stringFromDate:selectedDate];
            lbl.text =  strDatte;
        }
    }];
    
}

- (IBAction)startButtonActon:(UIButton*)sender {
    [self openDatePicker:self.lblStartDate button:sender];
}
- (IBAction)endButtonAction:(UIButton*)sender {
    [self openDatePicker:self.lblEndDate button:sender];
}

- (void)addOptionButtonClick:(UIButton*)sender {
    UITableViewCell *myCell=(UITableViewCell *)[sender.superview.superview superview];
    //NSLog(@"%ld",(long)myCell.tag);
    // check index
    NSMutableDictionary *dict=[arrMain objectAtIndex:myCell.tag];
    
    if(myCell.tag==0){
        NSArray *Arr=dict[@"arrValue"];
        if(Arr.count){
            UILabel *lblV=(UILabel *)[myCell.contentView viewWithTag:101];
            
            [self initialiseTheListViewWithDataSource:dict[@"arrValue"] withField:lblV
                                         selectedDict:dict
                                      selectionChoice:NO
                                           senderRect:sender SelectionType:SEL_OPTLVL];
        }
        else{
            
            NSMutableDictionary *parameter=[NSMutableDictionary new];
            NSPredicate *prd=[NSPredicate predicateWithFormat:@"org_member_id == %@",[NSString stringWithFormat:@"%ld",(long)self.lblDegniation.tag]];
            
            NSArray *Arr=[self.arrDesination filteredArrayUsingPredicate:prd];
            if(Arr.count){
                parameter[@"frm_level_id"]=[[Arr firstObject ]valueForKey:@"frm_level_id"];
                parameter[@"to_location_id"]=[[Arr firstObject ]valueForKey:@"to_location_id"];
                parameter[@"to_level_id"]=dict[@"to_level_id"];
            }
            [self getOptionalMenuData:parameter add:myCell with:NO];
        }
    }
    else{
        NSMutableDictionary *dictPr=[arrMain objectAtIndex:myCell.tag-1];
        NSArray *ArrPr=dictPr[@"arrValueS"];
        
        if([ArrPr count]){
            NSArray *Arr=dict[@"arrValue"];
            
            if(Arr.count){
                UILabel *lblV=(UILabel *)[myCell.contentView viewWithTag:101];

                   if([dict[@"to_level_id"] isEqualToString:@"99999"]){
                    [self initialiseTheListViewWithDataSource:dict[@"arrValue"] withField:lblV
                                                 selectedDict:dict
                                              selectionChoice:NO
                                                   senderRect:sender SelectionType:SEL_MEMB];
                }
                else{
                    [self initialiseTheListViewWithDataSource:dict[@"arrValue"] withField:lblV
                                                 selectedDict:dict
                                              selectionChoice:NO
                                                   senderRect:sender SelectionType:SEL_OPTLVL];
                }
                
            
            }
            else{
                if([dict[@"to_level_id"] isEqualToString:@"9999"]){
                    NSMutableDictionary *parameter=[NSMutableDictionary new];
                    
                    parameter[@"frm_level_id"]=[[ArrPr firstObject ]valueForKey:@"frm_level_id"];
                    parameter[@"to_location_id"]=[[ArrPr valueForKeyPath:@"to_location_id"] componentsJoinedByString:@","];
                  //  parameter[@"to_level_id"]=dict[@"to_level_id"];
                    NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_ASSIGNDEST];

                    [self getOptionalMenuDataForMember:parameter add:myCell baseURL:baseURL SelectionType:SEL_OPTLVL] ;
                }
                else  if([dict[@"to_level_id"] isEqualToString:@"99999"]){
                    NSMutableDictionary *parameter=[NSMutableDictionary new];
                    
                    parameter[@"frm_level_id"]=[[ArrPr firstObject ]valueForKey:@"frm_level_id"];
                    parameter[@"to_location_id"]=[[ArrPr firstObject ]valueForKey:@"to_location_id"];
                    parameter[@"to_designation_id"]=[[ArrPr valueForKeyPath:@"designation_id"] componentsJoinedByString:@","];
                    
                    NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_ASSIGNMEMBER];
                    
                    [self getOptionalMenuDataForMember:parameter add:myCell baseURL:baseURL SelectionType:SEL_MEMB] ;
                }
                else{
                    NSMutableDictionary *parameter=[NSMutableDictionary new];
                    
                    parameter[@"frm_level_id"]=[[ArrPr firstObject ]valueForKey:@"frm_level_id"];
                    parameter[@"to_location_id"]=[[ArrPr firstObject ]valueForKey:@"to_location_id"];
                    parameter[@"to_level_id"]=dict[@"to_level_id"];
                    
                    [self getOptionalMenuData:parameter add:myCell with:NO] ;
                }
               
            }
        }
        else{
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please select Previous fields first." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        }
        
    }
    
}
- (void)selectOptionButtonClick:(UIButton*)sender {
    UITableViewCell *myCell=(UITableViewCell *)[sender.superview.superview superview];
    //NSLog(@"%ld",(long)myCell.tag);
    // check index
    NSMutableDictionary *dict=[arrMain objectAtIndex:myCell.tag];

    if(myCell.tag==0){
        NSArray *Arr=dict[@"arrValue"];
        if(Arr.count){
            UILabel *lblV=(UILabel *)[myCell.contentView viewWithTag:101];

            [self initialiseTheListViewWithDataSource:dict[@"arrValue"] withField:lblV
                                        selectedDict:dict
                                      selectionChoice:YES
                                           senderRect:sender SelectionType:SEL_OPTLVL];
        }
        else{
            NSMutableDictionary *parameter=[NSMutableDictionary new];
            NSPredicate *prd=[NSPredicate predicateWithFormat:@"org_member_id == %@",[NSString stringWithFormat:@"%ld",(long)self.lblDegniation.tag]];
            
            NSArray *Arr=[self.arrDesination filteredArrayUsingPredicate:prd];
            if(Arr.count){
                parameter[@"frm_level_id"]=[[Arr firstObject ]valueForKey:@"frm_level_id"];
                parameter[@"to_location_id"]=[[Arr firstObject ]valueForKey:@"to_location_id"];
                parameter[@"to_level_id"]=dict[@"to_level_id"];
            }
            [self getOptionalMenuData:parameter add:myCell with:YES];
        }
    }
    else{
        NSMutableDictionary *dictPr=[arrMain objectAtIndex:myCell.tag-1];
        NSArray *ArrPr=dictPr[@"arrValueS"];

        if([ArrPr count]){
            NSArray *Arr=dict[@"arrValue"];

            if(Arr.count){
                UILabel *lblV=(UILabel *)[myCell.contentView viewWithTag:101];
                
                [self initialiseTheListViewWithDataSource:dict[@"arrValue"] withField:lblV
                                             selectedDict:dict
                                          selectionChoice:YES
                                               senderRect:sender SelectionType:SEL_OPTLVL];
            }
            else{
                NSMutableDictionary *parameter=[NSMutableDictionary new];
                
                    parameter[@"frm_level_id"]=[[ArrPr firstObject ]valueForKey:@"frm_level_id"];
                    parameter[@"to_location_id"]=[[ArrPr firstObject ]valueForKey:@"to_location_id"];
                    parameter[@"to_level_id"]=dict[@"to_level_id"];
                
                [self getOptionalMenuData:parameter add:myCell with:YES];
            }
        }
        else{
             [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please select Previous fields first." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        }
        
    }
    
  }
- (IBAction)desginationButtonClick:(id)sender {
 
    [self initialiseTheListViewWithDataSource:self.arrDesination withField:self.lblDegniation
                                selectedDict:nil
                              selectionChoice:YES
                                   senderRect:sender SelectionType:SEL_DEST];
}
- (IBAction)areaButtonClick:(id)sender {
    if(self.lblDegniation.tag>0){
    if(self.arrArea.count){
    [self initialiseTheListViewWithDataSource:self.arrArea withField:self.lblArea
                                selectedDict:nil
                              selectionChoice:YES
                                   senderRect:sender SelectionType:SEL_LVL];
    }
    else{
        [self getAreaData:sender];
    }
    }
    else{
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please select designation first." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}
-(IBAction)imgButtonClick:(id)sender{
    if(self.imgView.image){
        ImageController *expandAndCollaps=(ImageController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ImageController"];
        expandAndCollaps.image = self.imgView.image;
        [self presentViewController:expandAndCollaps animated:YES completion:nil];
    }
}
- (IBAction)submitbuttonClick:(id)sender{
    NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];
    NSDate *endD=[formatter dateFromString:self.lblEndDate.text];
    NSDate *strtd=[formatter dateFromString:self.lblStartDate.text];
     if(self.txtField.text.length==0){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Please enter subject." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
   else if(arrMain.count==0){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Please select all data first." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else if([ self vailidateTheData]==NO){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Please select all the data." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else if([self.lblStartDate.text length]==0){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Please select start date." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else if([self.lblEndDate.text length]==0){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Please select end date." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else if([strtd timeIntervalSinceDate:endD]>0.0){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Start date should be less from end date." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else if(self.txtView.text.length==0){
        [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Please enter Details." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    else{
       [self createuAssignment];
    }
}
-(BOOL)vailidateTheData{
    BOOL isValid=YES;
    for(NSDictionary *dict in arrMain){
        if([dict[@"arrValueS"] count]==0){
            isValid=NO;
            break;
        }
    }
    return isValid;
}
- (IBAction)attachmentButtonClick:(id)sender{
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
            // There is not a camera on this device, so don't show the camera button.
            
        }
        else{
            [AppHelper showAlertViewWithTag:10 title:APP_NAME message:@"Camera not available." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        }
        // Camera button tapped.
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Library button tapped.
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        // [self launchSpecialController];
        
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}
- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    // imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}
#pragma mark UIImagePickerControllerDelegate ------------


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    self.imgView.image=image;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma  DROPDOWN
-(void)initialiseTheListViewWithDataSource:(NSArray *)dataSource withField:(UILabel*)txtField selectedDict:(NSMutableDictionary *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    [self.view endEditing:YES];
    // Initialise DataPickerViewController object with datasorce, selected datasource , isSigles selection criteria ,and selected array call back block -- >
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];

    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:[selectedRows valueForKey:@"arrValueS"] selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            if(type==SEL_DEST){
                 txtField.text=[NSString stringWithFormat:@"%@ - %@",[[selectedvalues firstObject] valueForKey:@"desingation"],[[selectedvalues firstObject] valueForKey:@"location"]];
                txtField.tag=[[[selectedvalues firstObject] valueForKey:@"org_member_id"] integerValue];
                   self.arrArea=nil;
                self.lblArea.tag=0;
                self.lblArea.text=nil;
                [arrMain removeAllObjects];
                [self.tblView reloadData];
            }
            else   if(type==SEL_LVL){
                txtField.text=[NSString stringWithFormat:@"%@",[[selectedvalues firstObject] valueForKey:@"title"]];
                txtField.tag=[[[selectedvalues firstObject] valueForKey:@"to_level_id"] integerValue];
                [self getTaskMenuData];
            }
            else if(type==SEL_OPTLVL){
                if(isSingleSelection){
                    txtField.text=[NSString stringWithFormat:@"%@",[AppHelper nullCheck:[[selectedvalues firstObject] valueForKey:@"title"]]];
                }
                
                selectedRows[@"arrValueS"]=[NSArray arrayWithArray:selectedvalues];

                NSUInteger iValiue=[arrMain indexOfObject:selectedRows];
                for(int i=(int)iValiue+1  ;i<arrMain.count;i++ ){
                    NSMutableDictionary *dictD=[arrMain objectAtIndex:i];
                    dictD[@"arrValue"]=[NSArray new];
                    dictD[@"arrValueS"]=[NSMutableArray new];
                
            }
                    [self.tblView reloadData ];
        }
        else if(type==SEL_MEMB){
                if(isSingleSelection){
                    txtField.text=[NSString stringWithFormat:@"%@",[[selectedvalues firstObject] valueForKey:@"fullname"]];
                }
                
                selectedRows[@"arrValueS"]=[NSArray arrayWithArray:selectedvalues];
        
                [self.tblView reloadData ];
            }
        }
    }];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];
    
}

#pragma  mark service
-(void)getOptionalMenuData:(NSMutableDictionary*)parameter add:(UITableViewCell*)myCell with:(BOOL)isSingle {
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_TASKREPORTLVL];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
             //   [self setUpData:[responseObject valueForKey:@"list"]];
                UILabel *lblV=(UILabel *)[myCell.contentView viewWithTag:101];
                UIButton *btnAdd=(UIButton *)[myCell.contentView viewWithTag:102];
                NSMutableDictionary *ArrDict=[arrMain objectAtIndex:myCell.tag];
                ArrDict[@"arrValue"]=[responseObject valueForKey:@"list"];
                [self initialiseTheListViewWithDataSource:ArrDict[@"arrValue"] withField:lblV
            selectedDict:ArrDict
            selectionChoice:isSingle
            senderRect:btnAdd SelectionType:SEL_OPTLVL];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}

-(void)getTaskMenuData{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        NSPredicate *prd=[NSPredicate predicateWithFormat:@"org_member_id == %@",[NSString stringWithFormat:@"%ld",(long)self.lblDegniation.tag]];
        
        NSArray *Arr=[self.arrDesination filteredArrayUsingPredicate:prd];
        if(Arr.count){
            parameter[@"frm_level_id"]=[[Arr firstObject ]valueForKey:@"frm_level_id"];
            parameter[@"to_location_id"]=[[Arr firstObject ]valueForKey:@"to_location_id"];
              parameter[@"to_level_id"]=[NSString stringWithFormat:@"%ld",(long)self.lblArea.tag];
        }

        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_TASKMENU];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                [self setUpData:[responseObject valueForKey:@"level_list"]];
             //   self.arrDesination=[responseObject valueForKey:@"level_list"];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
 
}
-(void)getDesination{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        //clientId
        parameter[@"clientId"]=@"1";
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_DESIGNATION];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                self.arrDesination=[responseObject valueForKey:@"designation_list"];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}
-(void)getAreaData:(UIButton*)sender{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
         NSMutableDictionary *parameter=[NSMutableDictionary new];
        NSPredicate *prd=[NSPredicate predicateWithFormat:@"org_member_id == %@",[NSString stringWithFormat:@"%ld",(long)self.lblDegniation.tag]];
        
        NSArray *Arr=[self.arrDesination filteredArrayUsingPredicate:prd];
        if(Arr.count){
            parameter[@"frm_level_id"]=[[Arr firstObject ]valueForKey:@"frm_level_id"];
             parameter[@"to_location_id"]=[[Arr firstObject ]valueForKey:@"to_location_id"];
        }

        [[AppHelper sharedInstance]showIndicator];
       
        //clientId
      //  parameter[@"clientId"]=@"1";
      
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_TASKLVL];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
      //  manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                self.arrArea=[responseObject valueForKey:@"level_list"];
                [self initialiseTheListViewWithDataSource:self.arrArea withField:self.lblArea
                                            selectedDict:nil
                                          selectionChoice:YES
                                               senderRect:sender SelectionType:SEL_LVL];

            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}
-(void)getOptionalMenuDataForMember:(NSMutableDictionary*)parameter add:(UITableViewCell*)myCell baseURL:(NSString*)baseURL SelectionType:(SelectionOptions)type {
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];

        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                //   [self setUpData:[responseObject valueForKey:@"list"]];
                UILabel *lblV=(UILabel *)[myCell.contentView viewWithTag:101];
                UIButton *btnAdd=(UIButton *)[myCell.contentView viewWithTag:102];
                NSMutableDictionary *ArrDict=[arrMain objectAtIndex:myCell.tag];
                ArrDict[@"arrValue"]=[responseObject valueForKey:@"list"];
                [self initialiseTheListViewWithDataSource:ArrDict[@"arrValue"] withField:lblV
                                             selectedDict:ArrDict
                                          selectionChoice:NO
                                               senderRect:btnAdd SelectionType:type];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}
-(void)createuAssignment{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        NSDateFormatter *formatter = [[DateFormatters sharedManager] formatterForString:@"dd-MM-yyyy"];
        NSMutableDictionary *dict=[arrMain lastObject];
        NSMutableDictionary *dictPr=[arrMain objectAtIndex:arrMain.count-2];
        NSArray *arrMemberS=dict[@"arrValueS"];
        NSArray *arrMember=dict[@"arrValue"];

         NSArray *arrDesignation=dictPr[@"arrValueS"];
        NSMutableDictionary *parameters=[NSMutableDictionary new];
        parameters[@"clientId"]=@"1";
        parameters[@"subject"]=self.txtField.text;
        parameters[@"message"]=self.txtView.text;
        parameters[@"org_member_id"]=[NSString stringWithFormat:@"%ld",(long)self.lblDegniation.tag];
        parameters[@"frm_level_id"]=[[arrMember firstObject]valueForKey:@"frm_level_id"];
        parameters[@"to_location_id"]=[[arrMember firstObject]valueForKey:@"to_location_id"];
         parameters[@"designation_id"]=[[arrDesignation valueForKeyPath:@"designation_id"] componentsJoinedByString:@","];
        parameters[@"end_date"]=[AppHelper unixDateString:[formatter dateFromString:self.lblEndDate.text]];
        parameters[@"start_date"]=[AppHelper unixDateString:[formatter dateFromString:self.lblStartDate.text]];
    //    parameters[@"from_memberId"]=[AppHelper appDelegate].cureentUser.accountId;//[AppHelper appDelegate].cureentUser.accountId;
        NSArray *ArrD=   [arrMemberS valueForKeyPath:@"fullname"];
        if([ArrD containsObject:@"All Members"]){
            parameters[@"to_org_id"]=@"0";
        }
        else{
            parameters[@"to_org_id"]=[[arrMember valueForKeyPath:@"to_org_id"] componentsJoinedByString:@","];

        }
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_NEWASSIGNMNT];
        [[AppHelper sharedInstance]showIndicator];
        
        
        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
        NSData *imageData = UIImageJPEGRepresentation(self.imgView.image, 0.5);
        
        AFHTTPRequestOperation *op = [manager POST:baseURL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            //do not put image inside parameters dictionary as I did, but append it!
            if(imageData)
                [formData appendPartWithFileData:imageData name:@"uploaded_file" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Submitted Successfully.." delegate:self cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          //  NSLog(@"Error: %@ ***** %@", operation.responseString, error);
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
        [op start];
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==100){
        [[AppHelper navigationController] popViewControllerAnimated:YES];
        
    }
}
@end
