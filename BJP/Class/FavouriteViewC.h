//
//  FavouriteViewC.h
//  BJP
//
//  Created by PDSingh on 9/8/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface FavouriteViewC : BaseViewController< UITableViewDataSource,UITableViewDelegate >
@end
