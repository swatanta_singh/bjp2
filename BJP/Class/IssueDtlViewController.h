//
//  IssueDtlViewController.h
//  BJP
//
//  Created by swatantra on 12/20/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "BaseViewController.h"

@interface IssueDtlViewController : BaseViewController
@property(nonatomic,strong)NSDictionary *dictIssues;

@end
