

#import "VerifyViewController.h"

#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "HomeeViewController.h"
@interface VerifyViewController ()
{
    NSString *noStr;
    NSString *userID;
}

@end

@implementation VerifyViewController

- (void)viewDidLoad {
   
    [super viewDidLoad];
   
   UIToolbar  *numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    numberToolbar.items = @[flexibleItem,[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor darkGrayColor];
    [numberToolbar setBackgroundColor:[UIColor lightGrayColor]];
    
    [numberToolbar sizeToFit];
    codeTextField.inputAccessoryView = numberToolbar;
    [codeTextField becomeFirstResponder];


    verifyButton.enabled=YES;
}

-(void)doneWithNumberPad
{
    [codeTextField resignFirstResponder];
    
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
//    parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
//    parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
    //[AppHelper userDefaultsForKey:USER_NAME];
   
   // NSString *countryCode ;
//    
//    if(!noStr)
//        noStr=@"99999999999";
    
//    if(countryCode)
//      sendLabel.text=[NSString stringWithFormat:@"that was sent to +%@ %@",countryCode,noStr];
//   else
    
    
    noStr=self.strPhn;
    userID = [AppHelper appDelegate].cureentUser.accountId;
    sendLabel.text=[NSString stringWithFormat:@"that was sent to %@",noStr];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getOTP
{
  
    
    NSMutableDictionary *parameter=[NSMutableDictionary new];
//    parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
//    parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
    parameter[@"mobile"]=self.strPhn;

    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kRegenerateOTP];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//        manager.requestSerializer = [AFJSONRequestSerializer serializer];
//        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            NSString *errorMsg=@"";
            if(responseObject==nil)
            {
                errorMsg=@"Server not responding";
                
            }
            else if ([[responseObject valueForKey:@"error_code"] integerValue]==200) // Handle the error.
            {
                errorMsg=@"Successfully send.";
            }
            else{
                errorMsg=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
            }
            if(errorMsg.length>3){
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
           
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:error.description delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    

     
    
}

-(BOOL)navigationShouldPopOnBackButton
{
    return NO;
}

#pragma mark ---------------------
#pragma mark IBActions -----------

-(IBAction)verifyAction:(id)sender
{

    if(codeTextField.text.length==0){
        [AppHelper showAlertViewWithTag:113 title:APP_NAME message:@"Please enter the code." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        return;
    }
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        [[AppHelper sharedInstance]showIndicator];
        
            [[AppHelper sharedInstance]showIndicator];
            NSMutableDictionary *parameters =[NSMutableDictionary new];
            parameters[@"otppin"]=[AppHelper nullCheck:codeTextField.text];
            parameters[@"mobile"]=self.strPhn;
           // parameters[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
            NSString *baseURL = [NSString stringWithFormat:@"%@%@",BaseUrl,kSendOTP];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            NSString *errorMsg=@"";
            if(responseObject==nil)
            {
                errorMsg=@"Server not responding";
                
            }
            else if ([[responseObject valueForKey:@"error_code"] integerValue]==200) // Handle the error.
            {
                [AppHelper saveToUserDefaults:self.strPhn withKey:USER_NAME];
                [AppHelper saveToUserDefaults:[responseObject objectForKey:@"accessToken"] withKey:ACCESS_TOKEN];
                NSDictionary *resultDict = [[responseObject objectForKey:@"account"]lastObject];
                 [AppHelper saveToUserDefaults:[AppHelper nullCheck:[responseObject objectForKey:@"accountId"]] withKey:USER_ID];
                [AppHelper saveToUserDefaults:parameters[@"mobile"]withKey:USER_NAME];
                User *cuser=[[Service sharedEventController] parseUserData:[responseObject objectForKey:@"accountId"] withData:resultDict];
                [AppHelper appDelegate].cureentUser=cuser;
                [[AppHelper appDelegate] saveContext];
                [self dismissViewControllerAnimated:YES completion:^{
                    NSArray *allViewControllers = [[AppHelper navigationController] viewControllers];
                    for (UIViewController *aViewController in allViewControllers) {
                        if ([aViewController isKindOfClass:[HomeeViewController class]]) {
                            [[AppHelper navigationController] popToViewController:aViewController  animated:YES];
                        }
                    }
                }];
             
                }
                 else{
                     errorMsg=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
                 }
                if(errorMsg.length>3){
                    [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorMsg delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [[AppHelper sharedInstance]hideIndicator];
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"User verification failed!" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                
            }];
            
        }
    
    else{
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }


      
}

-(IBAction)regenrateCodeAction:(id)sender
{
    [codeTextField resignFirstResponder];
    
    codeTextField.text=@"";
    [self getOTP];
}

#pragma mark ---------------------
#pragma mark UITextFieldDelegate --------

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSRange spaceRange = [string rangeOfString:@" "];
    if (spaceRange.location != NSNotFound)
    {
        return NO;
    }
    if([textField.text length]==1 && range.length == 1)
    {
        verifyButton.enabled=NO;
    }
    else
    {
        verifyButton.enabled=YES;
    }
    if (textField.text.length >= 6 && range.length == 0)
    {
        return NO; // return NO to not change text
    }

    return YES;
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
   
    [codeTextField resignFirstResponder];
}


//-(void)httpRequestChangeNumber
//{
//    
//    NSMutableDictionary *parameter=[NSMutableDictionary new];
//    
//    parameter[@"UserRegdID"]=[AppHelper appDelegate].cureentUser.userRegdID;
//    parameter[@"UserPhoneNumber"]= [AppHelper appDelegate].cureentUser.userPhone;
//    parameter[@"UserRegdOTP"]= codeTextField.text;
//    parameter[@"CountryCode"] = [AppHelper appDelegate ].cureentUser.walletID;
//
//    if([AppHelper appDelegate].checkNetworkReachability)
//    {
//        [[AppHelper sharedInstance]showIndicator];
//        NSString *baseURL = [NSString stringWithFormat:@"%@%@%@", BaseUrl,kAuthPort,kChangePhoneNumber];
//        
//        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//        manager.requestSerializer = [AFJSONRequestSerializer serializer];
//        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
//        
//        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            
//            [[AppHelper sharedInstance]hideIndicator];
//
//            NSDictionary *resultDict=[responseObject objectForKey:@"Data"];
//            
//            if ([[resultDict objectForKey:@"StatusCode"] integerValue]==400)
//            {
//                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please enter correct OTP" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
//            }
//           else if ([[resultDict objectForKey:@"StatusCode"] integerValue]==404)
//            {
//            }
//           else if ([[resultDict objectForKey:@"StatusCode"] integerValue]==302)
//           {
//                 [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Mobile number already exits" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
//           }
//            else if ([[resultDict objectForKey:@"StatusCode"] intValue]== 200)
//            {
//                //
////                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"You have sucessfully change your contact number." preferredStyle:UIAlertControllerStyleAlert];
////                
////                [self presentViewController:alertController animated:YES completion:nil];
//                
//                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"You have sucessfully change your contact number." preferredStyle:UIAlertControllerStyleAlert];
//                [self presentViewController:alertController animated:YES completion:nil];
//                
//                
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [alertController dismissViewControllerAnimated:YES completion:^{
//                        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
//                        [loginManager logOut];
//                        [[AppHelper sharedInstance]removeMenuViewController];
//                        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//                        [defaults setObject:nil forKey:@"userID"];
//                        [AppHelper appDelegate].cureentUser=nil;
//                        [AppHelper saveToUserDefaults:nil withKey:USER_ID];
//                         [self dismissViewControllerAnimated:YES completion:nil];
//                        [[AppHelper navigationController] popToRootViewControllerAnimated:YES];
//                    }];
//                   
//                });
//
//
//                
//               
//            };
//            
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            
//            [[AppHelper sharedInstance]hideIndicator];
//            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
//        }];
//        
//    }
//    
//    else{
//        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
//    }
//    
//    
//}



@end
