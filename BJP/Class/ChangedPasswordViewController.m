
//
//  ChangedPasswordViewController.m
//  BJP
//
//  Created by toyaj on 8/31/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "ChangedPasswordViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"

@interface ChangedPasswordViewController ()
@property (weak, nonatomic) IBOutlet UIButton *chnagePwdButton;

@end

@implementation ChangedPasswordViewController

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    
    [self.chnagePwdButton setTitle:NSLocalizedString(@"Submit", nil) forState:UIControlStateNormal];


    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    __weak ChangedPasswordViewController *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"Password", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
        else{
            [weekSelf.view endEditing:YES];
            
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submit:(id)sender {
    
    BOOL isExist= [self validValues];
    if (!isExist)
        return;
    
    [self httpRequestChangePassword];
    
 
}

-(void)httpRequestChangePassword
{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"currentPassword"]=_old_password.text;
        parameter[@"newPassword"]=_reset_password.text;
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kChange_Password];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
            [AppHelper showAlertViewWithTag:100 title:APP_NAME message:@"Password changed successfully" delegate:self cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                
            }
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
                
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==100){
        
        [AppHelper appDelegate].cureentUser=nil;
        [AppHelper saveToUserDefaults:nil withKey:USER_ID];
        //[AppHelper saveToUserDefaults:nil withKey:USER_NAME];
        [AppHelper saveToUserDefaults:nil withKey:ACCESS_TOKEN];
        [[AppHelper navigationController] popToRootViewControllerAnimated:YES];
        
    }
}
-(BOOL)validValues{
    
    NSString *oldPassword=[self.old_password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *newPassword=[self.reset_password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
  
    
    if ([oldPassword length]==0) {
        
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please enter old Password" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        return NO;
    }
    else if ([newPassword length]==0) {
        
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Please enter new Password" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        return NO;
    }
   
    
    return YES;
}


- (CGRect) rightViewRectForBounds:(CGRect)bounds {
    
    CGRect textRect = [self rightViewRectForBounds:bounds];
    textRect.origin.x -= 10;
    return textRect;
}
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 6, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}
@end
