//
//  DetailsNewViewController.m
//  BJP
//
//  Created by toyaj on 8/29/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "DetailsNewViewController.h"
#import "AppHelper.h"
#define ZOOMPOINT -60
@interface DetailsNewViewController ()
@property(weak,nonatomic) IBOutlet UIScrollView *tableView;
@property (weak, nonatomic) IBOutlet UIView *tableHeader;

@property (strong,nonatomic) IBOutlet UIImageView *iv;
@property (strong,nonatomic) IBOutlet UILabel *newsDate;
@property (strong,nonatomic) NSLayoutConstraint *topCon;

@property (weak, nonatomic) IBOutlet UIImageView *shadowImage;


@end

@implementation DetailsNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  //  NSLog(@"detail data %@",self.servicDict);
    /*self.iv.contentMode = UIViewContentModeScaleToFill; //UIViewContentModeScaleAspectFill;
    self.iv.translatesAutoresizingMaskIntoConstraints = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view.
    self.topCon = [NSLayoutConstraint constraintWithItem:self.iv attribute:NSLayoutAttributeTop relatedBy:0 toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:ZOOMPOINT/2.0];
    [self.iv addConstraint:[NSLayoutConstraint constraintWithItem:self.iv attribute:NSLayoutAttributeHeight relatedBy:0 toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:self.tableHeader.frame.size.height - ZOOMPOINT*1.5]];
    [self.view addConstraint:self.topCon];
    [self.view layoutIfNeeded];
    */
    NSString *strImg=[AppHelper nullCheck:_servicDict[@"imgUrl"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    
    
 //  [_iv setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user"]];
    
   _iv.image =[UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    
    NSString * htmlString = [self.servicDict objectForKey:@"itemContent"];
    NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
     [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [attrStr length])];
     [attrStr addAttribute:NSFontAttributeName value:self.newsDate.font range:NSMakeRange(0, [attrStr length])];
    //add alignment
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    [attrStr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attrStr.length)];
    self.newsDate.attributedText = attrStr;
    //view_Image.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img_FULL]]];
}


-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
   
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    __weak DetailsNewViewController *weekSelf=self;
    [self.headerView.headerImageView setHidden:YES];
    [self setUpHeaderWithTitle:@"News Details" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
   /* if (scrollView.contentOffset.y < 0 && scrollView.contentOffset.y > ZOOMPOINT) {
        self.topCon.constant = ZOOMPOINT/2.0 - scrollView.contentOffset.y/2.0;
    }else if (scrollView.contentOffset.y <= ZOOMPOINT) {
        self.iv.transform = CGAffineTransformMakeScale(1 - (scrollView.contentOffset.y - ZOOMPOINT)/200, 1 - (scrollView.contentOffset.y - ZOOMPOINT)/200);
    }
    */
}



@end

