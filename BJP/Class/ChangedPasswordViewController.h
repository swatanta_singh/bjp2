//
//  ChangedPasswordViewController.h
//  BJP
//
//  Created by toyaj on 8/31/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ChangedPasswordViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *old_password;
@property (weak, nonatomic) IBOutlet UITextField *reset_password;

- (IBAction)submit:(id)sender;

@end
