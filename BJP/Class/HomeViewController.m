//
//  HomeViewController.m
//  BJP
//
//  Created by swatantra on 8/19/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "HomeViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "ContentViewController.h"
#import "UIImageView+AFNetworking.h"
#import "Service.h"
@interface HomeViewController ()
@property (nonatomic,retain) NSArray *menuTabArray;
@property(nonatomic,strong)NSArray *categoryArray;

-(void)httpRequestCompletionHandler:(void (^)(id result))completionHandler;

@end

@implementation HomeViewController

- (void)viewDidLoad {

     [super viewDidLoad];
    if([AppHelper  userDefaultsForKey:USER_ID]){
              [AppHelper appDelegate].cureentUser=[[Service sharedEventController] getLoginDetailsWith:[AppHelper userDefaultsForKey:USER_ID]];
    }
   }

-(void)addLeftView{
    [[[AppHelper sharedInstance]menuViewController] setUpMove];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    __weak HomeViewController *weekSelf=self;
    [self setUpHeaderWithTitle:@"News" withLeftbtn:@"menu" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [weekSelf addLeftView];
        }
        else{
            [weekSelf.view endEditing:YES];
            
        }
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark -
-(void)httpRequestCompletionHandler:(void (^)(id result))completionHandler
{
    
    NSMutableDictionary *parameter=[NSMutableDictionary new];
    parameter[@"categoryId"]=@"";
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        
        [[AppHelper sharedInstance]showIndicator];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGet_Categroy];
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:nil];
        NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSDictionary *param = @{@"body":json};
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        [manager POST:baseURL parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([responseObject objectForKey:@"items"] && [[responseObject objectForKey:@"items"] isKindOfClass:[NSArray class]]) {
                    self.categoryArray = [responseObject objectForKey:@"items"];
                    completionHandler( self.categoryArray);
                }
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
}


@end
