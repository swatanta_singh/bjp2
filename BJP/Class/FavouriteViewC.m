//
//  FavouriteViewC.m
//  BJP
//
//  Created by PDSingh on 9/8/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "FavouriteViewC.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "ContentViewController.h"
#import "UIImageView+AFNetworking.h"
#import "ContactMenuCollectionViewCell.h"
#import "ContactContainCollectionViewCell.h"
#import "DetlsViewController.h"
#import "Service.h"
#import "LanguageChangeViewController.h"
#import "PresentionHandler.h"
#import <MediaPlayer/MediaPlayer.h>


@interface FavouriteViewC ()
{
    NSUInteger selectIndex;
}
@property (strong, nonatomic) PresentionHandler *presentHandler;
@property(nonatomic,strong)NSArray *newsArray;

@property (weak, nonatomic) IBOutlet UITableView *likeTableNews;

@end

@implementation FavouriteViewC
-(void)finalMethoForLanguge{
    
    
    if([AppHelper nullCheck:  [AppHelper userDefaultsForKey:APP_LANGAUGE]].length==0){
      //  NSLog(@"ddddddd");
        [AppHelper saveToUserDefaults:@"en" withKey:APP_LANGAUGE];
        LanguageChangeViewController* objLanguageView= (LanguageChangeViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LanguageChangeViewController"];
        
        self.presentHandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objLanguageView fromPresentingViewController:self withStyle:UIModalPresentationCustom fromView:nil andPreferedSize:CGSizeMake(SCREEN_WIDTH-50, 280)];
    }
}
- (void)viewDidLoad {
    
    
    
    [super viewDidLoad];
    [self performSelector:@selector(finalMethoForLanguge) withObject:nil afterDelay:5];
    if([AppHelper  userDefaultsForKey:USER_ID]){
        [AppHelper appDelegate].cureentUser=[[Service sharedEventController] getLoginDetailsWith:[AppHelper userDefaultsForKey:USER_ID]];
    }
}

//-(void)refreshTable {
//    [self menuModel];
//    //[self getFeedsAgainstTopicIdWithText:[MCLocalization stringForKey:@"ML_CHT_LBL_Wait"]];
//    //    dispatch_after(2, dispatch_get_main_queue(), ^{
//    //        [refreshControl endRefreshing];
//    //    });
//}

-(void)addLeftView{
    [[[AppHelper sharedInstance]menuViewController] setUpMove];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self httpNewsRequest];
    [super viewWillAppear:animated];
    __weak FavouriteViewC *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"Favourite", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([self.newsArray count]>0 )
    {
        numOfSections  = 1;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"No update";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.newsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"NewsCell" forIndexPath:indexPath];
    NSDictionary *newsDictionary=[self.newsArray objectAtIndex:indexPath.row];
    UIImageView *imageV=[cellYouAvailable.contentView viewWithTag:1001];
    UILabel *newsType=[cellYouAvailable.contentView viewWithTag:1002];
    UILabel *newsDate=[cellYouAvailable.contentView viewWithTag:1003];
    UILabel *newsTitle=[cellYouAvailable.contentView viewWithTag:1004];
    UILabel *newsLike=[cellYouAvailable.contentView viewWithTag:1005];
    UIWebView *webView=[cellYouAvailable.contentView viewWithTag:111];
    
    imageV.hidden=NO;
    webView.hidden=YES;
    webView.opaque=NO;
    
    if ([newsDictionary[@"videoUrl"]length]>10)
    {
        imageV.hidden=YES;
        webView.hidden=NO;
        NSString *embedHTML =[NSString stringWithFormat:@"<iframe width=\"%f\" height=\"%f\" src=\"https://www.youtube.com/embed/K3cBfXF8724\" autoplay=1 frameborder=\"0\" allowfullscreen></iframe>",imageV.frame.size.width,imageV.frame.size.height];
        [webView loadHTMLString:embedHTML baseURL:nil];
    }
    else
    {
//        NSString *strImg2=[AppHelper nullCheck:newsDictionary[@"previewImgUrl"]];
//        NSURL *url2 = [NSURL URLWithString:[self normalizePath:strImg2]];
//        [imageV setImageWithURL:url2 placeholderImage:[UIImage imageNamed:@"Rectangle"]];
        
        NSString *strImg=[AppHelper nullCheck:newsDictionary[@"imgUrl"]];
        NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
        [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    }
    
    if ([newsDictionary[@"likesCount"]intValue]==0)
        newsLike.text=@"";
    else
        newsLike.text=[NSString stringWithFormat:@"👍 Likes %@",newsDictionary[@"likesCount"]];
    
    
    
    newsType.text=[AppHelper nullCheck:newsDictionary[@"categoryTitle"]];
    newsDate.text=[AppHelper nullCheck:newsDictionary[@"date"]];
    newsTitle.text=[[AppHelper nullCheck:newsDictionary[@"itemTitle"]]capitalizedString];
    
    newsType.text=[[AppHelper nullCheck:newsDictionary[@"categoryTitle"]]capitalizedString];
  //  NSLog(@"%@",[[AppHelper nullCheck:newsDictionary[@"categoryTitle"]]capitalizedString]);
  //  NSLog(@"%@",[[AppHelper nullCheck:newsDictionary[@"itemTitle"]]capitalizedString]);
    return cellYouAvailable;
}


-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[ self playBtnPressed:@"https://www.youtube.com/watch?v=wtQrl35l26Y"];
    DetlsViewController *expandAndCollaps=(DetlsViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"DetlsViewController"];
    expandAndCollaps.servicDict = [[self.newsArray objectAtIndex:indexPath.row]mutableCopy];
    [self.navigationController pushViewController:expandAndCollaps animated:YES];
    
}
#pragma mark video play

-(void)playBtnPressed:(UIButton *)sender{
    
    UITableViewCell *cell = (UITableViewCell *)sender.superview;
    NSIndexPath *indexPath = [self.likeTableNews indexPathForCell:cell];
    NSDictionary *newsDictionary =[self.newsArray objectAtIndex:indexPath.row];
    if(newsDictionary[@"videoUrl"]){
        NSURL *url=[[NSURL alloc] initWithString:newsDictionary[@"videoUrl"]];
        MPMoviePlayerViewController* moviePlayer=[[MPMoviePlayerViewController alloc] initWithContentURL:url];
        [moviePlayer.moviePlayer play];
        [self presentViewController:moviePlayer animated:YES completion:nil];
    }
    else {
        //here call the YouTube Video Player Code.
        }
}

#pragma mark - `


-(void)httpNewsRequest
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=@"1";
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        parameter[@"itemId"]=@"";
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kNewFavorites];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            self.newsArray=nil;
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([responseObject objectForKey:@"items"] && [[responseObject objectForKey:@"items"] isKindOfClass:[NSArray class]]) {
                    self.newsArray = [responseObject objectForKey:@"items"];
                }
                else
                     [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Record not found" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            else
            {
                NSString *messge=[AppHelper nullCheck:[responseObject valueForKey:@"error_description"]];
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:messge delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
            [self.likeTableNews reloadData];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          
            self.newsArray=nil;
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            [self.likeTableNews reloadData];
        }];
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}

@end
