//
//  SupportViewViewController.m
//  BJP
//
//  Created by toyaj on 12/14/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "SupportViewViewController.h"
#import "AppHelper.h"
#import "NameAddressVoterViewController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "DataPickerViewController.h"
#import "PresentionHandler.h"

@interface SupportViewViewController ()
{

    NSMutableArray *arrPreferrences;
    NSMutableArray *arrissue;
    
    NSMutableArray *arrdataValue;
    NSMutableDictionary *nameAddress;
}
@property(nonatomic,strong)PresentionHandler *presnthandler;
@property(nonatomic,strong)NSMutableSet *selectedCatgorySet;

@end

@implementation SupportViewViewController
@synthesize arrData=arrData;
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.selectedCatgorySet=[NSMutableSet new];

    arrData=[NSMutableArray new];
    arrPreferrences=[NSMutableArray new];
    arrissue=[NSMutableArray new];
    self.dictData=[NSMutableDictionary new];
    
}
#pragma mark - view will load
-(void)viewWillAppear:(BOOL)animated
{
    [self.selectedCatgorySet removeAllObjects];
}

#pragma mark - set up METHOd
-(void)setUpData{

    for (int i=0; i<5; i++) {
        NSMutableDictionary *dict=[NSMutableDictionary new];
        switch (i) {
            case 0:{
                dict[@"value"]=@"";
                dict[@"place"]=@"First Preferences ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"1";
                dict[@"id"]=@"";
            }
                break;
            case 1:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Second Preferences ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"1";
                dict[@"id"]=@"";
            }
                break;
            case 2:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Third Preferences ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"1";
                 dict[@"id"]=@"";
            }
                break;
            case 3:{
                
                dict[@"value"]=@"";
                dict[@"place"]=@"Four Preferences ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"1";
                dict[@"id"]=@"";
            }
                break;
            case 4:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Will Vote For BJP";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                dict[@"id"]=@"";
            }
                break;
            default:
                break;
        }
        
        
        [arrPreferrences addObject:dict];
        
    }
    for (int i=0; i<5; i++) {
        NSMutableDictionary *dict=[NSMutableDictionary new];
        switch (i) {
            case 0:{
                dict[@"value"]=@"";
                dict[@"place"]=@"First issue ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                dict[@"id"]=@"";
            }
                break;
            case 1:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Second issue ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                dict[@"id"]=@"";
            }
                break;
            case 2:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Third issue ";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                dict[@"id"]=@"";
            }
                break;
            case 3:{
                
                dict[@"value"]=@"";
                dict[@"place"]=@"Four issue";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                dict[@"id"]=@"";
            }
                break;
            case 4:{
                dict[@"value"]=@"";
                dict[@"place"]=@"Fifth issue";
                dict[@"type"]=@"1";
                dict[@"part"]=@"0";
                dict[@"id"]=@"";
            }
                break;
            default:
                break;
        }
        
        
        [arrissue addObject:dict];
        
    }
    
    [arrData addObject:arrPreferrences];
    [arrData addObject:arrissue];
    
}


#pragma mark - uicollectionview View


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    //UICollectionReusableView *reusableview = nil;
    UICollectionReusableView *collection=nil;
    if (kind == UICollectionElementKindSectionHeader) {
        collection = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        UILabel *lblname = (UILabel*)[collection viewWithTag:1000];
        if (indexPath.section==0)
            lblname.text=@"Party Preferences";
        else
            lblname.text=@"Issues";
        
        
        
        
    }
    
    return collection;
}

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [arrData count];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    //    return [[arrData objectAtIndex:section] count];;
    return [[arrData objectAtIndex:section] count];
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dict =[arrData[indexPath.section] objectAtIndex:indexPath.row] ;
    UILabel *lblName=(UILabel *)[cell.contentView viewWithTag:1001];
    UITextField *txtFild=(UITextField *)[cell.contentView viewWithTag:1002];
    UIButton *btnAction =(UIButton*)[cell.contentView viewWithTag:1003];
    btnAction.hidden=YES;
   
    
    if ([dict[@"type"] isEqualToString:@"1"]) {
        btnAction.hidden=NO;
    }
    txtFild.text=[NSString stringWithFormat:@"%@",dict[@"value"]];
    [btnAction addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    txtFild.enabled=NO;
    lblName.text=dict[@"place"];
    cell.tag=indexPath.row;
    cell.contentView.tag=indexPath.section;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict =[arrData[indexPath.section] objectAtIndex:indexPath.row];
    
    if([dict[@"part"] isEqualToString:@"1"]){
        return CGSizeMake(SCREEN_WIDTH/2-10, 70);
    }
    else{
        CGSize size = CGSizeMake(SCREEN_WIDTH-5,70);
        return size;
    }
}



-(NSMutableDictionary*)supportData{
    //    self.dictData[@"year"]=self.textField_year.text;
    //    self.dictData[@"month"]=self.textField_Month.text;
    //    self.dictData[@"rate"]=self.textField_hourly_Rate.text;
    
    return self.dictData;
}

#pragma mark textfieald
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    // if (textField.tag!=102) {
    [AppHelper setupViewAtUp:0 toview:self.view];
    UICollectionViewCell *cell=(UICollectionViewCell*)[textField.superview superview];
    NSMutableDictionary *dict=[arrData objectAtIndex:cell.tag];
    dict[@"value"]=textField.text;
    // }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    UICollectionViewCell *cell=(UICollectionViewCell*)[textField.superview superview];
    NSIndexPath *path=[self.collectionview indexPathForCell:cell];
    
    UICollectionViewCell *nextCell=[self.collectionview cellForItemAtIndexPath:[NSIndexPath indexPathForRow:path.row+1 inSection:path.section]];
    
    if(nextCell){
        
        UITextField *txt=(UITextField*)[nextCell.contentView viewWithTag:101];
        [txt becomeFirstResponder];
        
    }
    else{
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    UITableViewCell *cell=(UITableViewCell*)[textField.superview superview];
    
    NSIndexPath *path=[self.collectionview indexPathForCell:cell];
    //    if(path.section==0){
    //
    //
    //        if (path.row==1) {
    //            if (textField.text.length >= 14 && range.length == 0)
    //            {
    //                return NO; // return NO to not change text
    //            }
    //        }
    //        //  else if (path.row==1)
    //        //        {
    //        //            if (textField.text.length >= 25 && range.length == 0)
    //        //            {
    //        //                return NO; // return NO to not change text
    //        //            }
    //        //        }
    //    }
    //    if (path.row == 1 ) {
    //        textField.text = [numberformatter stringForObjectValue:textField.text];
    //    }
    
    return YES;
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if(IS_IPHONE_4_OR_LESS || IS_IPHONE_5)
    {
        UITableViewCell *cell=(UITableViewCell*)[textField.superview superview];
        if(cell.tag==3 ||cell.tag==4||cell.tag==5)
            [AppHelper setupViewAtUp:100 toview:self.view];
    }
    
    //  [self cancelPikcerViewAction:nil];
    return YES;
    
}

-(void)dimissKeyborad
{
    [self.view endEditing:YES];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if(IS_IPHONE_4_OR_LESS || IS_IPHONE_5)
    {
        UITableViewCell *cell=(UITableViewCell*)[textField.superview superview];
        if(cell.tag==3 ||cell.tag==4||cell.tag==5)
            [AppHelper setupViewAtUp:120 toview:self.view];
    }
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark-

-(void)setUpdataForStringtype:(NSString *)typeString keytype:(NSString*)keytype withField:(NSInteger)myCell withFieldSection:(NSInteger)withFieldSection{
    
    NSMutableDictionary *dir=[arrData[myCell] objectAtIndex:withFieldSection];
    
     if ([[nameAddress valueForKey:typeString] count]>0) {
         
         NSArray *arrId = [nameAddress valueForKey:typeString];
         
             if (arrId.count>0) {
                 NSPredicate *prd=[NSPredicate predicateWithFormat:@"selected == %d",1];
                 NSArray *Arr=[arrId filteredArrayUsingPredicate:prd];
                 if (Arr.count>0) {
                     NSString *value=[[Arr valueForKey:keytype] lastObject];
                     dir[@"value"] = value;
                      dir[@"id"]=[[Arr valueForKey:@"id"] lastObject];
                     [self.selectedCatgorySet addObject:value];

                 }
                 else
                     dir[@"value"] = @"Select";
             }
             else
                 dir[@"value"] = @"NA";
     }
     else
         dir[@"value"] = @"NA";

}

-(void)setUpdataForStringtypeValue:(NSString *)typeString keytype:(NSString*)keytype withField:(NSInteger)myCell withFieldSection:(NSInteger)withFieldSection withFieldrowCell:(NSInteger)withFieldrowCell{
    
    NSMutableDictionary *dir=[arrData[withFieldSection] objectAtIndex:withFieldrowCell];
    
    if ([[nameAddress valueForKey:typeString] count]>0) {
         NSArray *arrId = [[nameAddress valueForKey:typeString]objectAtIndex:withFieldrowCell];
            if (arrId.count>0) {
                NSPredicate *prd=[NSPredicate predicateWithFormat:@"selected == %d",1];
                NSArray *Arr=[arrId filteredArrayUsingPredicate:prd];
                if (Arr.count>0) {
                    NSString *value=[[Arr valueForKey:keytype] lastObject];
                    dir[@"value"] = value;
                    dir[@"id"]=[[Arr valueForKey:@"id"] lastObject];
                    [self.selectedCatgorySet addObject:value];
                }
                else
                    dir[@"value"] = @"Select";
            }
            else
                dir[@"value"] = @"NA";
        }
    else
        dir[@"value"] = @"NA";
   
   
}


-(void)btnAction:(UIButton*)btn{
    UICollectionViewCell *cel=(UICollectionViewCell*)[btn.superview.superview superview];
    
    NSInteger section =cel.contentView.tag;
    if (section==0) {
        switch (cel.tag) {
            case 0:
                [self politicalView:cel withsender:btn];
                break;
            case 1:
                [self politicalView:cel withsender:btn];
                
                break;
            case 2:
                [self politicalView:cel withsender:btn];
                
                
                break;
            case 3:
                [self politicalView:cel withsender:btn];
                
                break;
                
            case 4:
                [self pickerForwillVoteForBJP:cel withsender:btn];
                
                break;
            
            default:
                break;
        }
        
    }
    else
    {
    switch (cel.tag) {
        case 0:
            [self pickerForLocalIsseue:cel withsender:btn];
            break;
        case 1:
            [self pickerForLocalIsseue:cel withsender:btn];
            break;
        case 2:
            [self pickerForLocalIsseue:cel withsender:btn];
            break;
        case 3:
            [self pickerForLocalIsseue:cel withsender:btn];
            break;
            
        case 4:
            [self pickerForLocalIsseue:cel withsender:btn];
            break;
               default:
            break;
    }
    }
    
}


-(void)politicalView:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    if([[nameAddress valueForKey:@"party_preference"] count]>0)
    {
        NSMutableDictionary *preferenceArr = [[nameAddress valueForKey:@"party_preference"] objectAtIndex:mycell.tag];
//        NSArray *selected = [preferenceArr valueForKey:@"partyPrefrence"];
        [self pickerDataSource:preferenceArr withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_PREFERENCES];
    }
   
}

-(BOOL)checkIsAlreadySelect:(NSString *)selectTitle
{
    BOOL containsString2 = [self.selectedCatgorySet containsObject:selectTitle];
    if (containsString2) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Select other option" delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
        [alertView show];
        [self performSelector:@selector(dismissAlert:) withObject:alertView afterDelay:2.0f];
        return true;
    }
    else
    {
        return false;
    }
}
-(void)dismissAlert:(UIAlertView *)alertView
{
    [alertView dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)pickerForLocalIsseue:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    if([[nameAddress valueForKey:@"local_issues"] count]>0)
    {
        NSMutableDictionary *preferenceArr = [[nameAddress valueForKey:@"local_issues"] objectAtIndex:mycell.tag];
      
        [self pickerDataSource:preferenceArr withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_ISSUE];

    }
 }
-(void)pickerForwillVoteForBJP:(UICollectionViewCell*)mycell withsender:(UIButton*)sender{
    
    
    if([[nameAddress valueForKey:@"vote_bjp"] count]>0)
    {
//        NSArray *selected = [preferenceArr valueForKey:@"votepercentage"];
        [self pickerDataSource:nameAddress[@"vote_bjp"] withField:mycell selectedArray:nil selectionChoice:YES senderRect:sender SelectionType:SEL_WILL_VOTE];

        
    }

  }
-(void)pickerDataSource:(NSArray *)dataSource withField:(UICollectionViewCell*)myCell selectedArray:(NSArray *)selectedRows selectionChoice:(BOOL)isSingleSelection senderRect:sender SelectionType:(SelectionOptions)type {
    
    [self.view endEditing:YES];
    __weak SupportViewViewController *weekSelf=self;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DataPickerViewController *dataPickerViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataPickerViewController"];
    [dataPickerViewController initWitSingleSelectionCriteriaType:isSingleSelection dataSource:dataSource withSelectedDataSourceRows:selectedRows selectionType:type andSelectedValuesCallBack:^(NSArray *selectedvalues) {
        if(selectedvalues.count){
            if(type==SEL_PREFERENCES){
                
                BOOL isTaken=[self checkIsAlreadySelect:[[selectedvalues firstObject]valueForKey:@"partyPrefrence"]];
                if (!isTaken) {
                    [self.selectedCatgorySet addObject:[[selectedvalues firstObject]valueForKey:@"partyPrefrence"]];
                    NSMutableDictionary *dir=[arrData[myCell.contentView.tag]objectAtIndex:myCell.tag ];
                    dir[@"value"] = [AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"partyPrefrence"]];
                    dir[@"id"]=[AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"id"]];
                    [weekSelf.collectionview reloadData];
                }
                
            }
            else  if(type==SEL_DESIGNATION){
                
                BOOL isTaken=[self checkIsAlreadySelect:[[selectedvalues firstObject]valueForKey:@"partyIssue"]];
                if (!isTaken) {
                    
                     [self.selectedCatgorySet addObject:[[selectedvalues firstObject]valueForKey:@"partyIssue"]];
                    NSMutableDictionary *dir=[arrData[myCell.contentView.tag]objectAtIndex:myCell.tag ];
                    dir[@"value"] = [AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"partyIssue"]];
                    dir[@"id"]=[AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"id"]];
                    [weekSelf.collectionview reloadData];

                }
                
            }
            else  if(type==SEL_WILL_VOTE){
                
                BOOL isTaken=[self checkIsAlreadySelect:[[selectedvalues firstObject]valueForKey:@"votepercentage"]];
                if (!isTaken) {
                      [self.selectedCatgorySet addObject:[[selectedvalues firstObject]valueForKey:@"votepercentage"]];
                    NSMutableDictionary *dir=[arrData[myCell.contentView.tag]objectAtIndex:myCell.tag ];
                    dir[@"value"] = [AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"votepercentage"]];
                    dir[@"id"]=[AppHelper nullCheck:[[selectedvalues firstObject]valueForKey:@"id"]];
                    [weekSelf.collectionview reloadData];
                }
               
            }
            else
            {
//                BOOL isTaken= [selectedvalues firstObject];
//                if (!isTaken) {
//                    [self.selectedCatgorySet addObject:[selectedvalues firstObject]];;
//                    NSMutableDictionary *dir=[arrData[myCell.contentView.tag]objectAtIndex:myCell.tag ];
//                    NSString *value=[selectedvalues firstObject];
//                    dir[@"value"] = value;
//                    [weekSelf.collectionview reloadData];
//                }
            }
        }
    }];
    
    self.presnthandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:dataPickerViewController fromPresentingViewController:self withStyle:UIModalPresentationPopover fromView:sender andPreferedSize:DropDownSize];
}

-(void)setViewFrameData
{
    nameAddress=self.dicdataRecived;
    [self setUpData];
    [self setUpdataForStringtype:@"vote_bjp" keytype:@"votepercentage" withField:0 withFieldSection:4];
    [self setUpdataForStringtypeValue:@"party_preference" keytype:@"partyPrefrence" withField:0 withFieldSection:0 withFieldrowCell:0];
     [self setUpdataForStringtypeValue:@"party_preference" keytype:@"partyPrefrence" withField:1 withFieldSection:0 withFieldrowCell:1];
    [self setUpdataForStringtypeValue:@"party_preference" keytype:@"partyPrefrence" withField:2 withFieldSection:0 withFieldrowCell:2];
    [self setUpdataForStringtypeValue:@"party_preference" keytype:@"partyPrefrence" withField:3 withFieldSection:0 withFieldrowCell:3];
    
    [self setUpdataForStringtypeValue:@"local_issues" keytype:@"partyIssue" withField:0 withFieldSection:1 withFieldrowCell:0];
     [self setUpdataForStringtypeValue:@"local_issues" keytype:@"partyIssue" withField:1 withFieldSection:1 withFieldrowCell:1];
    [self setUpdataForStringtypeValue:@"local_issues" keytype:@"partyIssue" withField:2 withFieldSection:1 withFieldrowCell:2];
    [self setUpdataForStringtypeValue:@"local_issues" keytype:@"partyIssue" withField:3 withFieldSection:1 withFieldrowCell:3];
    [self setUpdataForStringtypeValue:@"local_issues" keytype:@"partyIssue" withField:4 withFieldSection:1 withFieldrowCell:4];
    
    [self.collectionview reloadData];
}


@end
