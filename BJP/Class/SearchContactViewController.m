//
//  SearchContactViewController.m
//  BJP
//
//  Created by swatantra on 11/12/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "SearchContactViewController.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"
#import "AppHelper.h"
#import "UIImageView+AFNetworking.h"

@interface SearchContactViewController (){
}
@property(nonatomic,strong)NSArray *arrData;
@property (weak, nonatomic) IBOutlet UITableView *tbleView;
@property (weak, nonatomic) IBOutlet UISearchBar *serchBar;
@property (strong, nonatomic)  NSMutableArray *arrSelected;

@end

@implementation SearchContactViewController
- (void)igetSelectedContact:(DidSelectIContact)selectedItemsCallBack{
    self.selectedContact=selectedItemsCallBack;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrSelected=[NSMutableArray new];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    __weak SearchContactViewController *weekSelf=self;
    [self setUpHeaderWithTitle:@"Search Contact" withLeftbtn:@"back" withRigthbtn:@"Favorite" WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [ weekSelf.navigationController popViewControllerAnimated:YES];
        }
          else if(navigateValue==2){
              if(weekSelf.arrSelected.count){
                  weekSelf.selectedContact(weekSelf.arrSelected);
                    [ weekSelf.navigationController popViewControllerAnimated:YES];
              }
              else{
                  [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please select atleast one contact." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
              }
          }
    }];
}
#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrData.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCell" forIndexPath:indexPath];
      cell.accessoryType=UITableViewCellAccessoryNone;
    NSDictionary *groupList=[self.arrData objectAtIndex:indexPath.row];
    UILabel *lblname = (UILabel*)[cell.contentView viewWithTag:1001];
    lblname.text=[groupList[@"name"]capitalizedString];
    UILabel *lblSublable = (UILabel*)[cell.contentView viewWithTag:1002];
    lblSublable.text=[groupList[@"mandal"]capitalizedString];
    
    UILabel *lblDestinaton = (UILabel*)[cell.contentView viewWithTag:1005];
    lblDestinaton.text=[groupList[@"designation"]capitalizedString];
    
    UILabel *lblDistrict = (UILabel*)[cell.contentView viewWithTag:1004];
    lblDistrict.text=[groupList[@"state"]capitalizedString];
    UIImageView *imageV = (UIImageView*)[cell.contentView viewWithTag:1003];
    NSString *strImg=[AppHelper nullCheck:groupList[@"member_img"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    // chk
    UIImageView *imageVC = (UIImageView*)[cell.contentView viewWithTag:1006];
     imageVC.hidden=YES;
    if([self.arrSelected containsObject:groupList]){
        imageVC.hidden=NO;
    }
    return cell;
}
-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict=self.arrData[indexPath.row];
    if([self.arrSelected containsObject:dict]){
        [self.arrSelected removeObject:dict];
    }
    else{
        [self.arrSelected addObject:dict];
    }
    [self.tbleView reloadData];
//            UIViewController *expandAndCollaps=[AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"ShowTaskViewController"];
//            [self.navigationController pushViewController:expandAndCollaps animated:YES];
    }
#pragma mark serch bar
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    if(searchBar.text.length>2){
        [self getSearchContct];
        [self.view endEditing:YES];
    }
    else{
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please enter minimum three charcter." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}
#pragma  mark service
-(void)getSearchContct{
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        //clientId
        parameter[@"clientId"]=@"1";
        parameter[@"from_memberId"]=[AppHelper appDelegate].cureentUser.accountId;
//        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"member_name"]=self.serchBar.text;
            NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kGET_CONTCT];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                self.arrData=[responseObject valueForKey:@"list"];
                [self.tbleView reloadData];
            }
            
            else{
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[responseObject valueForKey:@"error_description"] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
    
}
@end
