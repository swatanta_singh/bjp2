//
//  ReplyDetailViewController.m
//  BJP
//
//  Created by toyaj on 10/21/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "ReplyDetailViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"

@interface ReplyDetailViewController ()

@end

@implementation ReplyDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"get value previous %@",_categoryArrayList);
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    
    __weak ReplyDetailViewController *weekSelf=self;
    
    NSString *titleText = NSLocalizedString(@"Replies", nil);
    [self setUpHeaderWithTitle:titleText withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [weekSelf.navigationController popViewControllerAnimated:YES];
            
        }
        else{
            [weekSelf.view endEditing:YES];
            
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
      return 2;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ReplyListsubject" forIndexPath:indexPath];
    
    
    
    UILabel *lbldescription = (UILabel*)[cell.contentView viewWithTag:1001];

    
    UILabel *lblDate = (UILabel*)[cell.contentView viewWithTag:1002];
    if (indexPath.row==0) {
        lbldescription.text =[NSString stringWithFormat:@"%@",[AppHelper nullCheck:[self.categoryArrayList  valueForKey:@"subject"]]];
        
        double stimeStamp=[[self.categoryArrayList  valueForKey:@"replyAt"] doubleValue];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd MMM, yyyy HH:mm";
        NSDate *sdate = [NSDate dateWithTimeIntervalSince1970:stimeStamp];
        lblDate.text=[dateFormatter stringFromDate:sdate];
    }
    else
    {
        lbldescription.text =[NSString stringWithFormat:@"%@",[AppHelper nullCheck:[self.categoryArrayList  valueForKey:@"reply"]]];
        lbldescription.textColor=[UIColor blackColor];
        
     
        lblDate.hidden=@"";
    }
    
    
   
    
 
    return cell;
}





/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

/*
-(void)httpCategoryRequest
{
    
    NSMutableDictionary *parameter=[NSMutableDictionary new];
    parameter[@"clientId"]=@"1";
    parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
    parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSString *baseURL = [NSString stringWithFormat:@"%@%@",BaseUrl,kGet_Reply];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                //                if ( [[responseObject objectForKey:@"items"] isKindOfClass:[NSArray class]]) {
                self.categoryArray = [responseObject objectForKey:@"Data"];
                //}
                [_tblView reloadData];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}

*/
#pragma mark table life cycle
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /* Return an estimated height or calculate
     * estimated height dynamically on information
     * that makes sense in your case.
     */
    return 1300.0f;
}


@end
