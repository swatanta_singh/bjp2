//
//  HeaderViewController.h
//  KINCT
//
//  Created by Ashish on 02/03/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnRight;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;

@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;


@end
