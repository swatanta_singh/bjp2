//
//  EventsViewController.m
//  BJP
//
//  Created by PDSingh on 8/29/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "EventsViewController.h"
#import "DetlsViewController.h"
#import "EventsDetailsVC.h"

@interface EventsViewController ()
@property(nonatomic,strong)NSArray *eventsArray;
@property (weak, nonatomic) IBOutlet UITableView *tableViewNews;
@end

@implementation EventsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    __weak EventsViewController *weekSelf=self;
    
    [self httpEventsRequest];

    
    NSString *titleText = NSLocalizedString(@"Events", nil);

    [self setUpHeaderWithTitle:titleText withLeftbtn:@"menu" withRigthbtn:nil WithComilation:^(int navigateValue) {
        [weekSelf.view endEditing:YES];
        if(navigateValue==1){
            [[[AppHelper sharedInstance]menuViewController] setUpMove];
        }
        else{
            [weekSelf.view endEditing:YES];
            
        }
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    NSInteger numOfSections = 0;
    if ([self.eventsArray count]>0 )
    {
        numOfSections  = 1;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"No update events";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.eventsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"NewsCell" forIndexPath:indexPath];
    NSDictionary *newsDictionary=[self.eventsArray objectAtIndex:indexPath.row];
    UIImageView *imageV=[cellYouAvailable.contentView viewWithTag:1001];
    UILabel *evntType=[cellYouAvailable.contentView viewWithTag:1003];
    UILabel *eventsTittle=[cellYouAvailable.contentView viewWithTag:1004];
  
    
//    NSString *strImg2=[AppHelper nullCheck:newsDictionary[@"previewImgUrl"]];
//    NSURL *url2 = [NSURL URLWithString:[self normalizePath:strImg2]];
//    [imageV setImageWithURL:url2 placeholderImage:[UIImage imageNamed:@"Rectangle"]];
    
    NSString *strImg=[AppHelper nullCheck:newsDictionary[@"imgUrl"]];
    NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
    [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];

    
   NSString *state= [AppHelper nullCheck:newsDictionary[@"eventState"]];
    if ([state isEqualToString:@"Past"])
        evntType.text=@"Expired";
    
    else  if ([state isEqualToString:@"Present"])
        evntType.text=@"Ongoing";
    else
        evntType.text=@"Upcoming";
    
    eventsTittle.text=[AppHelper nullCheck:newsDictionary[@"eventTitle"]];
    
    
    return cellYouAvailable;
}

-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventsDetailsVC *expandAndCollaps=(EventsDetailsVC*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"EventsDetailsVC"];
    expandAndCollaps.servicDict = [self.eventsArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:expandAndCollaps animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
     return 253.0f;;
}



-(void)httpEventsRequest
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=[NSNumber numberWithInt:1];
        parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
        parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];

       NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kRecent_events];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                if ([responseObject objectForKey:@"events"] && [[responseObject objectForKey:@"events"] isKindOfClass:[NSArray class]]) {
                    self.eventsArray = [responseObject objectForKey:@"events"];
                }
                
                [self.tableViewNews reloadData];
            };
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}


@end
