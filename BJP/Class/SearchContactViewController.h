//
//  SearchContactViewController.h
//  BJP
//
//  Created by swatantra on 11/12/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "BaseViewController.h"
typedef void(^DidSelectIContact)(NSArray *selectedContact);

@interface SearchContactViewController : BaseViewController
@property (nonatomic,copy)DidSelectIContact selectedContact;
- (void)igetSelectedContact:(DidSelectIContact)selectedItemsCallBack;
@end
