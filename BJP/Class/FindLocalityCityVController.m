//
//  FindLocalityCityVController.m
//  KINCT
//
//  Created by PDSingh on 5/5/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import "Defines.h"
#import "FindLocalityCityVController.h"

#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"


@interface FindLocalityCityVController ()
{
    NSMutableArray *searchLocationData;
    NSMutableArray *googlePlaceArrayLocation;

    SPGooglePlacesAutocompleteQuery *query;

}

@property (weak, nonatomic) IBOutlet UITableView *searchPlaceTable;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation FindLocalityCityVController

- (void)viewDidLoad {
    
    [self initilaizeGoolePlaceApi  ];
    [self.searchBar becomeFirstResponder];
    [super viewDidLoad];
    
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    numberToolbar.items = @[flexibleItem,[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    numberToolbar.tintColor=[UIColor darkGrayColor];
    [numberToolbar setBackgroundColor:[UIColor lightGrayColor]];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)doneWithNumberPad
{
    [self.view endEditing:YES];
    
}

//-(void)viewWillAppear:(BOOL)animated {
//    
//    [super viewWillAppear:YES];
//  
//     self.automaticallyAdjustsScrollViewInsets = false;
//    __weak FindLocalityCityVController *weekSelf=self;
//    
//    [self setUpHeaderWithTitle:@"Find Locality/City" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
//        [weekSelf.navigationController popViewControllerAnimated:YES];
//    }];
//    
//}

-(void)initilaizeGoolePlaceApi
{
    query = [[SPGooglePlacesAutocompleteQuery alloc] initWithApiKey:KGOOGLE_PLACE_API_KEY];
    query.radius = 100.0;   // search addresses close to user
    query.types = SPPlaceTypeGeocode;
}


- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([searchLocationData count]) {
        return [searchLocationData objectAtIndex:indexPath.row];
    }
    else
        return  nil;
    
}


#pragma mark- UITable View Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([searchLocationData count]>0) {
        return [searchLocationData count];
    }
    else
        return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell.textLabel setFont:[UIFont fontWithName:@"Proxima Nova" size:18]];
    cell.textLabel.text = [self placeAtIndexPath:indexPath].name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *address = [self placeAtIndexPath:indexPath].name;
    self.address(address);
    [self.searchBar resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - Search Bar Delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar
{
    
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)theSearchBar
{
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    if ([theSearchBar text]>0)
    {
        //[self performSelectorOnMainThread:@selector(getAddress:) withObject:theSearchBar.text waitUntilDone:YES];
    }
}


//NEW - to handle filtering
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    query.input=searchBar.text;
    [query fetchPlaces:^(NSArray *places, NSError *error)
     {
         searchLocationData=[[NSMutableArray alloc]initWithArray:places];
         [self.searchPlaceTable reloadData];
     }];
}


#pragma mark - Block
-(void)getAddressBlock:(DidSelectLocationblock)address
{
    self.address=address;
}
@end
