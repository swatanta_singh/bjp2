//
//  BaseViewController.h
//  KINCT
//
//  Created by Swatantra on 02/03/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderViewController.h"
typedef void(^CompilationHeader)(int navigateValue);

@interface BaseViewController : UIViewController
@property (nonatomic,copy)CompilationHeader navigatevalue;

@property(nonatomic,strong)HeaderViewController *headerView;
-(void)setUpHeaderWithTitle:(NSString*)title withLeftbtn:(NSString*)leftimage withRigthbtn:(NSString*)Rightimage WithComilation:(CompilationHeader)navigatevalue;

@end
