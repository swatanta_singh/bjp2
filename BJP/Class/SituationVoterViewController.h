//
//  SituationVoterViewController.h
//  BJP
//
//  Created by toyaj on 12/14/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SituationVoterViewController : UIViewController
-(NSMutableDictionary*)situationData;

@property (strong, nonatomic)  NSMutableDictionary *dictData;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(strong,nonatomic) NSMutableDictionary *dicdataRecived;
@property(strong,nonatomic) NSMutableArray *arrData;
@property (strong, nonatomic) NSString  *serveyId;

-(void)displayViewSetMEthod;

@end
