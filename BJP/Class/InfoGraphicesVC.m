//
//  FavouriteViewC.m
//  BJP
//
//  Created by PDSingh on 9/8/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "InfoGraphicesVC.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "ContentViewController.h"
#import "UIImageView+AFNetworking.h"
#import "ContactMenuCollectionViewCell.h"
#import "ContactContainCollectionViewCell.h"
#import "DetlsViewController.h"
#import "Service.h"
#import "LanguageChangeViewController.h"
#import "PresentionHandler.h"
#import <MediaPlayer/MediaPlayer.h>
#import "DateFormatters.h"
#import "ImageController.h"
@interface InfoGraphicesVC ()
{
    NSUInteger selectIndex;
    NSUInteger maxCount;
    NSString *lastId;
    BOOL isLoadmore;
    
    UILabel *noDataLabel;

}
@property (strong, nonatomic) PresentionHandler *presentHandler;
@property(nonatomic,strong)NSArray *newsArray;

@property (weak, nonatomic) IBOutlet UITableView *likeTableNews;

@end

@implementation InfoGraphicesVC
-(void)finalMethoForLanguge{
    
    
    if([AppHelper nullCheck:  [AppHelper userDefaultsForKey:APP_LANGAUGE]].length==0){
        [AppHelper saveToUserDefaults:@"en" withKey:APP_LANGAUGE];
        LanguageChangeViewController* objLanguageView= (LanguageChangeViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"LanguageChangeViewController"];
        
        self.presentHandler = [[PresentionHandler alloc] initWithViewControllerToBePresented:objLanguageView fromPresentingViewController:self withStyle:UIModalPresentationCustom fromView:nil andPreferedSize:CGSizeMake(SCREEN_WIDTH-50, 280)];
    }
}
- (void)viewDidLoad {
    
    
    
    [super viewDidLoad];
    
    [self performSelector:@selector(finalMethoForLanguge) withObject:nil afterDelay:5];
    if([AppHelper  userDefaultsForKey:USER_ID]){
        [AppHelper appDelegate].cureentUser=[[Service sharedEventController] getLoginDetailsWith:[AppHelper userDefaultsForKey:USER_ID]];
    }
}

//-(void)refreshTable {
//    [self menuModel];
//    //[self getFeedsAgainstTopicIdWithText:[MCLocalization stringForKey:@"ML_CHT_LBL_Wait"]];
//    //    dispatch_after(2, dispatch_get_main_queue(), ^{
//    //        [refreshControl endRefreshing];
//    //    });
//}

-(void)addLeftView{
    [[[AppHelper sharedInstance]menuViewController] setUpMove];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];

    
    maxCount=1;
    lastId=@"0";
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        [self httpNewsRequest];
    }
    __weak InfoGraphicesVC *weekSelf=self;
    NSString *titleText = NSLocalizedString(@"Infographics", nil);
    [self setUpHeaderWithTitle:titleText withLeftbtn:@"menu" withRigthbtn:nil WithComilation:^(int navigateValue) {
        if(navigateValue==1){
            [weekSelf addLeftView];
        }

     }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - Table View

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row>=self.newsArray.count){
        return 40.0;
    }
    else{
        return 350.0;
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([self.newsArray count]>0 )
    {
        numOfSections  = 1;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView = nil;
    }
    else
    {
        
        if (!noDataLabel) {
            noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
            noDataLabel.text             =  NSLocalizedString(@"NOUpdate", nil);
            noDataLabel.textColor        = [UIColor blackColor];
            noDataLabel.textAlignment    = NSTextAlignmentCenter;
        }
    }
    return numOfSections;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.newsArray.count == maxCount)
        return self.newsArray.count;
    else
        return self.newsArray.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if((indexPath.row > self.newsArray.count-1)){
        
        UITableViewCell *myCell=[tableView dequeueReusableCellWithIdentifier:@"ProviderDetail" forIndexPath:indexPath];
        return myCell;
    }
    else
    {
        UITableViewCell *cellYouAvailable =[tableView dequeueReusableCellWithIdentifier:@"NewsCell" forIndexPath:indexPath];
        NSDictionary *newsDictionary=[self.newsArray objectAtIndex:indexPath.row];
        UIImageView *imageV=[cellYouAvailable.contentView viewWithTag:1001];
        UILabel *newsDate=[cellYouAvailable.contentView viewWithTag:1002];
        UIButton *btnD=[cellYouAvailable.contentView viewWithTag:1003];
        UILabel *newsTitle=[cellYouAvailable.contentView viewWithTag:1004];
        UILabel *newsLike=[cellYouAvailable.contentView viewWithTag:1005];
        
        [btnD addTarget:self action:@selector(shareAction:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *strImg=[AppHelper nullCheck:newsDictionary[@"originPhotoUrl"]];
        NSURL *url = [NSURL URLWithString:[self normalizePath:strImg]];
        [imageV setImageWithURL:url placeholderImage:[UIImage imageNamed:@"Rectangle"]];
        
        if ([newsDictionary[@"share_count"]intValue]==0)
            newsLike.text=@"";
        else
            newsLike.text=[NSString stringWithFormat:@"Share: %@",newsDictionary[@"share_count"]];
        
        double stimeStamp=[newsDictionary[@"createAt"] doubleValue];
        NSDateFormatter  *dateFormatter  = [[DateFormatters sharedManager] formatterForString:@"dd MMM, yyyy HH:mm"];
        
        NSDate *sdate = [NSDate dateWithTimeIntervalSince1970:stimeStamp];
        dateFormatter.dateFormat = @"dd MMM, yyyy";
        newsDate.text=[dateFormatter stringFromDate:sdate];
        newsTitle.text=[[AppHelper nullCheck:newsDictionary[@"title"]]capitalizedString];
        cellYouAvailable.tag=indexPath.row;
        return cellYouAvailable;
    }
    
}


-(NSString *)normalizePath:(NSString *)path {
    NSArray *pathComponents = [path componentsSeparatedByString:@"/../"];
    return [pathComponents componentsJoinedByString:@"/"];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *mycell=(UITableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    UIImageView *imageV=[mycell.contentView viewWithTag:1001];
    UIImage *shareImage = imageV.image;
    NSDictionary *dict=self.newsArray[indexPath.row];

    //[ self playBtnPressed:@"https://www.youtube.com/watch?v=wtQrl35l26Y"];
 ImageController *expandAndCollaps=(ImageController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"Main" WithName:@"ImageController"];
    expandAndCollaps.image = shareImage;
     expandAndCollaps.dictImage = dict;
   [self presentViewController:expandAndCollaps animated:YES completion:nil];
    
}


-(void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    float reload_distance = 10;
    
    if(y > h + reload_distance) {
        
        if (isLoadmore)
        {
            if(self.newsArray.count < maxCount)
            {
                isLoadmore = NO;
                [self httpNewsRequest];
            }
        }
    }
}

#pragma button action
-(void)shareAction:(UIButton*)btn {
    
    UITableViewCell *mycell=(UITableViewCell*)[btn.superview.superview superview];
       NSDictionary *dict=self.newsArray[mycell.tag];
      NSString * message = [[AppHelper nullCheck:dict[@"title"]]capitalizedString];;
     UIImageView *imageV=[mycell.contentView viewWithTag:1001];
     UIImage *shareImage = imageV.image;
    
    NSArray *excludeActivities = @[
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    //   activityVC.excludedActivityTypes = excludeActivities;
   // NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/app/up-bjp/id1148583016?ls=1&mt=8"];
    NSArray * shareItems = @[message, shareImage];
    UIActivityViewController * activityViewController = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    activityViewController.excludedActivityTypes = excludeActivities;
    __weak __typeof(self)weakSelf = self;
    
    [self presentViewController:activityViewController animated:YES completion:nil];
    [activityViewController setCompletionWithItemsHandler:
     ^(NSString *activityType, BOOL completed, NSArray* returnedItems, NSError *activityError) {
         if(completed){
             [weakSelf updateShareCount:dict[@"id"] with:activityType];
         }
         
     }];
    
}
#pragma mark - Reload News

-(void)updateShareCount:(NSString*)strId with:(NSString*)strType{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=@"1";
             parameter[@"id"]=strId;
        // parameter[@"itemId"]=@"";
        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kINFOGRAFICS_SHARE_COUNT];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                NSString *errorstr=@"Share Successfully";
                if([strType isEqualToString:@"com.apple.UIKit.activity.CopyToPasteboard"]){
                    errorstr=@"Image copy successfully.";
                }
                else if([strType isEqualToString:@"com.apple.UIKit.activity.SaveToCameraRoll"]){
                        errorstr=@"Image save successfully.";
                }
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:errorstr delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                  [self httpNewsRequest];
            }
            else{
                  [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Some error found." delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            }
            [self.likeTableNews reloadData];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            [self.likeTableNews reloadData];
        }];
        
    }
    
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }
    
}
-(void)httpNewsRequest
{
    
        NSMutableDictionary *parameter=[NSMutableDictionary new];
        parameter[@"clientId"]=@"1";
       // parameter[@"accountId"]=[AppHelper appDelegate].cureentUser.accountId;
       // parameter[@"accessToken"]=[AppHelper userDefaultsForKey:ACCESS_TOKEN];
        parameter[@"language"]=[AppHelper userDefaultsForKey:APP_LANGAUGE];
        parameter[@"lastId"]=lastId;

        NSString *baseURL = [NSString stringWithFormat:@"%@%@", BaseUrl,kINFOGRAFICS];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager POST:baseURL parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
           
            isLoadmore = YES;
            
            [[AppHelper sharedInstance]hideIndicator];
            
            if (!responseObject)
            {
                [AppHelper showAlertViewWithTag:11 title:APP_NAME message:@"Server not responding" delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
                return ;
            }
            else if ([[responseObject objectForKey:@"error_code"] intValue]== 200)
            {
                
                maxCount=[[responseObject objectForKey:@"MaxCount"]intValue];
                if ([lastId intValue]>0)
                    self.newsArray =[self.newsArray arrayByAddingObjectsFromArray:[responseObject objectForKey:@"list"]];
                else
                    self.newsArray = [responseObject objectForKey:@"list"];
//                if ([responseObject objectForKey:@"list"] && [[responseObject objectForKey:@"list"] isKindOfClass:[NSArray class]]) {
//                    self.newsArray = [responseObject objectForKey:@"list"];
//                }
                if ([self.newsArray count]>0) {
                    lastId=[[self.newsArray lastObject] objectForKey:@"id"];
                }

            }
            [self.likeTableNews reloadData];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            self.newsArray=nil;
            [[AppHelper sharedInstance]hideIndicator];
            [AppHelper showAlertViewWithTag:11 title:APP_NAME message:[error localizedDescription] delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
            [self.likeTableNews reloadData];
        }];
        
    
}

@end
