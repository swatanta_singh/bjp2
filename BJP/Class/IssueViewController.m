//
//  IssueViewController.m
//  BJP
//
//  Created by swatantra on 12/16/16.
//  Copyright © 2016 swatantra. All rights reserved.
//

#import "IssueViewController.h"
#import "ViewLocalIssueViewController.h"
#import "LocalIssueViewController.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"

@interface IssueViewController ()

- (IBAction)segmentButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property(nonatomic,strong)LocalIssueViewController *viewCreateIssue ;
@property(nonatomic,strong)ViewLocalIssueViewController *viewIssue ;
@end

@implementation IssueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.viewCreateIssue=(LocalIssueViewController*) [AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"LocalIssueViewController"];
    self.viewIssue = (ViewLocalIssueViewController*)[AppHelper intialiseViewControllerFromMainStoryboard:@"MyTask" WithName:@"ViewLocalIssueViewController"];
   // self.viewCreateIssue.view.frame=CGRectMake(0, 0, SCREEN_WIDTH, 500);
    //self.viewIssue.view.frame=CGRectMake(0, 0, SCREEN_WIDTH, 500);
    [self.viewContainer addSubview:self.viewCreateIssue.view];
    self.viewCreateIssue.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.viewIssue.view.translatesAutoresizingMaskIntoConstraints = NO;

    [self setUpFrame:self.viewContainer with:self.viewCreateIssue.view];
    [self addChildViewController:self.viewCreateIssue];
    [self.viewCreateIssue didMoveToParentViewController:self];
}
- (void)setUpFrame:(UIView*)containerView with:(UIView*)newSubview
{
      [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeTop
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeTop
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeLeading
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeLeading
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    [containerView addConstraint:[NSLayoutConstraint constraintWithItem:newSubview
                                                              attribute:NSLayoutAttributeTrailing
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:containerView
                                                              attribute:NSLayoutAttributeTrailing
                                                             multiplier:1.0
                                                               constant:0.0]];
    
    // the frame is still `CGRectZero` at this point
    
    
    // if not doing this in `viewDidLoad` (e.g. you're doing this in
    // `viewDidAppear` or later), you can force `layoutIfNeeded` if you want
    // to look at `frame` values. Generally you don't need to do this unless
    // manually inspecting `frame` values or when changing constraints in a
    // `animations` block of `animateWithDuration`.
    
    [containerView layoutIfNeeded];
    
    // everything is ok here
    
  }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL) shouldAutomaticallyForwardAppearanceMethods {
    return NO;
}
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
        __weak IssueViewController *weekSelf=self;
        [self setUpHeaderWithTitle:@"Local Issues" withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
            if(navigateValue==1){
                [  weekSelf.navigationController popViewControllerAnimated:YES];
            }
        }];

}
- (IBAction)segmentButtonAction:(UISegmentedControl*)sender {
    [self.view endEditing:YES];
    if([sender selectedSegmentIndex]==0){
        [self.viewIssue willMoveToParentViewController:nil];
        [self.viewIssue removeFromParentViewController];
        [self.viewIssue.view  removeFromSuperview];
        
        
        [self.viewContainer addSubview:self.viewCreateIssue.view];
        [self setUpFrame:self.viewContainer with:self.viewCreateIssue.view];

        [self addChildViewController:self.viewCreateIssue];
        [self.viewCreateIssue didMoveToParentViewController:self];
    }
    else{
        [self.viewCreateIssue willMoveToParentViewController:nil];
        [self.viewCreateIssue removeFromParentViewController];
          [self.viewCreateIssue.view  removeFromSuperview];
        [self.viewContainer addSubview:self.viewIssue.view];
        [self setUpFrame:self.viewContainer with:self.viewIssue.view];

        [self addChildViewController:self.viewIssue];
        [self.viewIssue didMoveToParentViewController:self];
    }
}
@end
