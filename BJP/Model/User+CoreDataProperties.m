//
//  User+CoreDataProperties.m
//  BJP
//
//  Created by swatantra on 8/31/16.
//  Copyright © 2016 swatantra. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

@dynamic accountId;
@dynamic bigPhotoUrl;
@dynamic blockId;
@dynamic commentsCount;
@dynamic constituencyId;
@dynamic countryId;
@dynamic coverUrl;
@dynamic districtId;
@dynamic email;
@dynamic fb_id;
@dynamic fullname;
@dynamic language;
@dynamic mobile;
@dynamic my_page;
@dynamic normalCoverUrl;
@dynamic normalPhotoUrl;
@dynamic notificationsCount;
@dynamic assemblyId;
@dynamic itemsCount;
@dynamic lat;
@dynamic lng;
@dynamic likesCount;
@dynamic sex;
@dynamic stateId;
@dynamic country_name;
@dynamic district_name;
@dynamic state_name;
@dynamic constituency_name;
@dynamic block_name;
@dynamic dateOfBirth;
@dynamic about;

@end
