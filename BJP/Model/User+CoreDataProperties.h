//
//  User+CoreDataProperties.h
//  BJP
//
//  Created by swatantra on 8/31/16.
//  Copyright © 2016 swatantra. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User.h"

NS_ASSUME_NONNULL_BEGIN

@interface User (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *accountId;
@property (nullable, nonatomic, retain) NSString *bigPhotoUrl;
@property (nullable, nonatomic, retain) NSString *blockId;
@property (nullable, nonatomic, retain) NSString *commentsCount;
@property (nullable, nonatomic, retain) NSString *constituencyId;
@property (nullable, nonatomic, retain) NSString *countryId;
@property (nullable, nonatomic, retain) NSString *coverUrl;
@property (nullable, nonatomic, retain) NSString *districtId;
@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSString *fb_id;
@property (nullable, nonatomic, retain) NSString *fullname;
@property (nullable, nonatomic, retain) NSString *language;
@property (nullable, nonatomic, retain) NSString *mobile;
@property (nullable, nonatomic, retain) NSString *my_page;
@property (nullable, nonatomic, retain) NSString *normalCoverUrl;
@property (nullable, nonatomic, retain) NSString *normalPhotoUrl;
@property (nullable, nonatomic, retain) NSString *notificationsCount;
@property (nullable, nonatomic, retain) NSString *assemblyId;
@property (nullable, nonatomic, retain) NSString *itemsCount;
@property (nullable, nonatomic, retain) NSString *lat;
@property (nullable, nonatomic, retain) NSString *lng;
@property (nullable, nonatomic, retain) NSString *likesCount;
@property (nullable, nonatomic, retain) NSString *sex;
@property (nullable, nonatomic, retain) NSString *stateId;
@property (nullable, nonatomic, retain) NSString *country_name;
@property (nullable, nonatomic, retain) NSString *district_name;
@property (nullable, nonatomic, retain) NSString *state_name;
@property (nullable, nonatomic, retain) NSString *constituency_name;
@property (nullable, nonatomic, retain) NSString *block_name;
@property (nullable, nonatomic, retain) NSString *dateOfBirth;
@property (nullable, nonatomic, retain) NSString *about;

@end

NS_ASSUME_NONNULL_END
