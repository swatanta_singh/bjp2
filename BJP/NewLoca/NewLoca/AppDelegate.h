//
//  AppDelegate.h
//  NewLoca
//
//  Created by toyaj on 9/19/16.
//  Copyright © 2016 toyaj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

