//
//  ChatViewController.m
//  KINCT
//
//  Created by Toyaj Nigam on 3/10/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import "ChatViewController.h"
#import "ContainerCollectionViewCell.h"
#import "AppHelper.h"
#import "Defines.h"
#import "AFNetworking.h"
#import "Service.h"



@interface ChatViewController (){
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

@implementation ChatViewController

#pragma mark viewLife
- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}




-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
      [self.view endEditing:YES];
    __weak ChatViewController *weekSelf=self;
    [self setUpHeaderWithTitle:@"Recent Chat" withLeftbtn:@"menu" withRigthbtn:nil WithComilation:^(int navigateValue) {
        
        [[[AppHelper sharedInstance]menuViewController] setUpMove];
        [weekSelf.view endEditing:YES];
    }];
}
#pragma mark - Update Table View


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 15;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCell" forIndexPath:indexPath];
    
    // if ([AppHelper nullCheck:subServiceName].length) {
    
    // }
    // NSString *subServiceName = [[self.serviceArray objectAtIndex:indexPath.row] objectForKey:@"SubServiceName"];
    
        UILabel *lblname = (UILabel*)[cell.contentView viewWithTag:1001];
        UILabel *lblSublable = (UILabel*)[cell.contentView viewWithTag:1002];
        UIImageView *userAndGroupProfileImageV = (UIImageView*)[cell.contentView viewWithTag:1003];

    if (indexPath.row%2==0) {
        lblname.text=@"Jack";
        lblSublable.text=@"Tutor";
        [userAndGroupProfileImageV setImage:[UIImage imageNamed:@"avatar-1"]];
    }
    else
    {
        
        lblname.text=@"Kinct Group";
        lblSublable.text=@"10 member";
        [userAndGroupProfileImageV setImage:[UIImage imageNamed:@"Group-icon"]];

    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController* objVerifyView= [AppHelper intialiseViewControllerFromMainStoryboard:@"Tabbarboard" WithName:@"ChatDtlViewController"];

//    if (indexPath.row%2==0) {
//        objVerifyView.title=@"Jack";
//    }
//    else
//    {
//        objVerifyView.title=@"Kinct Group";
//    }
    [self.navigationController  pushViewController:objVerifyView animated:YES];
    
}




@end
