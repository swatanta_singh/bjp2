//
//  ChatDtlViewController.m
//  KINCT
//
//  Created by swatantra on 3/16/16.
//  Copyright © 2016 KINCT. All rights reserved.
//

#import "ChatDtlViewController.h"

#import "Defines.h"
#import "AppHelper.h"
#import "UIBubbleTableView.h"
#import "UIBubbleTableViewDataSource.h"
#import "NSBubbleData.h"
#import "DAKeyboardControl.h"
#import "HPGrowingTextView.h"
#import "MessageComposerView.h"




@interface ChatDtlViewController()<UIBubbleTableViewDataSource,HPGrowingTextViewDelegate,MessageComposerViewDelegate>
{
    BOOL isConnectionConnect;
    NSMutableArray *bubbleData;
    
    BOOL isSentMessage;
    
    NSString *selectedChannellName;
    UIRefreshControl  *refreshControl;
    NSOperationQueue *sentMessageQueue;

}

@property (nonatomic, strong) MessageComposerView *messageComposerView;
@property (weak, nonatomic) IBOutlet UIBubbleTableView *bubbleTable;
@property (weak, nonatomic) IBOutlet HPGrowingTextView *txtViewMessagebox;
@property (weak, nonatomic) IBOutlet UIView *chatParentView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end

@implementation ChatDtlViewController
@synthesize title;
- (void)viewDidLoad {
    
    [super viewDidLoad];
     bubbleData = [[NSMutableArray alloc] init];
    [self loadChatTableView];
    // Do any additional setup after loading the view.
}                                             

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setTitleAndBackAction];
    if (![bubbleData count]) {
        [self getUserChatHistory];
    }
}


#pragma mark - Title & Back Action
-(void)setTitleAndBackAction
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardDidShowNotification:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardDidHideNotification:) name:UIKeyboardWillHideNotification object:nil];
    
    __weak ChatDtlViewController *weekSelf=self;
    [self setUpHeaderWithTitle:self.title withLeftbtn:@"back" withRigthbtn:nil WithComilation:^(int navigateValue) {
       // [weekSelf.headerView.lblStatus setHidden:YES];
        [weekSelf.navigationController popViewControllerAnimated:YES];
    }];
    
   // [self.headerView.lblStatus setHidden:NO];
}
-(void)hideKeyBoardOntap
{
    [self.messageComposerView.messageTextView resignFirstResponder];
    
    
}



#pragma mark - Keyboard Notification
- (void)handleKeyboardDidShowNotification:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    _bubbleTable.contentInset = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0);
    [_bubbleTable scrollBubbleViewToBottomAnimated:NO];
}

- (void)handleKeyboardDidHideNotification:(NSNotification *)notification
{
    _bubbleTable.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [_bubbleTable scrollBubbleViewToBottomAnimated:NO];

}


-(void)loadingDefaultsSetup
{
    
    sentMessageQueue = [NSOperationQueue mainQueue];

    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyBoardOntap)];
    [self.view addGestureRecognizer:tap];
    
     refreshControl = [[UIRefreshControl alloc]init];
    [_bubbleTable addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    [self bottomChatTextBoxSetup];
   // [self configureSharedManager];
   // [self configureAndSubscribeChannel];
   
   }
#pragma mark - Send Text Messages and Delegate
-(void)bottomChatTextBoxSetup
{
    self.messageComposerView = [[MessageComposerView alloc] init];
    self.messageComposerView.delegate = self;
    self.messageComposerView.messagePlaceholder = @"Type a message";
    [self.view addSubview:self.messageComposerView];
    
}
- (void)messageComposerSendMessageClickedWithMessage:(NSString*)message{
    
    NSInvocationOperation *sentMessageOperation=[[NSInvocationOperation alloc]initWithTarget:self selector:@selector(messageSent:) object:message];
    [sentMessageQueue addOperation:sentMessageOperation];}


-(void)messageSent:(NSString*)message
{
    if (isConnectionConnect) {
        
        NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
        NSString *messsageText = [message stringByTrimmingCharactersInSet:charSet];
        
        if ([messsageText length]>0)
            {
            
            isSentMessage=YES;
            
//            NSDictionary *messageDict=[NSMutableDictionary new];
//            [messageDict setValue:messsageText forKey:@"content"];
//            [messageDict setValue:@"14605295722620557" forKey:@"sentTime"];
//            [messageDict setValue:@"0" forKey:@"type"];
//            [messageDict setValue:@"iosUser" forKey:@"from"];
            
           // if ([self.title isEqualToString:@"Jack"]) {
              
//                [[SharedManager shared] sendMessage:messageDict toUser:clientReciver channelName:senderChannel WithCompilationBlock:^(id message) {
//                    
                    _bubbleTable.typingBubble = NSBubbleTypingTypeNobody;
                    NSBubbleData *sayBubble = [NSBubbleData dataWithText:messsageText date:[NSDate date] type:BubbleTypeMine];
                    [bubbleData addObject:sayBubble];
                    [_bubbleTable reloadData];
                    [_bubbleTable scrollBubbleViewToBottomAnimated:YES];
//
//                    [self sentMessageForHistoryOnSharedChannel:sharedChannel message:messageDict senderObject:clientReciver];
//
//                }];
                
            }
            
    }

}
- (void)messageComposerFrameDidChange:(CGRect)frame withAnimationDuration:(CGFloat)duration andCurve:(NSInteger)curve
{
    //_bubbleTable.contentInset=UIEdgeInsetsMake(0, 0, frame.origin.y/2+30, 0);
}

- (void)messageComposerUserTyping
{
    
}
#pragma mark -

-(void)loadChatTableView
{
    
    _bubbleTable.bubbleDataSource = self;
    
    // The line below sets the snap interval in seconds. This defines how the bubbles will be grouped in time.
    // Interval of 120 means that if the next messages comes in 2 minutes since the last message, it will be added into the same group.
    // Groups are delimited with header which contains date and time for the first message in the group.
    
    _bubbleTable.snapInterval = 120;
    
    // The line below enables avatar support. Avatar can be specified for each bubble with .avatar property of NSBubbleData.
    // Avatars are enabled for the whole table at once. If particular NSBubbleData misses the avatar, a default placeholder will be set (missingAvatar.png)
    
    _bubbleTable.showAvatars = YES;
    
    // Uncomment the line below to add "Now typing" bubble
    // Possible values are
    //    - NSBubbleTypingTypeSomebody - shows "now typing" bubble on the left
    //    - NSBubbleTypingTypeMe - shows "now typing" bubble on the right
    //    - NSBubbleTypingTypeNone - no "now typing" bubble
    
    
    
    NSBubbleData *otherBubble = [NSBubbleData dataWithText:@"Hi" date:[NSDate dateWithTimeIntervalSinceNow:0] type:BubbleTypeSomeoneElse];
            [bubbleData addObject:otherBubble];
    
    NSBubbleData *sayBubble = [NSBubbleData dataWithText:@"Hi, how how are?" date:[NSDate dateWithTimeIntervalSinceNow:0] type:BubbleTypeMine];
    [bubbleData addObject:sayBubble];

    _bubbleTable.typingBubble = NSBubbleTypingTypeSomebody;
    
    if ([bubbleData count]>0) {
        [_bubbleTable reloadData];
    }
    
    // Keyboard events
    //
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    return [bubbleData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    return [bubbleData objectAtIndex:row];
}

#pragma mark - Keyboard events

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = _chatParentView.frame;
        frame.origin.y -= kbSize.height;
        _chatParentView.frame = frame;
        
        frame = _bottomView.frame;
        frame.size.height -= kbSize.height;
        _bottomView.frame = frame;
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = _chatParentView.frame;
        //        frame.origin.y += kbSize.height;
        //        _chatParentView.frame = frame;
        
        frame = _bottomView.frame;
        frame.size.height += kbSize.height;
        _bottomView.frame = frame;
    }];
}


#pragma mark-

//-(void)configureSharedManager
//{
//    sharedManager=[[SharedManager alloc]init];
//    sharedManager.delegate=self;
//    [sharedManager statusWithCompilationBlock:^(BOOL status) {
//        
//        if (status) {
//            self.headerView.lblStatus.text=@"Online";
//        }
//        else
//            self.headerView.lblStatus.text=@"Offline";
//            isConnectionConnect = status;
//    }];
//    
//}
//
//-(void)configureAndSubscribeChannel
//{
//  
//    [sharedManager configurePubNubPublish];
//    clientSender = [PubNub clientWithConfiguration:sharedManager.pnbConfigure];
//    [sharedManager  setPubNubClientObjectDelegate:clientSender];
//    [sharedManager setChannel:@[recieveChannel] clientObject:clientSender];
//    
//    //[sharedManager addPushNotification:clientReciver channaleName:@[recieveChannel]];
//    //[sharedManager pushNotificationEnableChannelsForDeviceTokenWithObject:clientReciver];
//    clientReciver = [PubNub clientWithConfiguration:sharedManager.pnbConfigure];
//    [sharedManager  setPubNubClientObjectDelegate:clientReciver];
//    [sharedManager setChannel:@[@"ReciveChannel"] clientObject:clientReciver];
//   // [sharedManager addPushNotification:clientReciver channaleName:@[recieveChannel]];
//   // [sharedManager pushNotificationEnableChannelsForDeviceTokenWithObject:clientReciver];
//    
//    clientReciver = [PubNub clientWithConfiguration:sharedManager.pnbConfigure];
//    [sharedManager  setPubNubClientObjectDelegate:clientReciver];
//    [sharedManager setChannel:@[groupChannel] clientObject:clientReciver];
//   // [sharedManager addPushNotification:clientReciver channaleName:@[recieveChannel]];
//   // [sharedManager pushNotificationEnableChannelsForDeviceTokenWithObject:clientReciver];
//    
//}
//
//
//
//#pragma mark - 
//-(void)didReceiveMessage:(id)message
//{
//
//        if (message) {
//            
//             if ([self.title isEqualToString:@"Jack"]) {
//                
//                 PNMessageResult *messageResult=(PNMessageResult*)message;
//                 if ([[messageResult.data.message objectForKey:@"content"] length]>0) {
//                     
//                     _bubbleTable.typingBubble = NSBubbleTypingTypeSomebody;
//                     NSBubbleData *sayBubble = [NSBubbleData dataWithText:[messageResult.data.message objectForKey:@"content"] date:[NSDate dateWithTimeIntervalSinceNow:0] type:BubbleTypeSomeoneElse];
//                     [bubbleData addObject:sayBubble];
//                     [_bubbleTable reloadData];
//                     if ([bubbleData count]>0) {
//                         [_bubbleTable scrollBubbleViewToBottomAnimated:YES];
//                     }
//                 }
//
//             }
//          
//        }
//}

-(void)getUserChatHistory
{
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [[AppHelper sharedInstance]showIndicator];
       // [self requestForHistry];
     
    }
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }    
    
}

//-(void)requestForHistry
//{
//    
//    if ([self.title isEqualToString:@"Jack"]) {
//        selectedChannellName=sharedChannel;
//    }
//    else
//    {
//        selectedChannellName=groupChannel;
//    }
//    [clientReciver historyForChannel:selectedChannellName start:nil end:nil limit:historyMessageCountInt reverse:NO withCompletion:^(PNHistoryResult * _Nullable result, PNErrorStatus * _Nullable status) {
//        
//        [[AppHelper sharedInstance]hideIndicator];
//        for (NSDictionary *messageDict in result.data.messages) {
//            
//            if ([[messageDict objectForKey:@"content"] length]>0) {
//                
//                if ([[messageDict objectForKey:@"from" ] isEqualToString:@"Om"] ) {
//                    
//                    NSBubbleData *sayBubble = [NSBubbleData dataWithText:[messageDict objectForKey:@"content"] date:[NSDate dateWithTimeIntervalSinceNow:0] type:BubbleTypeSomeoneElse];
//                    [bubbleData addObject:sayBubble];
//                }
//                else
//                {
//                    NSBubbleData *sayBubble = [NSBubbleData dataWithText:[messageDict objectForKey:@"content"] date:[NSDate dateWithTimeIntervalSinceNow:0] type:BubbleTypeMine];
//                    [bubbleData addObject:sayBubble];
//                }
//            }
//        }
//        
//        if ([bubbleData count]>0) {
//            
//            [self loadChatTableView];
//            
//            if ([refreshControl isRefreshing]) {
//                [refreshControl endRefreshing];
//                [_bubbleTable scrollBubbleViewToPositionAnimated:NO load:YES];
//            }
//            else
//            [_bubbleTable scrollBubbleViewToBottomAnimated:NO];
//            
//        }
//
//    }];
//
//}
////-(void)sentMessageForHistoryOnSharedChannel:(NSString *)channel message:(NSDictionary *)message senderObject:(PubNub *)userObject
////{
////    [[SharedManager shared] sendMessage:message toUser:userObject channelName:channel WithCompilationBlock:^(id message) {
////    }];
////
////}
//
-(void)refreshTable {
    
    if([AppHelper appDelegate].checkNetworkReachability)
    {
        [refreshControl beginRefreshing];
       // historyMessageCountInt=historyMessageCountInt+20;
       // [self requestForHistry];
        
    }
    else{
        [AppHelper showAlertViewWithTag:11 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:Alert_Ok_Button otherButtonTitles:nil];
    }

}

@end
